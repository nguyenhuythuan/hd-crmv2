<?php
namespace Services\Sms\FimSms;
// use Services\Mail\FimMailler\RequestMail;
use Services\Sms\FimSMS\RequestSms;
class SendSms{
    private $serviceUrl;
    private $servicePrivateKey;
    private $servicePublicKey;
    private $serviceUser;
    private $servicePass;

    private $email;
	private $template;
    private $event = "autoSubs";
    private $title = "";
    
    // 
    /**
     *  body example 
     * [
	 *   "email" : "ntdat.tb@gmail.com",
	 *   "template" : "afdfasdf ",
	 *   "event" : "auto Subs",
	 *   "title" : "test mail"
     * ]
     */

    public static function sendVMGSaveSMS(array $data){
        // sendVMGSaveSMS
        $req = new RequestSms();
        $resultData = $req->url('sendVMGSaveSMS')
                    ->environment('test')
                    ->phone($data['phone'])
                    ->contentType(0)
                    ->body(['content'=>$data['content']])
                    ->sendRequest();
        return $resultData;
    }


}