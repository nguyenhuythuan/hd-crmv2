<?php
namespace Services\Sms\FimSms;
use GuzzleHttp\Client;
use GuzzleHttp\Middleware;

class RequestSms{
    /* 
    info service */
    private $serviceUrl = 'http://10.10.14.147:8000/';
    const CONTENT_TYPE = [
        'CSKH','QC','OTP'
    ];

    private $headerRequest =[
        'Content-Type' => 'application/x-www-form-urlencoded'
    ];
    private $actionSource = 'Mautic';
    private $provider = 'VMG';
    private $source = 'FIM+ DIGITAL';
    private $methodRequest = 'POST';
    private $bodyRequest = [];
    private $phoneTo;
    private $contentType;
    private $environmentReq = 'test';
    

/*     actionSource	string	Optional	nguồn source gọi api (hd1-sms-gateway, hd1-notify)
    content	string		Nội dung tin nhắn
    contentType	string		Loại tin nhắn (CSKH/QC/OTP)
    createdAt	string	Optional	Ngày khởi tạo tin nhắn (mặc định là giờ hiện tại)
    environment	string		Môi trường source gọi api
    eventId	string		ID của event
    phoneNumber	string		Số điện thoại nhận tin
    requestId	string		ID của hệ thống đối tác
    sendTime	string	Optional	Thời gian đặt lịch gửi. Định dạng: dd/MM/YYYY HH:mm, mặc định là trống nếu không phải tin đặt lịch
    type	string		Loại tin nhắn (CSKH/QC/OTP) */

    /* 
        data:[{
            "phoneNumber":"0899969964",
            "contentType":"OTP",
            "source":"FIM+ DIGITAL",
            "provider":"VMG",
            "actionSource":"data-api",
            "environment":"test",
            "content":"Ma xac nhan dich vu Fim+ cua ban la: 0214", 
            "createdAt":"2019-11-04 08:32:22"
        }]
     */
    private function setContentType($value){
        $this->contentType = $value;
    }
    private function getContentType(){
        return $this->contentType;
    }
    private function getSource(){
        return $this->source;
    }
    private function getProvider(){
        return $this->provider;
    }
    private function getActionSource(){
        return $this->actionSource;
    }


    public function phone(string $value){
        $this->phoneTo = $value;
        return $this;
    }
    public function environment(string $value){
        $this->environmentReq = $value;
        return $this;
    }
    public function contentType($value){
        $this->setContentType(self::CONTENT_TYPE[$value]);
        return $this;
    }
    public function url(string $value){
        $this->serviceUrl .= $value;
        return $this;
    }
    public function method(string $value){
        $this->methodRequest = $value;
        return $this;
    }
    public function header(array $value){
        $this->headerRequest = array_merge($this->headerRequest,$value);
        return $this;
    }
    public function body(array $value){
        $data = json_encode([[
            "phoneNumber" => $this->phoneTo,
            "contentType" => $this->getContentType(),
            "source" => $this->getSource(),
            "provider" => $this->getProvider(),
            "actionSource" => $this->getActionSource(),
            "environment" => $this->environmentReq,
            "content" => $value['content'],
            "createdAt" => date("Y-d-m H:i:s")
        ]]);
        $this->bodyRequest = ["data" => $data];
        return $this;
    }
    public function query(array $value){
        $this->queryRequest = $value;// array_merge($this->headerRequest,$value);
        return $this;
    }
    public function sendRequest(){
        
        $method     = $this->methodRequest;
        $url        = $this->serviceUrl;
        $header     = $this->headerRequest;
        $body       = $this->bodyRequest;
        /* var_dump($body);
        exit; */
        try{
            $client   = new Client();
            $clientHandler = $client->getConfig('handler');
            $response = $client->request($method, $url,[
                'headers' => $header,
                'form_params' => $body,
            ]);
            $result = [
                'error' => true,
                'message' => '',
                'data' => null
            ];
            $statusCode = $response->getStatusCode();
            $content = $response->getBody()->getContents();
            
            if((int)$statusCode != 200){
                $result['data'] = ['statusCode'=>$statusCode];
                if($content != '' || $content != null){
                    $result['data'] = json_decode($content);
                }
                return $result;
            }
            $result['data'] = json_decode($content);
            $result['error'] = false;
            return $result;
        }catch(\Exception $e){
            return $result = [
                'error' => true,
                'message' => $e->getMessage(),
                'data' => ''
            ];
        }
        
    }
}