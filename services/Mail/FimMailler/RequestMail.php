<?php
namespace Services\Mail\FimMailler;
// use Services\Symfony\Component\HttpClient\HttpClient;
use GuzzleHttp\Client;
use GuzzleHttp\Middleware;

class RequestMail{
    /* 
    info service */
    private $serviceUrl = 'https://api.fimplus.io/notify/v1/mail/send';
    private $servicePrivateKey;
    private $servicePublicKey;
    private $serviceUser;
    private $servicePass;

    private $headerRequest =[
        'api-key' => 'AIzaSyABJleJJ25CDXW7IQY22MQ3ybLoj20WwCLMHCTUSJdj',
        'Content-Type' => 'application/json'
    ];
    private $methodRequest = 'POST';
    private $bodyRequest = [];
    private $paramRequest;
    private $emailTo;
    

    private $email;
    public function email(string $value){
        $this->emailTo = $value;
        return $this;
    }
    public function method(string $value){
        $this->methodRequest = $value;
        return $this;
    }
    public function header(array $value){
        $this->headerRequest = array_merge($this->headerRequest,$value);
        return $this;
    }
    public function body(array $value){
        $this->bodyRequest = $value;// array_merge($this->headerRequest,$value);
        return $this;
    }
    public function sendRequest(){

        $method     = $this->methodRequest;
        $url        = $this->serviceUrl;
        $header     = $this->headerRequest;
        $body       = $this->bodyRequest;
        try{
            $client   = new Client();
            $clientHandler = $client->getConfig('handler');
            $response = $client->request($method, $url,[
                'headers' => $header,
                'json' => $body
            ]);
            $result = [
                'error' => true,
                'message' => '',
                'data' => null
            ];
            $statusCode = $response->getStatusCode();
            if((int)$statusCode != 200){
                $result['data'] = ['statusCode'=>$statusCode];
                return $result;
            }
            $content = $response->getBody()->getContents();
            $result['data'] = json_decode($content);
            $result['error'] = false;
            return $result;
        }catch(\Exception $e){
            return $result = [
                'error' => true,
                'message' => $e->getMessage(),
                'data' => $e
            ];
        }
        
    }
    public function testRequest(){

        $method     = $this->methodRequest;
        $url        = $this->serviceUrl;
        $header     = $this->headerRequest;
        $body       = $this->bodyRequest;
        $client   = new Client();

        $clientHandler = $client->getConfig('handler');
        $response = $client->request($method, $url,[
            'headers' => $header,
            'json' => $body
        ]);
        $result = [
            'error' => true,
            'message' => '',
            'data' => null
        ];
        $statusCode = $response->getStatusCode();
        if((int)$statusCode != 200){
            $result['data'] = ['statusCode'=>$statusCode];
            return $result;
        }
        $content = $response->getBody()->getContents();
        $result['data'] = json_decode($content);
        $result['error'] = false;
        return $result;
    }

}