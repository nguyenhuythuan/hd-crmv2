<?php
namespace Services\Mail\FimMailler;
// use Services\Mail\FimMailler\RequestMail;
use Services\Mail\FimMailler\RequestMail;
class SendMail{
    private $serviceUrl;
    private $servicePrivateKey;
    private $servicePublicKey;
    private $serviceUser;
    private $servicePass;

    private $email;
	private $template;
    private $event = "autoSubs";
    private $title = "";
    
    // 
    /**
     *  body example 
     * [
	 *   "email" : "ntdat.tb@gmail.com",
	 *   "template" : "afdfasdf ",
	 *   "event" : "auto Subs",
	 *   "title" : "test mail"
     * ]
     */
    public static function sendEmail($body){
        $reqMail = new RequestMail();
        $result = $reqMail->body($body)->sendRequest();
        return $result;
    }


}