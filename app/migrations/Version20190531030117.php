<?php

namespace Mautic\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20190531030117 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP INDEX email_date_read ON email_stats');
        $this->addSql('DROP INDEX tracking_id ON lead_devices');
        $this->addSql('DROP INDEX page_hit_url ON page_hits');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE INDEX email_date_read ON email_stats (date_read)');
        $this->addSql('CREATE UNIQUE INDEX tracking_id ON lead_devices (tracking_id)');
        $this->addSql('CREATE INDEX page_hit_url ON page_hits (url)');
    }
}
