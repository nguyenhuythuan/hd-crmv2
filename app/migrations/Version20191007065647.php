<?php

namespace Mautic\Migrations;

use Doctrine\DBAL\Migrations\SkipMigrationException;
use Doctrine\DBAL\Schema\Schema;
use Mautic\CoreBundle\Doctrine\AbstractMauticMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20191007065647 extends AbstractMauticMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        $table = $schema->getTable($this->prefix.'promotion');
        $tableFCampaign = $schema->getTable($this->prefix.'fimplus_campaign');
        if ($table->hasColumn('fimplus_promotion_id')) {
            $this->addSql("ALTER TABLE {$this->prefix}promotion CHANGE COLUMN `fimplus_promotion_id` `uuid` CHAR(100)");
        }
        $tablePromotionSql = $this->createTablePromotionObject();
        $this->addSql($tablePromotionSql);
       
    }
    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        $table = $schema->getTable($this->prefix.'promotion');
        if ($table->hasColumn('uuid')) {
            $this->addSql("ALTER TABLE {$this->prefix}promotion CHANGE `uuid` `fimplus_promotion_id` CHAR(100)");
        }
        $this->addSql("DROP TABLE {$this->prefix}promotion_object");
    }
    protected function createTablePromotionObject(){
        $promotionCPFk  = $this->generatePropertyName('promotion_object', 'fk', ['campaign_id']);
        $promotionFk  = $this->generatePropertyName('promotion_object', 'fk', ['promotion_id']);
        $sql = <<<SQL
CREATE TABLE IF NOT EXISTS {$this->prefix}promotion_object(
  id INT AUTO_INCREMENT NOT NULL,  
  uuid CHAR(100) UNIQUE NOT NULL ,
  campaign_id INT NOT NULL,
  promotion_id INT NOT NULL,
  is_published TINYINT(1) DEFAULT 1,
  date_added DATETIME  COMMENT '(DC2Type:datetime)',
  date_modified DATETIME  COMMENT '(DC2Type:datetime)',
  created_by INT DEFAULT NULL,
  created_by_user VARCHAR(255) DEFAULT NULL,
  modified_by INT DEFAULT NULL,
  modified_by_user VARCHAR(255) DEFAULT NULL,
  checked_out DATETIME DEFAULT NULL,
  checked_out_by INT(11) DEFAULT NULL,
  checked_out_by_user VARCHAR(255) DEFAULT 'Admin user',
  PRIMARY KEY(id),
  CONSTRAINT $promotionCPFk FOREIGN KEY (campaign_id) REFERENCES {$this->prefix}fimplus_campaign (id) ON DELETE CASCADE,
  CONSTRAINT $promotionFk FOREIGN KEY (promotion_id) REFERENCES {$this->prefix}promotion (id) ON DELETE CASCADE
) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB
SQL;
        return $sql;
    }

}
