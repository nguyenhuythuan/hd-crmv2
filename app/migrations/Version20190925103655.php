<?php

namespace Mautic\Migrations;

use Doctrine\DBAL\Migrations\SkipMigrationException;
use Doctrine\DBAL\Schema\Schema;
use Mautic\CoreBundle\Doctrine\AbstractMauticMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20190925103655 extends AbstractMauticMigration
{
    public function preUp(Schema $schema)
    {
        
    }

    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        $table = $schema->getTable($this->prefix.'promotion');
        if (!$table->hasColumn('is_published')) {
            $this->addSql("ALTER TABLE {$this->prefix}promotion ADD is_published TINYINT(1) DEFAULT 1");
        }
        if (!$table->hasColumn('checked_out')) {
            $this->addSql("ALTER TABLE {$this->prefix}promotion ADD checked_out DATETIME DEFAULT NULL");
        }
        if (!$table->hasColumn('checked_out_by')) {
            $this->addSql("ALTER TABLE {$this->prefix}promotion ADD checked_out_by INT(11) DEFAULT NULL");
        }
        if (!$table->hasColumn('checked_out_by_user')) {
            $this->addSql("ALTER TABLE {$this->prefix}promotion ADD checked_out_by_user VARCHAR(255) DEFAULT 'Admin user'");
        }
       
    }
    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs

        $promotion = $this->prefix . 'promotion';
        $tablePromotion = $schema->getTable($promotion);

        if ($tablePromotion->hasColumn('is_published')) {
            $this->addSql("ALTER TABLE {$promotion} DROP COLUMN is_published");
        }
        if ($tablePromotion->hasColumn('checked_out')) {
            $this->addSql("ALTER TABLE {$promotion} DROP COLUMN checked_out");
        }
        if ($tablePromotion->hasColumn('checked_out_by')) {
            $this->addSql("ALTER TABLE {$promotion} DROP COLUMN checked_out_by");
        }
        if ($tablePromotion->hasColumn('checked_out_by_user')) {
            $this->addSql("ALTER TABLE {$promotion} DROP COLUMN checked_out_by_user");
        }
    }
}
