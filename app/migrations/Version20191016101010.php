<?php

namespace Mautic\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;
use Doctrine\DBAL\Migrations\SkipMigrationException;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20191016101010 extends AbstractMigration
{
    public function preUp(Schema $schema)
    {
        // if ($schema->getTable($this->prefix.'subscription')) {
        //     throw new SkipMigrationException('Schema includes this migration');
        // }
    }

    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $tableSql = $this->createTablePromotionObject();
        $this->addSql($tableSql);
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql("DROP TABLE {$this->prefix}subscription");

    }
    protected function createTablePromotionObject(){

        $sql = <<<SQL
CREATE TABLE $this->prefix.subscription (
    `id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
    `subsId` CHAR(100) NOT NULL,
    `userId` CHAR(100) NOT NULL,
    `planId` CHAR(100) NULL,
    `titleId` CHAR(100) NULL,
    `startDate` DATETIME NULL,
    `expireAt` DATETIME NULL,
    `nextRenewDate` DATETIME NULL,
    `methodId` CHAR(100) NULL,

    `profileCode` CHAR(100) NULL,
    `lastTransactionId` CHAR(100) NULL,
    `distributor` INT(2) NULL,
    `renewTimesRemain` INT(5) NULL DEFAULT 0,
    `retryTimesRemain` INT(5) NULL DEFAULT 0,

    `channelPayment` CHAR(50) NULL,
    `action` CHAR(50) NULL,
    `createdAt` DATETIME NULL,
    `updatedAt` DATETIME NULL,
    `statusAutoRenew` TINYINT(2) NULL ,
    `status` TINYINT(2) NULL DEFAULT 0,

    `is_published` TINYINT(1) NULL DEFAULT 0,
    `date_added` DATETIME NULL DEFAULT NULL,
    `created_by` INT(13) NULL DEFAULT 0,
    `created_by_user` VARCHAR(255) NULL DEFAULT 0,
    `date_modified` DATETIME NULL DEFAULT NULL,
    `modified_by` TINYINT(1) NULL DEFAULT 0,
    `modified_by_user` VARCHAR(255) NULL DEFAULT 0,
    `checked_out` DATETIME NULL DEFAULT NULL,
    `checked_out_by` INT(13) NULL DEFAULT NULL,
    `checked_out_by_user` VARCHAR(255) NULL DEFAULT NULL,
    
  PRIMARY KEY (`id`),
  UNIQUE INDEX `id_UNIQUE` (`id` ASC),
  UNIQUE INDEX `subsId_UNIQUE` (`subsId` ASC));
SQL;
        return $sql;
    }
}
