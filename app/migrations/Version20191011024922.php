<?php

/*
 * @package     Mautic
 * @copyright   2019 Mautic Contributors. All rights reserved.
 * @author      Mautic
 * @link        http://mautic.org
 * @license     GNU/GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
 */

namespace Mautic\Migrations;

use Mautic\CoreBundle\Doctrine\AbstractMauticMigration;
use Doctrine\DBAL\Schema\Schema;
use Doctrine\DBAL\Migrations\SkipMigrationException;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20191011024922 extends AbstractMauticMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        if ($schema->getTable($this->prefix.'subscription')) {
            $this->addSql("DROP TABLE {$this->prefix}user_context");
        }else{
            throw new SkipMigrationException('Schema includes this migration');
        }
        // $tablePromotionSql = $this->createTablePromotionObject();
        // $this->addSql("DROP TABLE {$this->prefix}user_context");
        // $this->addSql($tablePromotionSql);
       
    }
    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        $this->addSql("DROP TABLE {$this->prefix}user_context");
    }
    protected function createTablePromotionObject(){
        $sql = <<<SQL
CREATE TABLE IF NOT EXISTS {$this->prefix}user_context(
    `ida` INT NOT NULL AUTO_INCREMENT,
    `svodPaid` TINYINT(1) NULL DEFAULT 0,
    `tvodPaid` TINYINT(1) NULL DEFAULT 0,
    `mobifonePaid` TINYINT(1) NULL DEFAULT 0,
    `vnptPaid` TINYINT(1) NULL DEFAULT 0,
    `svodWholesale` TINYINT(1) NULL DEFAULT 0,
    `tvodWholesale` TINYINT(1) NULL DEFAULT 0,
    `mobifoneWholesale` TINYINT(1) NULL DEFAULT 0,
    `vnptWholesale` TINYINT(1) NULL DEFAULT 0,
    `monthlyActive` TINYINT(1) NULL DEFAULT 0,
    `monthlySubscribers` TINYINT(1) NULL DEFAULT 0,
    `userReturn` TINYINT(1) NULL DEFAULT 0,
    `conversionFromBundling` TINYINT(1) NULL DEFAULT 0,
    `conversionFromRegistration` TINYINT(1) NULL DEFAULT 0,
    `manualRenewUser` TINYINT(1) NULL DEFAULT 0,
    `autoRenewUser` TINYINT(1) NULL DEFAULT 0,
    `userExpired` TINYINT(1) NULL DEFAULT 0,
    `userLeave` TINYINT(1) NULL DEFAULT 0,
    `currentUser` TINYINT(1) NULL DEFAULT 0,

    `is_published` TINYINT(1) NULL DEFAULT 0,
    `date_added` DATETIME NULL DEFAULT 0,
    `created_by` INT(13) NULL DEFAULT 0,
    `created_by_user` VARCHAR(255) NULL DEFAULT 0,
    `date_modified` DATETIME NULL DEFAULT 0,
    `modified_by` TINYINT(1) NULL DEFAULT 0,
    `modified_by_user` VARCHAR(255) NULL DEFAULT 0,
    `checked_out` DATETIME NULL DEFAULT 0,
    `checked_out_by` INT(13) NULL DEFAULT 0,
    `checked_out_by_user` VARCHAR(255) NULL DEFAULT 0,

    `userId` VARCHAR(100) NOT NULL,
    `createdAt` DATETIME NULL,
    `updatedAt` DATETIME NULL,
    PRIMARY KEY (`id`, `userId`),
    UNIQUE INDEX `id_UNIQUE` (`id` ASC),
    UNIQUE INDEX `userId_UNIQUE` (`userId` ASC)
)
SQL;
        return $sql;
    }

}
