<?php

namespace Mautic\Migrations;

use Doctrine\DBAL\Migrations\SkipMigrationException;
use Doctrine\DBAL\Schema\Schema;
use Mautic\CoreBundle\Doctrine\AbstractMauticMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20191021065447 extends AbstractMauticMigration
{
    protected $lead = 'leads';
    protected $leadField = 'lead_fields';
    protected $fieldGroup = 'core';
    protected $object = 'lead';
    protected $isVisible = 1;
    protected $isShortVisible = 1;
    protected $isListable = 1;
    protected $isRequired = 0;
    protected $isPublished = 1;
    protected $isFixed = 0;
    protected $isPubliclyUpdatable = 0;
    protected $properties = 'a:0:{}'; //DC2Type
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        $leadName = $this->prefix . $this->lead;
        $leadFieldName = $this->prefix . $this->leadField;

        $tableLead = $schema->getTable($leadName);

        //birthday
        if (!$tableLead->hasColumn('created_at')) {
            $this->addSql("ALTER TABLE {$leadName} ADD created_at DATETIME");
        }
        $this->addSql("DELETE FROM {$leadFieldName} WHERE alias = 'created_at'");
        $this->addSql("INSERT INTO {$leadFieldName}  
                            (label, alias, type, field_group, is_visible, is_short_visible, is_listable, object,properties, is_published, is_required, is_fixed, is_publicly_updatable) 
                            VALUES ('Created at', 'created_at', 'datetime','{$this->fieldGroup}', {$this->isVisible}, {$this->isShortVisible}, {$this->isListable}, '{$this->object}', '{$this->properties}', {$this->isPublished}, {$this->isRequired}, {$this->isFixed}, {$this->isPubliclyUpdatable})");

        //gender
        if (!$tableLead->hasColumn('updated_at')) {
            $this->addSql("ALTER TABLE {$leadName} ADD updated_at DATETIME");
        }
        $this->addSql("DELETE FROM {$leadFieldName} WHERE alias = 'updated_at'");
        $this->addSql("INSERT INTO {$leadFieldName}  
                            (label, alias, type, field_group, is_visible, is_short_visible, is_listable, object,properties, is_published, is_required, is_fixed, is_publicly_updatable) 
                            VALUES ('Updated at', 'updated_at', 'datetime','{$this->fieldGroup}', {$this->isVisible}, {$this->isShortVisible}, {$this->isListable}, '{$this->object}', '{$this->properties}', {$this->isPublished}, {$this->isRequired}, {$this->isFixed}, {$this->isPubliclyUpdatable})");

        // platform
        if (!$tableLead->hasColumn('platform')) {
            $this->addSql("ALTER TABLE {$leadName} ADD platform VARCHAR(200)");
            //demo "platform" => "tv|unknown|androidtv|2.4.4|sdk_google_atv_x86",
        }
        $this->addSql("DELETE FROM {$leadFieldName} WHERE alias = 'platform'");
        $this->addSql("INSERT INTO {$leadFieldName}  
                            (label, alias, type, field_group, is_visible, is_short_visible, is_listable, object,properties, is_published, is_required, is_fixed, is_publicly_updatable) 
                            VALUES ('platform', 'platform', 'text','{$this->fieldGroup}', {$this->isVisible}, {$this->isShortVisible}, {$this->isListable}, '{$this->object}', '{$this->properties}', {$this->isPublished}, {$this->isRequired}, {$this->isFixed}, {$this->isPubliclyUpdatable})");
        // action
        if (!$tableLead->hasColumn('action')) {
            $this->addSql("ALTER TABLE {$leadName} ADD action CHAR(36)");
        }
        $this->addSql("DELETE FROM {$leadFieldName} WHERE alias = 'action'");
        $this->addSql("INSERT INTO {$leadFieldName}  
                            (label, alias, type, field_group, is_visible, is_short_visible, is_listable, object,properties, is_published, is_required, is_fixed, is_publicly_updatable) 
                            VALUES ('action', 'action', 'text','{$this->fieldGroup}', {$this->isVisible}, {$this->isShortVisible}, {$this->isListable}, '{$this->object}', '{$this->properties}', {$this->isPublished}, {$this->isRequired}, {$this->isFixed}, {$this->isPubliclyUpdatable})");
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs

        $leadName = $this->prefix . $this->lead;
        $leadFieldName = $this->prefix . $this->leadField;

        $tableLead = $schema->getTable($leadName);
       // $tableLeadField = $schema->getTable($leadFieldName);

        if ($tableLead->hasColumn('created_at')) {
            $this->addSql("ALTER TABLE {$leadName} DROP COLUMN created_at");
        }
        $this->addSql("DELETE FROM {$leadFieldName} WHERE alias = 'created_at'");

        if ($tableLead->hasColumn('updated_at')) {
            $this->addSql("ALTER TABLE {$leadName} DROP COLUMN updated_at");
        }
        $this->addSql("DELETE FROM {$leadFieldName} WHERE alias = 'updated_at'");

        if ($tableLead->hasColumn('platform')) {
            $this->addSql("ALTER TABLE {$leadName} DROP COLUMN platform");
        }
        $this->addSql("DELETE FROM {$leadFieldName} WHERE alias = 'platform'");
        
        if ($tableLead->hasColumn('action')) {
            $this->addSql("ALTER TABLE {$leadName} DROP COLUMN action");
        }
        $this->addSql("DELETE FROM {$leadFieldName} WHERE alias = 'action'");
    }
}
