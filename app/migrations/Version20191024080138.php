<?php

namespace Mautic\Migrations;

use Doctrine\DBAL\Migrations\SkipMigrationException;
use Doctrine\DBAL\Schema\Schema;
use Mautic\CoreBundle\Doctrine\AbstractMauticMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20191024080138 extends AbstractMauticMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        $tableInvoice = $this->createTableActiveCodeBussinessInvoice();
        $this->addSql($tableInvoice);
        $tableCode = $this->createTableactiveCodeBusinessCode();
        $this->addSql($tableCode);
       
    }
    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        $this->addSql("DROP TABLE IF EXISTS {$this->prefix}active_code_business_code");
        $this->addSql("DROP TABLE IF EXISTS {$this->prefix}active_code_bussiness_invoice");
    }
    protected function createTableActiveCodeBussinessInvoice(){
        $Fk  = $this->generatePropertyName('active_code_bussiness_invoice', 'fk', ['campaign_id']);
        $sql = <<<SQL
CREATE TABLE IF NOT EXISTS {$this->prefix}active_code_bussiness_invoice(
  id CHAR(100) NOT NULL,  
  campaign_id INT NOT NULL,
  orders CHAR(1) NOT NULL,  
  quantity INT NOT NULL,  
  total_price DOUBLE(15,2) NOT NULL,
  is_published TINYINT(1) DEFAULT 1,
  date_added DATETIME  COMMENT '(DC2Type:datetime)',
  date_modified DATETIME  COMMENT '(DC2Type:datetime)',
  start_date DATETIME  COMMENT '(DC2Type:datetime)',
  end_date DATETIME  COMMENT '(DC2Type:datetime)',
  created_by INT DEFAULT NULL,
  created_by_user VARCHAR(255) DEFAULT NULL,
  modified_by INT DEFAULT NULL,
  modified_by_user VARCHAR(255) DEFAULT NULL,
  checked_out DATETIME DEFAULT NULL,
  checked_out_by INT(11) DEFAULT NULL,
  checked_out_by_user VARCHAR(255) DEFAULT 'Admin user',
  PRIMARY KEY(id),
  CONSTRAINT $Fk FOREIGN KEY (campaign_id) REFERENCES {$this->prefix}fimplus_campaign (id) ON DELETE CASCADE
) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB
SQL;
        return $sql;
    }
    protected function createTableactiveCodeBusinessCode(){
        $Fk  = $this->generatePropertyName('active_code_business_code', 'fk', ['campaign_id']);
        $FkIN  = $this->generatePropertyName('active_code_business_code', 'fk', ['invoice_id']);
        $sql = <<<SQL
CREATE TABLE IF NOT EXISTS {$this->prefix}active_code_business_code(
  id CHAR(100) NOT NULL,  
  campaign_id INT NOT NULL,
  invoice_id CHAR(100) NOT NULL,
  code CHAR(8) NOT NULL UNIQUE,  
  serial CHAR(20) UNIQUE,  
  status TINYINT(1) NOT NULL DEFAULT 0,  
  is_published TINYINT(1) DEFAULT 1,
  date_added DATETIME  COMMENT '(DC2Type:datetime)',
  date_modified DATETIME  COMMENT '(DC2Type:datetime)',
  created_by INT DEFAULT NULL,
  created_by_user VARCHAR(255) DEFAULT NULL,
  modified_by INT DEFAULT NULL,
  modified_by_user VARCHAR(255) DEFAULT NULL,
  checked_out DATETIME DEFAULT NULL,
  checked_out_by INT(11) DEFAULT NULL,
  checked_out_by_user VARCHAR(255) DEFAULT 'Admin user',
  PRIMARY KEY(id),
  CONSTRAINT $Fk FOREIGN KEY (campaign_id) REFERENCES {$this->prefix}fimplus_campaign (id) ON DELETE CASCADE,
  CONSTRAINT $FkIN FOREIGN KEY (invoice_id) REFERENCES {$this->prefix}active_code_bussiness_invoice (id) ON DELETE CASCADE
) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB
SQL;
        return $sql;
    }

}

