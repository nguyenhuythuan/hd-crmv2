<?php

namespace Mautic\Migrations;

use Doctrine\DBAL\Migrations\SkipMigrationException;
use Doctrine\DBAL\Schema\Schema;
use Mautic\CoreBundle\Doctrine\AbstractMauticMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20190916034552 extends AbstractMauticMigration
{
    protected $lead = 'leads';
    protected $leadField = 'lead_fields';
    protected $fieldGroup = 'core';
    protected $object = 'lead';
    protected $isVisible = 1;
    protected $isShortVisible = 1;
    protected $isListable = 1;
    protected $isRequired = 0;
    protected $isPublished = 1;
    protected $isFixed = 0;
    protected $isPubliclyUpdatable = 0;
    protected $properties = 'a:0:{}'; //DC2Type
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        $leadName = $this->prefix . $this->lead;
        $leadFieldName = $this->prefix . $this->leadField;

        $tableLead = $schema->getTable($leadName);
        // $tableLeadField = $schema->getTable($leadFieldName);


        //birthday
        if (!$tableLead->hasColumn('birthday')) {
            $this->addSql("ALTER TABLE {$leadName} ADD birthday DATETIME");
        }
        $this->addSql("DELETE FROM {$leadFieldName} WHERE alias = 'birthday'");
        $this->addSql("INSERT INTO {$leadFieldName}  
                            (label, alias, type, field_group, is_visible, is_short_visible, is_listable, object,properties, is_published, is_required, is_fixed, is_publicly_updatable) 
                            VALUES ('Birthday', 'birthday', 'datetime','{$this->fieldGroup}', {$this->isVisible}, {$this->isShortVisible}, {$this->isListable}, '{$this->object}', '{$this->properties}', {$this->isPublished}, {$this->isRequired}, {$this->isFixed}, {$this->isPubliclyUpdatable})");

        //gender
        if (!$tableLead->hasColumn('gender')) {
            $this->addSql("ALTER TABLE {$leadName} ADD gender ENUM('male', 'female', 'other')");
        }
        $this->properties = serialize(array("male", "female", "other"));
        $this->addSql("DELETE FROM {$leadFieldName} WHERE alias = 'gender'");
        $this->addSql("INSERT INTO {$leadFieldName}  
                            (label, alias, type, field_group, is_visible, is_short_visible, is_listable, object,properties, is_published, is_required, is_fixed, is_publicly_updatable) 
                            VALUES ('Gender', 'gender', 'select','{$this->fieldGroup}', {$this->isVisible}, {$this->isShortVisible}, {$this->isListable}, '{$this->object}', '{$this->properties}', {$this->isPublished}, {$this->isRequired}, {$this->isFixed}, {$this->isPubliclyUpdatable})");

        //id fimplus
        if (!$tableLead->hasColumn('id_fimplus')) {
            $this->addSql("ALTER TABLE {$leadName} ADD id_fimplus CHAR(36)");
        }
        $this->addSql("DELETE FROM {$leadFieldName} WHERE alias = 'id_fimplus'");
        $this->addSql("INSERT INTO {$leadFieldName}  
                            (label, alias, type, field_group, is_visible, is_short_visible, is_listable, object,properties, is_published, is_required, is_fixed, is_publicly_updatable) 
                            VALUES ('Fimplus ID', 'id_fimplus', 'text','{$this->fieldGroup}', {$this->isVisible}, {$this->isShortVisible}, {$this->isListable}, '{$this->object}', '{$this->properties}', {$this->isPublished}, {$this->isRequired}, {$this->isFixed}, {$this->isPubliclyUpdatable})");

        //is banned
        if (!$tableLead->hasColumn('is_banned')) {
            $this->addSql("ALTER TABLE {$leadName} ADD is_banned TINYINT(1)");
        }
        $this->addSql("DELETE FROM {$leadFieldName} WHERE alias = 'is_banned'");
        $this->addSql("INSERT INTO {$leadFieldName}  
                            (label, alias, type, field_group, is_visible, is_short_visible, is_listable, object,properties, is_published, is_required, is_fixed, is_publicly_updatable) 
                            VALUES ('Is Banned', 'is_banned', 'select','{$this->fieldGroup}', {$this->isVisible}, {$this->isShortVisible}, {$this->isListable}, '{$this->object}', '{$this->properties}', {$this->isPublished}, {$this->isRequired}, {$this->isFixed}, {$this->isPubliclyUpdatable})");
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs

        $leadName = $this->prefix . $this->lead;
        $leadFieldName = $this->prefix . $this->leadField;

        $tableLead = $schema->getTable($leadName);
       // $tableLeadField = $schema->getTable($leadFieldName);

        if ($tableLead->hasColumn('birthday')) {
            $this->addSql("ALTER TABLE {$leadName} DROP COLUMN birthday");
        }
        $this->addSql("DELETE FROM {$leadFieldName} WHERE alias = 'birthday'");

        if ($tableLead->hasColumn('gender')) {
            $this->addSql("ALTER TABLE {$leadName} DROP COLUMN gender");
        }
        $this->addSql("DELETE FROM {$leadFieldName} WHERE alias = 'gender'");

        if ($tableLead->hasColumn('id_fimplus')) {
            $this->addSql("ALTER TABLE {$leadName} DROP COLUMN id_fimplus");
        }
        $this->addSql("DELETE FROM {$leadFieldName} WHERE alias = 'idfimplus'");

        if ($tableLead->hasColumn('is_banned')) {
            $this->addSql("ALTER TABLE {$leadName} DROP COLUMN is_banned");
        }
        $this->addSql("DELETE FROM {$leadFieldName} WHERE alias = 'isbanned'");
    }
}
