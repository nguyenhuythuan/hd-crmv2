<?php

namespace Mautic\Migrations;

use Doctrine\DBAL\Migrations\SkipMigrationException;
use Doctrine\DBAL\Schema\Schema;
use Mautic\CoreBundle\Doctrine\AbstractMauticMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20190926081347 extends AbstractMauticMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
       $this->addSql($this->createTableFimplusCampaign());
       $this->addSql($this->createTablePromotionCondition());
       $this->addSql($this->createTablePromotionConditionDetail());

       $this->addSql("INSERT INTO {$this->prefix}promotion_condition  
                            (uuid, fimplus_campaign_type, type, orders, level, parentId, date_added, date_modified, created_by_user) 
            VALUES ('13852949-3329-489b-878a-93b3c238b7ea', 1, 'all', 2, 0, NULL, '2019-08-12 16:46:08', '2019-08-12 16:46:08', 'thietlv@fimplus.vn'),
                   ('13852949-3329-489b-878a-93b3c238b7ea', 1, 'all', 1, 0, NULL, '2019-08-12 16:46:08', '2019-08-12 16:46:08', 'thietlv@fimplus.vn')");
      
    }
    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        $this->addSql("DROP TABLE {$this->prefix}promotion_condition_detail");
        $this->addSql("DROP TABLE {$this->prefix}promotion_condition");
        $this->addSql("DROP TABLE {$this->prefix}fimplus_campaign");
    }

    /**
*
* table fimplus campaign  
*
**/
protected function createTableFimplusCampaign(){
    $sql = <<<SQL
CREATE TABLE IF NOT EXISTS {$this->prefix}fimplus_campaign (
id INT AUTO_INCREMENT NOT NULL,
uuid CHAR(100),
name VARCHAR(255)  NOT NULL,
description VARCHAR(255)  NOT NULL,
priority TINYINT(3)  NOT NULL,
type TINYINT(1)  NOT NULL,
apply_times INT(1)  NOT NULL DEFAULT 1,
prevent_apply_same_type INT(1)  NOT NULL DEFAULT 1,
event_page TEXT,
start_date DATETIME NOT NULL,
end_date DATETIME NOT NULL,
distributor INT NOT NULL,
is_published TINYINT(1) NOT NULL DEFAULT 1,
date_added DATETIME  COMMENT '(DC2Type:datetime)',
date_modified DATETIME  COMMENT '(DC2Type:datetime)',
created_by INT DEFAULT NULL,
created_by_user VARCHAR(255) DEFAULT NULL,
modified_by INT DEFAULT NULL,
modified_by_user VARCHAR(255) DEFAULT NULL,
checked_out DATETIME DEFAULT NULL,
checked_out_by INT(11) DEFAULT NULL,
checked_out_by_user VARCHAR(255) DEFAULT 'Admin user',
PRIMARY KEY(id)
) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB
SQL;
    return $sql;
}

/**
*
* table promotion condition 
*
**/
protected function createTablePromotionCondition(){
    $sql = <<<SQL
CREATE TABLE IF NOT EXISTS {$this->prefix}promotion_condition (
id INT AUTO_INCREMENT NOT NULL,
uuid CHAR(100),
fimplus_campaign_type TINYINT(1) NOT NULL,
type VARCHAR(255) NOT NULL,
orders TINYINT(1) NOT NULL COMMENT '(1 - get promotion, 2 - validate offer)',
level TINYINT(1) NOT NULL DEFAULT 0,
parentId CHAR(100),
is_published TINYINT(1) NOT NULL DEFAULT 1,
date_added DATETIME  COMMENT '(DC2Type:datetime)',
date_modified DATETIME  COMMENT '(DC2Type:datetime)',
created_by INT DEFAULT NULL,
created_by_user VARCHAR(255) DEFAULT NULL,
modified_by INT DEFAULT NULL,
modified_by_user VARCHAR(255) DEFAULT NULL,
checked_out DATETIME DEFAULT NULL,
checked_out_by INT(11) DEFAULT NULL,
checked_out_by_user VARCHAR(255) DEFAULT 'Admin user',
PRIMARY KEY(id)
) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB
SQL;
    return $sql;
}

/**
*
* table promotion condition detail 
*
**/
protected function createTablePromotionConditionDetail(){
    $promotionFk  = $this->generatePropertyName('promotion_condition_detail', 'fk', ['campaign_id']);
    $sql = <<<SQL
CREATE TABLE IF NOT EXISTS {$this->prefix}promotion_condition_detail (
id INT AUTO_INCREMENT NOT NULL,
campaign_id INT  NOT NULL,
fact VARCHAR(255) NOT NULL,
operator VARCHAR(255) NOT NULL,
value TEXT NOT NULL,
type_value VARCHAR(255) NOT NULL,
is_published TINYINT(1) NOT NULL DEFAULT 1,
date_added DATETIME  COMMENT '(DC2Type:datetime)',
date_modified DATETIME  COMMENT '(DC2Type:datetime)',
created_by INT DEFAULT NULL,
created_by_user VARCHAR(255) DEFAULT NULL,
modified_by INT DEFAULT NULL,
modified_by_user VARCHAR(255) DEFAULT NULL,
checked_out DATETIME DEFAULT NULL,
checked_out_by INT(11) DEFAULT NULL,
checked_out_by_user VARCHAR(255) DEFAULT 'Admin user',
PRIMARY KEY(id),
CONSTRAINT $promotionFk FOREIGN KEY (campaign_id) REFERENCES {$this->prefix}fimplus_campaign (id) ON DELETE CASCADE
) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB
SQL;
    return $sql;
}

}

