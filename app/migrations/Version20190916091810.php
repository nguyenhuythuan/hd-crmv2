<?php

namespace Mautic\Migrations;

use Doctrine\DBAL\Migrations\SkipMigrationException;
use Doctrine\DBAL\Schema\Schema;
use Mautic\CoreBundle\Doctrine\AbstractMauticMigration;
use Symfony\Component\Finder\Finder;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;


/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20190916091810 extends AbstractMauticMigration
{
    
    public function up(Schema $schema)
    {
        $tablePromotionSql = $this->createTablePromotion();
        $tablePromotionItemSql = $this->createTablePromotionItem();
        $tableTitleSql = $this->createTableTitle();
        $tableVideoContentSql = $this->createTableVideoContent();
        $tablePromotionLeadSql = $this->createTablePromotionLead();
        $this->addSql($tableTitleSql);
        $this->addSql($tableVideoContentSql);
        $this->addSql($tablePromotionSql);
        $this->addSql($tablePromotionItemSql);
        $this->addSql($tablePromotionLeadSql);
    }

    public function down(Schema $schema)
    {
        $this->addSql("DROP TABLE {$this->prefix}promotion_lead");
        $this->addSql("DROP TABLE {$this->prefix}promotion_item");
        $this->addSql("DROP TABLE {$this->prefix}promotion");
        $this->addSql("DROP TABLE {$this->prefix}video_content");
        $this->addSql("DROP TABLE {$this->prefix}title");
    }

/**
*
* table promotion 
*
**/
    protected function createTablePromotion(){
        $sql = <<<SQL
CREATE TABLE IF NOT EXISTS {$this->prefix}promotion (
  id INT AUTO_INCREMENT NOT NULL,
  fimplus_promotion_id CHAR(32),
  promotion_name CHAR(100) NOT NULL,
  promotion_type TINYINT(1) NOT NULL COMMENT "( 1 - TVOD Discount - Flat
                                                2 - TVOD Discount - Percent
                                                3 - TVOD gift
                                                4 - SVOD Discount - Flat
                                                5 - SVOD Discount - Percent
                                                6 - SVOD - gift )",
  promotion_value VARCHAR(255) NOT NULL,
  promotion_item_list TEXT DEFAULT NULL,
  apply_timing TINYINT(1) NOT NULL,
  waiting_apply_duration INT,
  force_expire_date DATETIME,
  tvod_apply_condition TINYINT(1) COMMENT '(1 - all profile
                                            2 - HD profile only
                                            3 - 4K profile only 
                                            Field này chỉ sử dụng cho item TVOD.)',
  date_added DATETIME  COMMENT '(DC2Type:datetime)',
  date_modified DATETIME  COMMENT '(DC2Type:datetime)',
  created_by INT DEFAULT NULL,
  created_by_user VARCHAR(255) DEFAULT NULL,
  modified_by INT DEFAULT NULL,
  modified_by_user VARCHAR(255) DEFAULT NULL,
  PRIMARY KEY(id)
) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB
SQL;
        return $sql;
    }

/**
*
* table title 
*
**/
    protected function createTableTitle(){
        $sql = <<<SQL
CREATE TABLE IF NOT EXISTS {$this->prefix}title (
  id INT AUTO_INCREMENT NOT NULL,
  promotion_id INT  NOT NULL,
  title_name VARCHAR(255) NOT NULL COMMENT '(tên phim)',
  status TINYINT(1) NOT NULL DEFAULT 0 COMMENT '( Trạng thái phim: 0 - unActive; 1 - active ; 2 - inReview)',
  legacy_id VARCHAR(50),
  PRIMARY KEY(id)
  ) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB
SQL;
        return $sql;
    }

/**
*
* table title 
*
**/
    protected function createTableVideoContent(){
    $titleFk  = $this->generatePropertyName('video_content', 'fk', ['title_id']);
        $sql = <<<SQL
CREATE TABLE IF NOT EXISTS {$this->prefix}video_content (
  id INT AUTO_INCREMENT NOT NULL,
  title_id INT  NOT NULL,
  fimplus_video_content_id VARCHAR(50),
  type TINYINT(1) NOT NULL COMMENT '(1 - HD ; 2 - 4K)',
  PRIMARY KEY(id),
  CONSTRAINT $titleFk FOREIGN KEY (title_id) REFERENCES {$this->prefix}title (id) ON DELETE CASCADE
  ) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB
SQL;
        return $sql;
    }


/**
*
* table promotion item 
*
**/
    protected function createTablePromotionItem(){
        $promotionFk  = $this->generatePropertyName('promotion_item', 'fk', ['promotion_id']);
        $videoContentFk  = $this->generatePropertyName('promotion_item', 'fk', ['asset_logical_id']);
        $sql = <<<SQL
CREATE TABLE IF NOT EXISTS {$this->prefix}promotion_item (
  id INT AUTO_INCREMENT NOT NULL,
  promotion_id INT  NOT NULL,
  asset_logical_id INT NOT NULL COMMENT '(video content id)',
  status TINYINT(1) NOT NULL DEFAULT 0 COMMENT '( Trạng thái phim: 0 - unActive; 1 - active ; 2 - inReview)',
  date_added DATETIME  COMMENT '(DC2Type:datetime)',
  date_modified DATETIME  COMMENT '(DC2Type:datetime)',
  created_by INT DEFAULT NULL,
  created_by_user VARCHAR(255) DEFAULT NULL,
  modified_by INT DEFAULT NULL,
  modified_by_user VARCHAR(255) DEFAULT NULL,
  PRIMARY KEY(id),
  CONSTRAINT $promotionFk FOREIGN KEY (promotion_id) REFERENCES {$this->prefix}promotion (id) ON DELETE CASCADE,
  CONSTRAINT $videoContentFk FOREIGN KEY (asset_logical_id) REFERENCES {$this->prefix}video_content (id) ON DELETE CASCADE
) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB
SQL;
        return $sql;
    }


/**
*
* table promotion item 
*
**/
    protected function createTablePromotionLead(){
        $leadFk  = $this->generatePropertyName('promotion_lead', 'fk', ['lead_id']);
        $promotionFk  = $this->generatePropertyName('promotion_lead', 'fk', ['promotion_id']);
        $transactionFk  = $this->generatePropertyName('promotion_lead', 'fk', ['transaction_id']);
        $sql = <<<SQL
CREATE TABLE IF NOT EXISTS {$this->prefix}promotion_lead (
  id INT AUTO_INCREMENT NOT NULL,
  lead_id INT NOT NULL,
  promotion_id INT NOT NULL,
  expires_date DATETIME,
  transaction_id INT NOT NULL COMMENT '(Khoá ngoại đến table transaction của billing service.
                                        Để xác định promotion nhận được dựa trên giao dịch nào.)',
  status TINYINT(1) NOT NULL DEFAULT 0 COMMENT '( 0 - ACTIVE : có thể sử dụng
                                                    1 - USED : đã được sử dụng
                                                    2 - SUSPENDED : khi phát hiện hack/cheat hoặc item không hợp lệ)',
  used_date DATETIME  COMMENT '(Thời điểm promotion được sử dụng)',
  date_added DATETIME  COMMENT '(DC2Type:datetime)',
  date_modified DATETIME  COMMENT '(DC2Type:datetime)',
  created_by INT DEFAULT NULL,
  created_by_user VARCHAR(255) DEFAULT NULL,
  modified_by INT DEFAULT NULL,
  modified_by_user VARCHAR(255) DEFAULT NULL,
  PRIMARY KEY(id),
  CONSTRAINT $leadFk FOREIGN KEY (lead_id) REFERENCES {$this->prefix}leads (id) ON DELETE CASCADE,
  CONSTRAINT $promotionFk FOREIGN KEY (promotion_id) REFERENCES {$this->prefix}promotion (id) ON DELETE NO ACTION,
  CONSTRAINT $transactionFk FOREIGN KEY (transaction_id) REFERENCES {$this->prefix}transaction (id) ON DELETE NO ACTION
) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB
SQL;
        return $sql;
    }
}
