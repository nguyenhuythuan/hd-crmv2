<?php

namespace Mautic\Migrations;

use Doctrine\DBAL\Migrations\SkipMigrationException;
use Doctrine\DBAL\Schema\Schema;
use Mautic\CoreBundle\Doctrine\AbstractMauticMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20190904075525 extends AbstractMauticMigration
{
    public function preUp(Schema $schema)
    {
        $table = $schema->getTable($this->prefix.'transaction');
        if ($table->hasColumn('lead_id')) {
            throw new SkipMigrationException('Schema includes this migration');
        }
    }

    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        $this->addSql("ALTER TABLE {$this->prefix}transaction ADD lead_id INT(11) NOT NULL");
    }
}
