<?php

namespace Mautic\Migrations;

use Doctrine\DBAL\Migrations\SkipMigrationException;
use Doctrine\DBAL\Schema\Schema;
use Mautic\CoreBundle\Doctrine\AbstractMauticMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20191022102943 extends AbstractMauticMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        $activeCode = $this->prefix . 'promotion';
        $table = $schema->getTable($this->prefix.'promotion');
        if (!$table->hasColumn('film')) {
            $this->addSql("ALTER TABLE {$activeCode} ADD film CHAR(255)");
        }
        if (!$table->hasColumn('package')) {
            $this->addSql("ALTER TABLE {$activeCode} ADD package INT(11)");
        }
    }
    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
       // this down() migration is auto-generated, please modify it to your needs

       $activeCode = $this->prefix . 'promotion';
       $tablePromotion = $schema->getTable($activeCode);

       if ($tablePromotion->hasColumn('film')) {
           $this->addSql("ALTER TABLE {$activeCode} DROP COLUMN film");
       }
       if ($tablePromotion->hasColumn('package')) {
           $this->addSql("ALTER TABLE {$activeCode} DROP COLUMN package");
       }
    }
}

