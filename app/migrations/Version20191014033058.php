<?php

namespace Mautic\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20191014033058 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        // $table = $schema->getTable($this->prefix.'transaction');
        /* if (!$table->hasColumn('is_published')) {
            $this->addSql("ALTER TABLE {$this->prefix}transaction ADD is_published TINYINT(1) DEFAULT 1");
        } */

        $tablePromotionSql = $this->createTablePromotionObject();
        $this->addSql($tablePromotionSql);

    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql("DROP TABLE {$this->prefix}transaction");

    }

    protected function createTablePromotionObject(){

        $sql = <<<SQL
ALTER TABLE $this->prefix.transaction
    -- ADD COLUMN `hashId` char(100) DEFAULT NULL,
    -- ADD COLUMN `transId` char(100) DEFAULT NULL,
    -- ADD COLUMN `originalIAPTransId` char(100) DEFAULT NULL,
    -- ADD COLUMN `iapTransId` char(100) DEFAULT NULL,
    -- ADD COLUMN `activeCode` char(50) DEFAULT NULL,
    -- ADD COLUMN `amount` decimal(15,2) DEFAULT NULL,
    -- ADD COLUMN `metadata` text,
    -- ADD COLUMN `userId` char(100) DEFAULT NULL,
    -- ADD COLUMN `subscriptionId` char(100) DEFAULT NULL,
    -- ADD COLUMN `methodId` char(100) DEFAULT NULL,
    -- ADD COLUMN `paymentResult` text,
    -- ADD COLUMN `paymentChargeId` varchar(255) DEFAULT NULL,
    -- ADD COLUMN `planId` char(100) DEFAULT NULL,
    -- ADD COLUMN `videoContentId` char(100) DEFAULT NULL,
    -- ADD COLUMN `macAddress` char(100) DEFAULT NULL,
    -- ADD COLUMN `deviceModel` char(100) DEFAULT NULL,
    -- ADD COLUMN `startDate` datetime DEFAULT NULL,
    -- ADD COLUMN `expireTime` datetime DEFAULT NULL,
    -- ADD COLUMN `paymentId` char(100) DEFAULT NULL,
    -- ADD COLUMN `statusText` char(20) DEFAULT NULL,
    -- ADD COLUMN `createdAt` datetime NOT NULL,
    -- ADD COLUMN `updatedAt` datetime NOT NULL,
    -- ADD COLUMN `channelType` char(100) DEFAULT NULL,
    -- ADD COLUMN `assetLogicalId` char(100) DEFAULT NULL,
    -- ADD COLUMN `profile` char(100) DEFAULT NULL,
    -- ADD COLUMN `titleId` char(100) DEFAULT NULL,
    -- ADD COLUMN `user_id` char(100) DEFAULT NULL
SQL;
        return $sql;
    }
}
