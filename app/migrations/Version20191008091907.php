<?php

namespace Mautic\Migrations;

use Doctrine\DBAL\Migrations\SkipMigrationException;
use Doctrine\DBAL\Schema\Schema;
use Mautic\CoreBundle\Doctrine\AbstractMauticMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20191008091907 extends AbstractMauticMigration
{
    public function preUp(Schema $schema)
    {
        
    }

    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        $table = $schema->getTable($this->prefix.'promotion_condition_detail');
        if (!$table->hasColumn('orders')) {
            $this->addSql("ALTER TABLE {$this->prefix}promotion_condition_detail ADD orders TINYINT(1) DEFAULT 1");
        }
        if (!$table->hasColumn('uuid')) {
            $this->addSql("ALTER TABLE {$this->prefix}promotion_condition_detail ADD uuid CHAR(100)");
        }
       
    }
    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs

        $promotion = $this->prefix . 'promotion_condition_detail';
        $tablePromotion = $schema->getTable($promotion);

        if ($tablePromotion->hasColumn('orders')) {
            $this->addSql("ALTER TABLE {$promotion} DROP COLUMN orders");
        }
        if ($tablePromotion->hasColumn('uuid')) {
            $this->addSql("ALTER TABLE {$promotion} DROP COLUMN uuid");
        }
    }
}
