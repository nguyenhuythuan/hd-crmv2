<?php

namespace Mautic\Migrations;

use Doctrine\DBAL\Migrations\SkipMigrationException;
use Doctrine\DBAL\Schema\Schema;
use Mautic\CoreBundle\Doctrine\AbstractMauticMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20191022040556 extends AbstractMauticMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        $activeCode = $this->prefix . 'fimplus_campaign_activecode';
        $table = $schema->getTable($this->prefix.'fimplus_campaign_activecode');
        if (!$table->hasColumn('apply_success_message')) {
            $this->addSql("ALTER TABLE {$activeCode} ADD apply_success_message VARCHAR(255)");
        }
        if (!$table->hasColumn('config_note')) {
            $this->addSql("ALTER TABLE {$activeCode} ADD config_note TEXT");
        }
        if (!$table->hasColumn('status')) {
            $this->addSql("ALTER TABLE {$activeCode} ADD status TINYINT(4)");
        }
    }
    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
       // this down() migration is auto-generated, please modify it to your needs

       $activeCode = $this->prefix . 'fimplus_campaign_activecode';
       $tablePromotion = $schema->getTable($activeCode);

       if ($tablePromotion->hasColumn('apply_success_message')) {
           $this->addSql("ALTER TABLE {$activeCode} DROP COLUMN apply_success_message");
       }
       if ($tablePromotion->hasColumn('config_note')) {
           $this->addSql("ALTER TABLE {$activeCode} DROP COLUMN config_note");
       }
       if ($tablePromotion->hasColumn('status')) {
           $this->addSql("ALTER TABLE {$activeCode} DROP COLUMN status");
       }
    }
}

