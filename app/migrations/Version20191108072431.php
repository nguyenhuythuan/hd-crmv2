<?php

namespace Mautic\Migrations;

use Doctrine\DBAL\Migrations\SkipMigrationException;
use Doctrine\DBAL\Schema\Schema;
use Mautic\CoreBundle\Doctrine\AbstractMauticMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20191108072431 extends AbstractMauticMigration
{
    public function preUp(Schema $schema)
    {
        
    }

    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        $table = $schema->getTable($this->prefix.'fimplus_campaign_activecode');
        if (!$table->hasColumn('prefix')) {
            $this->addSql("ALTER TABLE {$this->prefix}fimplus_campaign_activecode ADD prefix CHAR(5) UNIQUE NOT NULL");
        }
       
    }
    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs

        $activeCode = $this->prefix . 'fimplus_campaign_activecode';
        $tableActiveCode = $schema->getTable($activeCode);

        if ($tableActiveCode->hasColumn('prefix')) {
            $this->addSql("ALTER TABLE {$activeCode} DROP COLUMN prefix");
        }
    }
}
