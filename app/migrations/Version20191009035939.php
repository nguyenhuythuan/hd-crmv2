<?php

namespace Mautic\Migrations;

use Doctrine\DBAL\Migrations\SkipMigrationException;
use Doctrine\DBAL\Schema\Schema;
use Mautic\CoreBundle\Doctrine\AbstractMauticMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20191009035939 extends AbstractMauticMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
       $this->addSql($this->createTableFimplusCampaignActiveCodeDetail());
    }
    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        $this->addSql("DROP TABLE {$this->prefix}fimplus_campaign_activecode");
    }

    /**
*
* table fimplus campaign  activecode detail
*
**/
protected function createTableFimplusCampaignActiveCodeDetail(){
    $campaignFk  = $this->generatePropertyName('fimplus_campaign_activecode', 'fk', ['fimplus_campaign_id']);
    $sql = <<<SQL
CREATE TABLE IF NOT EXISTS {$this->prefix}fimplus_campaign_activecode (
id INT AUTO_INCREMENT NOT NULL,
fimplus_campaign_id INT NOT NULL,
uuid CHAR(100),
partner INT(11) NOT NULL,
code_type TINYINT(3) NOT NULL,
package INT(11),
film CHAR(100),
code_value INT(11),
is_percent TINYINT(1) NOT NULL DEFAULT 0,
max_discount_value INT(11) NOT NULL DEFAULT 0,
original_price INT(11) NOT NULL,
is_published TINYINT(1) NOT NULL DEFAULT 1,
date_added DATETIME  COMMENT '(DC2Type:datetime)',
date_modified DATETIME  COMMENT '(DC2Type:datetime)',
created_by INT DEFAULT NULL,
created_by_user VARCHAR(255) DEFAULT NULL,
modified_by INT DEFAULT NULL,
modified_by_user VARCHAR(255) DEFAULT NULL,
checked_out DATETIME DEFAULT NULL,
checked_out_by INT(11) DEFAULT NULL,
checked_out_by_user VARCHAR(255) DEFAULT 'Admin user',
PRIMARY KEY(id),
CONSTRAINT $campaignFk FOREIGN KEY (fimplus_campaign_id) REFERENCES {$this->prefix}fimplus_campaign (id) ON DELETE CASCADE
) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB
SQL;
    return $sql;
}



}

