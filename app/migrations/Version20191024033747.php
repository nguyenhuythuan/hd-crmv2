<?php

namespace Mautic\Migrations;

use Doctrine\DBAL\Migrations\SkipMigrationException;
use Doctrine\DBAL\Schema\Schema;
use Mautic\CoreBundle\Doctrine\AbstractMauticMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20191024033747 extends AbstractMauticMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        $activeCode = $this->prefix . 'fimplus_campaign_activecode';
       $tablePromotion = $schema->getTable($activeCode);

       if ($tablePromotion->hasColumn('film')) {
           $this->addSql("ALTER TABLE {$activeCode} DROP COLUMN film");
       }
       if ($tablePromotion->hasColumn('package')) {
           $this->addSql("ALTER TABLE {$activeCode} DROP COLUMN package");
       }
    }
    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
       // this down() migration is auto-generated, please modify it to your needs
    }
}

