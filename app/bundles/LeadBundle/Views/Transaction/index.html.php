<?php

/*
 * @copyright   2014 Mautic Contributors. All rights reserved
 * @author      Mautic
 *
 * @link        http://mautic.org
 *
 * @license     GNU/GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
 */
?>
<!-- filter form -->
<form method="post" action="<?php echo $view['router']->path('mautic_contact_transaction_action', ['leadId' => $lead->getId()]); ?>" class="panel" id="transaction-filters">
    <div class="form-control-icon pa-xs">
        <input type="text" class="form-control bdr-w-0" name="search" id="search" placeholder="
        <?php echo $view['translator']->trans('mautic.core.search.placeholder'); ?>"
               value="<?php echo $view->escape($events['filters']['search']); ?>">
        <span class="the-icon fa fa-search text-muted mt-xs"></span>
    </div>
    <input type="hidden" name="transactionId" id="transactionId" value="<?php echo $view->escape($lead->getId()); ?>" />
</form>

<div id="transaction-table">
    <?php $view['slots']->output('_content'); ?>
</div>
