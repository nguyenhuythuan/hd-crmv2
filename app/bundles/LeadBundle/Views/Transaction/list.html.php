<?php

/*
 * @copyright   2014 Mautic Contributors. All rights reserved
 * @author      Mautic
 *
 * @link        http://mautic.org
 *
 * @license     GNU/GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
 */
if (isset($tmpl) && $tmpl == 'index') {
    $view->extend('MauticLeadBundle:Transaction:index.html.php');
}

$baseUrl = $view['router']->path(
    'mautic_contact_transaction_action',
    [
        'leadId' => $lead->getId(),
    ]
);
?>

<div class="table-responsive page-list">
    <table class="table table-hover table-bordered" id="contact-transaction">
        <thead>
        <tr>
            <?php
            echo $view->render(
                'MauticCoreBundle:Helper:tableheader.html.php',
                [
                    'orderBy'    => '',
                    'text'       => 'Creation Date',
                    'class'      => 'timeline-name',
                    'sessionVar' => 'transaction',
                    'baseUrl'    => $baseUrl,
                    'target'     => '#transaction-table',
                ]
            );

            echo $view->render(
                'MauticCoreBundle:Helper:tableheader.html.php',
                [
                    'orderBy'    => 'uuid',
                    'text'       => 'Transaction Id',
                    'class'      => 'visible-md visible-lg timeline-type',
                    'sessionVar' => 'lead.'.$lead->getId().'.transaction',
                    'baseUrl'    => $baseUrl,
                    'target'     => '#transaction-table',
                ]
            );

            echo $view->render(
                'MauticCoreBundle:Helper:tableheader.html.php',
                [
                    'orderBy'    => '',
                    'text'       => 'Item Name',
                    'class'      => 'timeline-name',
                    'sessionVar' => 'transaction',
                    'baseUrl'    => $baseUrl,
                    'target'     => '#transaction-table',
                ]
            );

            echo $view->render(
                'MauticCoreBundle:Helper:tableheader.html.php',
                [
                    'orderBy'    => '',
                    'text'       => 'Item Know As',
                    'class'      => 'timeline-name',
                    'sessionVar' => 'transaction',
                    'baseUrl'    => $baseUrl,
                    'target'     => '#transaction-table',
                ]
            );

            echo $view->render(
                'MauticCoreBundle:Helper:tableheader.html.php',
                [
                    'orderBy'    => '',
                    'text'       => 'Type',
                    'class'      => 'timeline-name',
                    'sessionVar' => 'transaction',
                    'baseUrl'    => $baseUrl,
                    'target'     => '#transaction-table',
                ]
            );

            echo $view->render(
                'MauticCoreBundle:Helper:tableheader.html.php',
                [
                    'orderBy'    => '',
                    'text'       => 'Device',
                    'class'      => 'timeline-name',
                    'sessionVar' => 'transaction',
                    'baseUrl'    => $baseUrl,
                    'target'     => '#transaction-table',
                ]
            );

            echo $view->render(
                'MauticCoreBundle:Helper:tableheader.html.php',
                [
                    'orderBy'    => '',
                    'text'       => 'Status',
                    'class'      => 'timeline-name',
                    'sessionVar' => 'transaction',
                    'baseUrl'    => $baseUrl,
                    'target'     => '#transaction-table',
                ]
            );

            echo $view->render(
                'MauticCoreBundle:Helper:tableheader.html.php',
                [
                    'orderBy'    => '',
                    'text'       => 'Price',
                    'class'      => 'timeline-name',
                    'sessionVar' => 'transaction',
                    'baseUrl'    => $baseUrl,
                    'target'     => '#transaction-table',
                ]
            );

            echo $view->render(
                'MauticCoreBundle:Helper:tableheader.html.php',
                [
                    'orderBy'    => '',
                    'text'       => 'Updated Date',
                    'class'      => 'timeline-name',
                    'sessionVar' => 'transaction',
                    'baseUrl'    => $baseUrl,
                    'target'     => '#transaction-table',
                ]
            );
            ?>
        </tr>
        <tbody>
        <?php foreach ($events['events'] as $counter => $event):?>
            <tr>
                <td class="timeline-timestamp">
                    <?php echo $view['date']->toText($event['timestamp'], 'local', 'Y-m-d H:i:s', true); ?>
                </td>
                <td><?php echo $event['eventLabel']?></td>
                <td><?php echo $event['itemName']?></td>
                <td><?php echo $event['itemKnowAs']?></td>
                <td></td>
                <td><?php echo $event['device']?></td>
                <td>
                    <?php if ($event['status'] ===1):?>
                        <span class="label label-success"<?php echo $style; ?>>
                                <?php echo 'success'; ?>
                        </span>
                    <?php endif?>
                    <?php if ($event['status'] ===2):?>
                        <span class="label label-danger"<?php echo $style; ?>>
                                <?php echo 'failed'; ?>
                        </span>
                    <?php endif?>
                </td>
                <td><?php echo $event['price']?></td>
                <td class="timeline-timestamp">
                    <?php echo $view['date']->toText($event['updateDate'], 'local', 'Y-m-d H:i:s', true); ?>
                </td>
            </tr>
        <?php endforeach; ?>
        </tbody>
    </table>
</div>

<?php echo $view->render(
    'MauticCoreBundle:Helper:pagination.html.php',
    [
        'page'       => $events['page'],
        'fixedPages' => $events['maxPages'],
        'fixedLimit' => true,
        'baseUrl'    => $baseUrl,
        'target'     => '#transaction-table',
        'totalItems' => $events['total'],
    ]
); ?>


