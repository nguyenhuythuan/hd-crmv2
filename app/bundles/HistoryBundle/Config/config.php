<?php

/*
 * @copyright   2014 Mautic Contributors. All rights reserved
 * @author      Mautic
 *
 * @link        http://mautic.org
 *
 * @license     GNU/GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
 */

return [
    'routes' => [
        'main' => [
            'mautic_history_index'        => [
                'path'       => '/histories/{page}',
                'controller' => 'MauticHistoryBundle:History:index',
            ],
        ],
    ],

//    'menu' => [
//        'main' => [
//            'mautic.history.menu.index' => [
//                'iconClass' => 'fa-clock-o',
//                'route'     => 'mautic_history_index',
//                'access'    => 'history:history:view',
//                'priority'  => 50,
//            ],
//        ],
//    ],

    'models'       => [
        'mautic.customer.model.customer'  => [
            'class'     => \Mautic\MauticCustomerBundle\Model\CustomerModel::class,
//            'arguments' => [
//                'mautic.lead.model.lead',
//                'mautic.lead.model.list',
//                'mautic.form.model.form',
//                'mautic.campaign.event_collector',
//                'mautic.campaign.helper.removed_contact_tracker',
//                'mautic.campaign.membership.manager',
//                'mautic.campaign.membership.builder',
//            ],
        ],
    ],
    'repositories' => [
        'mautic.customer.repository.customer' => [
            'class'     => Doctrine\ORM\EntityRepository::class,
            'factory'   => ['@doctrine.orm.entity_manager', 'getRepository'],
            'arguments' => [
                \Mautic\HistoryBundle\Entity\Customer::class,
            ],
        ],
    ],
];
