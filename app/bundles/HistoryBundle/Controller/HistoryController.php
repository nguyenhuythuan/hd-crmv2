<?php

/*
 * @copyright   2014 Mautic Contributors. All rights reserved
 * @author      Mautic
 *
 * @link        http://mautic.org
 *
 * @license     GNU/GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
 */

namespace Mautic\HistoryBundle\Controller;

use Mautic\CoreBundle\Controller\AbstractStandardFormController;
//use Mautic\HistoryBundle\Entity\Event;
//use Mautic\HistoryBundle\Entity\LeadEventLogRepository;
//use Mautic\HistoryBundle\EventListener\CampaignActionJumpToEventSubscriber;
//use Mautic\HistoryBundle\Model\EventModel;
use Mautic\HistoryBundle\Entity\History;
use Mautic\LeadBundle\Controller\EntityContactsTrait;

class HistoryController extends AbstractStandardFormController
{
    use EntityContactsTrait;

    /**
     * @var
     */
    protected $sessionId;

    /**
     * @return array
     */
    protected function getPermissions()
    {
        //set some permissions
        return (array) $this->get('mautic.security')->isGranted(
            [
                'history:histories:viewown',
                'history:histories:viewother',
                'history:histories:create',
                'history:histories:editown',
                'history:histories:editother',
                'history:histories:cloneown',
                'history:histories:cloneother',
                'history:histories:deleteown',
                'history:histories:deleteother',
                'history:histories:publishown',
                'history:histories:publishother',
            ],
            'RETURN_ARRAY'
        );
    }

    public function indexAction($page = null)
    {
        return $this->delegateView([
            'viewParameters' => [
                'security'      => $this->get('mautic.security'),
            ],
            'contentTemplate' => 'MauticHistoryBundle:Default:index.html.php',
            'passthroughVars' => [
                'activeLink'    => '#mautic_history_index',
                'mauticContent' => 'history',
                'route'         => $this->generateUrl('mautic_history_index'),
            ],
        ]);
//        return $this->indexStandard($page);
    }

    /**
     * @return string
     */
    protected function getModelName()
    {
        return 'history';
    }
}
