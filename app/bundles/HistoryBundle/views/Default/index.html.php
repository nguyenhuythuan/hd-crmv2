<?php

/*
 * @copyright   2014 Mautic Contributors. All rights reserved
 * @author      Mautic
 *
 * @link        http://mautic.org
 *
 * @license     GNU/GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
 */
$view->extend('MauticCoreBundle:Default:content.html.php');
$view['slots']->set('headerTitle', $view['translator']->trans('mautic.history.header.index'));
$view['slots']->set('mauticContent', 'Customer');

?>
<div class="box-layout">
    <div class="col-xs-12 bg-white height-auto">
        <!-- tabs controls -->
        <ul class="bg-auto nav nav-tabs pr-md pl-md">
            <li class="active"><a href="#details-container" role="tab"
                                  data-toggle="tab"><?php echo $view['translator']->trans('mautic.history.tab1.index'); ?></a>
            </li>
            <li class=""><a href="#permissions-container" role="tab"
                            data-toggle="tab"><?php echo $view['translator']->trans('mautic.history.tab2.index'); ?></a>
            </li>
        </ul>
    </div>
</div>

