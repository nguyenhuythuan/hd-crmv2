<?php

/*
 * @copyright   2014 Mautic Contributors. All rights reserved
 * @author      Mautic
 *
 * @link        http://mautic.org
 *
 * @license     GNU/GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
 */

namespace Mautic\HistoryBundle\Entity;

//use Mautic\CampaignBundle\Entity\Result\CountResult;
//use Mautic\CampaignBundle\Executioner\ContactFinder\Limiter\ContactLimiter;
use Mautic\CoreBundle\Entity\CommonRepository;

/**
 * Class CampaignRepository.
 */
class HistoryRepository extends CommonRepository
{
//    use ContactLimiterTrait;
//    use SlaveConnectionTrait;

    /**
     * {@inheritdoc}
     */
    public function getEntities(array $args = [])
    {
    }

    /**
     * {@inheritdoc}
     *
     * @param object $entity
     * @param bool   $flush
     */
    public function deleteEntity($entity, $flush = true)
    {
    }
}
