<?php

/*
 * @copyright   2014 Mautic Contributors. All rights reserved
 * @author      Mautic
 *
 * @link        http://mautic.org
 *
 * @license     GNU/GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
 */

namespace Mautic\HistoryBundle\Model;

//use Mautic\HistoryBundle\CampaignEvents;
//use Mautic\HistoryBundle\Entity\Campaign;
//use Mautic\HistoryBundle\Entity\Event;
//use Mautic\HistoryBundle\Entity\Lead as CampaignLead;
//use Mautic\HistoryBundle\Event as Events;
//use Mautic\HistoryBundle\EventCollector\EventCollector;
//use Mautic\HistoryBundle\Executioner\ContactFinder\Limiter\ContactLimiter;
//use Mautic\HistoryBundle\Helper\ChannelExtractor;
//use Mautic\HistoryBundle\Helper\RemovedContactTracker;
//use Mautic\HistoryBundle\Membership\MembershipBuilder;
//use Mautic\HistoryBundle\Membership\MembershipManager;
use Mautic\CoreBundle\Model\FormModel as CommonFormModel;
use Mautic\FormBundle\Model\FormModel;
use Mautic\LeadBundle\Entity\Lead;
use Mautic\LeadBundle\Model\LeadModel;
use Mautic\LeadBundle\Model\ListModel;

/**
 * Class CampaignModel
 * {@inheritdoc}
 */
class HistoryModel extends CommonFormModel
{
    /**
     * @var LeadModel
     */
    protected $leadModel;

    /**
     * @var ListModel
     */
    protected $leadListModel;

    /**
     * @var FormModel
     */
    protected $formModel;

    /**
     * @var EventCollector
     */
    private $eventCollector;

    /**
     * @var RemovedContactTracker
     */
    private $removedContactTracker;

    /**
     * @var MembershipManager
     */
    private $membershipManager;

    /**
     * @var MembershipBuilder
     */
    private $membershipBuilder;

    /**
     * CampaignModel constructor.
     *
     * @param LeadModel             $leadModel
     * @param ListModel             $leadListModel
     * @param FormModel             $formModel
     * @param EventCollector        $eventCollector
     * @param RemovedContactTracker $removedContactTracker
     * @param MembershipManager     $membershipManager
     * @param MembershipBuilder     $membershipBuilder
     */
    public function __construct(
        LeadModel $leadModel,
        ListModel $leadListModel,
        FormModel $formModel,
        EventCollector $eventCollector,
        RemovedContactTracker $removedContactTracker,
        MembershipManager $membershipManager,
        MembershipBuilder $membershipBuilder
    ) {
        $this->leadModel             = $leadModel;
        $this->leadListModel         = $leadListModel;
        $this->formModel             = $formModel;
        $this->eventCollector        = $eventCollector;
        $this->removedContactTracker = $removedContactTracker;
        $this->membershipManager     = $membershipManager;
        $this->membershipBuilder     = $membershipBuilder;
    }

    /**
     * {@inheritdoc}
     *
     * @return \Mautic\HistoryBundle\Entity\CampaignRepository
     */
    public function getRepository()
    {
        $repo = $this->em->getRepository('MauticCustomerBundle:Customer');
        $repo->setCurrentUser($this->userHelper->getUser());

        return $repo;
    }

    /**
     * @return \Mautic\HistoryBundle\Entity\EventRepository
     */
    public function getEventRepository()
    {
        return $this->em->getRepository('MauticHistoryBundle:Event');
    }

    /**
     * @return \Mautic\HistoryBundle\Entity\LeadRepository
     */
    public function getCampaignLeadRepository()
    {
        return $this->em->getRepository('MauticHistoryBundle:Lead');
    }

    /**
     * @return \Mautic\HistoryBundle\Entity\LeadEventLogRepository
     */
    public function getCampaignLeadEventLogRepository()
    {
        return $this->em->getRepository('MauticHistoryBundle:LeadEventLog');
    }

    /**
     * {@inheritdoc}
     *
     * @return string
     */
    public function getPermissionBase()
    {
        return 'history:histories';
    }

    /**
     * {@inheritdoc}
     *
     * @param       $entity
     * @param       $formFactory
     * @param null  $action
     * @param array $options
     *
     * @return mixed
     *
     * @throws \Symfony\Component\HttpKernel\Exception\NotFoundHttpException
     */
    public function createForm($entity, $formFactory, $action = null, $options = [])
    {
    }
}
