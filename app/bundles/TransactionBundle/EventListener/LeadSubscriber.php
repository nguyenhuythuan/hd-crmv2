<?php

namespace Mautic\TransactionBundle\EventListener;


use Mautic\CoreBundle\CoreEvents;
use Mautic\CoreBundle\Event\CustomContentEvent;
use Mautic\CoreBundle\Event\CustomTemplateEvent;
use Mautic\CoreBundle\EventListener\CommonSubscriber;
use Mautic\TransactionBundle\Model\TransactionModel;
use Mautic\LeadBundle\LeadEvents;
use Mautic\LeadBundle\Event\LeadTimelineEvent;

class LeadSubscriber extends CommonSubscriber
{
     /**
     * @var TransactionModel
     */
    protected $transactionModel;
    protected $transactionLead;
    protected $engine;
    protected $transactionTab;
    protected $transactionTabContent;


    /**
     * TransactionEventListener constructor.
     *
     * @param TransactionModel       $transactionModel
     */
    public function __construct(TransactionModel $transactionModel, $transactionLead = [])
    {
        $this->transactionModel             = $transactionModel;
        $this->transactionLead              = $transactionLead;
    }
    public static function getSubscribedEvents()
    {
        return [
            CoreEvents::VIEW_INJECT_CUSTOM_CONTENT => ['injectViewContent', 0],
            CoreEvents::VIEW_INJECT_CUSTOM_TEMPLATE => ['injectViewTemplate', 0],
            LeadEvents::TIMELINE_ON_GENERATE =>['onTimelineGenerate',0]
        ];
    }

    protected $request;

    public function onTimelineGenerate(LeadTimelineEvent $event){
        $leadId = $event->getLeadId();
        $this->transactionLead = $this->transactionModel->getRepository()->getTransactionByLeadIds($leadId);
        $lead =  $this->factory->getModel('lead')->getEntity($leadId);
        $this->transactionTab =  $this->templating->renderResponse(
            'MauticTransactionBundle:EventListener:transaction_tab.html.php',
            [
                'total' => count($this->transactionLead),
            ]
        )->getContent();
        $limit = 30;
        $page = 1;
        $search ='';
        //$this->session->set('mautic.lead.' . $leadId . '.transaction.filter', $search);

        $this->transactionTabContent =  $this->templating->renderResponse(
            'MauticTransactionBundle:EventListener:transaction_tab_content.html.php',
            [
                'transactions'  => $this->transactionLead,
                'lead'     => $lead,
                'tmpl'          => '',
                'leadId'        => $leadId,
                'page'              => $page,
                'limit'             => $limit,
                'search'            => $search,
            ]
        )->getContent();
    }
    /**
     * @param  $event
     */
    public function injectViewContent(CustomContentEvent $event)
    {
       

        $router = $this->request->attributes->get('_route');
        if (0 === strpos($router, 'mautic_contact_action')) {
            $context = $event->getContext();
            if ($context == 'tabs') {
                $event->addContent($this->transactionTab);
            }
            if($context == 'tabs.content'){
                $event->addContent($this->transactionTabContent);
            }
            
        }
    }

    /**
     * @param CustomButtonEvent $event
     */
    public function injectViewTemplate(CustomTemplateEvent $event)
    {   
        $this->request = $event->getRequest();
    }
}
