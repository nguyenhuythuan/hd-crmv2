<?php
namespace Mautic\TransactionBundle\Service;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Validator\Constraints\Date;
use Mautic\LeadBundle\Model\Lead;


class TransService
{
    public $channel;
    public $connection;
    public $container;
    const CREATE_TRANS = 'CREATE_TXN';
    const UPDATE_TRANS = 'UPDATE_TXN';

    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
    }

    protected $keyRequire = [
        'transId',
        'hashId',
        'ip',
        'platform',
        'uuid',
        'originalIAPTransId',
        'iapTransId',
        'activeCode',
        'status',
        'imei',
        'amount',
        'metadata',
        'userId',
        'subscriptionId',
        'methodId',
        'paymentResult',
        'paymentChargeId',
        'planId',
        'videoContentId',
        'macAddress',
        'deviceModel',
        'startDate',
        'expireTime',
        'paymentId',
        'statusText',
        'createdAt',
        'updatedAt',
        'channelType',
        'assetLogicalId',
        'profile',
        'titleId',
        'leadId'
    ];
    public function getRecord($data){
        $em = $this->container->get('doctrine')->getEntityManager();
        $reponstory = $em->getRepository('MauticTransactionBundle:Transaction');
        $transItem = $reponstory->findOneBy([
            'transId' => isset($data->id)?$data->id:'',
        ]);
        if($transItem != null){
            return $transItem;
        }
        return false;
    }
    public function getLeadRecord($whereData = [],$fetch = true){
        $result = $this->container->get('doctrine')->getEntityManager()->getConnection()->createQueryBuilder()
            ->select('*')
            ->from(MAUTIC_TABLE_PREFIX.'leads', 'l');
            if($whereData && count($whereData) > 0){
                foreach($whereData as $key => $value){
                    $result->where("{$key} = :{$key}")
                    ->setParameter($key, $value);
                }
            }
            // ->where(['id_fimplus'=>'e58bf659-11eb-4e19-8516-c472676cf161'])
            $result = $result->execute();
            if($fetch){
                return $result->fetch();
            }
        return $result->fetchAll();
    }

    public function insertData($data){
        // $data = [];
        if($data == '' || $data == null){
            return false;
        }
        if(is_array($data)){
            $data = (object)$data;
        }

        $dataCondition = $this->keyRequire;
        try{
            $em = $this->container->get('doctrine')->getEntityManager();
            if($this->getRecord($data)){
                return true;
            }
            $model = $this->container->get('mautic.transaction.model.transaction');
            $entity = $model->getEntity();    

            $where = ['id_fimplus' => $data->userId];
            $leadData = $this->getLeadRecord($where,true);
            if(is_array($leadData)){
                $leadData = (object)$leadData;
            }
            // set lead_id in transaction 
            $data->leadId = 0;
            // var_dump($leadData);
            // exit;
            if($leadData && is_numeric($leadData->id)){
                $data->leadId = $leadData->id;
            }
            foreach($dataCondition as $key => $value){
                if(!isset($data->{$value}) && $value != 'transId'){
                    $data->{$value} = null;
                }
                if($value == 'transId'){
                    $data->transId = $data->id;
                }
                $date = '';
                if($value == 'startDate' ){
                    $data->startDate = $this->convertDateToFomartDb($data->startDate);
                }
                if($value == 'expireTime'){
                    $data->expireTime = $this->convertDateToFomartDb($data->expireTime);
                }
                if($value == 'updatedAt' && $data->updatedAt != ''){
                    $data->updatedAt = $this->convertDateToFomartDb($data->updatedAt);
                }
                if($value == 'createdAt'){
                    $data->createdAt = $this->convertDateToFomartDb($data->createdAt);
                }
                unset($data->id);
                $property = (string)'set'.ucfirst($value);
                if(property_exists($entity,$value)){
                    $entity->{$property}($data->{$value});
                }

            }
            $em->persist($entity);
            $em->flush();
            if($entity->getId()){
                return $entity;
            }
            return false;
        }catch(\Exception $e){
            // throw new error()
            print_r($e->getMessage());
            return false;
        }

    }
    public function updateData($data){
        if($data == '' || $data == null){
            return false;
        }
        if(is_array($data)){
            $data = (object)$data;
        }

        $dataCondition = $this->keyRequire;
        try{
            $em = $this->container->get('doctrine')->getEntityManager();
            $transItem = $this->getRecord($data);
            if(!$transItem){
                return $this->insertData($data);
            }

            $model = $this->container->get('mautic.transaction.model.transaction');
            $entity = $model->getEntity($transItem->id);

            $where = ['id_fimplus' => $data->userId];
            $leadData = $this->getLeadRecord($where,true);
            if(is_array($leadData)){
                $leadData = (object)$leadData;
            }
            // set lead_id in transaction 
            $data->leadId = 0;
            if(is_numeric($leadData->id)){
                $data->leadId = $leadData->id;
            }

            foreach($dataCondition as $key => $value){
                if(!isset($data->{$value}) && $value != 'transId'){
                    $data->{$value} = null;
                }
                if($value == 'transId'){
                    $data->transId = $data->id;
                }
                $date = '';
                if($value == 'startDate' ){
                    $data->startDate = $this->convertDateToFomartDb($data->startDate);
                }
                if($value == 'expireTime'){
                    $data->expireTime = $this->convertDateToFomartDb($data->expireTime);
                }
                if($value == 'updatedAt' && $data->updatedAt != ''){
                    $data->updatedAt = $this->convertDateToFomartDb($data->updatedAt);
                }
                if($value == 'createdAt'){
                    $data->createdAt = $this->convertDateToFomartDb($data->createdAt);
                }
                unset($data->id);
                $property = (string)'set'.ucfirst($value);
                /* echo "\n";
                echo $property;
                var_dump(property_exists($entity,$value));
                echo "\n"; */
                if(property_exists($entity,$value)){
                    $entity->{$property}($data->{$value});
                }

            }
            /* var_dump($entity->getUserId());
            var_dump($data->userId);
            exit(); */
            $em->persist($entity);
            $em->flush();
            if($entity->getId()){
                return $entity;
            }
            return false;
        }catch(\Exception $e){
            // throw new error()
            print_r($e->getMessage());
            return false;
        }
        


    }
    public function upChangeData($data){
        // die("1222");
        $action = $data->action;
        /* var_dump($action);
        exit; */
        switch($action){
            case self::CREATE_TRANS : return $this->insertData($data->data) ; break;
            case self::UPDATE_TRANS : return $this->updateData($data->data) ; break;
            default: return false ; break;
        }
        return true;
    }
    public function convertDateToFomartDb($time = ''){
        // exit;
        if($time == '' || $time == null || $time == false){
            return null;
        }
        if(is_numeric($time)){
            return date('Y-m-d H:i:s',$time);
        }else{
            $date = date_create($time);
            if(date_format('Y-m-d H:i:s',$date)){
                return date_format('Y-m-d H:i:s',$date);    
            }
            return null;
        }
    }
}