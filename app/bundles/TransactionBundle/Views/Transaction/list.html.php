<?php
    if ($tmpl == 'index') {
        $view->extend('MauticTransactionBundle:Transaction:index.html.php');
    }
    $sessionVar = 'lead'. $leadId . 'transaction';
    $target = '#transaction-container';
?>
<?php if (count($transactions)): ?>
    <div class="table-responsive">
        <table class="table table-hover table-striped table-bordered campaign-list" id="transactionTable">
            <thead>
            <tr>
                
                <?php
                
                echo $view->render(
                    'MauticCoreBundle:Helper:tableheader.html.php',
                    [
                        'sessionVar' => $sessionVar,
                        'orderBy'    => '',
                        'text'       => '#',
                        'class'      => 'visible-md visible-lg col-campaign-id',
                        'default'    => true,
                    ]
                );
                
                echo $view->render(
                    'MauticCoreBundle:Helper:tableheader.html.php',
                    [
                        'sessionVar' => $sessionVar,
                        'orderBy'    => '',
                        'text'       => 'Creation Date',
                        'class'      => 'visible-md visible-lg col-campaign-id',
                    ]
                );

                echo $view->render(
                    'MauticCoreBundle:Helper:tableheader.html.php',
                    [
                        'sessionVar' => $sessionVar,
                        'orderBy'    => '',
                        'text'       => 'Transaction ID',
                        'class'      => 'visible-md visible-lg col-campaign-id',
                    ]
                );

                echo $view->render(
                    'MauticCoreBundle:Helper:tableheader.html.php',
                    [
                        'sessionVar' => $sessionVar,
                        'orderBy'    => '',
                        'text'       => 'Item Name',
                        'class'      => 'visible-md visible-lg col-campaign-id',
                    ]
                );

                echo $view->render(
                    'MauticCoreBundle:Helper:tableheader.html.php',
                    [
                        'sessionVar' => $sessionVar,
                        'orderBy'    => '',
                        'text'       => 'Item Know As',
                        'class'      => 'visible-md visible-lg col-campaign-id',
                    ]
                );

                echo $view->render(
                    'MauticCoreBundle:Helper:tableheader.html.php',
                    [
                        'sessionVar' => $sessionVar,
                        'orderBy'    => '',
                        'text'       => 'Type',
                        'class'      => 'visible-md visible-lg col-campaign-id',
                    ]
                );

                echo $view->render(
                    'MauticCoreBundle:Helper:tableheader.html.php',
                    [
                        'sessionVar' => $sessionVar,
                        'orderBy'    => '',
                        'text'       => 'Device',
                        'class'      => 'visible-md visible-lg col-campaign-id',
                    ]
                );

                echo $view->render(
                    'MauticCoreBundle:Helper:tableheader.html.php',
                    [
                        'sessionVar' => $sessionVar,
                        'orderBy'    => '',
                        'text'       => 'Core ID',
                        'class'      => 'visible-md visible-lg col-campaign-id',
                    ]
                );

                echo $view->render(
                    'MauticCoreBundle:Helper:tableheader.html.php',
                    [
                        'sessionVar' => $sessionVar,
                        'orderBy'    => '',
                        'text'       => 'Status',
                        'class'      => 'visible-md visible-lg col-campaign-id',
                    ]
                );

                echo $view->render(
                    'MauticCoreBundle:Helper:tableheader.html.php',
                    [
                        'sessionVar' => $sessionVar,
                        'orderBy'    => '',
                        'text'       => 'Price',
                        'class'      => 'visible-md visible-lg col-campaign-id',
                    ]
                );

                echo $view->render(
                    'MauticCoreBundle:Helper:tableheader.html.php',
                    [
                        'sessionVar' => $sessionVar,
                        'orderBy'    => '',
                        'text'       => 'Updated Date',
                        'class'      => 'visible-md visible-lg col-campaign-id',
                    ]
                );

                echo $view->render(
                    'MauticCoreBundle:Helper:tableheader.html.php',
                    [
                        'sessionVar' => $sessionVar,
                        'orderBy'    => '',
                        'text'       => 'Charge Code',
                        'class'      => 'visible-md visible-lg col-campaign-id',
                    ]
                );

                echo $view->render(
                    'MauticCoreBundle:Helper:tableheader.html.php',
                    [
                        'sessionVar' => $sessionVar,
                        'orderBy'    => '',
                        'text'       => 'Action',
                        'class'      => 'visible-md visible-lg col-campaign-id',
                    ]
                );
                ?>
            </tr>
            </thead>
            <tbody>
            <?php $index = 1; ?>
            <?php 
                foreach ($transactions as $item): 
                    //Use a separate layout for AJAX generated content
                    echo $view->render('MauticTransactionBundle:Transaction:transaction.html.php', [
                        'item'        => $item,
                        'lead'        => $lead,
                        'index'       => $index
                    ]); 
                    $index++;
                 endforeach; 
            ?>
            </tbody>
        </table>
    </div>
    <div class="panel-footer">
        <?php echo $view->render(
            'MauticCoreBundle:Helper:pagination.html.php',
            [
                'totalItems' => count($transactions),
                'page'       => $page,
                'limit'      => $limit,
                'menuLinkId' => 'mautic_contacttransaction_index',
                'baseUrl'    => $view['router']->path('mautic_contacttransaction_index', ['leadId' => isset($leadId) ? $leadId : 0]),
                'sessionVar' => $sessionVar,
                'target'     => $target,
            ]
        ); ?>
    </div>
<?php else: ?>
    <?php echo $view->render('MauticCoreBundle:Helper:noresults.html.php', ['tip' => 'mautic.transaction.noresults.tip']); ?>
<?php endif; ?>
