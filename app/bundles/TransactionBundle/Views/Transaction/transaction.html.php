<tr>
    <td>
        <?php echo $index; ?>
    </td>
    <td>
        <?php echo $view['date']->toFull($item->getDateAdded()); ?>
    </td>
    <td>
        <?php echo $item->getUuid(); ?>
    </td>
    <td>
        <?php echo $item->getItemId(); ?>
    </td>
    <td>
        <?php echo ''; ?>
    </td>
    <td>
        <?php echo $item->getPaymentMethod(); ?>
    </td>
    <td>
        <?php echo $item->getStatus(); ?>
    </td>
    <td>
        <?php echo ''; ?>
    </td>
    <td>
        <?php echo ''; ?>
    </td>
    <td>
        <?php echo $item->getPaidPrice(); ?>
    </td>
    <td>
        <?php echo $view['date']->toFull($item->getDateModified()); ?>
    </td>
    <td>
        <?php echo ''; ?>
    </td>
    <td>
        <a class="btn btn-default btn-xs" href="<?php echo $view['router']->generate('mautic_contacttransaction_action', ['leadId' => $lead->getId(), 'objectAction' => 'edit', 'objectId' => $item->getId()]); ?>" data-toggle="ajaxmodal" data-target="#MauticSharedModal" data-header="<?php echo $view['translator']->trans('mautic.lead.transaction.header.edit'); ?>"><i class="fa fa-pencil"></i></a>
        <a class="btn btn-default btn-xs" data-toggle="confirmation" href="<?php echo $view['router']->path('mautic_contactnote_action', ['objectAction' => 'delete', 'objectId' => $item->getId(), 'leadId' => $lead->getId()]); ?>" data-message="<?php echo $view->escape($view['translator']->trans('mautic.lead.transaction.confirmdelete')); ?>" data-confirm-text="<?php echo $view->escape($view['translator']->trans('mautic.core.form.delete')); ?>" data-confirm-callback="executeAction" data-cancel-text="<?php echo $view->escape($view['translator']->trans('mautic.core.form.cancel')); ?>">
            <i class="fa fa-trash text-danger"></i>
        </a>
    </td>
</tr>