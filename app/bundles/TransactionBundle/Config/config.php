<?php

return [
    'routes' => [
        'main'  => [
            'mautic_contacttransaction_index' => [
                'path'       => '/transaction/{leadId}/{page}',
                'controller' => 'MauticTransactionBundle:Transaction:index',
                'defaults'   => [
                    'leadId' => 0,
                ],
                'requirements' => [
                    'leadId' => '\d+',
                ],
            ],
            'mautic_contacttransaction_action' => [
                'path'         => '/contacts/transaction/{leadId}/{objectAction}/{objectId}',
                'controller'   => 'MauticTransactionBundle:Transaction:executeTransaction',
                'requirements' => [
                    'leadId' => '\d+',
                ],
            ]
        ]
    ],
    'services' => [
        'repositories' => [
            'mautic.transaction.repository.transaction' => [
                'class'     => Doctrine\ORM\EntityRepository::class,
                'factory'   => ['@doctrine.orm.entity_manager', 'getRepository'],
                'arguments' => [
                    'Mautic\TransactionBundle\Entity\Transaction',
                ],
            ]
        ],

        'models' => [
            'mautic.transaction.model.transaction' => [
                'class'     => 'Mautic\TransactionBundle\Model\TransactionModel',
                'arguments' => []
            ]
         ],

        'events' => [
            'mautic.transaction.leadbundle.subscriber' => [
                'class'     => \Mautic\TransactionBundle\EventListener\LeadSubscriber::class,
                'arguments' => [
                    'mautic.transaction.model.transaction',
                ]
            ],
        ],
        'orther' => [
            'mautic.transaction.service.transService' => [
                'class'     => 'Mautic\TransactionBundle\Service\TransService',
                'arguments' => [
                    'service_container',
                ],
            ]
        ],

    ],

];
