<?php
namespace Mautic\TransactionBundle\Entity;

use Doctrine\DBAL\Types\DecimalType;
use Doctrine\ORM\Mapping as ORM;
use Mautic\CampaignBundle\Executioner\Scheduler\Mode\DateTime;
use Mautic\CoreBundle\Doctrine\Mapping\ClassMetadataBuilder;
use Mautic\CoreBundle\Entity\FormEntity;
use \Mautic\LeadBundle\Entity\Lead;

class Transaction extends FormEntity
{
    const STATUS = [
        "code" => [
            "NEW" => 1, // mới khởi tạo
            "LINKED" => 2, // liên kết với payment thành công
            "PAYING" => 3, // đang tiến hành thanh toán
            "PAID" => 4, // Đã thanh toán thành công
            "ERROR" => -1, // Lỗi
            "EXPIRED" => -2, // Hết hạn
        ],
        "text" => [
            "NEW" => 'NEW', // mới khởi tạo
            "LINKED" => 'LINKED', // liên kết với payment thành công
            "PAYING" => 'PAYING', // đang tiến hành thanh toán
            "PAID" => 'PAID', // Đã thanh toán thành công
            "ERROR" => 'ERROR', // Lỗi
            "EXPIRED" => 'EXPIRED', // Hết hạn
        ]
    ];

    /**
     * @var string
     */
    private $transId;
    /**
     * @var int
     */
    private $id;

    /**
     * @var int
     */
    private $leadId;
    /**
     * @var \Mautic\LeadBundle\Entity\Lead
     */
    private $lead;

    /**
     * @var string
     */
    private $hashId;

    /**
     * @var string
     */
    private $ip;

    /**
     * @var string
     */
    private $platform;

    /**
     * @var string
     */
    private $uuid;

    /**
     * @var string
     */
    private $originalIAPTransId;

    /**
     * @var string
     */
    private $iapTransId;

    /**
     * @var string
     */
    private $activeCode;

    /**
     * @var string
     */
    private $status;

    /**
     * @var string
     */
    private $imei;

    /**
     * @var string
     */
    private $amount;

    /**
     * @var string
     */
    private $metadata;

    /**
     * @var string
     */
    private $user_id;
    private $userId;

    /**
     * @var string
     */
    private $subscriptionId;

    /**
     * @var string
     */
    private $methodId;

    /**
     * @var string
     */
    private $paymentResult;

    /**
     * @var string
     */
    private $paymentChargeId;

    /**
     * @var string
     */
    private $planId;

    /**
     * @var string
     */
    private $videoContentId;

    /**
     * @var string
     */
    private $macAddress;

    /**
     * @var string
     */
    private $deviceModel;

    /**
     * @var string
     */
    private $startDate;

    /**
     * @var string
     */
    private $expireTime;

    /**
     * @var string
     */
    private $paymentId;

    /**
     * @var string
     */
    private $statusText;

    /**
     * @var string
     */
    private $createdAt;

    /**
     * @var string
     */
    private $updatedAt;

    /**
     * @var string
     */
    private $channelType;

    /**
     * @var string
     */
    private $assetLogicalId;

    /**
     * @var string
     */
    private $profile;

    /**
     * @var string
     */
    private $titleId;
    /**
     * @var string
     */
    private $lead_id;




    /** 
     * ================== get function ==========
    */
    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }
    public function setHashId($value){
        return $this->hashId = $value;
    }
    public function setIp($value){
        return $this->ip = $value;
    }
    public function setPlatform($value){
        return $this->platform = $value;
    }
    public function setUuid($value){
        return $this->uuid = $value;
    }
    public function setOriginalIAPTransId($value){
        return $this->originalIAPTransId = $value;
    }
    public function setIapTransId($value){
        return $this->iapTransId = $value;
    }
    public function setActiveCode($value){
        return $this->activeCode = $value;
    }
    public function setStatus($value){
        return $this->status = $value;
    }
    public function setImei($value){
        return $this->imei = $value;
    }
    public function setAmount($value){
        return $this->amount = $value;
    }
    public function setMetadata($value){
        return $this->metadata = $value;
    }
    public function setUserId($value){
        return $this->user_id = $value;
    }
    public function setSubscriptionId($value){
        return $this->subscriptionId = $value;
    }
    public function setMethodId($value){
        return $this->methodId = $value;
    }
    public function setPaymentResult($value){
        return $this->paymentResult = $value;
    }
    public function setPaymentChargeId($value){
        return $this->paymentChargeId = $value;
    }
    public function setPlanId($value){
        return $this->planId = $value;
    }
    public function setVideoContentId($value){
        return $this->videoContentId = $value;
    }
    public function setMacAddress($value){
        return $this->macAddress = $value;
    }
    public function setDeviceModel($value){
        return $this->deviceModel = $value;
    }
    public function setStartDate($value){
        return $this->startDate = $value;
    }
    public function setExpireTime($value){
        return $this->expireTime = $value;
    }
    public function setPaymentId($value){
        return $this->paymentId = $value;
    }
    public function setStatusText($value){
        return $this->statusText = $value;
    }
    public function setCreatedAt($value){
        return $this->createdAt = $value;
    }
    public function setUpdatedAt($value){
        return $this->updatedAt = $value;
    }
    public function setChannelType($value){
        return $this->channelType = $value;
    }
    public function setAssetLogicalId($value){
        return $this->assetLogicalId = $value;
    }
    public function setProfile($value){
        return $this->profile = $value;
    }
    public function setTitleId($value){
        return $this->titleId = $value;
    }
    /**
     * @param string $lead_id
     */
    public function setLeadId($value)
    {
        $this->lead_id = $value;
    }
    /**
     * @param string
     */
    public function setTransId($value)
    {
        $this->transId = $value;
    }

    /** 
     * ================== get function ==========
    */
    public function getHashId(){
        return $this->hashId;
    }
    public function getIp(){
        return $this->ip;
    }
    public function getPlatform(){
        return $this->platform;
    }
    public function getUuid(){
        return $this->uuid;
    }
    public function getOriginalIAPTransId(){
        return $this->originalIAPTransId;
    }
    public function getIapTransId(){
        return $this->iapTransId;
    }
    public function getActiveCode(){
        return $this->activeCode;
    }
    public function getStatus(){
        return $this->status;
    }
    public function getImei(){
        return $this->imei;
    }
    public function getAmount(){
        return $this->amount;
    }
    public function getMetadata(){
        return $this->metadata;
    }
    public function getUserId(){
        return $this->user_id;
    }
    public function getSubscriptionId(){
        return $this->subscriptionId;
    }
    public function getMethodId(){
        return $this->methodId;
    }
    public function getPaymentResult(){
        return $this->paymentResult;
    }
    public function getPaymentChargeId(){
        return $this->paymentChargeId;
    }
    public function getPlanId(){
        return $this->planId;
    }
    public function getVideoContentId(){
        return $this->videoContentId;
    }
    public function getMacAddress(){
        return $this->macAddress;
    }
    public function getDeviceModel(){
        return $this->deviceModel;
    }
    public function getStartDate(){
        return $this->startDate;
    }
    public function getExpireTime(){
        return $this->expireTime;
    }
    public function getPaymentId(){
        return $this->paymentId;
    }
    public function getStatusText(){
        return $this->statusText;
    }
    public function getCreatedAt(){
        return $this->createdAt;
    }
    public function getUpdatedAt(){
        return $this->updatedAt;
    }
    public function getChannelType(){
        return $this->channelType;
    }
    public function getAssetLogicalId(){
        return $this->assetLogicalId;
    }
    public function getProfile(){
        return $this->profile;
    }
    public function getTitleId(){
        return $this->titleId;
    }
    /**
     * @return int
     */
    public function getLeadId()
    {
        return $this->lead_id;
    }
    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }
    /**
     * @return string
     */
    public function getTransId()
    {
        return $this->transId;
    }
    /**
     * chưa edit type data 
     */
    public static function loadMetadata(ORM\ClassMetadata $metadata)
    {
        $builder = new ClassMetadataBuilder($metadata);

        $builder->setTable('transaction')
            ->setCustomRepositoryClass('Mautic\TransactionBundle\Entity\TransactionRepository');

        $builder->createField('id', 'integer')
            ->makePrimaryKey()
            ->generatedValue()
            ->build();

        $builder->createField('hashId', 'string')
            ->columnName('hashId')
            ->columnDefinition('CHAR(50) NULL')
            ->build();
            
        $builder->createField('ip','string')
            ->columnName('ip')
            ->columnDefinition('CHAR(50) NULL')
            ->build();
        $builder->createField('transId','string')
            ->columnName('transId')
            ->columnDefinition('VARCHAR(100) NULL')
            ->build();
        $builder->createField('platform','string')
            ->columnName('platform')
            ->columnDefinition('CHAR(50) NULL')
            ->build();
        $builder->createField('uuid','string')
            ->columnName('uuid')
            ->columnDefinition('CHAR(100) NULL')
            ->build();
        $builder->createField('originalIAPTransId','string')
            ->columnName('originalIAPTransId')
            ->columnDefinition('CHAR(250) NULL')
            ->build();
        $builder->createField('iapTransId','string')
            ->columnName('iapTransId')
            ->columnDefinition('CHAR(100) NULL')
            ->build();
        $builder->createField('activeCode','string')
            ->columnName('activeCode')
            ->columnDefinition('CHAR(50) NULL')
            ->build();
        $builder->createField('status','integer')
            ->columnName('status')
            ->columnDefinition('int(2) NULL')
            ->build();
        $builder->createField('imei','string')
            ->columnName('imei')
            ->columnDefinition('CHAR(200) NULL')
            ->build();
        $builder->createField('amount','float')
            ->columnName('amount')
            ->columnDefinition('float NULL')
            ->build();
        $builder->createField('metadata','string')
            ->columnName('metadata')
            ->columnDefinition('CHAR(50) NULL')
            ->build();
        $builder->createField('subscriptionId','string')
            ->columnName('subscriptionId')
            ->columnDefinition('CHAR(50) NULL')
            ->build();
        $builder->createField('methodId','string')
            ->columnName('methodId')
            ->columnDefinition('CHAR(50) NULL')
            ->build();
        $builder->createField('paymentResult','string')
            ->columnName('paymentResult')
            ->columnDefinition('text NULL')
            ->build();
        $builder->createField('paymentChargeId','string')
            ->columnName('paymentChargeId')
            ->columnDefinition('CHAR(50) NULL')
            ->build();
        $builder->createField('planId','string')
            ->columnName('planId')
            ->columnDefinition('CHAR(50) NULL')
            ->build();
        $builder->createField('videoContentId','string')
            ->columnName('videoContentId')
            ->columnDefinition('CHAR(50) NULL')
            ->build();
        $builder->createField('macAddress','string')
            ->columnName('macAddress')
            ->columnDefinition('CHAR(50) NULL')
            ->build();
        $builder->createField('deviceModel','string')
            ->columnName('deviceModel')
            ->columnDefinition('CHAR(50) NULL')
            ->build();
        $builder->createField('startDate','string')
            ->columnName('startDate')
            ->columnDefinition('CHAR(50) NULL')
            ->build();
        $builder->createField('expireTime','string')
            ->columnName('expireTime')
            ->columnDefinition('CHAR(50) NULL')
            ->build();
        $builder->createField('paymentId','string')
            ->columnName('paymentId')
            ->columnDefinition('CHAR(50) NULL')
            ->build();
        $builder->createField('statusText','string')
            ->columnName('statusText')
            ->columnDefinition('CHAR(50) NULL')
            ->build();
        $builder->createField('createdAt','string')
            ->columnName('createdAt')
            ->columnDefinition('CHAR(150) NULL')
            ->build();
        $builder->createField('updatedAt','string')
            ->columnName('updatedAt')
            ->columnDefinition('CHAR(150) NULL')
            ->build();
        $builder->createField('channelType','string')
            ->columnName('channelType')
            ->columnDefinition('CHAR(50) NULL')
            ->build();
        $builder->createField('assetLogicalId','string')
            ->columnName('assetLogicalId')
            ->columnDefinition('CHAR(100) NULL')
            ->build();
        $builder->createField('profile','string')
            ->columnName('profile')
            ->columnDefinition('CHAR(50) NULL')
            ->build();
        $builder->createField('titleId','string')
            ->columnName('titleId')
            ->columnDefinition('CHAR(50) NULL')
            ->build();
        $builder->createField('lead_id','integer')
            ->columnName('lead_id')
            ->columnDefinition('int(13) NULL')
            ->build();
        $builder->createField('user_id','string')
            ->columnName('user_id')
            ->columnDefinition('char(100) NULL')
            ->build();
        
    }

     /**
     * @return Lead
     */
    public function getLead()
    {
        return $this->lead;
    }

    /**
     * @param Lead $lead
     */
    public function setLead(Lead $lead)
    {
        $this->lead = $lead;
    }

}
