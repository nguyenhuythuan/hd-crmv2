<?php


namespace Mautic\TransactionBundle\Entity;


use Mautic\CoreBundle\Entity\CommonRepository;
use Mautic\LeadBundle\Entity\Lead;
use Mautic\TransactionBundle\Entity\Transaction;

class TransactionRepository extends CommonRepository
{
    public function getEntities(array $args = [])
    {
        return parent::getEntities($args);
    }

    protected function addCatchAllWhereClause($q, $filter)
    {
        return $this->addStandardCatchAllWhereClause($q, $filter, [
            'e.uuid'
        ]);
    }

    /**
     * @param Transaction       $transaction
     * @return bool|string
     * @param array| null $filters
     */
    public function getTransactionCount(Transaction $transaction,  array $filters = null)
    {
        $query = $this->_em->getConnection()->createQueryBuilder()
            ->from(MAUTIC_TABLE_PREFIX.'transaction', 'al')
            ->select('count(*)');
        if (is_array($filters) && !empty($filters['search'])) {
            $query->Where('al.uuid like \'%'.$filters['search'].'%\'');
        }
        if (is_array($filters) && !empty($filters['includeEvents'])) {
            $includeList = "'".implode("','", $filters['includeEvents'])."'";
            $query->andWhere('al.uuid in ('.$includeList.')');
        }

        if (is_array($filters) && !empty($filters['excludeEvents'])) {
            $excludeList = "'".implode("','", $filters['excludeEvents'])."'";
            $query->andWhere('al.uuid not in ('.$excludeList.')');
        }
        return $query->execute()->fetchColumn();
    }

    /**
     * @param Transaction       $transaction
     * @param array|null $orderBy
     * @param array|null $filters
     * @param int $page
     * @param int $limit
     * @return mixed
     */
    public function getListTransaction(Transaction $transaction, array $filters = null, array $orderBy = null, $page=1, $limit = 25)
    {
        $query = $this->createQueryBuilder('al')
            ->select(
                'al.uuid,
                al.dateAdded,
                al.item_id,
                al.status,
                al.iap_trans_id,
                al.paid_price,
                al.device_model,
                al.platform,
                al.payment_method,
                al.expiry_date'
            );
//            ->where('al.id = :id')
//            ->setParameter('id', $transaction->getId());
        if (is_array($filters) && !empty($filters['search'])) {
            $query->andWhere('al.uuid like \'%'.$filters['search'].'%\'');
        }

        if (is_array($filters) && !empty($filters['includeEvents'])) {
            $includeList = "'".implode("','", $filters['includeEvents'])."'";
            $query->andWhere('al.uuid in ('.$includeList.')');
        }

        if (is_array($filters) && !empty($filters['excludeEvents'])) {
            $excludeList = "'".implode("','", $filters['excludeEvents'])."'";
            $query->andWhere('al.uuid not in ('.$excludeList.')');
        }

        if (0 === $page) {
            $page = 1;
        }
        $query->setFirstResult(($page - 1) * $limit);
        $query->setMaxResults($limit);
        return $query->getQuery()->getArrayResult();
    }
    public function getTransactionByLeadIds(int $leadIds)
    {
        return $this->findBy([
                'lead_id' => $leadIds
        ]);
    }
    
    public function findAllByUserId($userId)
    {
        $result = $this->_em->getConnection()->createQueryBuilder()
            ->select('*')
            ->from(MAUTIC_TABLE_PREFIX.'transaction', 'l')
            ->where("user_id = :userId")
            ->setParameter('userId', $userId)
            ->orderBy('unix_timestamp(createdAt)','DESC')
            ->execute();
        return $result->fetchAll();
    }
    public function countByFilter($filter = [],$ortherFilter = [])
    {
        $result = $this->_em->getConnection()->createQueryBuilder()
            ->select('count(t.id) as num')
            ->from(MAUTIC_TABLE_PREFIX.'transaction', 't');
        if($ortherFilter && $ortherFilter != null){
            foreach($ortherFilter as $k => $v){
                if($k == 0 ){
                    $result->where("{$v}");
                }else{
                    $result->andwhere("{$v}");
                }
            }
        }

        if($filter && $filter != null){
            foreach($filter as $key => $value){
                $result->andwhere("{$key} = :{$key}")
                    ->setParameter($key, $value);
            }
        }
        
        $result = $result->execute()->fetch();
        return (int)$result['num'];
    }
    public function getStatusDefine(){
        return (object)[
            "NEW" => 1, // mới khởi tạo
            "LINKED" => 2, // liên kết với subs thành công
            "PAYING" => 3, // đang tiến hành thanh toán
            "PAID" => 4, // Đã thanh toán thành công
            "ERROR" => -1, // Lỗi
            "EXPIRED" => -2, // Hết hạn
        ];
    }
}
