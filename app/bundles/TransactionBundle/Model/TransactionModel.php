<?php


namespace Mautic\TransactionBundle\Model;


use Mautic\CoreBundle\Model\FormModel;
use Mautic\TransactionBundle\Entity\Transaction;

/**
 * Class TransactionModel
 * {@inheritdoc}
 */
class TransactionModel extends FormModel
{
    /**
     * {@inheritdoc}
     *
     * @return \Mautic\TransactionBundle\Entity\TransactionRepository
     */
    public function getRepository()
    {
        $repo = $this->em->getRepository('MauticTransactionBundle:Transaction');
        return $repo;
    }
    public function getEntity($id = null)
    {
        if ($id === null) {
            return new Transaction();
        }

        $entity = parent::getEntity($id);
        var_dump();
        return $entity;
    }
}