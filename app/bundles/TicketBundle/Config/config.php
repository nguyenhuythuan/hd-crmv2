<?php

/*
 * @copyright   2014 Mautic Contributors. All rights reserved
 * @author      Mautic
 *
 * @link        http://mautic.org
 *
 * @license     GNU/GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
 */

return [
    'routes' => [
        'main' => [
            'mautic_ticket_index' => [
                'path'       => '/tickets/{page}',
                'controller' => 'MauticTicketBundle:Ticket:index',
            ],
            'mautic_ticket_action' => [
                'path'       => '/tickets/{objectAction}/{objectId}',
                'controller' => 'MauticTicketBundle:Ticket:execute',
            ],
        ],
    ],
    'menu' => [
        'main' => [
            'mautic.tickets.menu.index' => [
                'route'     => 'mautic_ticket_index',
                'iconClass' => 'fa fa-credit-card',
                'access'    => ['ticket:tickets:view'],
                'priority'  => 95,
            ],
        ],
    ],
    'services' => [
        'repository' => [
            'mautic.ticket.repository.ticket' => [
                'class' => Doctrine\ORM\EntityRepository::class,
                'factory' => ['@doctrine.orm.entity_manager', 'getRepository'],
                'arguments' => [
                    'Mautic\TicketBundle\Entity\Ticket'
                ]
            ]
        ],
        'events' => [
            'mautic.ticket.search.subscriber' => [
                'class'     => 'Mautic\TicketBundle\EventListener\SearchSubscriber',
                'arguments' => [],
            ],
        ],
        'models' => [
            'mautic.ticket.model.ticket' => [
                'class'     => 'Mautic\TicketBundle\Model\TicketModel',
                'arguments' => []
            ],
        ],
    ]
];
