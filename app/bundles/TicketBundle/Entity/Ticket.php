<?php

namespace Mautic\TicketBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Mautic\CoreBundle\Doctrine\Mapping\ClassMetadataBuilder;
use Mautic\CoreBundle\Entity\FormEntity;

/**
 * Ticket
 */
class Ticket  extends FormEntity
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var
     */
    private $uuid;

    /**
     * @return mixed
     */
    public function getUuid()
    {
        return $this->uuid;
    }

    /**
     * @param mixed $uuid
     */
    public function setUuid($uuid)
    {
        $this->uuid = $uuid;
    }

    /**
     * @var int
     *
     * @ORM\Column(name="type", type="integer", nullable=true)
     */
    private $type;

    /**
     * @var int
     *
     * @ORM\Column(name="status", type="integer", nullable=true)
     */
    private $status;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="string", length=255)
     */
    private $description;

    /**
     * @var string
     *
     * @ORM\Column(name="platform", type="string", length=255)
     */
    private $platform;

    /**
     * @var int
     *
     * @ORM\Column(name="assignedGroupId", type="int")
     */
    private $assignedGroupId;

    /**
     * @var string
     * @ORM\Column(name="account", type="string", length=255)
     */
    private $account;

    /**
     * @var string
     * @ORM\Column(name="createdByName", type="string", length=255)
     */
    private $createdByName;

    /**
     * @var int
     * @ORM\Column(name="categoryId", type="int")
     */
    private $categoryId;

    /**
     * @return int
     */
    public function getCategoryId()
    {
        return $this->categoryId;
    }

    /**
     * @param int $categoryId
     */
    public function setCategoryId($categoryId)
    {
        $this->categoryId = $categoryId;
    }

    /**
     * @return int
     */
    public function getAssignedGroupId()
    {
        return $this->assignedGroupId;
    }

    /**
     * @param int $assignedGroupId
     */
    public function setAssignedGroupId($assignedGroupId)
    {
        $this->assignedGroupId = $assignedGroupId;
    }

    /**
     * @return string
     */
    public function getAccount()
    {
        return $this->account;
    }

    /**
     * @param string $account
     */
    public function setAccount($account)
    {
        $this->account = $account;
    }

    /**
     * @return string
     */
    public function getCreatedByName()
    {
        return $this->createdByName;
    }

    /**
     * @param string $createdByName
     */
    public function setCreatedByName($createdByName)
    {
        $this->createdByName = $createdByName;
    }

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set type
     *
     * @param integer $type
     *
     * @return Ticket
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type
     *
     * @return int
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set status
     *
     * @param integer $status
     *
     * @return Ticket
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return int
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set description
     *
     * @param string $description
     *
     * @return Ticket
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set platform
     *
     * @param string $platform
     *
     * @return Ticket
     */
    public function setPlatform($platform)
    {
        $this->platform = $platform;

        return $this;
    }

    /**
     * Get platform
     *
     * @return string
     */
    public function getPlatform()
    {
        return $this->platform;
    }

    public static function loadMetadata(ORM\ClassMetadata $metadata)
    {
        $builder = new ClassMetadataBuilder($metadata);

        $builder->setTable('ticket')
            ->setCustomRepositoryClass('Mautic\TicketBundle\Entity\TicketRepository')
        ;

        $builder->createField('id', 'integer')
            ->makePrimaryKey()
            ->generatedValue()
            ->build();

        $builder->createField('uuid', 'string')
            ->columnName('uuid')
            ->columnDefinition('CHAR(36)')
            ->build();

        $builder->createField('type', 'integer')
            ->columnName('type')
            ->build();

        $builder->createField('status', 'integer')
            ->columnName('status')
            ->build();

        $builder->createField('description', 'string')
            ->columnName('description')
            ->nullable()
            ->build();

        $builder->createField('platform', 'string')
            ->columnName('platform')
            ->nullable()
            ->build();

        $builder->createField('assignedGroupId', 'string')
            ->columnName('assignedGroupId')
            ->nullable()
            ->build();

        $builder->createField('account', 'string')
            ->columnName('account')
            ->nullable()
            ->build();

        $builder->createField('createdByName', 'string')
            ->columnName('createdByName')
            ->nullable()
            ->build();

        $builder->createField('categoryId', 'string')
            ->columnName('categoryId')
            ->build();
    }
}

