<?php

/*
 * @copyright   2014 Mautic Contributors. All rights reserved
 * @author      Mautic
 *
 * @link        http://mautic.org
 *
 * @license     GNU/GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
 */
if ($tmpl == 'index') {
    $view->extend('MauticTicketBundle:Ticket:index.html.php');
}
?>
<?php if (count($items)): ?>
    <div class="table-responsive page-list">
        <table class="table table-hover table-striped table-bordered ticket-list" id="ticketTable">
            <thead>
            <tr>
                <?php
//                echo $view->render(
//                    'MauticCoreBundle:Helper:tableheader.html.php',
//                    [
//                        'checkall'        => 'true',
//                        'target'          => '#ticketTable',
//                        'routeBase'       => 'ticket',
//                        'templateButtons' => [
//                            'delete' => $permissions['ticket:tickets:delete'],
//                        ],
//                    ]
//                );
                echo $view->render(
                    'MauticCoreBundle:Helper:tableheader.html.php',
                    [
                        'sessionVar' => 'ticket',
                        'orderBy'    => 'e.id',
                        'text'       => '#',
                    ]
                );
                echo $view->render(
                    'MauticCoreBundle:Helper:tableheader.html.php',
                    [
                        'sessionVar' => 'ticket',
                        'text'       => 'mautic.core.id',
                        'class'      => 'visible-md visible-lg col-ticket-id',
                    ]
                );

                echo $view->render(
                    'MauticCoreBundle:Helper:tableheader.html.php',
                    [
                        'sessionVar' => 'ticket',
                        'orderBy'    => 'e.createdByName',
                        'text'       => 'mautic.core.ticket.created.name',
                        'class'      => 'col-ticket-name',
                        'default'    => true,
                    ]
                );

                echo $view->render(
                    'MauticCoreBundle:Helper:tableheader.html.php',
                    [
                        'sessionVar' => 'ticket',
                        'text'       => 'mautic.core.ticket.creation.date',
                        'class'      => 'visible-md visible-lg col-ticket-date',
                    ]
                );

                echo $view->render(
                    'MauticCoreBundle:Helper:tableheader.html.php',
                    [
                        'sessionVar' => 'ticket',
                        'orderBy'    => 'e.type',
                        'text'       => 'mautic.core.ticket.type',
                        'class'      => 'visible-md visible-lg col-ticket-type',
                    ]
                );

                echo $view->render(
                    'MauticCoreBundle:Helper:tableheader.html.php',
                    [
                        'sessionVar' => 'ticket',
                        'orderBy'    => 'e.status',
                        'text'       => 'mautic.core.ticket.status',
                        'class'      => 'visible-md visible-lg col-ticket-status',
                    ]
                );

                echo $view->render(
                    'MauticCoreBundle:Helper:tableheader.html.php',
                    [
                        'sessionVar' => 'ticket',
                        'orderBy'    => 'e.account',
                        'text'       => 'mautic.core.ticket.account',
                        'class'      => 'visible-md visible-lg col-ticket-account',
                    ]
                );

                echo $view->render(
                    'MauticCoreBundle:Helper:tableheader.html.php',
                    [
                        'sessionVar' => 'ticket',
                        'orderBy'    => 'e.platform',
                        'text'       => 'mautic.core.ticket.platform',
                        'class'      => 'visible-md visible-lg col-ticket-platform',
                    ]
                );

                echo $view->render(
                    'MauticCoreBundle:Helper:tableheader.html.php',
                    [
                        'sessionVar' => 'ticket',
                        'orderBy'    => 'c.categoryId',
                        'text'       => 'mautic.core.category',
                        'class'      => 'visible-md visible-lg col-ticket-category',
                    ]
                );
                ?>
            </tr>
            </thead>
            <tbody>
            <?php foreach ($items as $index => $item): ?>
                <tr>
<!--                    <td>-->
<!--                        --><?php
//                        echo $view->render(
//                            'MauticCoreBundle:Helper:list_actions.html.php',
//                            [
//                                'item'            => $item,
//                                'templateButtons' => [
//                                    'edit'   => $permissions['ticket:tickets:edit'],
//                                ],
//                                'routeBase' => 'ticket',
//                            ]
//                        );
//                        ?>
<!--                    </td>-->
                    <td class="visible-md visible-lg">
                        <?php echo $index; ?>
                    </td>
                    <td class="visible-md visible-lg">
                        <?php echo $item->getUuid(); ?>
                    </td>
                    <td>
                        <?php echo $item->getCreatedByName(); ?>
                    </td>
                    <td class="timeline-timestamp">
                        <p class="fs-12 dark-sm">
                            <small>
                                <?php echo $view['date']->toText($item->getDateAdded(), 'local', 'Y-m-d H:i:s', true); ?>
                            </small>
                        </p>
                    </td>
                    <td>
                        <?php if ($item->getType() ===1):?>
                            <span class="label label-success text-center"<?php echo $style; ?>>
                                <?php echo 'SUPPORT'; ?>
                            </span>
                        <?php endif?>
                        <?php if ($item->getType() ===2):?>
                            <span class="label label-danger text-center"<?php echo $style; ?>>
                                <?php echo 'BUG'; ?>
                            </span>
                        <?php endif?>
                        <?php if ($item->getType() ===3):?>
                            <span class="label label-primary text-center"<?php echo $style; ?>>
                                <?php echo 'GUIDE'; ?>
                            </span>
                        <?php endif?>
                        <?php if ($item->getType() ===4):?>
                            <span class="label label-warning text-center"<?php echo $style; ?>>
                                <?php echo 'OTHER'; ?>
                            </span>
                        <?php endif?>
                    </td>
                    <td>
                        <?php if ($item->getStatus() ===6):?>
                            <span class="label label-success"<?php echo $style; ?>>
                                <?php echo 'DONE'; ?>
                            </span>
                        <?php endif?>
                        <?php if ($item->getStatus() ===5):?>
                            <span class="label label-primary"<?php echo $style; ?>>
                                <?php echo 'FINISHED'; ?>
                            </span>
                        <?php endif?>
                        <?php if ($item->getStatus() ===4):?>
                            <span class="label label-info"<?php echo $style; ?>>
                                <?php echo 'REOPEN'; ?>
                            </span>
                        <?php endif?>
                        <?php if ($item->getStatus() ===3):?>
                            <span class="label label-danger"<?php echo $style; ?>>
                                <?php echo 'REJECTED'; ?>
                            </span>
                        <?php endif?>
                        <?php if ($item->getStatus() ===2):?>
                            <span class="label label-warning"<?php echo $style; ?>>
                                <?php echo 'IN PROGRESS'; ?>
                            </span>
                        <?php endif?>
                        <?php if ($item->getStatus() ===1):?>
                            <span class="label label-primary"<?php echo $style; ?>>
                                <?php echo 'OPEN'; ?>
                            </span>
                        <?php endif?>
                    </td>

                    <td>
                        <?php echo $item->getAccount(); ?>
                    </td>

                    <td>
                        <?php echo $item->getPlatform(); ?>
                    </td>
                    <td class="visible-md visible-lg">
                        <?php echo $item->getCategoryId(); ?>
                    </td>
                </tr>
            <?php endforeach; ?>
            </tbody>
        </table>
    </div>
    <div class="panel-footer">
        <?php echo $view->render(
            'MauticCoreBundle:Helper:pagination.html.php',
            [
                'totalItems' => count($items),
                'page'       => $page,
                'limit'      => $limit,
                'menuLinkId' => 'mautic_ticket_index',
                'baseUrl'    => $view['router']->generate('mautic_ticket_index'),
                'sessionVar' => 'ticket',
            ]
        ); ?>
    </div>
<?php else: ?>
    <?php echo $view->render(
        'MauticCoreBundle:Helper:noresults.html.php',
        ['tip' => 'mautic.ticket.action.noresults.tip']
    ); ?>
<?php endif; ?>
