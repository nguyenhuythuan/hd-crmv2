<?php

/*
 * @copyright   2014 Mautic Contributors. All rights reserved
 * @author      Mautic
 *
 * @link        http://mautic.org
 *
 * @license     GNU/GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
 */

namespace Mautic\TicketBundle\Controller;

use Mautic\CoreBundle\Controller\FormController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class TicketController.
 */
class TicketController extends FormController
{
    /**
     * @param int $page
     *
     * @return JsonResponse|Response
     */
    public function indexAction($page = 1)
    {
        $permissions = $this->get('mautic.security')->isGranted(
            [
                'ticket:tickets:view',
                'ticket:tickets:create',
                'ticket:tickets:edit',
                'ticket:tickets:delete',
                'ticket:tickets:publish',
            ],
            'RETURN_ARRAY'
        );

        $this->setListFilters();
        if (!$permissions['ticket:tickets:view']) {
            return $this->accessDenied();
        }
        //set limits
        $limit = $this->get('session')->get(
            'mautic.ticket.limit',
            $this->coreParametersHelper->getParameter('default_pagelimit')
        );
        $start = ($page === 1) ? 0 : (($page - 1) * $limit);
        if ($start < 0) {
            $start = 0;
        }

        $search = $this->request->get('search', $this->get('session')->get('mautic.ticket.filter', ''));
        $this->get('session')->set('mautic.ticket.filter', $search);
        $filter     = ['string' => $search, 'force' => []];
        $orderBy    = $this->get('session')->get('mautic.ticket.orderby', 'e.uuid');
        $orderByDir = $this->get('session')->get('mautic.ticket.orderbydir', 'ASC');
        $tickets = $this->getModel('ticket')->getEntities(
            [
                'start'      => $start,
                'limit'      => $limit,
                'filter'     => $filter,
                'orderBy'    => $orderBy,
                'orderByDir' => $orderByDir,
            ]
        );
//        foreach ($tickets as $item) {
//            $b = $this->getModel('ticket')->getEntity($item->getId());
//            $a = $b->getDateAdded();
//        }
//        $aa = array_push($tickets, $a);
//        var_dump($aa);die;
        $count = count($tickets);
        if ($count && $count < ($start + 1)) {
            $lastPage = ($count === 1) ? 1 : (ceil($count / $limit)) ?: 1;
            $this->get('session')->set('mautic.ticket.page', $lastPage);
            $returnUrl = $this->generateUrl('mautic_ticket_index', ['page' => $lastPage]);

            return $this->postActionRedirect(
                [
                    'returnUrl'       => $returnUrl,
                    'viewParameters'  => ['page' => $lastPage],
                    'contentTemplate' => 'MauticTicketBundle:Ticket:index',
                    'passthroughVars' => [
                        'activeLink'    => '#mautic_ticket_index',
                        'mauticContent' => 'ticket',
                    ],
                ]
            );
        }
        $this->get('session')->set('mautic.ticket.page', $page);
        $tmpl = $this->request->isXmlHttpRequest() ? $this->request->get('tmpl', 'index') : 'index';
        return $this->delegateView(
            [
                'viewParameters' => [
                    'searchValue' => $search,
                    'items'       => $tickets,
                    'page'        => $page,
                    'limit'       => $limit,
                    'permissions' => $permissions,
                    'tmpl'        => $tmpl,
                ],
                'contentTemplate' => "MauticTicketBundle:Ticket:list.html.php",
                'passthroughVars' => [
                    'activeLink'    => '#mautic_ticket_index',
                    'mauticContent' => 'ticket',
                    'route'         => $this->generateUrl('mautic_ticket_index', ['page' => $page]),
                ],
            ]
        );
    }
}
