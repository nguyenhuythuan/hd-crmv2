<?php

/*
 * @copyright   2014 Mautic Contributors. All rights reserved
 * @author      Mautic
 *
 * @link        http://mautic.org
 *
 * @license     GNU/GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
 */

namespace Mautic\TicketBundle\Model;

use Mautic\CoreBundle\Model\FormModel;

/**
 * Class TicketModel.
 */
class TicketModel extends FormModel
{
    public function getRepository()
    {
        $repo = $this->em->getRepository('MauticTicketBundle:Ticket');

        return $repo;
    }
    /**
     * {@inheritdoc}
     */
    public function getPermissionBase()
    {
        return 'ticket:tickets';
    }
}
