<?php
$event = [
    "eventSubs" => [
        "addSubs" => "subs.add",
        "updateSubs" => "subs.update"
    ],
    "eventTrans" => [
        "addTrans" => "trans.add",
        "updateTrans" => "trans.update"
    ],
    "eventUsers" => [
        "addUsers" => "users.add",
        "updateUsers" => "users.update"
    ]
];
$container->setParameter('mautic.event.config.info', $event);