<?php

return [
    'routes' => [
        'main'  => [
            
        ]
    ],
    'services' => [
        'repositories' => [
            'mautic.method.repository.methodRepo' => [
                'class'     => Doctrine\ORM\EntityRepository::class,
                'factory'   => ['@doctrine.orm.entity_manager', 'getRepository'],
                'arguments' => [
                    'MauticPlugin\MethodBundle\Entity\Method',
                ],
            ]
        ],

        'models' => [
            'mautic.method.model.method' => [
                'class'     => 'MauticPlugin\MethodBundle\Model\MethodModel',
                'arguments' => []
            ]
         ],

        'events' => [
        ],
        'orther' => [
            'mautic.method.service.methodService' => [
                'class'     => 'MauticPlugin\MethodBundle\Service\MethodService',
                'arguments' => [
                    'service_container',
                ],
            ]
        ],

    ],

];
