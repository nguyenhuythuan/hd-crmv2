<?php
namespace MauticPlugin\MethodBundle\Entity;

use Doctrine\DBAL\Types\DecimalType;
use Doctrine\ORM\Mapping as ORM;
use Mautic\CampaignBundle\Executioner\Scheduler\Mode\DateTime;
use Mautic\CoreBundle\Doctrine\Mapping\ClassMetadataBuilder;
use Mautic\CoreBundle\Entity\FormEntity;
use \Mautic\LeadBundle\Entity\Lead;

class Method extends FormEntity
{
    const STATUS = [
    ];

    /**
     * @var int
     */
    private $id;
    private $methodId;
    private $userId;
    private $channelId;
    private $metadata;
    private $channelType;
    private $createdAt;
    private $updatedAt;
    private $status;
    private $action;


    /** 
     * ================== get function ==========
    */
    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    public function setMethodId($value){
        $this->methodId = $value;
    }
    public function setUserId($value){
        $this->userId = $value;
    }
    public function setChannelId($value){
        $this->channelId = $value;
    }
    public function setMetadata($value){
        $this->metadata = $value;
    }
    public function setChannelType($value){
        $this->channelType = $value;
    }
    public function setCreatedAt($value){
        $this->createdAt = $value;
    }
    public function setUpdatedAt($value){
        $this->updatedAt = $value;
    }
    public function setStatus($value){
        $this->status = $value;
    }
    public function setAction($value){
        $this->action = $value;
    }
    

    /** 
     * ================== get function ==========
    */
    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    public function getMethodId(){
        return $this->methodId;
    }
    public function getUserId(){
        return $this->userId;
    }
    public function getChannelId(){
        return $this->channelId;
    }
    public function getMetadata(){
        return $this->metadata;
    }
    public function getChannelType(){
        return $this->channelType;
    }
    public function getCreatedAt(){
        return $this->createdAt;
    }
    public function getUpdatedAt(){
        return $this->updatedAt;
    }
    public function getStatus(){
        return $this->status;
    }
    public function getAction(){
        return $this->action;
    }



    /**
     * chưa edit type data 
     */
    public static function loadMetadata(ORM\ClassMetadata $metadata)
    {
        $builder = new ClassMetadataBuilder($metadata);

        $builder->setTable('method')
            ->setCustomRepositoryClass('MauticPlugin\MethodBundle\Entity\MethodRepository');

        $builder->createField('id', 'integer')
            ->makePrimaryKey()
            ->generatedValue()
            ->build();
        $builder->createField('methodId','string')
            ->columnName('methodId')
            ->columnDefinition('CHAR(100) NULL')
            ->build();
        $builder->createField('userId','string')
            ->columnName('userId')
            ->columnDefinition('CHAR(100) NULL')
            ->build();
        $builder->createField('channelId','string')
            ->columnName('channelId')
            ->columnDefinition('CHAR(100) NULL')
            ->build();
        $builder->createField('metadata','string')
            ->columnName('metadata')
            ->columnDefinition('text NULL')
            ->build();
        $builder->createField('channelType','string')
            ->columnName('channelType')
            ->columnDefinition('CHAR(100) NULL')
            ->build();
        $builder->createField('createdAt','string')
            ->columnName('createdAt')
            ->columnDefinition('CHAR(100) NULL')
            ->build();
        $builder->createField('updatedAt','string')
            ->columnName('updatedAt')
            ->columnDefinition('CHAR(100) NULL')
            ->build();
        $builder->createField('status','string')
            ->columnName('status')
            ->columnDefinition('CHAR(100) NULL')
            ->build();
        $builder->createField('action','string')
            ->columnName('action')
            ->columnDefinition('CHAR(100) NULL')
            ->build();
    }

}
