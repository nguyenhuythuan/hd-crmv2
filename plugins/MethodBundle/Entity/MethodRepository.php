<?php
namespace MauticPlugin\MethodBundle\Entity;


use Mautic\CoreBundle\Entity\CommonRepository;
use Mautic\LeadBundle\Entity\Lead;

class MethodRepository extends CommonRepository
{
    public function getEntities(array $args = [])
    {
        return parent::getEntities($args);
    }

    protected function addCatchAllWhereClause($q, $filter)
    {
        return $this->addStandardCatchAllWhereClause($q, $filter, [
            'e.uuid'
        ]);
    }
    public function findAllByUserId($userId)
    {
        $result = $this->_em->getConnection()->createQueryBuilder()
            ->select('userId,methodId,channelId,channelType,createdAt,unix_timestamp(createdAt) as ctAt,updatedAt,status')
            ->from(MAUTIC_TABLE_PREFIX.'method', 'm')
            ->where("userId = :userId")
            ->orderBy('unix_timestamp(m.createdAt)','DESC')
            ->setParameter('userId', $userId)
            ->execute();
        // $data = $result->fetchAll();
        $data = $result->fetchAll();
        return $data;
    }

}
