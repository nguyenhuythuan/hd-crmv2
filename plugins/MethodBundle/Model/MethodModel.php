<?php


namespace MauticPlugin\MethodBundle\Model;


use Mautic\CoreBundle\Model\FormModel;
use MauticPlugin\MethodBundle\Entity\Method;
use MauticPlugin\MethodBundle\Entity\MethodRepository;

/**
 * Class TransactionModel
 * {@inheritdoc}
 */
class MethodModel extends FormModel
{
    /**
     * {@inheritdoc}
     *
     * @return \MauticPlugin\MethodBundle\Entity\MethodRepository
     */
    public function getRepository()
    {
        $repo = $this->em->getRepository('MethodBundle:Method');
        return $repo;
    }
    public function getEntity($id = null)
    {
        if ($id === null) {
            return new Method();
        }

        $entity = parent::getEntity($id);
        return $entity;
    }
}