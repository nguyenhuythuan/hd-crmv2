<?php
namespace MauticPlugin\MethodBundle\Service;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Validator\Constraints\Date;
use Mautic\LeadBundle\Model\Lead;


class MethodService
{
    private $action = '';
    public $channel;
    public $connection;
    public $container;
    protected $subsModel;
    const CREATE_SUBS = 'CREATE_SUBS';
    const UPDATE_SUBS = 'UPDATE_SUBS';
    

    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
    }

    
    public function getRecord($whereData = []){
        $this->subsModel = null;
        return true;
    }
    public function changeData($data){
        if(is_array($data)){
            $data = (object)$data;
        }

        try{
            $em = $this->container->get('doctrine')->getEntityManager();
            $reponstory = $em->getRepository('MethodBundle:Method');
            $item = $reponstory->findOneBy([
                'methodId' => isset($data->id)?$data->id:'',
            ]);
            $model = $this->container->get('mautic.method.model.method');
            $entity = $model->getEntity();
            if($item != null){
                $entity = $model->getEntity($item->getId());
            }
            // $entity = $em->getEntity('SubscriptionBundle:Subscription');
            $entity->setMethodId(isset($data->id)?$data->id:'');
            $entity->setUserId(isset($data->userId)?$data->userId:'');
            $entity->setChannelId(isset($data->channelId)?$data->channelId:'');
            $entity->setMetaData(isset($data->metaData)?$data->metaData:'');
            $createdAt =null;
            if(isset($data->createdAt)){
                $createdAt = $this->convertDateToFomartDb($data->createdAt);
            }
            $updatedAt =null;
            if(isset($data->updatedAt)){
                $updatedAt = $this->convertDateToFomartDb($data->updatedAt);
            }

            $entity->setCreatedAt($createdAt);
            $entity->setUpdatedAt($updatedAt);
            $entity->setChannelType($data->channelPayment);
            $entity->setStatus($data->status);
            $entity->setAction($this->action);

            $em->persist($entity);
            $em->flush();
            if($entity->getId()){
                return $entity;
            }
            return false;

        }catch(\Exception $e){
            print_r($e->getMessage());
            return false;
        }
    }

    public function upChangeData($data){
        
        $action = $this->action = $data->action;
        return $this->changeData($data->data);
        return true;
    }
    
    public function convertDateToFomartDb($time = ''){
        // exit;
        if($time == ''){
            return false;
        }
        if(is_numeric($time)){
            return date('Y-m-d H:i:s',$time);
        }else{
            $date = date_create($time);
            return date_format('Y-m-d H:i:s',$time);
        }
    }
}