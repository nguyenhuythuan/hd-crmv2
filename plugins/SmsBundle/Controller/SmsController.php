<?php

namespace MauticPlugin\SmsBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Mautic\CoreBundle\Controller\FormController;

class SmsController extends FormController
{
    public $prefix = 'FimplusBundle:FCampaign';
    public function indexAction()
    {
        return $this->delegateView(
            [
                'viewParameters' => [
                    'tmpl'    => $this->request->isXmlHttpRequest() ? $this->request->get('tmpl', 'index') : 'index',
                    'form'   => '',
                    'entity'   => '',
                    // 'actions' => $actions['actions'],
                ],
                'contentTemplate' => $this->prefix.':index.html.php',
                'passthroughVars' => [
                    'activeLink'    => '#mautic_sms_list_index',
                    'route'         => $this->generateUrl('mautic_sms_list_index', ['page' => $page]),
                ]
            ]
        );
        
        //return $this->render('SmsBundle:Default:index.html.twig');
    }

    public function addAction()
    {
        return $this->delegateView(
            [
                'viewParameters' => [
                    'tmpl'    => $this->request->isXmlHttpRequest() ? $this->request->get('tmpl', 'index') : 'index',
                    'form'   => '',
                    'entity'   => '',
                    // 'actions' => $actions['actions'],
                ],
                'contentTemplate' => $this->prefix.':form.html.php',
                'passthroughVars' => [
                    'activeLink'    => '#mautic_sms_list_add',
                    'route'         => $this->generateUrl('mautic_sms_list_add'),
                ]
            ]
        );
        //return $this->render('SmsBundle:Default:index.html.twig');
    }
}
