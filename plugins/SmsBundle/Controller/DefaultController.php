<?php

namespace MauticPlugin\SmsBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller
{
    public function indexAction()
    {
        return $this->render('SmsBundle:Default:index.html.twig');
    }
}
