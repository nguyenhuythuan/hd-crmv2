<?php
$view->extend('MauticCoreBundle:Default:content.html.php');
$view['slots']->set('mauticContent', 'promotion');

$header = ($entity->getId()) ?
    $view['translator']->trans('mautic.promotion.menu.edit',
        ['%name%' => $view['translator']->trans($entity->getName())]) :
    $view['translator']->trans('mautic.promotion.menu.new');
$view['slots']->set('headerTitle', $header);

echo $view['form']->start($form);
?>
    <!-- start: box layout -->
    <div class="box-layout">
        <!-- container -->
        <div class="col-md-9 bg-auto height-auto bdr-r">
            <div class="row">
                <div class="col-md-6">
                    <div class="pa-md">
                    <?php echo $view['form']->row($form['promotion_name']);?>
                    <?php echo $view['form']->row($form['promotion_type']); ?>
                    <?php echo $view['form']->row($form['promotion_value']); ?>
                    <?php echo $view['form']->row($form['apply_timing']); ?>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="pa-md">
                    <?php echo $view['form']->row($form['promotion_item_list']); ?>
                    <?php echo $view['form']->row($form['waiting_apply_duration']); ?>
                    <?php echo $view['form']->row($form['force_expire_date']); ?>
                    <?php echo $view['form']->row($form['tvod_apply_condition']); ?>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-3 bg-white height-auto">
            <div class="pr-lg pl-lg pt-md pb-md">
            </div>
        </div>
    </div>
<?php echo $view['form']->end($form); ?>