<?php

/*
 * @copyright   2014 Mautic Contributors. All rights reserved
 * @author      Mautic
 *
 * @link        http://mautic.org
 *
 * @license     GNU/GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
 */

namespace MauticPlugin\FimplusBundle\Model;

use Doctrine\DBAL\Query\Expression\ExpressionBuilder;
use Mautic\CoreBundle\Model\FormModel as CommonFormModel;
use Symfony\Component\HttpKernel\Exception\MethodNotAllowedHttpException;
use MauticPlugin\FimplusBundle\Event\PromotionBuilderEvent;
use MauticPlugin\FimplusBundle\FimplusEvents;
use MauticPlugin\FimplusBundle\Entity\FCampaign;
use Proxies\__CG__\MauticPlugin\FimplusBundle\Entity\PromotionConditionDetail;

/**
 * Class PromotionConditionDetailModel.
 */
class PromotionConditionDetailModel extends CommonFormModel
{
    public function getRepository()
    {
        return $this->em->getRepository('FimplusBundle:PromotionConditionDetail');
    }
    public function createForm($entity, $formFactory, $action = null, $options = [])
    {
        if (!$entity instanceof PromotionConditionDetail) {
            throw new MethodNotAllowedHttpException(['Promotion Condition Detail']);
        }
        if (!empty($action)) {
            $options['action'] = $action;
        }
        // if (empty($options['campaignActions'])) {
        //     $options['campaignActions'] = $this->getCampaignActions();
        // }
        $form = $formFactory->create('promotionconditiondetail', $entity, $options);
        return  $form;
    }
    public function getEntity($id = null)
    {
        if ($id === null) {
            return new PromotionConditionDetail();
        }

        $entity = parent::getEntity($id);

        return $entity;
    }
      
}
