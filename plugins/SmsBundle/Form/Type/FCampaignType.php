<?php

/*
 * @copyright   2014 Mautic Contributors. All rights reserved
 * @author      Mautic
 *
 * @link        http://mautic.org
 *
 * @license     GNU/GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
 */

namespace MauticPlugin\FimplusBundle\Form\Type;

use Mautic\CoreBundle\Factory\MauticFactory;
use Mautic\CoreBundle\Form\EventListener\CleanFormSubscriber;
use Mautic\CoreBundle\Form\EventListener\FormExitSubscriber;
use MauticPlugin\FimplusBundle\Entity\Campaign;
use MauticPlugin\FimplusBundle\Entity\Promotion;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\Valid;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use MauticPlugin\FimplusBundle\Entity\PromotionConditionDetail;

/**
 * Class CampaignType.
 */
class FCampaignType extends AbstractType
{
    /**
     * @var \Mautic\CoreBundle\Security\Permissions\CorePermissions
     */
    private $security;

    /**
     * @var \Symfony\Bundle\FrameworkBundle\Translation\Translator
     */
    private $translator;

    /**
     * @param MauticFactory $factory
     */
    public function __construct(MauticFactory $factory)
    {
        $this->translator = $factory->getTranslator();
        $this->security   = $factory->getSecurity();
    }

    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        // $builder->addEventSubscriber(new CleanFormSubscriber(['description' => 'html']));
        // $builder->addEventSubscriber(new FormExitSubscriber('campaign', $options));
        $optionsType = $options['data']->getTypeOption();
        $optionsApplySameType = $options['data']->getPreventApplySameTypeOption();
        $optionsDistributor = $options['data']->getDistributorOption();
        $promotion = $options['promotion'];
        $builder->add(
            'name',
            'text',
            [
                'label'      => 'mautic.fimplus.campaign.name',
                'label_attr' => ['class' => 'control-label'],
                'attr'       => ['class' => 'form-control'],
                'property_path' => 'name',
             ]);
        $builder->add(
            'description',
            'textarea', 
            [
                'label'      => 'mautic.fimplus.campaign.description',
                'label_attr' => [
                    'class' => 'control-label',
                ], 
                'attr' => [
                    'class' => 'form-control',
                ], 
                'property_path' => 'description',
                'mapped'     => true,
            ]);
        $builder->add(
            'priority',
            'number',
            [
                'label'      => 'mautic.fimplus.campaign.priority',
                'label_attr' => ['class' => 'control-label'],
                'attr'       => ['class' => 'form-control not-chosen'],
                'mapped'     => true,
                'property_path' => 'priority',
            ]
        );
        $builder->add(
            'type',
            'choice',
            [
                'choices'    => $optionsType,
                'label'      => 'mautic.fimplus.campaign.type',
                'label_attr' => ['class' => 'control-label'],
                'attr'       => ['class' => 'form-control not-chosen'],
                'mapped'     => true,
                'empty_value' => 'mautic.fimplus.campaign.type.emptyvalue',
                'property_path' => 'type',
            ]
        );
        $builder->add(
            'apply_times',
            'number', 
            [
                'label'      => 'mautic.fimplus.campaign.applytimes',
                'label_attr' => [
                    'class' => 'control-label',
                ], 'attr' => [
                    'class' => 'form-control',
                ],
                'property_path' => 'applyTimes',
                'mapped'     => true,
            ]);
        $builder->add(
            'prevent_apply_same_type',
            'choice',
            [
                'choices'    => $optionsApplySameType,
                'label'      => 'mautic.fimplus.campaign.applysametype',
                'label_attr' => ['class' => 'control-label'],
                'attr'       => ['class' => 'form-control not-chosen'],
                'mapped'     => true,
                'empty_value' => 'mautic.fimplus.campaign.type.emptyvalue',
                'property_path' => 'preventApplySameType',
            ]
        );
    
        $builder->add(
            'start_date',
            'datetime',
            [
                'widget'     => 'single_text',
                'label'      => 'mautic.fimplus.campaign.startdate',
                'label_attr' => ['class' => 'control-label'],
                'attr'       => [
                    'class'       => 'form-control',
                    'data-toggle' => 'date',
                    'autocomplete' => 'off',
                ],
                'format'   => 'yyyy-MM-dd',
                'property_path' => 'startDate',
                'input' => 'string',
                'mapped'     => true,
            ]);
    
        $builder->add(
            'end_date',
            'datetime',
            [
                'widget'     => 'single_text',
                'label'      => 'mautic.fimplus.campaign.enddate',
                'label_attr' => ['class' => 'control-label'],
                'attr'       => [
                    'class'       => 'form-control',
                    'data-toggle' => 'date',
                    'autocomplete' => 'off',
                ],
                'format'   => 'yyyy-MM-dd',
                'property_path' => 'endDate',
                'input' => 'string',
                'mapped'     => true,
            ]);
        $builder->add(
            'distributor',
            'choice',
            [
                'choices'    => $optionsDistributor,
                'label'      => 'mautic.fimplus.campaign.distributor',
                'label_attr' => ['class' => 'control-label'],
                'attr'       => ['class' => 'form-control not-chosen'],
                'mapped'     => true,
                'empty_value' => 'mautic.fimplus.campaign.distributor.emptyvalue',
                'property_path' => 'distributor',
            ]
        );
        $builder->add(
            'promotionConditionDetail',
            CollectionType::class,
            [
                'entry_type' => PromotionConditionDetailType::class,
                'entry_options' => [
                    'attr' => ['class' => 'condition-item'],
                    'options'=> $options,
                    'label' => false
                ],
                'label' => false,
                'allow_add' => true,
                'allow_delete' => true,
                'by_reference' => false,
                'cascade_validation' => true,
                'constraints' => array(new Valid()),
                'mapped' => true,
            ]
         );
        // $builder->add('promotion', PromotionType::class);
        $builder->add(
            'promotion',
            'choice',
            [
                'choices'    => $promotion,
                'label'      => 'mautic.fimplus.campaign.promotion',
                'label_attr' => ['class' => 'control-label'],
                'attr'       => ['class' => 'form-control not-chosen'],
                'mapped'     => true,
                'empty_value' => 'mautic.fimplus.campaign.promotion.emptyvalue',
                'property_path' => 'promotion',
                'multiple'  => true,
            ]
        );
        $builder->add('isPublished', 'yesno_button_group');
        $builder->add('buttons', 'form_buttons');
        if (!empty($options['action'])) {
            $builder->setAction($options['action']);
        }
     
    }

    /**
     * {@inheritdoc}
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults([
            'data_class' => 'MauticPlugin\FimplusBundle\Entity\FCampaign',
            'options' => null, //this will register the 'options', otherwise we get an error [The option "options" does not exist
            'promotion' => null
        ]);
        $resolver->setRequired('options');
        $resolver->setRequired('promotion');
         // you can also define the allowed types, allowed values and
        // any other feature supported by the OptionsResolver component
    }

    /**
     * {@inheritdoc}
     */
    public function getName()
    {
        return 'fcampaign';
    }
}
