<?php

/*
 * @copyright   2014 Mautic Contributors. All rights reserved
 * @author      Mautic
 *
 * @link        http://mautic.org
 *
 * @license     GNU/GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
 */

namespace MauticPlugin\FimplusBundle\Form\Type;

use Mautic\CoreBundle\Factory\MauticFactory;
use Mautic\CoreBundle\Form\EventListener\CleanFormSubscriber;
use Mautic\CoreBundle\Form\EventListener\FormExitSubscriber;
use MauticPlugin\FimplusBundle\Entity\Promotion;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Symfony\Component\Validator\Constraints\NotBlank;

/**
 * Class PromotionType.
 */
class PromotionType extends AbstractType
{
    /**
     * @var \Mautic\CoreBundle\Security\Permissions\CorePermissions
     */
    private $security;

    /**
     * @var \Symfony\Bundle\FrameworkBundle\Translation\Translator
     */
    private $translator;

    /**
     * @param MauticFactory $factory
     */
    public function __construct(MauticFactory $factory)
    {
        $this->translator = $factory->getTranslator();
        $this->security   = $factory->getSecurity();
    }

    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->addEventSubscriber(new CleanFormSubscriber(['description' => 'html']));
        $builder->addEventSubscriber(new FormExitSubscriber('promotion', $options));

        $optionsType = $options['data']->getPromotionTypeOption();
        $optionsApplyTiming = $options['data']->getApplyTimingOption();
        $optionsTVODCondition = $options['data']->getTVODConditionOption();
        $builder->add(
            'promotion_name',
            'text', 
            [
                'label'      => 'mautic.fimplus.promotion.name',
                'label_attr' => [
                    'class' => 'control-label',
                ], 
                'attr' => [
                    'class' => 'form-control',
                ], 
                'property_path' => 'promotionName',
            ]);
        $builder->add(
            'promotion_type',
            'choice',
            [
                'choices'    => $optionsType,
                'label'      => 'mautic.fimplus.promotion.type',
                'label_attr' => ['class' => 'control-label'],
                'attr'       => ['class' => 'form-control not-chosen'],
                'mapped'     => true,
                'empty_value' => 'mautic.fimplus.promotiontype.emptyvalue',
                'property_path' => 'promotionType',
            ]
        );
        $builder->add(
            'promotion_value',
            'text', 
            [
                'label'      => 'mautic.fimplus.promotion.value',
                'label_attr' => [
                    'class' => 'control-label',
                ], 'attr' => [
                    'class' => 'form-control',
                ],
                'property_path' => 'promotionValue',
            ]);
        $builder->add(
            'promotion_item_list',
            'text', 
            [
                'label'      => 'mautic.fimplus.promotion.itemlist',
                'label_attr' => [
                    'class' => 'control-label',
                ], 'attr' => [
                    'class' => 'form-control',
                ], 
                'required'   => false,
                'property_path' => 'promotionItemList',
            ]);
        $builder->add(
            'apply_timing',
            'choice',
            [
                'choices'    => $optionsApplyTiming,
                'label'      => 'mautic.fimplus.promotion.applytiming',
                'label_attr' => ['class' => 'control-label'],
                'attr'       => ['class' => 'form-control not-chosen'],
                'mapped'     => true,
                'empty_value' => 'mautic.fimplus.applytiming.emptyvalue',
                'property_path' => 'applyTiming',
            ]
        );
        
        $builder->add(
            'waiting_apply_duration', 
            'number', 
            [
                'label'      => 'mautic.fimplus.promotion.waitingduration',
                'label_attr' => [
                    'class' => 'control-label',
                ], 'attr' => [
                    'class' => 'form-control',
                ], 
                'required'   => false,
                'property_path' => 'waitingApplyDuration',
            ]);
        $builder->add(
            'force_expire_date',
            'datetime',
            [
                'widget'     => 'single_text',
                'label'      => 'mautic.fimplus.promotion.forceexpire',
                'label_attr' => ['class' => 'control-label'],
                'attr'       => [
                    'class'       => 'form-control',
                    'data-toggle' => 'datetime',
                    'autocomplete' => 'off',
                ],
                'format'   => 'yyyy-MM-dd HH:mm',
                'required' => false,
                'property_path' => 'forceExpireDate',
                'input' => 'string',
            ]);
        $builder->add(
            'tvod_apply_condition',
            'choice',
            [
                'choices'    => $optionsTVODCondition,
                'label'      => 'mautic.fimplus.promotion.tvodcondition',
                'label_attr' => ['class' => 'control-label'],
                'attr'       => ['class' => 'form-control not-chosen'],
                'required'   => false,
                'mapped'     => true,
                'empty_value' => 'mautic.fimplus.tvodcondition.emptyvalue',
                'property_path' => 'tvodApplyCondition',
            ]
        );
        $builder->add('buttons', 'form_buttons');
        if (!empty($options['action'])) {
            $builder->setAction($options['action']);
        }
     
    }

    /**
     * {@inheritdoc}
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults([
            'data_class' => 'MauticPlugin\FimplusBundle\Entity\Promotion',
        ]);

        //$resolver->setOptional(['promotionActions', 'actionType']);
        $resolver->setRequired(['promotionActions']);

        $resolver->setOptional(['actionType']);
    }

    /**
     * {@inheritdoc}
     */
    public function getName()
    {
        return 'promotion';
    }
}
