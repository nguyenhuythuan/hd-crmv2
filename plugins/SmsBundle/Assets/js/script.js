mQuery(document).ready(function () {
    var $collectionHolder;
    // setup an "add a tag" link
   // var $addConditionButton = mQuery('<button type="button" class="add_condition_link">Add condition</button>');
   // var $newLinkLi = mQuery('<li></li>').append($addConditionButton);
    // Get the ul that holds the collection of tags
    $collectionHolder = mQuery('.condition-app .app-items');
    
    // add the "add a tag" anchor and li to the tags ul
    //$collectionHolder.append($newLinkLi);
    
    // count the current form inputs we have (e.g. 2), use that as the new
    // index when inserting a new item (e.g. 2)
    $collectionHolder.data('index', $collectionHolder.find('.condition-item').length);
    
    mQuery('.add_condition_link').click(function(){
        // add a new tag form (see next code block)
        addConditionForm($collectionHolder);
    });
    removeCondition();
    mQuery('#fcampaign_promotion').chosen();
    
   
    function addConditionForm($collectionHolder) {
        // Get the data-prototype explained earlier
        var prototype = $collectionHolder.find('#fcampaign_promotionConditionDetail').data('prototype');

        // get the new index
        var index = $collectionHolder.data('index');

        var newForm = prototype;
        // You need this only if you didn't set 'label' => false in your tags field in TaskType
        // Replace '__name__label__' in the prototype's HTML to
        // instead be a number based on how many items we have
        // newForm = newForm.replace(/__name__label__/g, index);

        // Replace '__name__' in the prototype's HTML to
        // instead be a number based on how many items we have
        newForm = newForm.replace(/__name__label__/g, '');
        newForm = newForm.replace(/__name__/g, index);

        // increase the index with one for the next item
        $collectionHolder.data('index', index + 1);

        // Display the form in the page in an li, before the "Add a tag" link li
        $collectionHolder.append(newForm);
        removeCondition();
        //$newLinkLi.before($newFormLi);
    }
    function removeCondition() {
        mQuery('.remove-condition').click(function(){
            // remove the li for the tag form
            mQuery(this).parent().parent().parent().remove();
        });
    }
});
