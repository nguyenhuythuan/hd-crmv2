<?php

/*
 * @copyright   2019 Fimplus. All rights reserved
 * @author      longnv
 *
 * @link        http://fimplus.vn
 *
 * @license     no license
 */

return [
    'name'        => 'RabbitmqBundle',
    'description' => 'RabbitmqBundle Producer',
    'version'     => '1.0',
    'author'      => 'datnt',
    'routes' => [
        'main' => [
            'mautic_sms_list_index' => [
                'path'       => '/fimplus_sms/{page}',
                'controller' => 'SmsBundle:Sms:index',
            ],
            'mautic_sms_list_add' => [
                'path'       => '/fimplus_sms/add',
                'controller' => 'SmsBundle:Sms:add',
            ],
        ],
    ],

    'services' => [
        'orther' => [
            'mautic.sms.service.transCons' => [
                'class'     => MauticPlugin\RabbitmqBundle\Services\TransCons::class,
                'arguments' => [
                    'service_container',
                ],
            ],
        ]
    ],

    'menu' => [
        'main' => [
            'mautic.sms' => [
                'iconClass' => 'fa-video-camera',
                // 'route'    => 'mautic_fimplus_index',
                // 'access'   => 'plugin:fimplus:items:view',
                //'parent'   => 'mautic.core.channels',
                'priority' => 10,
            ],
            'mautic.sms.list' => [
                'route'    => 'mautic_sms_list_index',
                // 'access'   => 'plugin:fimplus:items:view',
                'parent'   => 'mautic.sms',
                'priority' => 10,
            ],
            'mautic.sms.add' => [
                'route'    => 'mautic_sms_list_add',
                // 'access'   => 'plugin:fimplus:items:view',
                'parent'   => 'mautic.sms',
                'priority' => 10,
            ]
        ],
    ],

    'categories' => [
        'plugin:sms' => 'mautic.sms',
    ],
];
