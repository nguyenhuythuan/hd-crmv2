Mautic.fcampaignOnLoad = function(container, response){
    mQuery(document).ready(function() {
        var listFilmTemp = {};
        var $campaignType = mQuery('#fcampaign_type');
        // When campaignType gets selected ...
        $campaignType.change(function() {
            // ... retrieve the corresponding form.
            var $form = mQuery(this).closest('form');
            // Simulate form data, but only include the selected campaignType value.
            var data = {};
            data[$campaignType.attr('name')] = $campaignType.val();
            data['getForm'] = true;
            // Submit data via AJAX to the form's action path.
            mQuery.ajax({
                url: $form.attr('action'),
                type: $form.attr('method'),
                data: data,
                success: function(html) {
                    var append = mQuery(html.newContent).find('#fcampaign_campaignActiveCodeDetail');
                    if (append.length == 0)
                        append = mQuery(html.newContent).find('#fcampaign_promotionConditionDetail');
                    mQuery('#campaignDetail').html('').append(append);
                }
            });
        });
    });
}