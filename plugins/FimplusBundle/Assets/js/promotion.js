Mautic.promotionOnLoad = function (container, response) {
    var listFilmTemp = mQuery('#promotion_customOption').val();
    if (listFilmTemp) {
        listFilmTemp = JSON.parse(listFilmTemp);
    }
    mQuery(document).ready(function () {
        mQuery('select.chosen').chosen();
        var $promotionType = mQuery('#promotion_promotion_type');
        tvodChange();
        // When promotionType gets selected ...
        getListFilmAjax();
        $promotionType.change(function () {
            // ... retrieve the corresponding form.
            var $form = mQuery(this).closest('form');
            // Simulate form data, but only include the selected promotionType value.
            var data = {};
            data[$promotionType.attr('name')] = $promotionType.val();
            data['actionAjax'] = 'loadField';
            data['fieldName'] = 'promotionType';
            // Submit data via AJAX to the form's action path.
            mQuery.ajax({
                url: $form.attr('action'),
                type: $form.attr('method'),
                data: data,
                success: function (html) {
                    var appendSvod = mQuery(html.newContent).find('.container-svod .content-svod');
                    var appendTvod = mQuery(html.newContent).find('.container-tvod .content-tvod');
                    mQuery('.container-svod').html('').append(appendSvod);
                    mQuery('.container-tvod').html('').append(appendTvod);
                    mQuery('select.chosen').chosen();
                    tvodChange();
                }
            });
        });
        function tvodChange() {
            var $tvodType = mQuery('input[name="promotion[tvodType]"]');
            $tvodType.change(function () {
                var $form = mQuery(this).closest('form');
                // Simulate form data, but only include the selected promotionType value.
                var data = {};
                data[$tvodType.attr('name')] = mQuery(this).val();
                data['actionAjax'] = 'loadField';
                data['fieldName'] = 'tvodType';
                data['promotion_type'] = $form.find('#promotion_promotion_type').val();
                // Submit data via AJAX to the form's action path.
                mQuery.ajax({
                    url: $form.attr('action'),
                    type: $form.attr('method'),
                    data: data,
                    success: function (html) {
                        var film = mQuery(html.newContent).find('.container-film .content-film');
                        mQuery('.container-film').html('').append(film);
                        getListFilmAjax();
                    }
                });

            });
        }
        function getListFilmAjax() {
            var getFilmUrl = '/s/fcampaign/listfilm';
            mQuery('#promotion_film').select2({
                ajax: {
                    url: getFilmUrl,
                    type: 'POST',
                    dataType: 'json',
                    delay: 250,
                    data: function (params) {
                        var query = {
                            search: params.term,
                        }
                        // Query parameters will be ?search=[term]&type=public
                        return query;
                    },
                    processResults: function (data) {
                        var datatToSend = '';
                        listFilmTemp = mQuery.extend(listFilmTemp, listFilmTemp, data);
                        datatToSend = JSON.stringify(listFilmTemp);

                        mQuery('#promotion_customOption').val(datatToSend);
                        return {
                            results: mQuery.map(data, function (item, key) {
                                return {
                                    text: item,
                                    id: key
                                }
                            })
                        };
                    },
                    placeholder: 'Search for a film',
                    minimumInputLength: 2,
                }
            });
        }
    });
}
