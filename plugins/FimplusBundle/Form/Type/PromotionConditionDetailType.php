<?php

/*
 * @copyright   2014 Mautic Contributors. All rights reserved
 * @author      Mautic
 *
 * @link        http://mautic.org
 *
 * @license     GNU/GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
 */

namespace MauticPlugin\FimplusBundle\Form\Type;

use Mautic\CoreBundle\Factory\MauticFactory;
use Mautic\CoreBundle\Form\EventListener\CleanFormSubscriber;
use Mautic\CoreBundle\Form\EventListener\FormExitSubscriber;
use MauticPlugin\FimplusBundle\Entity\PromotionConditionDetail;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Symfony\Component\Validator\Constraints\NotBlank;

/**
 * Class CampaignType.
 */
class PromotionConditionDetailType extends AbstractType
{
    /**
     * @var \Mautic\CoreBundle\Security\Permissions\CorePermissions
     */
    private $security;

    /**
     * @var \Symfony\Bundle\FrameworkBundle\Translation\Translator
     */
    private $translator;

    /**
     * @param MauticFactory $factory
     */
    public function __construct(MauticFactory $factory)
    {
        $this->translator = $factory->getTranslator();
        $this->security   = $factory->getSecurity();
    }

    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        // $builder->addEventSubscriber(new CleanFormSubscriber(['description' => 'html']));
        // $builder->addEventSubscriber(new FormExitSubscriber('campaign', $options));
        //$data = $options['options']['data'];
        $promotionConditionDetail = New PromotionConditionDetail();
        $optionsTypeValue = $promotionConditionDetail->getTypeValueOption();

        $optionsFact = $promotionConditionDetail->getFactOption();
        $optionsOperator = $promotionConditionDetail->getOperatorOption();
        $optionsOrders = $promotionConditionDetail->getOrdersOption();
       
        $builder->add(
            'fact',
            'choice',
            [
                'choices'    => $optionsFact,
                'label'      => 'mautic.fimplus.promotion.condition.detail.fact',
                'label_attr' => ['class' => 'control-label'],
                'attr'       => ['class' => 'form-control not-chosen'],
                'mapped'     => true,
                'empty_value' => 'mautic.fimplus.promotion.condition.detail.fact.emptyvalue',
                'property_path' => 'fact',
            ]
        );
       
        $builder->add(
            'operator',
            'choice',
            [
                'choices'    => $optionsOperator,
                'label'      => 'mautic.fimplus.promotion.condition.detail.operator',
                'label_attr' => ['class' => 'control-label'],
                'attr'       => ['class' => 'form-control not-chosen'],
                'mapped'     => true,
                'empty_value' => 'mautic.fimplus.promotion.condition.detail.operator.emptyvalue',
                'property_path' => 'operator',
            ]
        );
        $builder->add(
            'value',
            'textarea',
            [
                'label'      => 'mautic.fimplus.promotion.condition.detail.value',
                'label_attr' => ['class' => 'control-label'],
                'attr'       => ['class' => 'form-control'],
                'property_path' => 'value',
             ]);
        
        $builder->add(
            'type_value',
            'choice',
            [
                'choices'    => $optionsTypeValue,
                'label'      => 'mautic.fimplus.promotion.condition.detail.typevalue',
                'label_attr' => ['class' => 'control-label'],
                'attr'       => ['class' => 'form-control not-chosen'],
                'mapped'     => true,
                'empty_value' => 'mautic.fimplus.promotion.condition.detail.typevalue.emptyvalue',
                'property_path' => 'typeValue',
            ]
        );
        $builder->add(
            'orders',
            'choice',
            [
                'choices'    => $optionsOrders,
                'label'      => 'mautic.fimplus.promotion.condition.detail.orders',
                'label_attr' => ['class' => 'control-label'],
                'attr'       => ['class' => 'form-control not-chosen'],
                'mapped'     => true,
                'empty_value' => 'mautic.fimplus.promotion.condition.detail.orders.emptyvalue',
                'property_path' => 'orders',
            ]
        );
        // $builder->add('Remove', 'button', [
        //     'attr' => ['class' => 'remove-condition btn btn-danger'],
        // ]);
    }
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        
        $resolver->setDefaults([
            'data_class' => 'MauticPlugin\FimplusBundle\Entity\PromotionConditionDetail',
            'options' => null, //this will register the 'options', otherwise we get an error [The option "options" does not exist
        ]);
        $resolver->setRequired('options');
    }
   
    /**
     * {@inheritdoc}
     */
    public function getName()
    {
        return 'promotionConditionDetail';
    }
}
