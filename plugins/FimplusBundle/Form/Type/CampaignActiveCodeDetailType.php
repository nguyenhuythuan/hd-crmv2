<?php

/*
 * @copyright   2014 Mautic Contributors. All rights reserved
 * @author      Mautic
 *
 * @link        http://mautic.org
 *
 * @license     GNU/GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
 */

namespace MauticPlugin\FimplusBundle\Form\Type;

use Mautic\CoreBundle\Factory\MauticFactory;
use Proxies\__CG__\MauticPlugin\FimplusBundle\Entity\CampaignActiveCodeDetail;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Symfony\Component\Form\CallbackTransformer;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * Class CampaignType.
 */
class CampaignActiveCodeDetailType extends AbstractType
{
    /**
     * @var \Mautic\CoreBundle\Security\Permissions\CorePermissions
     */
    private $security;

    /**
     * @var \Symfony\Bundle\FrameworkBundle\Translation\Translator
     */
    private $translator;

    /**
     * @param MauticFactory $factory
     */
    public function __construct(MauticFactory $factory)
    {
        $this->translator = $factory->getTranslator();
        $this->security   = $factory->getSecurity();
    }

    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $activeCodeDetail = new CampaignActiveCodeDetail();
        $optionsPartner = $activeCodeDetail->getPartnerOption();
        $optionsCodeType = $activeCodeDetail->getCodeTypeOption();
        $optionsPackage = ['package 1', 'package 2', 'package 4', 'package 6'];

        $builder->add(
            'partner',
            'choice',
            [
                'choices'    => $optionsPartner,
                'label'      => 'Partner',
                'label_attr' => ['class' => 'control-label'],
                'attr'       => ['class' => 'form-control not-chosen'],
                'mapped'     => true,
                'empty_value' => '-- Select partner --',
                'property_path' => 'partner',
            ]
        );

        // $builder->add(
        //     'code_type',
        //     'choice',
        //     [
        //         'choices'    => $optionsCodeType,
        //         'label'      => 'Code type',
        //         'label_attr' => ['class' => 'control-label'],
        //         'attr'       => ['class' => 'form-control'],
        //         'mapped'     => true,
        //         'property_path' => 'codeType',
        //         'expanded' => true,
        //         'choice_attr' => function ($key, $val, $index) {
        //             $disabled = false;
        //             if (in_array($index, [1, 2]))
        //                 $disabled = true;
        //             return $disabled ? ['disabled' => 'disabled'] : [];
        //         },
        //         'data' => 3,
        //     ]
        // );
        $builder->add(
            'code_value',
            'integer',
            [
                'label'      => 'Quantity of Codes',
                'label_attr' => ['class' => 'control-label'],
                'attr'       => ['class' => 'form-control'],
                'mapped'     => true,
                'property_path' => 'codeValue',
            ]
        );

        // $builder->add(
        //     'is_percent',
        //     'checkbox',
        //     // CheckboxType::class,
        //     [
        //         'label' => 'Is percent',
        //         'attr'  => [
        //             'tooltip' => 'Is percent value',
        //         ],
        //         'mapped' => true,
        //         'property_path' => 'isPercent',
        //         'required' => false,
        //         'data' => false
        //     ]
        // );
        // $builder->add(
        //     'max_discount_value',
        //     'integer',
        //     [
        //         'label'      => 'Max discount value',
        //         'label_attr' => ['class' => 'control-label'],
        //         'attr'       => ['class' => 'form-control'],
        //         'mapped'     => true,
        //         'property_path' => 'maxDiscountValue',
        //     ]
        // );
        $builder->add(
            'original_price',
            'integer',
            [
                'label'      => 'Unit price',
                'label_attr' => ['class' => 'control-label'],
                'attr'       => ['class' => 'form-control'],
                'mapped'     => true,
                'property_path' => 'originalPrice',
            ]
        );
        $builder->add(
            'apply_success_message',
            'text',
            [
                'label'      => 'Apply success message',
                'label_attr' => ['class' => 'control-label'],
                'attr'       => ['class' => 'form-control'],
                'mapped'     => true,
                'property_path' => 'applySuccessMessage',
                'required'  => false,
            ]
        );
        $builder->add(
            'config_note',
            'textarea',
            [
                'label'      => 'Config note',
                'label_attr' => ['class' => 'control-label'],
                'attr'       => ['class' => 'form-control'],
                'mapped'     => true,
                'property_path' => 'configNote',
                'required'  =>false
            ]
        );
    }
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {

        $resolver->setDefaults([
            'data_class' => 'MauticPlugin\FimplusBundle\Entity\CampaignActiveCodeDetail',
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function getName()
    {
        return 'campaignActiveCodeDetail';
    }
}
