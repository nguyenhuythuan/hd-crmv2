<?php

/*
 * @copyright   2014 Mautic Contributors. All rights reserved
 * @author      Mautic
 *
 * @link        http://mautic.org
 *
 * @license     GNU/GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
 */

namespace MauticPlugin\FimplusBundle\Form\Type;

use Mautic\CoreBundle\Factory\MauticFactory;
use MauticPlugin\FimplusBundle\Entity\BussinessInvoice;
use MauticPlugin\FimplusBundle\Entity\CampaignActiveCodeDetail;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

/**
 * Class CampaignType.
 */
class BussinessInvoiceType extends AbstractType
{
    /**
     * @var \Mautic\CoreBundle\Security\Permissions\CorePermissions
     */
    private $security;

    /**
     * @var \Symfony\Bundle\FrameworkBundle\Translation\Translator
     */
    private $translator;
    private $factory;
    /**
     * @param MauticFactory $factory
     */
    public function __construct(MauticFactory $factory)
    {
        $this->translator = $factory->getTranslator();
        $this->security   = $factory->getSecurity();
        $this->factory = $factory;
    }

    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $capaignActive = $this->factory->getModel('fimplus.bussinessInvoice')->getActiveCodeCampaign(CampaignActiveCodeDetail::STATUS['APPROVED']);
        $camPaignOptions = [];
        foreach($capaignActive as $campaign){
            $camPaignOptions[$campaign->getId()] = $campaign->getName();
        }
        $builder->add(
            'campaign_id',
            'choice',
            [
                'choices'    => $camPaignOptions,
                'label'      => 'Campaign',
                'label_attr' => ['class' => 'control-label'],
                'attr'       => ['class' => 'form-control chosen'],
                'mapped'     => true,
                'empty_value' => '-- Select Campaign --',
                'property_path' => 'campaignId',
            ]
        );
       
        $builder->add(
            'quantity',
            'integer',
            [
                'label'      => 'Quantity',
                'label_attr' => ['class' => 'control-label'],
                'attr'       => ['class' => 'form-control not-chosen'],
                'mapped'     => true,
                'property_path' => 'quantity',
            ]
        );
        $builder->add(
            'start_date',
            'datetime',
            [
                'widget'     => 'single_text',
                'label'      => 'Start date',
                'label_attr' => ['class' => 'control-label'],
                'attr'       => [
                    'class'       => 'form-control',
                    'data-toggle' => 'date',
                    'autocomplete' => 'off',
                ],
                'format'   => 'yyyy-MM-dd',
                'property_path' => 'startDate',
                'input' => 'string',
                'mapped'     => true,
            ]
        );

        $builder->add(
            'end_date',
            'datetime',
            [
                'widget'     => 'single_text',
                'label'      => 'End date',
                'label_attr' => ['class' => 'control-label'],
                'attr'       => [
                    'class'       => 'form-control',
                    'data-toggle' => 'date',
                    'autocomplete' => 'off',
                ],
                'format'   => 'yyyy-MM-dd',
                'property_path' => 'endDate',
                'input' => 'string',
                'mapped'     => true,
            ]
        );
        // $builder->add('Remove', 'button', [
        //     'attr' => ['class' => 'remove-condition btn btn-danger'],
        // ]);
        $builder->add('buttons', 'form_buttons');
    }
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        
        $resolver->setDefaults([
            'data_class' => 'MauticPlugin\FimplusBundle\Entity\BussinessInvoice',
            // 'options' => null, //this will register the 'options', otherwise we get an error [The option "options" does not exist
        ]);
        // $resolver->setRequired('options');
    }
    public function getName()
    {
        return 'bussiness_invoice';
    }
  
}
