<?php

/*
 * @copyright   2014 Mautic Contributors. All rights reserved
 * @author      Mautic
 *
 * @link        http://mautic.org
 *
 * @license     GNU/GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
 */

namespace MauticPlugin\FimplusBundle\Form\Type;

use Mautic\CoreBundle\Factory\MauticFactory;
use Mautic\CoreBundle\Form\EventListener\CleanFormSubscriber;
use Mautic\CoreBundle\Form\EventListener\FormExitSubscriber;
use MauticPlugin\FimplusBundle\Entity\Promotion;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Validator\Constraints\NotBlank;

/**
 * Class PromotionType.
 */
class PromotionType extends AbstractType
{
    /**
     * @var \Mautic\CoreBundle\Security\Permissions\CorePermissions
     */
    private $security;

    /**
     * @var \Symfony\Bundle\FrameworkBundle\Translation\Translator
     */
    private $translator;

    /**
     * @param MauticFactory $factory
     */
    public function __construct(MauticFactory $factory)
    {
        $this->translator = $factory->getTranslator();
        $this->security   = $factory->getSecurity();
    }

    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->addEventSubscriber(new CleanFormSubscriber(['description' => 'html']));
        $builder->addEventSubscriber(new FormExitSubscriber('promotion', $options));

        $optionsType = $options['data']->getPromotionTypeOption();
        $optionsTvodType = $options['data']->getTvodTypeOption();
        $optionsApplyTiming = $options['data']->getApplyTimingOption();
        $optionsTVODCondition = $options['data']->getTVODConditionOption();
        $optionsPackage = ['package 1', 'package 2', 'package 4', 'package 6'];
        $builder->add(
            'promotion_name',
            'text',
            [
                'label'      => 'Add-on Name',
                'label_attr' => [
                    'class' => 'control-label',
                ],
                'attr' => [
                    'class' => 'form-control',
                ],
                'property_path' => 'promotionName',
            ]
        );
        $builder->add(
            'promotion_type',
            'choice',
            [
                'choices'    => $optionsType,
                'label'      => 'Add-on Type',
                'label_attr' => ['class' => 'control-label'],
                'attr'       => ['class' => 'form-control chosen'],
                'mapped'     => true,
                'empty_value' => 'mautic.fimplus.promotiontype.emptyvalue',
                'property_path' => 'promotionType',
            ]
        );
        $builder->add(
            'promotion_value',
            'text',
            [
                'label'      => 'Add-on Value',
                'label_attr' => [
                    'class' => 'control-label',
                ], 'attr' => [
                    'class' => 'form-control',
                ],
                'property_path' => 'promotionValue',
            ]
        );
        $builder->add(
            'apply_timing',
            'choice',
            [
                'choices'    => $optionsApplyTiming,
                'label'      => 'mautic.fimplus.promotion.applytiming',
                'label_attr' => ['class' => 'control-label'],
                'attr'       => ['class' => 'form-control not-chosen'],
                'mapped'     => true,
                'empty_value' => 'mautic.fimplus.applytiming.emptyvalue',
                'property_path' => 'applyTiming',
                'expanded'  => true
            ]
        );

        $builder->add(
            'waiting_apply_duration',
            'integer',
            [
                'label'      => 'mautic.fimplus.promotion.waitingduration',
                'label_attr' => [
                    'class' => 'control-label',
                ], 'attr' => [
                    'class' => 'form-control',
                ],
                'required'   => false,
                'property_path' => 'waitingApplyDuration',
            ]
        );
        $builder->add(
            'force_expire_date',
            'datetime',
            [
                'widget'     => 'single_text',
                'label'      => 'mautic.fimplus.promotion.forceexpire',
                'label_attr' => ['class' => 'control-label'],
                'attr'       => [
                    'class'       => 'form-control',
                    'data-toggle' => 'date',
                    'autocomplete' => 'off',
                ],
                'format'   => 'yyyy-MM-dd',
                'required' => false,
                'property_path' => 'forceExpireDate',
                'input' => 'string',
            ]
        );

        $builder->add('customOption', 'hidden', [
            'data' => [],
            'mapped' => false
        ]);
        $formModifier = function (FormInterface $form, $promotionType = '', $listFilm = [], $data = []) use ($options, $optionsTVODCondition, $optionsTvodType) {
            $showFilm = false;
            if (is_array($data)) {
                if (isset($data['tvodType']) && $data['tvodType'] == Promotion::TVOD_TYPE['LISTED']) {
                    $showFilm = true;
                }
            } elseif (is_object($data)) {
                if ($data->getTvodType() == Promotion::TVOD_TYPE['LISTED']) {
                    $showFilm = true;
                }
            }
            if ($showFilm) {
                $form->add(
                    'film',
                    'choice',
                    [
                        'choices'    => $listFilm,
                        'label'      => 'Film',
                        'label_attr' => ['class' => 'control-label'],
                        'attr'       => ['class' => 'form-control film-list not-chosen'],
                        'mapped'     => true,
                        'empty_value' => '-- Select film --',
                        'property_path' => 'film',
                        'multiple'  => true,
                    ]
                );
            }
            switch ($promotionType) {
                case Promotion::PROMOTION_TYPE['TVOD_GIFT']:
                    $form->add(
                        'tvod_apply_condition',
                        'choice',
                        [
                            'choices'    => $optionsTVODCondition,
                            'label'      => 'mautic.fimplus.promotion.tvodcondition',
                            'label_attr' => ['class' => 'control-label'],
                            'attr'       => ['class' => 'form-control chosen'],
                            'required'   => false,
                            'mapped'     => true,
                            'empty_value' => 'mautic.fimplus.tvodcondition.emptyvalue',
                            'property_path' => 'tvodApplyCondition',
                        ]
                    );
                    $form->add(
                        'tvodType',
                        'choice',
                        [
                            'choices'    => $optionsTvodType,
                            'label'      => 'Apply for',
                            'label_attr' => ['class' => 'control-label'],
                            'attr'       => ['class' => 'form-control chosen'],
                            'mapped'     => true,
                            'empty_value' => '-- Select type --',
                            'expanded'   => true,
                            'constraints' => [
                                new NotBlank(
                                    [
                                        'message' => 'mautic.core.value.required',
                                    ]
                                ),
                            ],
                        ]
                    );
                    break;
                case Promotion::PROMOTION_TYPE['SVOD_GIFT']:
                    $form->add(
                        'package',
                        'choice',
                        [
                            'choices'    => $options['listPackage'],
                            'label'      => 'Package',
                            'label_attr' => ['class' => 'control-label'],
                            'attr'       => ['class' => 'form-control chosen'],
                            'mapped'     => true,
                            'empty_value' => '-- Select package --',
                            'property_path' => 'package',
                        ]
                    );
                    break;
            }
        };
        $builder->addEventListener(
            FormEvents::PRE_SET_DATA,
            function (FormEvent $event) use ($formModifier) {
                // this would be your entity, i.e. SportMeetup
                $data = $event->getData();
                $formModifier($event->getForm(), $data->getPromotionType(), [], $data);
            }
        );
        $builder->addEventListener(
            FormEvents::PRE_SUBMIT,
            function (FormEvent $event) use ($formModifier) {
                $data = $event->getData();
                if (isset($data['promotion_type'])) {
                    $listFilm =  [];

                    if (!empty($data['customOption'])) {
                        $options = json_decode($data['customOption']);
                        foreach ($options as $key => $value) {
                            $listFilm[$key] = $value;
                        }
                    }
                    $formModifier(
                        $event->getForm(),
                        $data['promotion_type'],
                        $listFilm,
                        $data
                    );
                }
            }
        );

        $builder->add('buttons', 'form_buttons');
        if (!empty($options['action'])) {
            $builder->setAction($options['action']);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults([
            'data_class' => 'MauticPlugin\FimplusBundle\Entity\Promotion',
        ]);

        $resolver->setRequired(['promotionActions']);

        $resolver->setOptional(['actionType']);
        $resolver->setOptional(['listPackage']);
    }

    /**
     * {@inheritdoc}
     */
    public function getName()
    {
        return 'promotion';
    }
}
