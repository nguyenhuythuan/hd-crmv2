<?php

/*
 * @copyright   2014 Mautic Contributors. All rights reserved
 * @author      Mautic
 *
 * @link        http://mautic.org
 *
 * @license     GNU/GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
 */

namespace MauticPlugin\FimplusBundle\Form\Type;

use Mautic\CoreBundle\Factory\MauticFactory;
use MauticPlugin\FimplusBundle\Entity\FCampaign;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Symfony\Component\Validator\Constraints\Valid;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\Form\FormInterface;

/**
 * Class CampaignType.
 */
class FCampaignType extends AbstractType
{
    /**
     * @var \Mautic\CoreBundle\Security\Permissions\CorePermissions
     */
    private $security;

    /**
     * @var \Symfony\Bundle\FrameworkBundle\Translation\Translator
     */
    private $translator;

    /**
     * @param MauticFactory $factory
     */
    public function __construct(MauticFactory $factory)
    {
        $this->translator = $factory->getTranslator();
        $this->security   = $factory->getSecurity();
    }

    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        // $builder->addEventSubscriber(new CleanFormSubscriber(['description' => 'html']));
        // $builder->addEventSubscriber(new FormExitSubscriber('campaign', $options));
        $optionsType = $options['data']->getTypeOption();
        $optionsApplySameType = $options['data']->getPreventApplySameTypeOption();
        $optionsDistributor = $options['data']->getDistributorOption();
        $promotion = $options['promotion'];
        $builder->add(
            'name',
            'text',
            [
                'label'      => 'mautic.fimplus.campaign.name',
                'label_attr' => ['class' => 'control-label'],
                'attr'       => ['class' => 'form-control'],
                'property_path' => 'name',
            ]
        );
        $builder->add(
            'description',
            'textarea',
            [
                'label'      => 'mautic.fimplus.campaign.description',
                'label_attr' => [
                    'class' => 'control-label',
                ],
                'attr' => [
                    'class' => 'form-control',
                ],
                'property_path' => 'description',
                'mapped'     => true,
            ]
        );
        $builder->add(
            'priority',
            'number',
            [
                'label'      => 'mautic.fimplus.campaign.priority',
                'label_attr' => ['class' => 'control-label'],
                'attr'       => ['class' => 'form-control not-chosen'],
                'mapped'     => true,
                'property_path' => 'priority',
            ]
        );
        $builder->add(
            'type',
            'choice',
            [
                'choices'    => $optionsType,
                'label'      => 'mautic.fimplus.campaign.type',
                'label_attr' => ['class' => 'control-label'],
                'attr'       => ['class' => 'form-control not-chosen'],
                'mapped'     => true,
                'empty_value' => 'mautic.fimplus.campaign.type.emptyvalue',
                'property_path' => 'type',
            ]
        );
        $builder->add(
            'apply_times',
            'number',
            [
                'label'      => 'mautic.fimplus.campaign.applytimes',
                'label_attr' => [
                    'class' => 'control-label',
                ], 'attr' => [
                    'class' => 'form-control',
                ],
                'property_path' => 'applyTimes',
                'mapped'     => true,
            ]
        );
        $builder->add(
            'prevent_apply_same_type',
            'choice',
            [
                'choices'    => $optionsApplySameType,
                'label'      => 'mautic.fimplus.campaign.applysametype',
                'label_attr' => ['class' => 'control-label'],
                'attr'       => ['class' => 'form-control not-chosen'],
                'mapped'     => true,
                'empty_value' => 'mautic.fimplus.campaign.type.emptyvalue',
                'property_path' => 'preventApplySameType',
            ]
        );

        $builder->add(
            'start_date',
            'datetime',
            [
                'widget'     => 'single_text',
                'label'      => 'mautic.fimplus.campaign.startdate',
                'label_attr' => ['class' => 'control-label'],
                'attr'       => [
                    'class'       => 'form-control',
                    'data-toggle' => 'date',
                    'autocomplete' => 'off',
                ],
                'format'   => 'yyyy-MM-dd',
                'property_path' => 'startDate',
                'input' => 'string',
                'mapped'     => true,
            ]
        );

        $builder->add(
            'end_date',
            'datetime',
            [
                'widget'     => 'single_text',
                'label'      => 'mautic.fimplus.campaign.enddate',
                'label_attr' => ['class' => 'control-label'],
                'attr'       => [
                    'class'       => 'form-control',
                    'data-toggle' => 'date',
                    'autocomplete' => 'off',
                ],
                'format'   => 'yyyy-MM-dd',
                'property_path' => 'endDate',
                'input' => 'string',
                'mapped'     => true,
            ]
        );
        $builder->add(
            'distributor',
            'choice',
            [
                'choices'    => $optionsDistributor,
                'label'      => 'mautic.fimplus.campaign.distributor',
                'label_attr' => ['class' => 'control-label'],
                'attr'       => ['class' => 'form-control not-chosen'],
                'mapped'     => true,
                'empty_value' => 'mautic.fimplus.campaign.distributor.emptyvalue',
                'property_path' => 'distributor',
            ]
        );

        // $builder->add('promotion', PromotionType::class);
        $builder->add(
            'promotion',
            'choice',
            [
                'choices'    => $promotion,
                'label'      => 'mautic.fimplus.campaign.promotion',
                'label_attr' => ['class' => 'control-label'],
                'attr'       => ['class' => 'form-control chosen'],
                'mapped'     => true,
                'empty_value' => 'mautic.fimplus.campaign.promotion.emptyvalue',
                'property_path' => 'promotion',
                'multiple'  => true,
            ]
        );
        $formModifier = function (FormInterface $form, $formDetail = '') use ($options) {
            switch ($formDetail) {
                case FCampaign::ACTIVE_CODE:
                    $form->add(
                        'campaignActiveCodeDetail',
                        CollectionType::class,
                        [
                            'entry_type' => CampaignActiveCodeDetailType::class,
                            'entry_options' => [
                                'attr' => ['class' => 'condition-item'],
                                // 'options'=> $options,
                                'label' => false
                            ],
                            'label' => false,
                            // 'allow_add' => true,
                            // 'allow_delete' => true,
                            'by_reference' => false,
                            'cascade_validation' => true,
                            'constraints' => array(new Valid()),
                            'mapped' => true,
                        ]
                    );
                    break;
                case FCampaign::BUY_SVOD_GET_TVOD:
                    $form->add(
                        'promotionConditionDetail',
                        CollectionType::class,
                        [
                            'entry_type' => PromotionConditionDetailType::class,
                            'entry_options' => [
                                'attr' => ['class' => 'condition-item'],
                                'options' => $options,
                                'label' => false
                            ],
                            'label' => false,
                            'allow_add' => false,
                            'allow_delete' => false,
                            'by_reference' => false,
                            'cascade_validation' => true,
                            'constraints' => array(new Valid()),
                            'mapped' => true,
                        ]
                    );
                    break;
                case FCampaign::AUTO_ACTIVE:
                case FCampaign::CREDIT_CARD:
                case FCampaign::DISCOUNT:
                case FCampaign::TRIAL:

                    break;
            }
        };

        $builder->addEventListener(
            FormEvents::PRE_SET_DATA,
            function (FormEvent $event) use ($formModifier) {
                // this would be your entity, i.e. SportMeetup
                $data = $event->getData();
                //var_dump($data);exit;
                $formModifier($event->getForm(), $data->getType());
            }
        );
        $builder->get('type')->addEventListener(
            FormEvents::POST_SUBMIT,
            function (FormEvent $event) use ($formModifier) {
                // It's important here to fetch $event->getForm()->getData(), as
                // $event->getData() will get you the client data (that is, the ID)
                $data = $event->getForm()->getData();
                // since we've added the listener to the child, we'll have to pass on
                // the parent to the callback functions!
                $formModifier($event->getForm()->getParent(), $data);
                // $formModifier($event->getForm(), $sport);
            }
        );

        $builder->add('isPublished', 'yesno_button_group');
        $builder->add('buttons', 'form_buttons');
        if (!empty($options['action'])) {
            $builder->setAction($options['action']);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults([
            'data_class' => 'MauticPlugin\FimplusBundle\Entity\FCampaign',
            'options' => null, //this will register the 'options', otherwise we get an error [The option "options" does not exist
            'promotion' => null
        ]);
        $resolver->setRequired('options');
        $resolver->setRequired('promotion');
        // you can also define the allowed types, allowed values and
        // any other feature supported by the OptionsResolver component
    }

    /**
     * {@inheritdoc}
     */
    public function getName()
    {
        return 'fcampaign';
    }
}
