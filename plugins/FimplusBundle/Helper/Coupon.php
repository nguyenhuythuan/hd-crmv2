<?php 
namespace MauticPlugin\FimplusBundle\Helper;


class Coupon {
    CONST MIN_LENGTH = 8;
    static public function randomString($length = 4){
        $characters = 'QWERTYUPASDFGHJKZXCVBNM23456789';
        $randstring = '';
        for ($i = 0; $i < $length; $i++) {
            $randstring .= $characters[rand(0, strlen($characters)-1)];
        }
        return $randstring;
    }
    static public function basicGenerate($options){
        $length         = (isset($options['length']) ? filter_var($options['length'], FILTER_VALIDATE_INT, ['options' => ['default' => self::MIN_LENGTH, 'min_range' => 1]]) : self::MIN_LENGTH );
        $prefix         = (isset($options['prefix']) ? self::cleanString(filter_var($options['prefix'], FILTER_SANITIZE_STRING)) : '' );
        $suffix         = (isset($options['suffix']) ? self::cleanString(filter_var($options['suffix'], FILTER_SANITIZE_STRING)) : '' );
        $hasChecksum    = (isset($options['hasChecksum']) ? filter_var($options['hasChecksum'], FILTER_SANITIZE_STRING) : false );
        $code =  self::randomString($length);
        if($prefix && $prefix != 'false'){
            $code =  $prefix . $code;
        }
        if($suffix && $suffix != 'false'){
            $code =  $code . $suffix;
        }
        if($hasChecksum){
            // prefix = prefix(2 character) + orders(1 character)
            // checksum string = prefix + code 
            $checksum = self::getCharacterChecksum($prefix . $code);
            $code = $code.$checksum;
        }
        return $code;

    }
    /**
     * MASK FORMAT [XXX-XXX]
     * 'X' this is random symbols
     * '-' this is separator
     *
     * @param array $options
     * @return string
     * @throws Exception
     */
    static public function generate($options = []) {

        $length         = (isset($options['length']) ? filter_var($options['length'], FILTER_VALIDATE_INT, ['options' => ['default' => self::MIN_LENGTH, 'min_range' => 1]]) : self::MIN_LENGTH );
        $prefix         = (isset($options['prefix']) ? self::cleanString(filter_var($options['prefix'], FILTER_SANITIZE_STRING)) : '' );
        $suffix         = (isset($options['suffix']) ? self::cleanString(filter_var($options['suffix'], FILTER_SANITIZE_STRING)) : '' );
        $useLetters     = (isset($options['letters']) ? filter_var($options['letters'], FILTER_VALIDATE_BOOLEAN) : true );
        $useNumbers     = (isset($options['numbers']) ? filter_var($options['numbers'], FILTER_VALIDATE_BOOLEAN) : false );
        $useSymbols     = (isset($options['symbols']) ? filter_var($options['symbols'], FILTER_VALIDATE_BOOLEAN) : false );
        $useMixedCase   = (isset($options['mixed_case']) ? filter_var($options['mixed_case'], FILTER_VALIDATE_BOOLEAN) : false );
        $mask           = (isset($options['mask']) ? filter_var($options['mask'], FILTER_SANITIZE_STRING) : false );
        $hasChecksum    = (isset($options['hasChecksum']) ? filter_var($options['hasChecksum'], FILTER_SANITIZE_STRING) : false );

        $uppercase    = ['Q', 'W', 'E', 'R', 'T', 'Y', 'U', 'P', 'A', 'S', 'D', 'F', 'G', 'H', 'J', 'K', 'Z', 'X', 'C', 'V', 'B', 'N', 'M'];
        $lowercase    = [];   
        // $lowercase    = ['q', 'w', 'e', 'r', 't', 'y', 'u', 'p', 'a', 's', 'd', 'f', 'g', 'h', 'j', 'k', 'z', 'x', 'c', 'v', 'b', 'n', 'm'];
        $numbers      = [ 1, 2, 3, 4, 5, 6, 7, 8, 9];
        $symbols      = ['`', '~', '!', '@', '#', '$', '%', '^', '&', '*', '(', ')', '-', '_', '=', '+', '\\', '|', '/', '[', ']', '{', '}', '"', "'", ';', ':', '<', '>', ',', '.', '?'];

        $characters   = [];
        $coupon = '';

        if ($useLetters) {
            if ($useMixedCase) {
                $characters = array_merge($characters, $lowercase, $uppercase);
            } else {
                $characters = array_merge($characters, $uppercase);
            }
        }

        if ($useNumbers) {
            $characters = array_merge($characters, $numbers);
        }

        if ($useSymbols) {
            $characters = array_merge($characters, $symbols);
        }

        if ($mask) {
            for ($i = 0; $i < strlen($mask); $i++) {
                if ($mask[$i] === 'X') {
                    $coupon .= $characters[rand(0, count($characters) - 1)];
                } else {
                    $coupon .= $mask[$i];
                }
            }
        } else {
            for ($i = 0; $i < $length; $i++) {
                $coupon .= $characters[rand(0, count($characters) - 1)];
            }
            /* $coupon = substr(str_shuffle($str_result),  
                       0, $length_of_string); */
            
        }
        $code =  $coupon;
        if($prefix && $prefix != 'false'){
            $code =  $prefix . $code;
        }
        if($suffix && $suffix != 'false'){
            $code =  $code . $suffix;
        }
        if($hasChecksum){
            // prefix = prefix(2 character) + orders(1 character)
            // checksum string = prefix + code 
            $checksum = self::getCharacterChecksum($prefix . $code);
            $code = $code.$checksum;
        }
        return $code;
    }

    /**
     * @param int $maxNumberOfCoupons
     * @param array $options
     * @return array
     */
    static public function generate_coupons($maxNumberOfCoupons = 1, $options = []) {
        $coupons = [];
        for ($i = 0; $i < $maxNumberOfCoupons; $i++) {
            $temp = self::generate($options);
            $coupons[] = $temp;
        }
        return $coupons;
    }

    /**
     * @param int $maxNumberOfCoupons
     * @param $filename
     * @param array $options
     */
    static public function generate_coupons_to_xls($maxNumberOfCoupons = 1, $filename, $options = []) {
        $filename = (empty(trim($filename)) ? 'coupons' : trim($filename));

        header('Content-Type: application/vnd.ms-excel');

        echo 'Coupon Codes' . "\t\n";
        for ($i = 0; $i < $maxNumberOfCoupons; $i++) {
            $temp = self::generate($options);
            echo $temp . "\t\n";
        }

        header('Content-disposition: attachment; filename=' . $filename . '.xls');
    }

    /**
     * Strip all characters but letters and numbers
     * @param $string
     * @param array $options
     * @return string
     * @throws Exception
     */
    static private function cleanString($string, $options = []) {
        $toUpper = (isset($options['uppercase']) ? filter_var($options['uppercase'], FILTER_VALIDATE_BOOLEAN) : false);
        $toLower = (isset($options['lowercase']) ? filter_var($options['lowercase'], FILTER_VALIDATE_BOOLEAN) : false);

        $striped = preg_replace('/[^a-zA-Z0-9]/', '', trim($string));

        // make uppercase
        if ($toLower && $toUpper) {
            throw new Exception('You cannot set both options (uppercase|lowercase) to "true"!');
        } else if ($toLower) {
            return strtolower($striped);
        } else if ($toUpper) {
            return strtoupper($striped);
        } else {
            return $striped;
        }
    }
    static function createPrefix(){
        $filename =  __DIR__ . "/../Logs/CouponPrefix.log";
        $myfile = fopen( $filename, "a+") or die("Unable to open file!");
        $contents = fread($myfile, filesize($filename) == 0 ? 1 :filesize($filename));
        $arrPrefixExist = explode("\n", $contents);
        
        $pre = Coupon::returnPreFix($arrPrefixExist);
        file_put_contents($filename, $pre.PHP_EOL , FILE_APPEND | LOCK_EX);
        fclose($myfile);
        return $pre;
    }

    static public function returnPreFix($arrayExist, $numPre = 2){
        $uppercase    = ['Q', 'W', 'E', 'R', 'T', 'Y', 'U', 'P', 'A', 'S', 'D', 'F', 'G', 'H', 'J', 'K', 'Z', 'X', 'C', 'V', 'B', 'N', 'M', '2', '3','4','5','6','7','8','9'];
        $prefix = '';
        for($i = 0; $i < $numPre; $i++){
            $prefix .= $uppercase[rand(0,count($uppercase)-1)];
        }
        if($numPre == 1){
            if(count($arrayExist) == count($uppercase)){
                return 'No more value to return';
            }
        }
        if(!in_array($prefix,$arrayExist)){
            return $prefix;
        }else{
            return self::returnPreFix($arrayExist);
        }
    }
    static public function getCharacterChecksum($string){
        // $a = md5('X'.'3UXQUYZC'.'3U');
        $checkSum = md5($string);
        $checkSum = str_replace(['0','1', 'i', 'o', 'l'],'',$checkSum);
        $index = floor(strlen($checkSum)/2);
        $character = substr ( $checkSum , $index, 1 );
        return strtoupper($character);
    }
    static public function generateSerial(){
        $serial = microtime(true)*10000+rand(1,9999999999999)+rand(1,9999999999999);
        return $serial;
    }
}