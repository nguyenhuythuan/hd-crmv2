<?php 
namespace MauticPlugin\FimplusBundle\Helper;

class Param{
    const SEARCH_FILM_URL = 'https://agw.fimplus.io/movieIdx/cm/search';
    const ACCESS_TOKEN = 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzaWQiOiIxMjM0NTY3ODkwIiwidWlkIjoibmhhcHEiLCJpYXQiOiJKb2huIERvZSIsInBsdCI6IlRWOlNBTVNVTkc6VElaRU46NjExMSIsImp0aSI6IjEyMzQ1IiwiYWlkIjoxNTE2MjM5MDIyfQ.eOTm3fBR6Oq92vWNyvAtMnWBrE-KeRELuUIFCii58Lg';
    const PACKAGE_URL = 'https://agw.fimplus.io/promotion/v2/subscriptionPlans';
    const MAX_CODE_PER_COMMAND = 20000;
    const MAX_COMMAND = 5;
    const LENGTH_CODE_WITHOUT_PRE = 4;
}