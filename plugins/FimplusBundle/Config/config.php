<?php

/*
 * @copyright   2019 Fimplus. All rights reserved
 * @author      longnv
 *
 * @link        http://fimplus.vn
 *
 * @license     no license
 */

return [
    'name'        => 'FIMPLUS',
    'description' => 'Custom campaign and action for Fimplus',
    'version'     => '1.0',
    'author'      => 'Longnv',
    'routes' => [
        'main' => [
            'mautic_fimplus_campaign_index' => [
                'path'       => '/fcampaign/{page}',
                'controller' => 'FimplusBundle:FCampaign:index',
            ],
            'mautic_fimplus_promotion_index' => [
                'path'       => '/promotion/{page}',
                'controller' => 'FimplusBundle:Promotion:index',
            ],
            'mautic_fimplus_promotion_action' => [
                'path'       => '/promotion/{objectAction}/{objectId}',
                'controller' => 'FimplusBundle:Promotion:execute',
            ],
            'mautic_fimplus_campaign_action' => [
                'path'       => '/fcampaign/{objectAction}/{objectId}',
                'controller' => 'FimplusBundle:FCampaign:execute',
            ],
            'mautic_fimplus_lead_finfo' => [
                'path'       => '/fuser/{phone}',
                'controller' => 'FimplusBundle:Lead:info',
            ],
            'mautic_fimplus_get_list_film_ajax' => [
                'path'       => '/fcampaign/listfilm',
                'controller' => 'FimplusBundle:FCampaign:listFilm',
            ],
            'mautic_fimplus_bussiness_invoice_index' => [
                'path'       => '/code/invoice',
                'controller' => 'FimplusBundle:Bussiness:index',
            ],
            'mautic_fimplus_bussiness_invoice_action' => [
                'path'       => '/code/{objectAction}/{objectId}',
                'controller' => 'FimplusBundle:Bussiness:execute',
            ],
        ],
    ],

    'services' => [
        'repositories' => [
            'mautic.fimplus.repository.promotion' => [
                'class'     => Doctrine\ORM\EntityRepository::class,
                'factory'   => ['@doctrine.orm.entity_manager', 'getRepository'],
                'arguments' => [
                    'MauticPlugin\FimplusBundle\Entity\Promotion',
                ],
            ],
            'mautic.fimplus.repository.campaign' => [
                'class'     => Doctrine\ORM\EntityRepository::class,
                'factory'   => ['@doctrine.orm.entity_manager', 'getRepository'],
                'arguments' => [
                    'MauticPlugin\FimplusBundle\Entity\FCampaign',
                ],
            ],
            'mautic.fimplus.repository.promotionconditiondetail' => [
                'class'     => Doctrine\ORM\EntityRepository::class,
                'factory'   => ['@doctrine.orm.entity_manager', 'getRepository'],
                'arguments' => [
                    'MauticPlugin\FimplusBundle\Entity\PromotionConditionDetail',
                ],
            ],
            'mautic.fimplus.repository.campaignactivecodedetail' => [
                'class'     => Doctrine\ORM\EntityRepository::class,
                'factory'   => ['@doctrine.orm.entity_manager', 'getRepository'],
                'arguments' => [
                    'MauticPlugin\FimplusBundle\Entity\CampaignActiveCodeDetail',
                ],
            ],
            'mautic.fimplus.repository.bussinessinvoice' => [
                'class'     => Doctrine\ORM\EntityRepository::class,
                'factory'   => ['@doctrine.orm.entity_manager', 'getRepository'],
                'arguments' => [
                    'MauticPlugin\FimplusBundle\Entity\BussinessInvoice',
                ],
            ],
            'mautic.fimplus.repository.bussinesscode' => [
                'class'     => Doctrine\ORM\EntityRepository::class,
                'factory'   => ['@doctrine.orm.entity_manager', 'getRepository'],
                'arguments' => [
                    'MauticPlugin\FimplusBundle\Entity\BussinessCode',
                ],
            ],
        ],
        'events' => [
            'mautic.fimplus.subscriber.campaign_bundle' => [
                'class'     => \MauticPlugin\FimplusBundle\EventListener\FCampaignSubscriber::class,
                'arguments' => [
                    //'mautic.focus.model.focus',
                ],
            ],
        ],
        'forms' => [
            'mautic.form.type.promotion' => [
                'class' => 'MauticPlugin\FimplusBundle\Form\Type\PromotionType',
                'alias' => 'promotion',
                'arguments' => 'mautic.factory',
            ],
            'mautic.form.type.fcampaign' => [
                'class' => 'MauticPlugin\FimplusBundle\Form\Type\FCampaignType',
                'alias' => 'fcampaign',
                'arguments' => 'mautic.factory',
            ],
            'mautic.form.type.promotionconditiondetail' => [
                'class' => 'MauticPlugin\FimplusBundle\Form\Type\PromotionConditionDetailType',
                'alias' => 'promotionconditiondetail',
                'arguments' => 'mautic.factory',
            ],
            'mautic.form.type.campaignactivecodedetail' => [
                'class' => 'MauticPlugin\FimplusBundle\Form\Type\CampaignActiveCodeDetailType',
                'alias' => 'campaignactivecodedetail',
                'arguments' => 'mautic.factory',
            ],
            'mautic.form.type.bussiness_invoice' => [
                'class' => 'MauticPlugin\FimplusBundle\Form\Type\BussinessInvoiceType',
                'alias' => 'bussiness_invoice',
                'arguments' => 'mautic.factory',
            ],
        ],
        'models' => [
            'mautic.fimplus.model.promotion' => [
                'class'     => 'MauticPlugin\FimplusBundle\Model\PromotionModel',
                'arguments' => []
            ],
            'mautic.fimplus.model.fcampaign' => [
                'class'     => 'MauticPlugin\FimplusBundle\Model\FCampaignModel',
                'arguments' => []
            ],
            'mautic.fimplus.model.promotionConditionDetail' => [
                'class'     => 'MauticPlugin\FimplusBundle\Model\PromotionConditionDetailModel',
                'arguments' => []
            ],
            'mautic.fimplus.model.promotionObject' => [
                'class'     => 'MauticPlugin\FimplusBundle\Model\PromotionObjectModel',
                'arguments' => []
            ],
            'mautic.fimplus.model.campaignActiveCodeDetail' => [
                'class'     => 'MauticPlugin\FimplusBundle\Model\CampaignActiveCodeDetailModel',
                'arguments' => []
            ],
            'mautic.fimplus.model.bussinessInvoice' => [
                'class'     => 'MauticPlugin\FimplusBundle\Model\BussinessInvoiceModel',
                'arguments' => []
            ],
            'mautic.fimplus.model.bussinessCode' => [
                'class'     => 'MauticPlugin\FimplusBundle\Model\BussinessCodeModel',
                'arguments' => []
            ],
        ],
    ],

    'menu' => [
        'main' => [
            'mautic.fimplus' => [
                'iconClass' => 'fa-video-camera',
                // 'route'    => 'mautic_fimplus_index',
                // 'access'   => 'plugin:fimplus:items:view',
                //'parent'   => 'mautic.core.channels',
                'priority' => 10,
            ],
            'mautic.fimplus.campaigns' => [
                'route'    => 'mautic_fimplus_campaign_index',
                // 'access'   => 'plugin:fimplus:items:view',
                'parent'   => 'mautic.fimplus',
                'priority' => 10,
            ],
            'mautic.fimplus.promotions' => [
                'route'    => 'mautic_fimplus_promotion_index',
                // 'access'   => 'plugin:fimplus:items:view',
                'parent'   => 'mautic.fimplus',
                'priority' => 10,
            ],
            'mautic.fimplus.code' => [
                'route'    => 'mautic_fimplus_bussiness_invoice_index',
                // 'access'   => 'plugin:fimplus:items:view',
                'parent'   => 'mautic.fimplus',
                'priority' => 10,
            ],
        ],
    ],

    'categories' => [
        'plugin:fimplus' => 'mautic.fimplus',
    ],
];
