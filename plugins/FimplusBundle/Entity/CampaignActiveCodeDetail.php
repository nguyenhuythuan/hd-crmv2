<?php

/*
 * @copyright   2014 Mautic Contributors. All rights reserved
 * @author      Mautic
 *
 * @link        http://mautic.org
 *
 * @license     GNU/GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
 */

namespace MauticPlugin\FimplusBundle\Entity;

use Doctrine\ORM\Events;
use Doctrine\ORM\Mapping as ORM;
use Mautic\CoreBundle\Doctrine\Mapping\ClassMetadataBuilder;
use Mautic\CoreBundle\Entity\FormEntity;
use Symfony\Component\Validator\Mapping\ClassMetadata;
use Symfony\Component\Validator\Constraints as Assert;
use Ramsey\Uuid\Uuid;
use MauticPlugin\FimplusBundle\Helper\Coupon;
use Symfony\Component\Validator\Context\ExecutionContextInterface;

// use Mautic\LeadBundle\Entity\CustomFieldEntityInterface;
/**
 * Class Promotion.
 */
class CampaignActiveCodeDetail  extends FormEntity //implements CustomFieldEntityInterface
{
    const CODE_TYPE = [
        'VOUCHER' => 1,
        'GIFT_CODE' => 2,
        'SALE_CODE' => 3
    ];
    const STATUS = [
        'REQUESTED' => 1,
        'APPROVED' => 2,
        'REJECTED' => 3,
        'CANCEL' => 4
    ];
    /**
     * @var int
     */
    private $id;

    /**
     * @var
     */
    private $uuid;
    private $fcampaign;
    private $fcampaignId;

    /**
     * @var
     */
    private $partner;
    /**
     * @var
     */
    private $codeType;
    private $prefix;
    private $existPrefix;

    /**
     * @var
     */
    private $codeValue;
    private $isPercent;
    private $applySuccessMessage;
    private $configNote;
    private $status;

    /**
     * @var
     */
    private $maxDiscountValue;

    /**
     * @var
     */
    private $originalPrice;

    /**
     * @var
     */

    private $codeTypeLabel = [
        SELF::CODE_TYPE['VOUCHER'] => 'Voucher',
        SELF::CODE_TYPE['GIFT_CODE'] => 'Gift code',
        SELF::CODE_TYPE['SALE_CODE'] => 'Sale code'
    ];

    private $partnerLabel = [
        1 => 'partner_payoo',
        2 => 'Fim+_MKT',
        3 => 'QA team',
        4 => 'HNam Mobile',
        5 => 'Partner_LG',
        6 => 'vienthongA',
        7 => 'VNPT',
        8 => 'Sale Team',
        9 => 'partner_oversea',
        10 => 'shopee',
        11 => 'mobifone',
        12 => 'lazada',
        13 => 'Galaxy',
        14 => 'Appota',
        15 => 'fimplus digital',
        16 => 'partner_vtc365',
        17 => 'partner_fpt',
        18 => 'dienmayxanh',
        19 => 'galaxy_cinema',
        20 => 'Grab',
        21 => 'sony_smartTV',
        22 => 'Samsung Mobile',
        23 => 'autodb_code',
        24 => 'fimplus',
        25 => 'Fim+_HO',
        26 => 'fim plus partner',
        27 => 'samsung_smartTV',
        28 => 'Samsung Galaxy App',
        29 => 'thanhtoanonline',
        30 => 'Fanpage fim+',
        31 => 'Momo',
        32 => 'OPPO',
    ];

    /**
     * @param ORM\ClassMetadata $metadata
     */
    public static function loadMetadata(ORM\ClassMetadata $metadata)
    {
        $builder = new ClassMetadataBuilder($metadata);
        $builder->setTable('fimplus_campaign_activecode')
            ->setCustomRepositoryClass('MauticPlugin\FimplusBundle\Entity\CampaignActiveCodeDetailRepository')
            ->addLifecycleEvent('preSave', Events::prePersist);
        $builder->createField('id', 'integer')
            ->columnName('id')
            ->isPrimaryKey()
            ->generatedValue()
            ->build();

        $builder->createField('uuid', 'string')
            ->columnName('uuid')
            ->nullable(false)
            ->build();

        $builder->createField('fcampaignId', 'integer')
            ->columnName('fimplus_campaign_id')
            ->nullable(false)
            ->build();

        $builder->createField('partner', 'integer')
            ->columnName('partner')
            ->nullable(false)
            ->build();
        $builder->createField('codeType', 'integer')
            ->columnName('code_type')
            ->nullable(false)
            ->build();
        $builder->createField('codeValue', 'integer')
            ->columnName('code_value')
            ->nullable(false)
            ->build();
        $builder->createField('isPercent', 'integer')
            ->columnName('is_percent')
            ->nullable(false)
            ->build();
        $builder->createField('prefix', 'string')
            ->columnName('prefix')
            ->nullable(false)
            ->unique()
            ->build();
        $builder->createField('maxDiscountValue', 'integer')
            ->columnName('max_discount_value')
            ->build();
        $builder->createField('originalPrice', 'integer')
            ->columnName('original_price')
            ->build();
        $builder->createField('applySuccessMessage', 'string')
            ->columnName('apply_success_message')
            ->build();
        $builder->createField('configNote', 'string')
            ->columnName('config_note')
            ->build();
        $builder->createField('status', 'integer')
            ->columnName('status')
            ->build();
    }


    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    public function getUuid()
    {
        return $this->uuid;
    }
    public function setUuid($uuid)
    {
        $this->uuid = $uuid;
        return $this;
    }


    public function getFcampaign()
    {
        return $this->fcampaign;
    }
    public function setFcampaign($fcampaign)
    {
        $this->fcampaign = $fcampaign;
        return $this;
    }
    public function getPrefix()
    {
        return $this->prefix;
    }
    public function setPrefix($prefix)
    {
        $this->prefix = $prefix;
        return $this;
    }
    public function getExistPrefix()
    {
        return $this->existPrefix;
    }
    public function setExistPrefix($existPrefix)
    {
        $this->existPrefix = $existPrefix;
        return $this;
    }

    public function getFcampaignId()
    {
        return $this->fcampaignId;
    }
    public function setFcampaignId($fcampaignId)
    {
        $this->fcampaignId = $fcampaignId;
        return $this;
    }


    public function getPartner()
    {
        return $this->partner;
    }
    public function setPartner($partner)
    {
        $this->partner = $partner;
        return $this;
    }


    public function getCodeType()
    {
        return $this->codeType;
    }
    public function setCodeType($codeType)
    {
        $this->codeType = $codeType;
        return $this;
    }
    public function setFilm($film)
    {
        if(is_array($film)){
            $film = implode(',',$film);
        }
        $this->film = $film;
        return $this;
    }


    public function getCodeValue()
    {
        return $this->codeValue;
    }
    public function setCodeValue($codeValue)
    {
        $this->codeValue = $codeValue;
        return $this;
    }


    public function getIsPercent()
    {
        return $this->isPercent;
    }
    public function setIsPercent($isPercent)
    {
        $this->isPercent = $isPercent;
        return $this;
    }


    public function getMaxDiscountValue()
    {
        return $this->maxDiscountValue;
    }
    public function setMaxDiscountValue($maxDiscountValue)
    {
        $this->maxDiscountValue = $maxDiscountValue;
        return $this;
    }

    public function getOriginalPrice()
    {
        return $this->originalPrice;
    }
    public function setOriginalPrice($originalPrice)
    {
        $this->originalPrice = $originalPrice;
        return $this;
    }

    public function getPartnerLabel($key)
    {
        if (is_numeric($key))
            return $this->partnerLabel[$key];
        return null;
    }

    public function getPartnerOption()
    {
        $options = $this->partnerLabel;
        return $options;
    }

    public function getCodeTypeLabel($key)
    {
        if (is_numeric($key))
            return $this->codeTypeLabel[$key];
        return null;
    }

    public function getCodeTypeOption()
    {
        $options = $this->codeTypeLabel;
        return $options;
    }
    
    public function getCustomOption(){
        return $this->customOption;
    }
    public function setCustomOption($customOption){
        $this->customOption = $customOption;
        return $this;
    }
    public function getApplySuccessMessage(){
        return $this->applySuccessMessage;
    }
    public function setApplySuccessMessage($applySuccessMessage){
        $this->applySuccessMessage = $applySuccessMessage;
        return $this;
    }
    public function getConfigNote(){
        return $this->configNote;
    }
    public function setConfigNote($configNote){
        $this->configNote = $configNote;
        return $this;
    }
    public function getStatus(){
        return $this->status;
    }
    public function setStatus($status){
        $this->status = $status;
        return $this;
    }
    /**
     * @param ClassMetadata $metadata
     */
    public static function loadValidatorMetadata(ClassMetadata $metadata)
    {
        $metadata->addGetterConstraint('partner', new Assert\NotBlank([
            'message' => 'mautic.fimplus.default.required',
        ]));
        // $metadata->addPropertyConstraint('codeType', new Assert\NotBlank([
        //     'message' => 'mautic.fimplus.default.required',
        // ]));
        $metadata->addPropertyConstraint('codeValue', new Assert\NotBlank([
            'message' => 'mautic.fimplus.default.required',
        ]));
        // $metadata->addPropertyConstraint('maxDiscountValue', new Assert\NotBlank([
        //     'message' => 'mautic.fimplus.default.required',
        // ]));
        $metadata->addPropertyConstraint('originalPrice', new Assert\NotBlank([
            'message' => 'mautic.fimplus.default.required',
        ]));
        $metadata->addPropertyConstraint('originalPrice', new Assert\Range([
            'min'   => '1',
            'minMessage' => 'mautic.fimplus.campaign.original.minrequire',
        ]));
        $metadata->addPropertyConstraint('maxDiscountValue', new Assert\Range([
            'min'   => '1',
            'minMessage' => 'mautic.fimplus.campaign.maxdiscount.minrequire',
        ]));
        $metadata->addPropertyConstraint('codeValue', new Assert\Range([
            'min'   => '1',
            'minMessage' => 'mautic.fimplus.campaign.codevalue.minrequire',
        ]));
        $metadata->addPropertyConstraint('status', new Assert\Range([
            'min'   => '1',
            'minMessage' => 'mautic.fimplus.campaign.codevalue.minrequire',
        ]));
    }
    public function preSave(){
        if (!$this->getUuid()) {
            $this->uuid =Uuid::uuid4();
        }
        if(!$this->getCodeType()){
            $this->codeType =SELF::CODE_TYPE['SALE_CODE'];
        }
        if(!$this->getIsPercent()){
            $this->isPercent = 0;
        }
        if(!$this->getMaxDiscountValue()){
            $this->maxDiscountValue = 0;
        }
        if(!$this->getPrefix()){
            $arrExist = $this->getExistPrefix() ? $this->getExistPrefix() : [];
            $this->prefix = Coupon::returnPreFix($arrExist);
        }
    }
}
