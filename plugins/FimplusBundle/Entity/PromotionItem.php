<?php

/*
 * @copyright   2014 Mautic Contributors. All rights reserved
 * @author      Mautic
 *
 * @link        http://mautic.org
 *
 * @license     GNU/GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
 */

namespace MauticPlugin\FimplusBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Mautic\ApiBundle\Serializer\Driver\ApiMetadataDriver;
use Mautic\CoreBundle\Doctrine\Mapping\ClassMetadataBuilder;
use Mautic\CoreBundle\Entity\FormEntity;
use Mautic\LeadBundle\Entity\CustomFieldEntityInterface;

/**
 * Class Company.
 */
class PromotionItem extends FormEntity  //implements CustomFieldEntityInterface
{
   // use CustomFieldEntityTrait;

    const FIELD_ALIAS = 'promotionItem';

    /**
     * @var int
     */
    private $id;

    /**
     * @var Promotion
     */
    private $promotion;

    /**
     * @var VideoContent
     */
    private $videoContent;

    /**
     * @var
     */
    private $status;

    

    /**
     * @param ORM\ClassMetadata $metadata
     */
    public static function loadMetadata(ORM\ClassMetadata $metadata)
    {
        $builder = new ClassMetadataBuilder($metadata);
        $builder->setTable('promotion_item');

        $builder->createField('id', 'integer')
            ->isPrimaryKey()
            ->generatedValue()
            ->build();
        $builder->createField('status', 'integer')
            ->columnName('status')
            ->nullable(false)
            ->build();

        // relate to table promotion
        $builder->createManyToOne('promotion', 'Promotion')
        ->mappedBy('id')
        ->cascadeAll()
        ->fetchExtraLazy()
        ->build();

        //relate to video content
        /* $builder->createOneToOne('video_content', 'VideoContent')
        ->mappedBy('id')
        ->inversedBy('PromotionItem')
        ->build(); */
    }


    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    public function getPromotion(){
        return $this->promotion;
    }
    public function setPromotion($promotion){
        $this->promotion = $promotion;
        return $this;
    }


    public function getVideoContent(){
        return $this->videoContent;
    }
    public function setVideoContent($videoContent){
        $this->promotionName = $videoContent;
        return $this;
    }


    public function getStatus(){
        return $this->status;
    }
    public function setStatus($status){
        $this->status = $status;
        return $this;
    }

}
