<?php

/*
 * @copyright   2014 Mautic Contributors. All rights reserved
 * @author      Mautic
 *
 * @link        http://mautic.org
 *
 * @license     GNU/GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
 */

namespace MauticPlugin\FimplusBundle\Entity;

use Doctrine\ORM\Events;
use Doctrine\ORM\Mapping as ORM;
use Mautic\CoreBundle\Doctrine\Mapping\ClassMetadataBuilder;
use Mautic\CoreBundle\Entity\FormEntity;
use Symfony\Component\Validator\Mapping\ClassMetadata;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use MauticPlugin\FimplusBundle\Model\FCampaignModel;
use MauticPlugin\FimplusBundle\Entity\FCampaign;

use Ramsey\Uuid\Uuid;
use Symfony\Component\Validator\Context\ExecutionContextInterface;

// use Mautic\LeadBundle\Entity\CustomFieldEntityInterface;
/**
 * Class BussinessInvoice.
 */
class BussinessCode  extends FormEntity //implements CustomFieldEntityInterface
{
    const STATUS_ACTIVE = 0;
    /**
     * @var int
     */
    private $id;

    /**
     * @var
     */
    private $campaignId;
    private $invoiceId;
    private $code;
    private $status;
    private $serial;
   
    /**
     * @param ORM\ClassMetadata $metadata
     */
    public static function loadMetadata(ORM\ClassMetadata $metadata)
    {
        $builder = new ClassMetadataBuilder($metadata);
        $builder->setTable('active_code_business_code')
            ->setCustomRepositoryClass('MauticPlugin\FimplusBundle\Entity\BussinessCodeRepository')
            ->addLifecycleEvent('preSave', Events::prePersist);
        $builder->createField('id', 'string')
            ->columnName('id')
            ->isPrimaryKey()
            ->build();

        $builder->createField('campaignId', 'integer')
            ->columnName('campaign_id')
            ->nullable(false)
            ->build();

        $builder->createField('invoiceId', 'string')
            ->columnName('invoice_id')
            ->nullable(false)
            ->build();

        $builder->createField('code', 'string')
            ->columnName('code')
            ->nullable(false)
            ->unique()
            ->build();
        $builder->createField('serial', 'string')
            ->columnName('serial')
            ->nullable(false)
            ->unique()
            ->build();
        $builder->createField('status', 'integer')
            ->columnName('status')
            ->nullable(false)
            ->build();
        // $builder->createOneToOne('campaign', 'Fcampaign')
        //     ->inversedBy('failedLog')
        //     ->addJoinColumn('fimplus_campaign_id', 'id', false, false, 'CASCADE')
        //     ->build();
    }
    public static function loadValidatorMetadata(ClassMetadata $metadata)
    {
        $metadata->addGetterConstraint('campaignId', new Assert\NotBlank([
            'message' => 'mautic.fimplus.default.required',
        ]));
        $metadata->addGetterConstraint('invoiceId', new Assert\NotBlank([
            'message' => 'mautic.fimplus.default.required',
        ]));
        $metadata->addPropertyConstraint('code', new Assert\NotBlank([
            'message' => 'mautic.fimplus.default.required',
        ]));
        // $metadata->addPropertyConstraint('totalPrice', new Assert\NotBlank([
        //     'message' => 'mautic.fimplus.default.required',
        // ]));
        $metadata->addPropertyConstraint('serial', new Assert\NotBlank([
            'message' => 'mautic.fimplus.default.required',
        ]));
        $metadata->addConstraint(new UniqueEntity([
            'fields' => 'code',
            'errorPath' => 'code',
            'message' => 'This code is already exist.',
        ]));
        $metadata->addConstraint(new UniqueEntity([
            'fields' => 'serial',
            'errorPath' => 'serial',
            'message' => 'This serial is already exist.',
        ]));
    }

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }
    public function setId($id){
        $this->id = $id;
        return $this;
    }

    public function setCampaignId($campaignId)
    {
        $this->campaignId = $campaignId;
        return $this;
    }
    public function getCampaignId()
    {
        return $this->campaignId;
    }
    public function getInvoiceId()
    {
        return $this->invoiceId;
    }
    public function setInvoiceId($invoiceId)
    {
        $this->invoiceId = $invoiceId;
        return $this;
    }
    public function getCode()
    {
        return $this->code;
    }
    public function setCode($code)
    {
        $this->code = $code;
        return $this;
    }
    public function getSerial()
    {
        return $this->serial;
    }
    public function setSerial($serial)
    {
        $this->serial = $serial;
        return $this;
    }
    public function getStatus()
    {
        return $this->status;
    }
    public function setStatus($status)
    {
        $this->status = $status;
        return $this;
    }
    public function preSave(){
        if (!$this->getId()) {
            $this->id =Uuid::uuid4();
        }
    }
}
