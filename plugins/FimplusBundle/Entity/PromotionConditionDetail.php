<?php

/*
 * @copyright   2014 Mautic Contributors. All rights reserved
 * @author      Mautic
 *
 * @link        http://mautic.org
 *
 * @license     GNU/GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
 */

namespace MauticPlugin\FimplusBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Events;
use Mautic\CoreBundle\Doctrine\Mapping\ClassMetadataBuilder;
use Mautic\CoreBundle\Entity\FormEntity;
use Symfony\Component\Validator\Mapping\ClassMetadata;
use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\Common\Collections\ArrayCollection;
use Proxies\__CG__\Mautic\CampaignBundle\Entity\Campaign;
use Ramsey\Uuid\Uuid;

// use Mautic\LeadBundle\Entity\CustomFieldEntityInterface;
/**
 * Class Promotion.
 */
class PromotionConditionDetail  extends FormEntity //implements CustomFieldEntityInterface
{

    const FACT =[
        'TYPE' => 'Type',
        'PRODUCT_ID' => 'productId',
        'NEW_USER' => 'newUser',
        'NEW_REGISTER' => 'newRegister'
    ];
    const OPERATOR = [
        'EQUAL' => 'equal',
        'NOT_EQUAL' => 'notEqual',
        'LESS_THAN' => 'lessThan',
        'LESS_THAN_INCLUSIVE' => 'lessThanInclusive',
        'GREATER_THAN' => 'greaterThan',
        'GREATER_THAN_INCLUSIVE' => 'greaterThanInclusive',
        'IN' => 'in',
        'NOT_IN' => 'notIn'
    ];
    const TYPE_VALUE =[
        'JSON' => 1,
        'STRING' => 2,
        'BOOLEAN' => 3
    ];
    /**
     * @var int
     */
    private $id;

    /**
     * @var
     */
    private $campaign;
    private $uuid;
    private $orders;
    /**
     * @var
     */
    private $fact;
    /**
     * @var
     */
    private $operator;

    /**
     * @var
     */
    private $value;

    /**
     * @var
     */
    private $typeValue;
    private $campaignId;



    private $typeValueLabel = [
        SELF::TYPE_VALUE['JSON'] => 'Json',
        SELF::TYPE_VALUE['STRING'] => 'String',
        SELF::TYPE_VALUE['BOOLEAN'] => 'Boolean'
    ];
    private $factLabel = [
        self::FACT['TYPE'] => 'Type',
        self::FACT['PRODUCT_ID'] => 'Product ID',
        self::FACT['NEW_USER'] => 'New user',
        self::FACT['NEW_REGISTER'] => 'New register'
    ];
    private $operatorLabel = [
        self::OPERATOR['EQUAL'] => 'Equal',
        self::OPERATOR['NOT_EQUAL'] => 'Not equal',
        self::OPERATOR['LESS_THAN'] => 'Less than',
        self::OPERATOR['LESS_THAN_INCLUSIVE'] => 'Less than inclusive',
        self::OPERATOR['GREATER_THAN'] => 'Greater than',
        self::OPERATOR['GREATER_THAN_INCLUSIVE'] => 'Greater than inclusive',
        self::OPERATOR['IN'] => 'In',
        self::OPERATOR['NOT_IN'] => 'Not in'
    ];

    private $ordersLabel = [
        1 => 'Get promotion',
        2 => 'Validate offer'
    ];
    /**
     * @param ORM\ClassMetadata $metadata
     */
    public static function loadMetadata(ORM\ClassMetadata $metadata)
    {
        $builder = new ClassMetadataBuilder($metadata);
        $builder->setTable('promotion_condition_detail')
         ->setCustomRepositoryClass('MauticPlugin\FimplusBundle\Entity\PromotionConditionDetailRepository')
         ->addLifecycleEvent('generateUuid', Events::prePersist);
        $builder->createField('id', 'integer')
            ->columnName('id')
            ->isPrimaryKey()
            ->generatedValue()
            ->build();

        $builder->createField('uuid', 'string')
            ->columnName('uuid')
            ->build();
        $builder->createField('orders', 'integer')
            ->columnName('orders')
            ->build();

        $builder->createField('campaignId', 'integer')
            ->columnName('campaign_id')
            ->nullable(false)
            ->build();

        $builder->createField('fact', 'string')
            ->columnName('fact')
            ->nullable(false)
            ->build();

        $builder->createField('operator', 'string')
            ->columnName('operator')
            ->nullable(false)
            ->build();
        $builder->createField('value', 'string')
            ->columnName('value')
            ->nullable(false)
            ->build();
        $builder->createField('typeValue', 'string')
            ->columnName('type_value')
            ->nullable()
            ->build();
    }


    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    public function getCampaign()
    {
        return $this->campaign;
    }
    public function setCampaign($campaign)
    {
        $this->campaign = $campaign;
        return $this;
    }
    public function getUuid(){
        return $this->uuid;
    }
    public function setUuid($uuid){
        $this->uuid = $uuid;
        return $this;
    }
    public function getOrders(){
        return $this->orders;
    }
    public function setOrders($orders){
        $this->orders = $orders;
        return $this;
    }
    public function setCampaignId($campaignId)
    {
        $this->campaignId = $campaignId;
        return $this;
    }

    public function getFact()
    {
        return $this->fact;
    }
    public function setFact($fact)
    {
        $this->fact = $fact;
        return $this;
    }


    public function getOperator()
    {
        return $this->operator;
    }
    public function setOperator($operator)
    {
        $this->operator = $operator;
        return $this;
    }


    public function getValue()
    {
        return $this->value;
    }
    public function setValue($value)
    {
        $this->value = $value;
        return $this;
    }


    public function getTypeValue()
    {
        return $this->typeValue;
    }
    public function setTypeValue($typeValue)
    {
        $this->typeValue = $typeValue;
        return $this;
    }




    public function getTypeValueLabel($key)
    {
        if (is_numeric($key))
            return $this->typeValueLabel[$key];
        return $this->typeValueLabel[0];
    }

    public function getTypeValueOption()
    {
        $options = $this->typeValueLabel;
        return $options;
    }
    public function getFactOption()
    {
        $options = $this->factLabel;
        return $options;
    }
    public function getOperatorOption()
    {
        $options = $this->operatorLabel;
        return $options;
    }
    public function getOrdersOption()
    {
        $options = $this->ordersLabel;
        return $options;
    }


    /**
     * @param ClassMetadata $metadata
     */
    public static function loadValidatorMetadata(ClassMetadata $metadata)
    {
        $metadata->addGetterConstraint('fact', new Assert\NotBlank([
            'message' => 'mautic.fimplus.default.required',
        ]));
        $metadata->addPropertyConstraint('operator', new Assert\NotBlank([
            'message' => 'mautic.fimplus.default.required',
        ]));
        $metadata->addPropertyConstraint('value', new Assert\NotBlank([
            'message' => 'mautic.fimplus.default.required',
        ]));
        $metadata->addPropertyConstraint('typeValue', new Assert\NotBlank([
            'message' => 'mautic.fimplus.default.required',
        ]));
        $metadata->addPropertyConstraint('orders', new Assert\NotBlank([
            'message' => 'mautic.fimplus.default.required',
        ]));
    }

    public function isDatesValid()
    {
        return ($this->startDate < $this->endDate);
    }
    public function generateUuid(){
        
        if (!$this->getUuid()) {
            $this->uuid =Uuid::uuid4();
        }
    }
}
