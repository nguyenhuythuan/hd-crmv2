<?php

/*
 * @copyright   2014 Mautic Contributors. All rights reserved
 * @author      Mautic
 *
 * @link        http://mautic.org
 *
 * @license     GNU/GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
 */

namespace MauticPlugin\FimplusBundle\Entity;

use Doctrine\ORM\Events;
use Doctrine\ORM\Mapping as ORM;
use Mautic\CoreBundle\Doctrine\Mapping\ClassMetadataBuilder;
use Mautic\CoreBundle\Entity\FormEntity;
use Symfony\Component\Validator\Mapping\ClassMetadata;
use Symfony\Component\Validator\Constraints as Assert;
use MauticPlugin\FimplusBundle\Model\FCampaignModel;
use MauticPlugin\FimplusBundle\Entity\FCampaign;

use Ramsey\Uuid\Uuid;
use Symfony\Component\Validator\Context\ExecutionContextInterface;

// use Mautic\LeadBundle\Entity\CustomFieldEntityInterface;
/**
 * Class BussinessInvoice.
 */
class BussinessInvoice  extends FormEntity //implements CustomFieldEntityInterface
{

    /**
     * @var int
     */
    private $id;

    /**
     * @var
     */
    private $campaign;
    private $campaignId;
    private $orders;
    private $quantity;
    private $campaignQuantity;
    private $totalCodeCreated;
    /**
     * @var
     */
    private $totalPrice;
    /**
     * @var
     */
    private $startDate;

    /**
     * @var
     */
    private $endDate;


    /**
     * @param ORM\ClassMetadata $metadata
     */
    public static function loadMetadata(ORM\ClassMetadata $metadata)
    {
        $builder = new ClassMetadataBuilder($metadata);
        $builder->setTable('active_code_bussiness_invoice')
            ->setCustomRepositoryClass('MauticPlugin\FimplusBundle\Entity\BussinessInvoiceRepository')
            ->addLifecycleEvent('preSave', Events::prePersist);
        $builder->createField('id', 'string')
            ->columnName('id')
            ->isPrimaryKey()
            ->build();

        $builder->createField('quantity', 'integer')
            ->columnName('quantity')
            ->build();
        $builder->createField('orders', 'string')
            ->columnName('orders')
            ->build();

        $builder->createField('campaignId', 'integer')
            ->columnName('campaign_id')
            ->nullable(false)
            ->build();

        $builder->createField('totalPrice', 'integer')
            ->columnName('total_price')
            ->nullable(false)
            ->build();

        $builder->createField('startDate', 'datetime')
            ->columnName('start_date')
            ->nullable(false)
            ->build();
        $builder->createField('endDate', 'datetime')
            ->columnName('end_date')
            ->nullable(false)
            ->build();
        // $builder->createOneToOne('campaign', 'Fcampaign')
        //     ->inversedBy('failedLog')
        //     ->addJoinColumn('fimplus_campaign_id', 'id', false, false, 'CASCADE')
        //     ->build();
    }


    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }
    public function setId($id){
        $this->id = $id;
        return $this;
    }

    public function getCampaign()
    {
        return $this->campaign;
    }
    public function setCampaign($campaign)
    {
        $this->campaign = $campaign;
        return $this;
    }

    public function getOrders()
    {
        return $this->orders;
    }
    public function setOrders($orders)
    {
        $this->orders = $orders;
        return $this;
    }
    public function setCampaignId($campaignId)
    {
        $this->campaignId = $campaignId;
        return $this;
    }
    public function getCampaignId()
    {
        return $this->campaignId;
    }
    public function getQuantity()
    {
        return $this->quantity;
    }
    public function setQuantity($quantity)
    {
        $this->quantity = $quantity;
        return $this;
    }
    public function getCampaignQuantity()
    {
        return $this->campaignQuantity;
    }
    public function setCampaignQuantity($campaignQuantity)
    {
        $this->campaignQuantity = $campaignQuantity;
        return $this;
    }


    public function getTotalPrice()
    {
        return $this->totalPrice;
    }
    public function setTotalPrice($totalPrice)
    {
        $this->totalPrice = $totalPrice;
        return $this;
    }


    public function getStartDate()
    {
        return $this->startDate;
    }
    public function setStartDate($startDate)
    {
        $this->startDate = $startDate;
        return $this;
    }

    public function getEndDate()
    {
        return $this->endDate;
    }
    public function setEndDate($endDate)
    {
        $this->endDate = $endDate;
        return $this;
    }
    public function getTotalCodeCreated()
    {
        return $this->totalCodeCreated;
    }
    public function setTotalCodeCreated($totalCodeCreated)
    {
        $this->totalCodeCreated = $totalCodeCreated;
        return $this;
    }

    /**
     * @param ClassMetadata $metadata
     */
    public static function loadValidatorMetadata(ClassMetadata $metadata)
    {
        $metadata->addGetterConstraint('campaignId', new Assert\NotBlank([
            'message' => 'mautic.fimplus.default.required',
        ]));
        $metadata->addPropertyConstraint('quantity', new Assert\NotBlank([
            'message' => 'mautic.fimplus.default.required',
        ]));
        // $metadata->addPropertyConstraint('totalPrice', new Assert\NotBlank([
        //     'message' => 'mautic.fimplus.default.required',
        // ]));
        $metadata->addPropertyConstraint('startDate', new Assert\NotBlank([
            'message' => 'mautic.fimplus.default.required',
        ]));
        $metadata->addPropertyConstraint('endDate', new Assert\NotBlank([
            'message' => 'mautic.fimplus.default.required',
        ]));
        $metadata->addConstraint(new Assert\Callback('dateRangeValidate'));
    }
    public function dateRangeValidate(ExecutionContextInterface $context)
    {
        $campaignQuantity = (is_numeric($this->campaignQuantity)) ? $this->campaignQuantity : 0;
        $totalCodeCreated = (is_numeric($this->totalCodeCreated)) ? $this->totalCodeCreated : 0;
        $campaign = $this->campaign;
        $quantity = $this->quantity;
        if(null != $campaign){
            $startDate = date('Y-m-d',strtotime($this->startDate));
            $endDate = date('Y-m-d',strtotime($this->endDate));
            if($startDate < date_format($campaign->getStartDate(),'Y-m-d')){
                $context->buildViolation('Start date must greater than or equal start date of campaign('.date_format($campaign->getStartDate(),'d-m-Y').')')
                ->atPath('startDate')
                ->addViolation();
            }
            if($startDate > date_format($campaign->getEndDate(),'Y-m-d')){
                $context->buildViolation('Start date must less than or equal end date of campaign('.date_format($campaign->getEndDate(),'d-m-Y').')')
                ->atPath('startDate')
                ->addViolation();
            }
            if($endDate > date_format($campaign->getEndDate(),'Y-m-d')){
                $context->buildViolation('End date must less than or equal end date of campaign('.date_format($campaign->getEndDate(),'d-m-Y').')')
                ->atPath('endDate')
                ->addViolation();
            }
            if($endDate < date_format($campaign->getStartDate(),'Y-m-d')){
                $context->buildViolation('End date must greater than or equal start date of campaign('.date_format($campaign->getStartDate(),'d-m-Y').')')
                ->atPath('quantity')
                ->addViolation();
            }
        }
        if(null != $quantity && is_numeric($quantity)){
            $maxQuantity = $campaignQuantity - $totalCodeCreated;
            if($maxQuantity < $quantity){
                $context->buildViolation('Maximum quantity is '. number_format($maxQuantity))
                ->atPath('endDate')
                ->addViolation();
            }
        }
        if ($startDate > $endDate) {
            $context->buildViolation('End date must greater than start date')
                ->atPath('endDate')
                ->addViolation();
        }
    }
    public function preSave(){
        if (!$this->getId()) {
            $this->id =Uuid::uuid4();
        }
    }
}
