<?php

/*
 * @copyright   2014 Mautic Contributors. All rights reserved
 * @author      Mautic
 *
 * @link        http://mautic.org
 *
 * @license     GNU/GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
 */

namespace MauticPlugin\FimplusBundle\Entity;

use Doctrine\ORM\Events;
use Doctrine\ORM\Mapping as ORM;
use Mautic\CoreBundle\Doctrine\Mapping\ClassMetadataBuilder;
use Mautic\CoreBundle\Entity\FormEntity;
use Symfony\Component\Validator\Mapping\ClassMetadata;
use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\Common\Collections\ArrayCollection;
use MauticPlugin\FimplusBundle\Entity\PromotionConditionDetail;
use MauticPlugin\FimplusBundle\Entity\Promotion;
use Ramsey\Uuid\Uuid;
use Symfony\Component\Validator\Context\ExecutionContextInterface;

// use Mautic\LeadBundle\Entity\CustomFieldEntityInterface;
/**
 * Class Promotion.
 */
class FCampaign  extends FormEntity //implements CustomFieldEntityInterface
{
    const BUY_SVOD_GET_TVOD = 1;
    const AUTO_ACTIVE = 2;
    const ACTIVE_CODE = 3;
    const DISCOUNT = 4;
    const TRIAL = 5;
    const CREDIT_CARD = 6;

    const EVENT_PAGE_DEFINE = [
        self::BUY_SVOD_GET_TVOD => ['homepage', 'detail'],
        self::AUTO_ACTIVE => ['homepage'],
        self::ACTIVE_CODE => ['homepage','detail','payment'],
        self::DISCOUNT => ['homepage','detail'],
        self::TRIAL => ['payment'],
        self::CREDIT_CARD => ['homepage','detail','payment']
    ];

    /**
     * @var int
     */
    private $id;

    /**
     * @var
     */
    private $uuid;

    /**
     * @var
     */
    private $name;
    /**
     * @var
     */
    private $description;

    /**
     * @var
     */
    private $priority;

    /**
     * @var
     */
    private $type;

    /**
     * @var
     */
    private $applyTimes;
    private $preventApplySameType;

    /**
     * @var
     */
    private $eventPage;

    /**
     * @var
     */
    private $startDate;

    /**
     * @var
     */
    private $endDate;
    private $distributor;

    private $typeLabel = [
        self::BUY_SVOD_GET_TVOD => 'Buy svod get tvod',
        self::AUTO_ACTIVE => 'Auto active',
        self::ACTIVE_CODE => 'Active code',
        self::DISCOUNT => 'Discount',
        self::TRIAL => 'Trial',
        self::CREDIT_CARD => 'Credit card'
    ];

    private $preventApplySameTypeLabel = [
        1 => 'NOT Prevent',
        2 => 'prevent',
    ];

    private $statusLabel = [
        0 => 'ON',
        1 => 'OFF'
    ];

    private $distributorLabel = [
        1 => 'partner_payoo',
        2 => 'Fim+_MKT',
        3 => 'QA team',
        4 => 'HNam Mobile',
        5 => 'Partner_LG',
        6 => 'vienthongA',
        7 => 'VNPT',
        8 => 'Sale Team',
        9 => 'partner_oversea',
        10 => 'shopee',
        11 => 'mobifone',
        12 => 'lazada',
        13 => 'Galaxy',
        14 => 'Appota',
        15 => 'fimplus digital',
        16 => 'partner_vtc365',
        17 => 'partner_fpt',
        18 => 'dienmayxanh',
        19 => 'galaxy_cinema',
        20 => 'Grab',
        21 => 'sony_smartTV',
        22 => 'Samsung Mobile',
        23 => 'autodb_code',
        24 => 'fimplus',
        25 => 'Fim+_HO',
        26 => 'fim plus partner',
        27 => 'samsung_smartTV',
        28 => 'Samsung Galaxy App',
        29 => 'thanhtoanonline',
        30 => 'Fanpage fim+',
        31 => 'Momo',
        32 => 'OPPO',
    ];
    private $promotion;
    private $promotionConditionDetail;
    private $campaignActiveCodeDetail;

    public function __construct()
    {
        $this->promotionConditionDetail = new ArrayCollection();
        $this->campaignActiveCodeDetail = new ArrayCollection();
    }

    /**
     * @param ORM\ClassMetadata $metadata
     */
    public static function loadMetadata(ORM\ClassMetadata $metadata)
    {
        $builder = new ClassMetadataBuilder($metadata);
        $builder->setTable('fimplus_campaign')
            ->setCustomRepositoryClass('MauticPlugin\FimplusBundle\Entity\FCampaignRepository')
            ->addLifecycleEvent('preSaveCampaign', Events::prePersist);
        $builder->createField('id', 'integer')
            ->columnName('id')
            ->isPrimaryKey()
            ->generatedValue()
            ->build();

        $builder->createField('uuid', 'string')
            ->columnName('uuid')
            ->nullable(false)
            ->build();

        $builder->createField('name', 'string')
            ->columnName('name')
            ->nullable(false)
            ->build();

        $builder->createField('description', 'string')
            ->columnName('description')
            ->nullable(false)
            ->build();
        $builder->createField('eventPage', 'string')
            ->columnName('event_page')
            ->nullable(false)
            ->build();
        $builder->createField('priority', 'integer')
            ->columnName('priority')
            ->nullable()
            ->build();
        $builder->createField('type', 'integer')
            ->columnName('type')
            ->nullable(false)
            ->build();
        $builder->createField('applyTimes', 'integer')
            ->columnName('apply_times')
            ->nullable()
            ->build();
        $builder->createField('preventApplySameType', 'integer')
            ->columnName('prevent_apply_same_type')
            ->nullable()
            ->build();
        $builder->createField('startDate', 'datetime')
            ->columnName('start_date')
            ->nullable()
            ->build();
        $builder->createField('endDate', 'datetime')
            ->columnName('end_date')
            ->nullable()
            ->build();
        $builder->createField('distributor', 'integer')
            ->columnName('distributor')
            ->nullable()
            ->build();
    }


    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    public function getUuid()
    {
        return $this->uuid;
    }
    public function setUuid($uuid)
    {
        $this->uuid = $uuid;
        return $this;
    }


    public function getName()
    {
        return $this->name;
    }
    public function setName($name)
    {
        $this->name = $name;
        return $this;
    }


    public function getDescription()
    {
        return $this->description;
    }
    public function setDescription($description)
    {
        $this->description = $description;
        return $this;
    }


    public function getPriority()
    {
        return $this->priority;
    }
    public function setPriority($priority)
    {
        $this->priority = $priority;
        return $this;
    }


    public function getType()
    {
        return $this->type;
    }
    public function setType($type)
    {
        $this->type = $type;
        return $this;
    }
    public function getPromotion()
    {
        return $this->promotion;
    }
    public function setPromotion($promotion)
    {
        $this->promotion = $promotion;
        return $this;
    }


    public function getApplyTimes()
    {
        return $this->applyTimes;
    }
    public function setApplyTimes($applyTimes)
    {
        $this->applyTimes = $applyTimes;
        return $this;
    }


    public function getPreventApplySameType()
    {
        return $this->preventApplySameType;
    }
    public function setPreventApplySameType($preventApplySameType)
    {
        $this->preventApplySameType = $preventApplySameType;
        return $this;
    }


    public function getEventPage()
    {
        $event = trim(preg_replace("/[\[\]\"]/", "", $this->eventPage));
        return  $event;
    }
    public function setEventPage($eventPage)
    {
        $this->eventPage = $eventPage;
        return $this;
    }

    public function getStartDate()
    {
        return $this->startDate;
    }
    public function setStartDate($startDate)
    {
        $this->startDate = $startDate;
        return $this;
    }

    public function getEndDate()
    {
        return $this->endDate;
    }
    public function setEndDate($endDate)
    {
        $this->endDate = $endDate;
        return $this;
    }
    public function getDistributor()
    {
        return $this->distributor;
    }
    public function setDistributor($distributor)
    {
        $this->distributor = $distributor;
        return $this;
    }


    public function getTypeLabel($key)
    {
        if (is_numeric($key))
            return $this->typeLabel[$key];
        return null;
    }

    public function getTypeOption()
    {
        $options = $this->typeLabel;
        return $options;
    }

    public function getStatusLabel($key)
    {
        if (is_numeric($key))
            return $this->statusLabel[$key];
        return null;
    }

    public function getStatusOption()
    {
        $options = $this->statusLabel;
        return $options;
    }

    public function getDistributorLabel($key)
    {
        if (is_numeric($key))
            return $this->distributorLabel[$key];
        return null;
    }

    public function getDistributorOption()
    {
        $options = $this->distributorLabel;
        return $options;
    }


    public function getPreventApplySameTypeLabel($key)
    {
        if (is_numeric($key))
            return $this->preventApplySameTypeLabel[$key];
        return null;
    }

    public function getPreventApplySameTypeOption()
    {
        $options = $this->preventApplySameTypeLabel;
        return $options;
    }

    public function getPromotionConditionDetail(){
        return $this->promotionConditionDetail;
    }
    public function getCampaignActiveCodeDetail(){
        return $this->campaignActiveCodeDetail;
    }

    /**
     * @param ClassMetadata $metadata
     */
    public static function loadValidatorMetadata(ClassMetadata $metadata)
    {
        $metadata->addGetterConstraint('name', new Assert\NotBlank([
            'message' => 'mautic.fimplus.default.required',
        ]));
        $metadata->addPropertyConstraint('type', new Assert\NotBlank([
            'message' => 'mautic.fimplus.default.required',
        ]));
        $metadata->addPropertyConstraint('distributor', new Assert\NotBlank([
            'message' => 'mautic.fimplus.default.required',
        ]));
        $metadata->addPropertyConstraint('description', new Assert\NotBlank([
            'message' => 'mautic.fimplus.default.required',
        ]));
        $metadata->addPropertyConstraint('applyTimes', new Assert\NotBlank([
            'message' => 'mautic.fimplus.default.required',
        ]));
        $metadata->addPropertyConstraint('applyTimes', new Assert\Range([
            'min'   => '1',
            'minMessage' => 'mautic.fimplus.campaign.applytimes.minrequire',
        ]));
        $metadata->addPropertyConstraint('preventApplySameType', new Assert\NotBlank([
            'message' => 'mautic.fimplus.default.required',
        ]));
        $metadata->addPropertyConstraint('priority', new Assert\NotBlank([
            'message' => 'mautic.fimplus.default.required',
        ]));
        $metadata->addPropertyConstraint('priority', new Assert\Range([
            'min'   => '1',
            'max'   => '100',
            'minMessage' => 'mautic.fimplus.campaign.priority.minrequire',
            'maxMessage' => 'mautic.fimplus.campaign.priority.maxrequire',
        ]));
        $metadata->addPropertyConstraint('startDate', new Assert\NotBlank([
            'message' => 'mautic.fimplus.default.required',
        ]));
        $metadata->addPropertyConstraint('startDate', new Assert\DateTime([
            'message' => 'mautic.fimplus.campaign.datetime.format',
        ]));
        $metadata->addPropertyConstraint('endDate', new Assert\NotBlank([
            'message' => 'mautic.fimplus.default.required',
        ]));
        $metadata->addPropertyConstraint('endDate', new Assert\DateTime([
            'message' => 'mautic.fimplus.campaign.datetime.format',
        ]));
        $metadata->addConstraint(new Assert\Callback('dateRangeValidate'));
    }
    public function dateRangeValidate(ExecutionContextInterface $context)
    {
        if($this->startDate > $this->endDate){
            $context->buildViolation('End date must greater than start date')
                        ->atPath('endDate')
                        ->addViolation();
        }
    }
    public function addPromotionConditionDetail(PromotionConditionDetail $promotionConditionDetail)
    {
        $this->promotionConditionDetail->add($promotionConditionDetail);
        $promotionConditionDetail->setCampaign($this);
        return $this;
    }

    public function removePromotionConditionDetail(PromotionConditionDetail $promotionConditionDetail)
    {
        $this->promotionConditionDetail->removeElement($promotionConditionDetail);
        return $this;
    }
    public function addCampaignActiveCodeDetail(CampaignActiveCodeDetail $campaignActiveCodeDetail)
    {
        $this->campaignActiveCodeDetail->add($campaignActiveCodeDetail);
        $campaignActiveCodeDetail->setFCampaign($this);
        return $this;
    }

    public function removeCampaignActiveCodeDetail(CampaignActiveCodeDetail $campaignActiveCodeDetail)
    {
        $this->campaignActiveCodeDetail->removeElement($campaignActiveCodeDetail);
        return $this;
    }

    public function preSaveCampaign(){
        $this->generateUuid();
        $this->eventPageValue();
    }

    private function generateUuid(){
        
        if (!$this->getUuid()) {
            $this->uuid =Uuid::uuid4();
        }
    }
    private function eventPageValue(){
        switch($this->getType()){
            case self::BUY_SVOD_GET_TVOD:
                $this->eventPage = implode(",", self::EVENT_PAGE_DEFINE[self::BUY_SVOD_GET_TVOD]);
                break;
            case self::AUTO_ACTIVE:
            $this->eventPage = implode(",", self::EVENT_PAGE_DEFINE[self::AUTO_ACTIVE]);
                break;
            case self::ACTIVE_CODE:
            $this->eventPage = implode(",", self::EVENT_PAGE_DEFINE[self::ACTIVE_CODE]);
                break;
            case self::DISCOUNT:
            $this->eventPage = implode(",", self::EVENT_PAGE_DEFINE[self::DISCOUNT]);
                break;
            case self::TRIAL:
            $this->eventPage = implode(",", self::EVENT_PAGE_DEFINE[self::TRIAL]);
                break;
            case self::CREDIT_CARD:
            $this->eventPage = implode(",", self::EVENT_PAGE_DEFINE[self::CREDIT_CARD]);
                break;
        }
    }
}
