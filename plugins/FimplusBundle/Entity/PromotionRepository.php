<?php
namespace MauticPlugin\FimplusBundle\Entity;

use Doctrine\ORM\Tools\Pagination\Paginator;
use Mautic\CoreBundle\Entity\CommonRepository;

class PromotionRepository extends CommonRepository
{
    /**
     * @param string $product
     * @param string $email
     *
     * @return array
     */
    public function findByEmail($product, $email)
    {
        return $this->findBy(
            [
                'product' => $product,
                'email'   => $email,
            ]
        );
    }

    /**
     * Get a list of entities.
     *
     * @param array $args
     *
     * @return Paginator
     */
    public function getEntities(array $args = [])
    {
        return parent::getEntities($args);
    }

    /**
     * {@inheritdoc}
     *
     * @return string
     */
    public function getTableAlias()
    {
        return 'p';
    }
    public function getPromotions($limit = 100, $offset = 0)
    {
        $queryBuilder = $this->createQueryBuilder('p')
            ->select('p.id, p.promotionName');
        $queryBuilder->setFirstResult($offset);
        $queryBuilder->setMaxResults($limit);

        $promotion = $queryBuilder->getQuery()->getResult();
        return $promotion;
    }
}
