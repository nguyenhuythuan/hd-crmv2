<?php

/*
 * @copyright   2014 Mautic Contributors. All rights reserved
 * @author      Mautic
 *
 * @link        http://mautic.org
 *
 * @license     GNU/GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
 */

namespace MauticPlugin\FimplusBundle\Entity;

use Doctrine\ORM\Events;
use Doctrine\ORM\Mapping as ORM;
use Mautic\CoreBundle\Doctrine\Mapping\ClassMetadataBuilder;
use Mautic\CoreBundle\Entity\FormEntity;
use Symfony\Component\Validator\Mapping\ClassMetadata;
use Symfony\Component\Validator\Constraints as Assert;
use Mautic\LeadBundle\Entity\CustomFieldEntityInterface;
use Ramsey\Uuid\Uuid;
use Symfony\Component\Validator\Context\ExecutionContextInterface;

/**
 * Class Promotion.
 */
class Promotion  extends FormEntity //implements CustomFieldEntityInterface
{
    // use CustomFieldEntityTrait;

    // const FIELD_ALIAS = 'promotion';

    /**
     * @var int
     */
    private $id;
    private $uuid;

    /**
     * @var
     */

    /**
     * @var
     */
    private $promotionName;
    /**
     * @var
     */
    private $promotionType;

    /**
     * @var
     */
    private $promotionValue;

    /**
     * @var
     */
    private $promotionItemList;

    /**
     * @var
     */
    private $applyTiming;

    /**
     * @var
     */
    private $waitingApplyDuration;

    /**
     * @var
     */
    private $forceExpireDate;

    /**
     * @var
     */
    private $tvodApplyCondition;
    private $film;
    private $package;
    private $tvodType;
    const APPLY_TIMING = [
        'NOW' => 1,
        'LATER' => 2
    ];
    const PROMOTION_TYPE = [
        'TVOD_DISCOUNT_FLAT' => 1,
        'TVOD_DISCOUNT_PERCENT' => 2,
        'TVOD_GIFT' => 3,
        'SVOD_DISCOUNT_FLAT' => 4,
        'SVOD_DISCOUNT_PERCENT' => 5,
        'SVOD_GIFT' => 6,
    ];
    const TVOD_TYPE = [
        'ALL' => 1,
        'LISTED' => 2
    ];
    private $tvodTypeLabel = [
        self::TVOD_TYPE['ALL'] => 'ALL',
        self::TVOD_TYPE['LISTED'] => 'LISTED'
    ];
    private $typeLabel = [
        SELF::PROMOTION_TYPE['TVOD_DISCOUNT_FLAT'] => 'TVOD Discount - Flat',
        SELF::PROMOTION_TYPE['TVOD_DISCOUNT_PERCENT'] => 'TVOD Discount - Percent',
        SELF::PROMOTION_TYPE['TVOD_GIFT'] => 'TVOD gift',
        SELF::PROMOTION_TYPE['SVOD_DISCOUNT_FLAT'] => 'SVOD Discount - Flat',
        SELF::PROMOTION_TYPE['SVOD_DISCOUNT_PERCENT'] => 'SVOD Discount - Percent',
        SELF::PROMOTION_TYPE['SVOD_GIFT'] => 'SVOD - gift'
    ];

    private $applyTimingLabel = [
        self::APPLY_TIMING['NOW'] => 'Now',
        self::APPLY_TIMING['LATER'] => 'Later',
    ];

    private $TVODConditionLabel = [
        1 => 'All profile',
        2 => 'HD profile only',
        3 => '4K profile only',
    ];

    private $customOption;
    /**
     * @param ORM\ClassMetadata $metadata
     */
    public static function loadMetadata(ORM\ClassMetadata $metadata)
    {
        $builder = new ClassMetadataBuilder($metadata);
        $builder->setTable('promotion')
            ->setCustomRepositoryClass('MauticPlugin\FimplusBundle\Entity\PromotionRepository')
            ->addLifecycleEvent('preSave', Events::prePersist);
        $builder->createField('id', 'integer')
            ->columnName('id')
            ->isPrimaryKey()
            ->generatedValue()
            ->build();

        $builder->createField('promotionName', 'string')
            ->columnName('promotion_name')
            ->nullable(false)
            ->build();
        $builder->createField('uuid', 'string')
            ->columnName('uuid')
            ->build();

        $builder->createField('promotionType', 'string')
            ->columnName('promotion_type')
            ->nullable(false)
            ->build();

        $builder->createField('promotionValue', 'string')
            ->columnName('promotion_value')
            ->nullable(false)
            ->build();
        $builder->createField('promotionItemList', 'string')
            ->columnName('promotion_item_list')
            ->nullable()
            ->build();
        $builder->createField('applyTiming', 'integer')
            ->columnName('apply_timing')
            ->nullable(false)
            ->build();
        $builder->createField('waitingApplyDuration', 'integer')
            ->columnName('waiting_apply_duration')
            ->nullable()
            ->build();
        $builder->createField('forceExpireDate', 'datetime')
            ->columnName('force_expire_date')
            ->nullable()
            ->build();
        $builder->createField('tvodApplyCondition', 'integer')
            ->columnName('tvod_apply_condition')
            ->nullable()
            ->build();
        $builder->createField('package', 'string')
            ->columnName('package')
            ->build();
        $builder->createField('film', 'string')
            ->columnName('film')
            ->build();
        // relate to table promotion item
        /* $builder->createOneToMany('promotion_item', 'PromotionItem')
        ->setIndexBy('id')
        ->mappedBy('promotion_id')
        ->cascadeAll()
        ->fetchExtraLazy()
        ->build(); */

        //relate to fimplus campaign
        /* $builder->createOneToMany('promotion_item', 'PromotionItem')
        ->setIndexBy('id')
        ->mappedBy('promotion_id')
        ->cascadeAll()
        ->fetchExtraLazy()
        ->build(); */
    }


    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    public function getUuid()
    {
        return $this->uuid;
    }
    public function setUuid($uuid)
    {
        $this->uuid = $uuid;
        return $this;
    }
    public function getPromotionName()
    {
        return $this->promotionName;
    }
    public function setPromotionName($promotionName)
    {
        $this->promotionName = $promotionName;
        return $this;
    }


    public function getPromotionType()
    {
        return $this->promotionType;
    }
    public function setPromotionType($promotionType)
    {
        $this->promotionType = $promotionType;
        return $this;
    }

    public function getCustomOption()
    {
        return $this->customOption;
    }
    public function setCustomOption($customOption)
    {
        $this->customOption = $customOption;
        return $this;
    }


    public function getPromotionValue()
    {
        return $this->promotionValue;
    }
    public function setPromotionValue($promotionValue)
    {
        $this->promotionValue = $promotionValue;
        return $this;
    }


    public function getPromotionItemList()
    {
        return $this->promotionItemList;
    }
    public function setPromotionItemList($promotionItemList)
    {
        $this->promotionItemList = $promotionItemList;
        return $this;
    }


    public function getApplyTiming()
    {
        return $this->applyTiming;
    }
    public function setApplyTiming($applyTiming)
    {
        $this->applyTiming = $applyTiming;
        return $this;
    }


    public function getWaitingApplyDuration()
    {
        return $this->waitingApplyDuration;
    }
    public function setWaitingApplyDuration($waitingApplyDuration)
    {
        $this->waitingApplyDuration = $waitingApplyDuration;
        return $this;
    }


    public function getForceExpireDate()
    {
        return $this->forceExpireDate;
    }
    public function setForceExpireDate($forceExpireDate)
    {
        $this->forceExpireDate = $forceExpireDate;
        return $this;
    }

    public function getTvodApplyCondition()
    {
        return $this->tvodApplyCondition;
    }
    public function setTvodApplyCondition($tvodApplyCondition)
    {
        $this->tvodApplyCondition = $tvodApplyCondition;
        return $this;
    }
    public function getTypeLabel($key)
    {
        if (is_numeric($key))
            return $this->typeLabel[$key];
        return null;
    }
    public function getApplyTimingLabel($key)
    {
        if (is_numeric($key))
            return $this->applyTimingLabel[$key];
        return null;
    }

    public function getTVODConditionLabel($key)
    {
        if (is_numeric($key))
            return $this->TVODConditionLabel[$key];
        return null;
    }
    public function getPromotionTypeOption()
    {
        $options = $this->typeLabel;
        return $options;
    }
    public function getApplyTimingOption()
    {
        $options = $this->applyTimingLabel;
        return $options;
    }
    public function getTVODConditionOption()
    {
        $options = $this->TVODConditionLabel;
        return $options;
    }
    public function getTvodTypeOption()
    {
        $options = $this->tvodTypeLabel;
        return $options;
    }
    public function getTvodType()
    {
        return $this->tvodType;
    }
    public function setTvodType($tvodType)
    {
        $this->tvodType = $tvodType;
        return $this;
    }
    public function getPackage()
    {
        return $this->package;
    }
    public function setPackage($package)
    {
        $this->package = $package;
        return $this;
    }
    public function getFilm()
    {
        return $this->film;
    }
    public function setFilm($film)
    {
        if (is_array($film)) {
            $film = implode(',', $film);
        }
        $this->film = $film;
        return $this;
    }

    /**
     * @param ClassMetadata $metadata
     */
    public static function loadValidatorMetadata(ClassMetadata $metadata)
    {
        $metadata->addGetterConstraint('promotionName', new Assert\NotBlank([
            'message' => 'mautic.fimplus.promotion.name.required',
        ]));
        $metadata->addPropertyConstraint('promotionType', new Assert\NotBlank([
            'message' => 'mautic.fimplus.promotion.type.required',
        ]));
        $metadata->addPropertyConstraint('promotionValue', new Assert\NotBlank([
            'message' => 'mautic.fimplus.promotion.value.required',
        ]));
        $metadata->addPropertyConstraint('applyTiming', new Assert\NotBlank([
            'message' => 'mautic.fimplus.promotion.applytiming.required',
        ]));
        $metadata->addConstraint(new Assert\Callback('packageFilmValidate'));
        $metadata->addConstraint(new Assert\Callback('applyTimeValidate'));
    }

    public function packageFilmValidate(ExecutionContextInterface $context)
    {
        $promotionType = $this->getPromotionType();
        switch ($promotionType) {
            case self::PROMOTION_TYPE['TVOD_GIFT']:
                if (Promotion::TVOD_TYPE['LISTED'] == $this->getTvodType()) {
                    if (empty($this->getFilm())) {
                        $context->buildViolation('Film is required')
                            ->atPath('film')
                            ->addViolation();
                    }
                }
                break;
            case self::PROMOTION_TYPE['SVOD_GIFT']:
                if (empty($this->getPackage())) {
                    $context->buildViolation('Package is required')
                        ->atPath('package')
                        ->addViolation();
                }
                break;
        }
    }
    public function applyTimeValidate(ExecutionContextInterface $context){
        if(self::APPLY_TIMING['LATER'] == $this->getApplyTiming()){
            if(empty($this->getWaitingApplyDuration()) && empty($this->getForceExpireDate())){
                $context->buildViolation('"Waiting apply duration" OR  "Force expire date" must have value')
                        ->atPath('forceExpireDate')
                        ->addViolation();
            }
        }
    }
    public function preSave()
    {
        if (!$this->getUuid()) {
            $this->uuid = Uuid::uuid4();
        }
        $tvod = $this->getTvodType();
        switch ($tvod) {
            case Promotion::TVOD_TYPE['LISTED']:
            case Promotion::TVOD_TYPE['ALL']:
                $this->promotionItemList = $this->tvodTypeLabel[$tvod];
                break;
            default:
                $this->promotionItemList = '';
                break;
        }
    }
}
