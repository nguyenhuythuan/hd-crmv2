<?php

/*
 * @copyright   2014 Mautic Contributors. All rights reserved
 * @author      Mautic
 *
 * @link        http://mautic.org
 *
 * @license     GNU/GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
 */

namespace MauticPlugin\FimplusBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Events;
use Mautic\CoreBundle\Doctrine\Mapping\ClassMetadataBuilder;
use Mautic\CoreBundle\Entity\FormEntity;
use Symfony\Component\Validator\Mapping\ClassMetadata;
use Symfony\Component\Validator\Constraints as Assert;
use Ramsey\Uuid\Uuid;
// use Mautic\LeadBundle\Entity\CustomFieldEntityInterface;
/**
 * Class PromotionObject.
 */
class PromotionObject  extends FormEntity //implements CustomFieldEntityInterface
{
   // use CustomFieldEntityTrait;

    // const FIELD_ALIAS = 'promotion';

    /**
     * @var int
     */
    private $id;

    /**
     * @var
     */
    private $uuid;

    /**
     * @var
     */
    private $campaignId;
    /**
     * @var
     */
    private $promotionId;


    /**
     * @param ORM\ClassMetadata $metadata
     */
    public static function loadMetadata(ORM\ClassMetadata $metadata)
    {
        $builder = new ClassMetadataBuilder($metadata);
        $builder->setTable('promotion_object')
        ->setCustomRepositoryClass('MauticPlugin\FimplusBundle\Entity\PromotionObjectRepository')
        ->addLifecycleEvent('generateUuid', Events::prePersist);
        $builder->createField('id', 'integer')
             ->columnName('id')
            ->isPrimaryKey()
            ->generatedValue()
            ->build();
        $builder->createField('uuid', 'string')
            ->columnName('uuid')
            ->nullable(false)
            ->build();
        $builder->createField('campaignId', 'integer')
            ->columnName('campaign_id')
            ->nullable(false)
            ->build();

        $builder->createField('promotionId', 'integer')
            ->columnName('promotion_id')
            ->nullable(false)
            ->build();
    }


    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }
    public function getUuid()
    {
        return $this->uuid;
    }
    public function setUuid($uuid){
        $this->uuid = $uuid;
        return $this;
    }

    public function getCampaignId(){
        return $this->campaignId;
    }
    public function setCampaignId($campaignId){
        $this->campaignId = $campaignId;
        return $this;
    }
    public function getPromotionId(){
        return $this->promotionId;
    }
    public function setPromotionId($promotionId){
        $this->promotionId = $promotionId;
        return $this;
    }
    public function generateUuid(){
        
        if (!$this->getUuid()) {
            $this->uuid =Uuid::uuid4();
        }
    }
    /**
     * @param ClassMetadata $metadata
     */
    public static function loadValidatorMetadata(ClassMetadata $metadata)
    {
        // $metadata->addGetterConstraint('promotionName', new Assert\NotBlank([
        //     'message' => 'mautic.fimplus.promotion.name.required',
        // ]));
        // $metadata->addPropertyConstraint('promotionType', new Assert\NotBlank([
        //     'message' => 'mautic.fimplus.promotion.type.required',
        // ]));
        // $metadata->addPropertyConstraint('promotionValue', new Assert\NotBlank([
        //     'message' => 'mautic.fimplus.promotion.value.required',
        // ]));
        // $metadata->addPropertyConstraint('applyTiming', new Assert\NotBlank([
        //     'message' => 'mautic.fimplus.promotion.applytiming.required',
        // ]));
    }
}
