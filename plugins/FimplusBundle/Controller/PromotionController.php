<?php

namespace MauticPlugin\FimplusBundle\Controller;

use Mautic\CoreBundle\Controller\CommonController;
use Mautic\PluginBundle\Helper\IntegrationHelper;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Mautic\CoreBundle\Controller\FormController;
use MauticPlugin\FimplusBundle\Entity\Promotion;
use GuzzleHttp\Client;
use Symfony\Component\HttpFoundation\JsonResponse;
use MauticPlugin\FimplusBundle\Helper\Param;

class PromotionController extends FormController
{
    public function indexAction($page = 1)
    {
        $permissions = $this->get('mautic.security')->isGranted(
            [
                'fimplus:promotions:viewown',
                'fimplus:promotions:viewother',
                'fimplus:promotions:create',
                'fimplus:promotions:editown',
                'fimplus:promotions:editother',
                'fimplus:promotions:deleteown',
                'fimplus:promotions:deleteother',
            ],
            'RETURN_ARRAY'
        );
        $this->setListFilters();

        $model = $this->getModel('fimplus.promotion');

        $session = $this->get('session');
        //set limits
        $limit = $session->get('mautic.plugin.promotion.limit', $this->get('mautic.helper.core_parameters')->getParameter('default_pagelimit'));
        $start = ($page === 1) ? 0 : (($page - 1) * $limit);
        if ($start < 0) {
            $start = 0;
        }

        $search = $this->request->get('search', $session->get('mautic.plugin.promotion.filter', ''));
        $session->set('mautic.promotion.filter', $search);

        //do some default filtering
        $orderBy    = $session->get('mautic.plugin.promotion.orderby', $model->getRepository()->getTableAlias() . '.id');
        $orderByDir = $session->get('mautic.plugin.promotion.orderbydir', 'DESC');

        $filter      = ['string' => $search, 'force' => ''];

        $results = $model->getEntities([
            'start'          => $start,
            'limit'          => $limit,
            'filter'         => $filter,
            'orderBy'        => $orderBy,
            'orderByDir'     => $orderByDir,
            'withTotalCount' => true,
        ]);
        $this->get('session')->set('mautic.plugin.promotion.page', $page);
        $tmpl = $this->request->isXmlHttpRequest() ? $this->request->get('tmpl', 'index') : 'index';
        return $this->delegateView([
            'viewParameters' => [
                'searchValue'      => $search,
                'items'            => $results,
                'page'             => $page,
                'limit'            => $limit,
                'tmpl'             => $tmpl,
                'permissions'      => $permissions,
            ],
            'contentTemplate' => 'FimplusBundle:Promotion:list.html.php',
            'passthroughVars' => [
                'activeLink'    => '#mautic_fimplus_promotion_index',
                'mauticContent' => 'promotion',
                'route'         => $this->generateUrl('mautic_fimplus_promotion_index', ['page' => $page]),
            ]
        ]);
    }


    public function newAction($promotion = null)
    {
        $model = $this->getModel('fimplus.promotion');
        if (!($promotion instanceof Promotion)) {
            $promotion  = $model->getEntity();
        }
        if (!$this->get('mautic.security')->isGranted('fimplus:promotions:create')) {
            return $this->accessDenied();
        }
        //set the page we came from
        $page = $this->get('session')->get('mautic.plugin.promotion.page', 1);
        $viewParameters = ['page' => $page];
        $action = $this->generateUrl('mautic_fimplus_promotion_action', ['objectAction' => 'new']);
        $actionType = ($this->request->getMethod() == 'POST') ? $this->request->request->get('promotion[type]', '', true) : '';
        $actions = $model->getPromotionActions();
        $listPackage = $this->listPackage();
        $form = $model->createForm($promotion, $this->get('form.factory'), $action, [
            'promotionActions' => $actions,
            'actionType'   => $actionType,
            'listPackage' => $listPackage
        ]);

        ///Check for a submitted form and process it
        if ($this->request->getMethod() == 'POST') {
            $actionAjax = $this->get('request')->request->get('actionAjax');
            // var_dump($actionAjax);exit;
            if ($actionAjax == 'loadField') {
                $fieldName = $this->get('request')->request->get('fieldName');
                // var_dump($fieldName);exit;
                switch ($fieldName) {
                    case 'promotionType':
                        $promotionType = $this->get('request')->request->get('promotion')['promotion_type'];
                        $promotion->setPromotionType($promotionType);
                        break;
                    case 'tvodType':
                        $tvodType = $this->get('request')->request->get('promotion')['tvodType'];
                        $promotion->setPromotionType($this->get('request')->request->get('promotion_type'));
                        $promotion->setTvodType($tvodType);
                        break;
                }

                $form = $model->createForm($promotion, $this->get('form.factory'), $action, [
                    'promotionActions' => $actions,
                    'actionType'   => $actionType,
                    'listPackage' => $listPackage
                ]);
                return $this->delegateView(
                    [
                        'viewParameters' => [
                            'tmpl'    => $this->request->isXmlHttpRequest() ? $this->request->get('tmpl', 'index') : 'index',
                            'form'   => $form->createView(),
                            'entity'   => $promotion,
                            'actions' => $actions['actions']
                        ],
                        'contentTemplate' => 'FimplusBundle:Promotion:form.html.php',
                        'passthroughVars' => [
                            'activeLink'    => '#mautic_fimplus_promotion_index',
                            'mauticContent' => 'promotion',
                            'route'         => $this->generateUrl(
                                'mautic_fimplus_promotion_action',
                                [
                                    'objectAction' => 'new',
                                    'objectId'     => $promotion->getId(),
                                ]
                            ),
                        ],
                    ]
                );
            }
            $valid = false;
            if (!$cancelled = $this->isFormCancelled($form)) {
                if ($valid = $this->isFormValid($form)) {
                    //form is valid so process the data
                    $model->saveEntity($promotion);

                    $this->addFlash(
                        'mautic.core.notice.created',
                        [
                            '%name%'      => $promotion->getPromotionName(),
                            '%menu_link%' => 'mautic_fimplus_promotion_index',
                            '%url%'       => $this->generateUrl(
                                'mautic_fimplus_promotion_action',
                                [
                                    'objectAction' => 'edit',
                                    'objectId'     => $promotion->getId(),
                                ]
                            ),
                        ]
                    );

                    if ($form->get('buttons')->get('save')->isClicked()) {
                        $returnUrl = $this->generateUrl('mautic_fimplus_promotion_index', $viewParameters);
                        $template  = 'FimplusBundle:Promotion:index';
                    } else {
                        //return edit view so that all the session stuff is loaded
                        return $this->editAction($promotion->getId(), true);
                    }
                }
            } else {
                $returnUrl = $this->generateUrl('mautic_fimplus_promotion_index', $viewParameters);
                $template  = 'FimplusBundle:Promotion:index';
            }

            if ($cancelled || ($valid && $form->get('buttons')->get('save')->isClicked())) {
                return $this->postActionRedirect(
                    [
                        'returnUrl'       => $returnUrl,
                        'viewParameters'  => $viewParameters,
                        'contentTemplate' => $template,
                        'passthroughVars' => [
                            'activeLink'    => '#mautic_fimplus_promotion_index',
                            'mauticContent' => 'promotion',
                        ],
                    ]
                );
            }
        }

        return $this->delegateView(
            [
                'viewParameters' => [
                    'tmpl'    => $this->request->isXmlHttpRequest() ? $this->request->get('tmpl', 'index') : 'index',
                    'form'   => $form->createView(),
                    'entity'   => $promotion,
                    'actions' => $actions['actions']
                ],
                'contentTemplate' => 'FimplusBundle:Promotion:form.html.php',
                'passthroughVars' => [
                    'activeLink'    => '#mautic_fimplus_promotion_index',
                    'mauticContent' => 'promotion',
                    'route'         => $this->generateUrl(
                        'mautic_fimplus_promotion_action',
                        [
                            'objectAction' => 'new',
                            'objectId'     => $promotion->getId(),
                        ]
                    ),
                ],
            ]
        );
    }
    private function listPackage($key = 1)
    {
        $client   = new Client();
        $url = Param::PACKAGE_URL;
        $token = Param::ACCESS_TOKEN;
        $response = $client->request(
            'GET',
            $url,
            [
                // 'headers' => [
                //     'Access-Token' => $token,
                // ],
                'query' => [
                    'campaignType' => $key
                ]
            ]
        );

        // $statusCode = $response->getStatusCode();
        $data = json_decode($response->getBody()->getContents(), true);
        $listPackage = [];
        if (!empty($data['data'])) {
            foreach ($data['data'] as $package) {
                $listPackage[$package['id']] = $package['description'];
            }
        }
        return $listPackage;
    }
}
