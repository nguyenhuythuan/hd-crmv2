<?php

namespace MauticPlugin\FimplusBundle\Controller;

use Mautic\CoreBundle\Controller\FormController;
use Mautic\PluginBundle\Helper\IntegrationHelper;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use MauticPlugin\RabbitmqBundle\Model\Exchange;
use Mautic\LeadBundle\Controller\LeadController as LCtrl;

class LeadController extends FormController
{
    public function indexAction($page = 1)
    {
        $permissions = $this->get('mautic.security')->isGranted(
            [
                'fimplus:campaign:viewown',
                'fimplus:campaign:viewother',
                'fimplus:campaign:create',
                'fimplus:campaign:editown',
                'fimplus:campaign:editother',
                'fimplus:campaign:deleteown',
                'fimplus:campaign:deleteother',
            ],
            'RETURN_ARRAY'
        );
        $this->setListFilters();

        $model = $this->getModel('fimplus.fcampaign');

        $session = $this->get('session');
        //set limits
        $limit = $session->get('mautic.plugin.fcampaign.limit', $this->get('mautic.helper.core_parameters')->getParameter('default_pagelimit'));
        $start = ($page === 1) ? 0 : (($page - 1) * $limit);
        if ($start < 0) {
            $start = 0;
        }

        $search = $this->request->get('search', $session->get('mautic.plugin.fcampaign.filter', ''));
        $session->set('mautic.fcampaign.filter', $search);

        //do some default filtering
        $orderBy    = $session->get('mautic.plugin.fcampaign.orderby', $model->getRepository()->getTableAlias() . '.id');
        $orderByDir = $session->get('mautic.plugin.fcampaign.orderbydir', 'DESC');

        $filter      = ['string' => $search, 'force' => ''];

        $results = $model->getEntities([
            'start'          => $start,
            'limit'          => $limit,
            'filter'         => $filter,
            'orderBy'        => $orderBy,
            'orderByDir'     => $orderByDir,
            'withTotalCount' => true,
        ]);
        $this->get('session')->set('mautic.plugin.fcampaign.page', $page);
        $tmpl = $this->request->isXmlHttpRequest() ? $this->request->get('tmpl', 'index') : 'index';
        return $this->delegateView([
            'viewParameters' => [
                'searchValue'      => $search,
                'items'            => $results,
                'page'             => $page,
                'limit'            => $limit,
                'tmpl'             => $tmpl,
                'permissions'      => $permissions,
            ],
            'contentTemplate' => 'FimplusBundle:FCampaign:list.html.php',
            'passthroughVars' => [
                'activeLink'    => '#mautic_fimplus_campaign_index',
                'mauticContent' => 'campaign',
                'route'         => $this->generateUrl('mautic_fimplus_campaign_index', ['page' => $page]),
            ]
        ]);
    }
    
    public function infoAction($phone = 0){
        $service = $this->get('mautic.transaction.service.user_model');
        $where = [
            "phone" => (string)$phone
        ];
        $user = $service->getRecord($where,true);
        $id = 0;
        if(isset($user['id'])){
            $id = $user['id'];
        }
        return $this->redirect($this->get('router')
            ->generate(
                'mautic_contact_action',
                [
                    'objectAction' => 'view',
                    'objectId'     => $id,
                ])
            );
    }
}
