<?php

namespace MauticPlugin\FimplusBundle\Controller;

use Mautic\CoreBundle\Controller\FormController;
use Mautic\PluginBundle\Helper\IntegrationHelper;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use MauticPlugin\RabbitmqBundle\Model\Exchange;
use MauticPlugin\FimplusBundle\Entity\PromotionConditionDetail;
use MauticPlugin\FimplusBundle\Entity\FCampaign;
use Proxies\__CG__\MauticPlugin\FimplusBundle\Entity\CampaignActiveCodeDetail;
use GuzzleHttp\Client;
use Symfony\Component\HttpFoundation\JsonResponse;
use MauticPlugin\FimplusBundle\Helper\Param;

class FCampaignController extends FormController
{
    public function indexAction($page = 1)
    {
        $permissions = $this->get('mautic.security')->isGranted(
            [
                'fimplus:campaign:viewown',
                'fimplus:campaign:viewother',
                'fimplus:campaign:create',
                'fimplus:campaign:editown',
                'fimplus:campaign:editother',
                'fimplus:campaign:deleteown',
                'fimplus:campaign:deleteother',
            ],
            'RETURN_ARRAY'
        );
        $this->setListFilters();

        $model = $this->getModel('fimplus.fcampaign');

        $session = $this->get('session');
        //set limits
        $limit = $session->get('mautic.plugin.fcampaign.limit', $this->get('mautic.helper.core_parameters')->getParameter('default_pagelimit'));
        $start = ($page === 1) ? 0 : (($page - 1) * $limit);
        if ($start < 0) {
            $start = 0;
        }

        $search = $this->request->get('search', $session->get('mautic.plugin.fcampaign.filter', ''));
        $session->set('mautic.fcampaign.filter', $search);

        //do some default filtering
        $orderBy    = $session->get('mautic.plugin.fcampaign.orderby', $model->getRepository()->getTableAlias() . '.id');
        $orderByDir = $session->get('mautic.plugin.fcampaign.orderbydir', 'DESC');

        $filter      = ['string' => $search, 'force' => ''];

        $results = $model->getEntities([
            'start'          => $start,
            'limit'          => $limit,
            'filter'         => $filter,
            'orderBy'        => $orderBy,
            'orderByDir'     => $orderByDir,
            'withTotalCount' => true,
        ]);
        $this->get('session')->set('mautic.plugin.fcampaign.page', $page);
        $tmpl = $this->request->isXmlHttpRequest() ? $this->request->get('tmpl', 'index') : 'index';
        return $this->delegateView([
            'viewParameters' => [
                'searchValue'      => $search,
                'items'            => $results,
                'page'             => $page,
                'limit'            => $limit,
                'tmpl'             => $tmpl,
                'permissions'      => $permissions,
            ],
            'contentTemplate' => 'FimplusBundle:FCampaign:list.html.php',
            'passthroughVars' => [
                'activeLink'    => '#mautic_fimplus_campaign_index',
                'mauticContent' => 'campaign',
                'route'         => $this->generateUrl('mautic_fimplus_campaign_index', ['page' => $page]),
            ]
        ]);
    }

    public function newAction($campaign = null)
    {
        $model = $this->getModel('fimplus.fcampaign');

        if (!($campaign instanceof \MauticPlugin\FimplusBundle\Entity\FCampaign)) {
            $campaign  = $model->getEntity();
        }

        if (!$this->get('mautic.security')->isGranted('fimplus:campaigns:create')) {
            return $this->accessDenied();
        }
        $page = $this->get('session')->get('mautic.plugin.campaign.page', 1);
        $viewParameters = ['page' => $page];
        $action = $this->generateUrl('mautic_fimplus_campaign_action', ['objectAction' => 'new']);
        $promotionModel =  $this->getModel('fimplus.promotion');
        $promotionObjectModel =  $this->getModel('fimplus.promotionObject');

        $promotions =  $promotionModel->getRepository()->getPromotions();
        $promotionArr = [];
        foreach ($promotions as $promotion) {
            $promotionArr[$promotion['id']] = $promotion['promotionName'];
        }

        $promotionConditionDetailModel = $this->getModel('fimplus.promotionConditionDetail');
        $activecodeDetailModel = $this->getModel('fimplus.campaignActiveCodeDetail');

        $form = $model->createForm($campaign, $this->get('form.factory'), $action, ['promotion' => $promotionArr]);

        ///Check for a submitted form and process it
        if ($this->request->getMethod() == 'POST') {
            // get form detail
            $getform = $this->get('request')->request->get('getForm');
            $campaignType = $this->get('request')->request->get('fcampaign')['type'];
            if ($campaignType != null) {
                $campaign->setType($campaignType);

                //load template condition detail
                $this->loadTemplateDetail($campaign);
            }
            if ($getform == true) {
                $form = $model->createForm($campaign, $this->get('form.factory'), $action, ['promotion' => $promotionArr]);
                return $this->delegateView(
                    [
                        'viewParameters' => [
                            'tmpl'    => $this->request->isXmlHttpRequest() ? $this->request->get('tmpl', 'index') : 'index',
                            'form'   => $form->createView(),
                            'entity'   => $campaign,
                            // 'actions' => $actions['actions'],
                        ],
                        'contentTemplate' => 'FimplusBundle:FCampaign:form.html.php',
                        'passthroughVars' => [
                            'activeLink'    => '#mautic_fimplus_campaign_index',
                            'mauticContent' => 'fcampaign',
                            'route'         => $this->generateUrl(
                                'mautic_fimplus_campaign_action',
                                [
                                    'objectAction' => 'new',
                                    'objectId'     => $campaign->getId(),
                                ]
                            ),
                        ],
                    ]
                );
            }

            $valid = false;
            if (!$cancelled = $this->isFormCancelled($form)) {
                if ($valid = $this->isFormValid($form)) {
                    $model->saveEntity($campaign);
                    $dataQueue = [];
                    //add campaign to queue
                    $this->addDataToQueue($dataQueue, 'campaign', $campaign);

                    foreach ($campaign->getPromotionConditionDetail() as $conditionDetail) {
                        $conditionDetail->setCampaignId($campaign->getId());
                        $promotionConditionDetailModel->saveEntity($conditionDetail);

                        //add condition detail to queue
                        $this->addDataToQueue($dataQueue, 'conditionDetail', $conditionDetail);
                    }
                    $prefixData  = $activecodeDetailModel->getExistPrefix();
                    $prefixExist = [];
                    foreach($prefixData as $prefix){
                        $prefixExist[] = $prefix['prefix'];
                    }
                    foreach ($campaign->getCampaignActiveCodeDetail() as $activeCodeDetail) {
                        $activeCodeDetail->setFcampaignId($campaign->getId());
                        $activeCodeDetail->setExistPrefix($prefixExist);
                        $activecodeDetailModel->saveEntity($activeCodeDetail);
                        //add activeCodeDetail to queue
                        $this->addDataToQueue($dataQueue, 'activeCodeDetail', $activeCodeDetail);
                    }

                    foreach ($campaign->getPromotion() as $key => $value) {
                        $promotionObject =  $promotionObjectModel->getEntity();
                        $promotionObject->setCampaignId($campaign->getId());
                        $promotionObject->setPromotionId($value);
                        $promotionObjectModel->saveEntity($promotionObject);
                        //add promotion object to queue
                        $this->addDataToQueue($dataQueue, 'promotionObject', $promotionObject);
                    }
                    $this->addFlash(
                        'mautic.core.notice.created',
                        [
                            '%name%'      => $campaign->getName(),
                            '%menu_link%' => 'mautic_fimplus_campaign_index',
                            '%url%'       => $this->generateUrl(
                                'mautic_fimplus_campaign_action',
                                [
                                    // 'objectAction' => 'edit',
                                    'objectAction' => 'new',
                                    //'objectId'     => $campaign->getId(),
                                ]
                            ),
                        ]
                    );
                    if ($form->get('buttons')->get('save')->isClicked()) {
                        // push data to queue
                        $message = (string) json_encode($dataQueue);
                        $exName = 'Mautic.Campaign';
                        $routingKey = 'mautic.campaign.data';
                        // $exchange = new Exchange();
                        // $result = $exchange->pushMessageToEx($message,$routingKey,$exName);
                        // end queue
                        //log result
                        $returnUrl = $this->generateUrl('mautic_fimplus_campaign_index', $viewParameters);
                        $template  = 'FimplusBundle:FCampaign:index';
                    } else {
                        //return edit view so that all the session stuff is loaded
                        return $this->editAction($campaign->getId(), true);
                    }
                }
            } else {
                $returnUrl = $this->generateUrl('mautic_fimplus_campaign_index', $viewParameters);
                $template  = 'FimplusBundle:FCampaign:index';
            }

            if ($cancelled || ($valid && $form->get('buttons')->get('save')->isClicked())) {

                return $this->postActionRedirect(
                    [
                        'returnUrl'       => $returnUrl,
                        'viewParameters'  => $viewParameters,
                        'contentTemplate' => $template,
                        'passthroughVars' => [
                            'activeLink'    => '#mautic_fimplus_campaign_index',
                            'mauticContent' => 'fcampaign',
                        ],
                    ]
                );
            }
        }
        return $this->delegateView(
            [
                'viewParameters' => [
                    'tmpl'    => 'index',
                    'form'   => $form->createView(),
                    'entity'   => $campaign,
                ],
                'contentTemplate' => 'FimplusBundle:FCampaign:form.html.php',
                'passthroughVars' => [
                    'activeLink'    => '#mautic_fimplus_campaign_index',
                    'mauticContent' => 'fcampaign',
                    'route'         => $this->generateUrl(
                        'mautic_fimplus_campaign_action',
                        [
                            'objectAction' => 'new',
                            'objectId'     => $campaign->getId(),
                        ]
                    ),
                ],
            ]
        );
    }
    private function loadTemplateDetail(&$campaign)
    {
        switch ($campaign->getType()) {
            case FCampaign::ACTIVE_CODE:
                $activeCodeDetail = new CampaignActiveCodeDetail();
                $campaign->getCampaignActiveCodeDetail()->add($activeCodeDetail);
                break;
            case FCampaign::BUY_SVOD_GET_TVOD:
                //buy svod get tvod must have 2 condition of fact: type and product id
                $conditionType = new PromotionConditionDetail();
                $conditionProductId = new PromotionConditionDetail();
                $conditionType->setFact(PromotionConditionDetail::FACT['TYPE']);
                $conditionType->setOperator(PromotionConditionDetail::OPERATOR['EQUAL']);
                $conditionType->setValue('SVOD');
                $conditionType->setTypeValue(PromotionConditionDetail::TYPE_VALUE['STRING']);

                $campaign->getPromotionConditionDetail()->add($conditionType);
                $conditionProductId->setFact(PromotionConditionDetail::FACT['PRODUCT_ID']);
                $conditionProductId->setOperator(PromotionConditionDetail::OPERATOR['IN']);
                $conditionProductId->setTypeValue(PromotionConditionDetail::TYPE_VALUE['JSON']);
                $campaign->getPromotionConditionDetail()->add($conditionProductId);

                break;
            case FCampaign::AUTO_ACTIVE:
            case FCampaign::CREDIT_CARD:
            case FCampaign::DISCOUNT:
            case FCampaign::TRIAL:

                break;
        }
    }
    private function addDataToQueue(&$queue, $type, $object)
    {
        switch ($type) {
            case 'campaign':
                $queue['campaign'] = [
                    'id' => $object->getUuid(),
                    'name' => $object->getName(),
                    'description' => $object->getDescription(),
                    'priority' => $object->getPriority(),
                    'type' => $object->getType(),
                    'applyTimes' => $object->getApplyTimes(),
                    'preventApplySameType' => $object->getPreventApplySameType(),
                    'eventPage' => explode(',', $object->getEventPage()),
                    'startDate' => $object->getStartDate(),
                    'endDate' => $object->getEndDate(),
                    'status' => $object->getIsPublished(),
                    'distributor' => $object->getDistributor(),
                    'createdBy' => $object->getCreatedByUser()
                ];
                break;
            case 'conditionDetail':
                $queue['conditionDetail'][] = [
                    'id' => $object->getUuid(),
                    'fact' => $object->getFact(),
                    'operator' => $object->getOperator(),
                    'value' => $object->getValue(),
                    'typeValue' => strtolower($object->getTypeValueLabel($object->getTypeValue())),
                    'order' => $object->getOrders(),
                ];
                break;
            case 'promotionObject':
                $promotionUuid = $this->getModel('fimplus.promotion')->getEntity($object->getPromotionId())->getUuid();
                $queue['promotionObject'][] = [
                    'id' => $object->getUuid(),
                    'addon_id' => $promotionUuid,
                    'status' => $object->getIsPublished()
                ];
                break;
            case 'conditionDetail':
                $queue['conditionDetail'] = [
                    // 'partner'  => $object->getPartner(),
                    'acConsumerName' => $object->getPartnerLabel($object->getPartner()),
                    //'codeType' => $object->getCodeType(),
                    //'codeTypeLabel' => $object->getCodeTypeLabel($object->getCodeType()),
                   // 'package' => $object->getPackage(),
                   // 'film' => explode(',', $object->getFilm()),
                    'acCodeQuantity' => $object->getCodeValue(),
                    'acApplySuccessMessage' => $object->getApplySuccessMessage(),
                    'acConfigNote' => $object->getConfigNote(),
                    'acUnitPrice' => $object->getOriginalPrice(),
                    'status' => $object->getStatus()

                ];
                break;
        }
    }
    public function listFilmAction(){
        if ($this->request->getMethod() == 'POST') {
            $query = $this->request->get('search');

            $res = new JsonResponse();
            $listFilm = $this->listFilmForSelect($query);
            $res->setData($listFilm);
            return $res;
        }
    }
    private function listFilmForSelect($key,$size = 20){
        $client   = new Client();
        $url = Param::SEARCH_FILM_URL;
        $token = Param::ACCESS_TOKEN;
        $response = $client->request('GET', $url,
            [
                'headers' => [
                    'Access-Token' => $token,
                ],
                'query' => [
                    'q' => $key,
                    'size' => $size
                ]
            ]);

        // $statusCode = $response->getStatusCode();
        $data = json_decode($response->getBody()->getContents(),true);
        $listFilm = [];
        if(!empty($data['docs'])){
            foreach($data['docs'] as $film){
                $listFilm[$film['titleId']] = $film['alternateName'];
            }
        }
        return $listFilm;
    }
}
