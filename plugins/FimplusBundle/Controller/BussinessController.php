<?php

namespace MauticPlugin\FimplusBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Console\Application;
use Symfony\Component\Console\Input\ArrayInput;
use Symfony\Component\Console\Output\BufferedOutput;
use Symfony\Component\Console\Output\NullOutput;
use Mautic\CoreBundle\Controller\FormController;
use MauticPlugin\FimplusBundle\Entity\BussinessInvoice;
use GuzzleHttp\Client;
use Symfony\Component\HttpFoundation\JsonResponse;
use MauticPlugin\FimplusBundle\Helper\Param;
use MauticPlugin\FimplusBundle\Helper\Coupon;

class BussinessController extends FormController
{
    public function writeLogz($message)
    {
        $filename =  __DIR__ . "/../Logs/CouponPrefix.log";
        $myfile = fopen($filename, "a+") or die("Unable to open file!aaa");
        file_put_contents($filename, $message . PHP_EOL, FILE_APPEND | LOCK_EX);
        fclose($myfile);
    }
    public function indexAction($page = 1)
    {
        $permissions = $this->get('mautic.security')->isGranted(
            [
                'fimplus:bussiness_invoice:viewown',
                'fimplus:bussiness_invoice:viewother',
                'fimplus:bussiness_invoice:create',
                'fimplus:bussiness_invoice:editown',
                'fimplus:bussiness_invoice:editother',
                'fimplus:bussiness_invoice:deleteown',
                'fimplus:bussiness_invoice:deleteother',
            ],
            'RETURN_ARRAY'
        );
        $this->setListFilters();

        $model = $this->getModel('fimplus.bussinessInvoice');

        $session = $this->get('session');
        //set limits
        $limit = $session->get('mautic.plugin.bussinessInvoice.limit', $this->get('mautic.helper.core_parameters')->getParameter('default_pagelimit'));
        $start = ($page === 1) ? 0 : (($page - 1) * $limit);
        if ($start < 0) {
            $start = 0;
        }

        $search = $this->request->get('search', $session->get('mautic.plugin.bussinessInvoice.filter', ''));
        $session->set('mautic.bussinessInvoice.filter', $search);

        //do some default filtering
        $orderBy    = $session->get('mautic.plugin.bussinessInvoice.orderby', $model->getRepository()->getTableAlias() . '.id');
        $orderByDir = $session->get('mautic.plugin.bussinessInvoice.orderbydir', 'DESC');

        $filter      = ['string' => $search, 'force' => ''];

        $results = $model->getEntities([
            'start'          => $start,
            'limit'          => $limit,
            'filter'         => $filter,
            'orderBy'        => $orderBy,
            'orderByDir'     => $orderByDir,
            'withTotalCount' => true,
        ]);
        $this->get('session')->set('mautic.plugin.bussinessInvoice.page', $page);
        $tmpl = $this->request->isXmlHttpRequest() ? $this->request->get('tmpl', 'index') : 'index';
        return $this->delegateView([
            'viewParameters' => [
                'searchValue'      => $search,
                'items'            => $results,
                'page'             => $page,
                'limit'            => $limit,
                'tmpl'             => $tmpl,
                'permissions'      => $permissions,
            ],
            'contentTemplate' => 'FimplusBundle:BussinessInvoice:list.html.php',
            'passthroughVars' => [
                'activeLink'    => '#mautic_fimplus_bussiness_invoice_index',
                'mauticContent' => 'bussinessInvoice',
                'route'         => $this->generateUrl('mautic_fimplus_bussiness_invoice_index', ['page' => $page]),
            ]
        ]);
    }


    public function newAction($bussinessInvoice = null)
    {   
        // $bsModel = $this->getModel('fimplus.bussinessInvoice');
        // $ordersData  = $bsModel->getExistOrderByCampaign(71);
        // $orderExist = [];
        // foreach($ordersData as $order){
        //     $orderExist[] = $order['orders'];
        // }
        // $orders = Coupon::returnPreFix($orderExist,1);
        // var_dump($orders);exit;

        $model = $this->getModel('fimplus.bussinessInvoice');
        if (!($bussinessInvoice instanceof BussinessInvoice)) {
            $bussinessInvoice  = $model->getEntity();
        }
        if (!$this->get('mautic.security')->isGranted('fimplus:bussiness_invoice:create')) {
            return $this->accessDenied();
        }
        //set the page we came from
        $page = $this->get('session')->get('mautic.plugin.bussinessInvoice.page', 1);
        $viewParameters = ['page' => $page];
        $action = $this->generateUrl('mautic_fimplus_bussiness_invoice_action', ['objectAction' => 'new']);
        $form = $model->createForm($bussinessInvoice, $this->get('form.factory'), $action);

        ///Check for a submitted form and process it
        if ($this->request->getMethod() == 'POST') {
            $valid = false;
            if (!$cancelled = $this->isFormCancelled($form)) {
                $campaignId = $this->get('request')->request->get('bussiness_invoice')['campaign_id'];
                if (is_numeric($campaignId)) {
                    $modelCampaign = $this->getModel('fimplus.Fcampaign');
                    $campaign = $modelCampaign->getEntity($campaignId);
                    $activeCode = $this->getModel('fimplus.campaignActiveCodeDetail')->getRepository()->findOneBy(['fcampaignId' => $campaignId]);
                    $campaignQuantity = $activeCode->getCodeValue();
                    $bsModel = $this->getModel('fimplus.bussinessInvoice');
                    $totalCodeCreated = $bsModel->getTotalCodeCreated($campaignId)[0]['total'];
                    if (!empty($campaign)) {
                        $bussinessInvoice->setCampaign($campaign);
                        $bussinessInvoice->setCampaignQuantity($campaignQuantity);
                        $bussinessInvoice->setTotalCodeCreated($totalCodeCreated);
                    }
                }
                if ($valid = $this->isFormValid($form)) {
                    //form is valid so process the data
                    $ordersData  = $bsModel->getExistOrderByCampaign($campaignId);
                    $orderExist = [];
                    foreach($ordersData as $order){
                        $orderExist[] = $order['orders'];
                    }
                    $orders = substr(Coupon::returnPreFix($orderExist,1),0,1);
                    $bussinessInvoice->setOrders($orders);
                    $totalPrice = $bussinessInvoice->getQuantity() * $activeCode->getOriginalPrice();
                    $bussinessInvoice->setTotalPrice($totalPrice);
                    $model->saveEntity($bussinessInvoice);
                    /**
                     * generate code
                     * with multi comand
                     * */
                    $prefix = $activeCode->getPrefix() . $orders;
                    $suffix = 'false';
                    $devide = floor($bussinessInvoice->getQuantity() / Param::MAX_COMMAND);
                    $mod = $bussinessInvoice->getQuantity() % Param::MAX_COMMAND;
                    $length = Param::LENGTH_CODE_WITHOUT_PRE;
                    $hasChecksum = 'true';
                    $invoiceId = $bussinessInvoice->getId();
                    /* var_dump("php " . MAUTIC_ROOT_DIR . "/app/console fimplus:coupon:generate {$campaignId} {$invoiceId} {$length} {$mod} {$prefix} {$suffix} {$hasChecksum} 0> /dev/null &");
                    exit; */
                    
                    try{
                        if ($mod != 0) {
                            $script = "php " . MAUTIC_ROOT_DIR . "/app/console fimplus:coupon:generate {$campaignId} {$invoiceId} {$length} {$mod} {$prefix} {$suffix} {$hasChecksum} 6> /dev/null &";
                            exec($script);
                        }
                        if ($devide > 0) {
                            $quantity = $devide;
                            $times = Param::MAX_COMMAND;
                            for ($i = 1; $i <= $times; $i++) {
                                exec("php " . MAUTIC_ROOT_DIR . "/app/console fimplus:coupon:generate {$campaignId} {$invoiceId} {$length} {$quantity} {$prefix} {$suffix} {$hasChecksum} {$i} > /dev/null &");
                                usleep(100);
                            }
                        }
                    }catch(\Exception $e){
                        throw $e;
                    }

                    $this->addFlash(
                        'mautic.core.notice.created',
                        [
                            '%name%'      => 'Bussiness Invoice',
                            '%menu_link%' => 'mautic_fimplus_bussiness_invoice_index',
                            '%url%'       => $this->generateUrl(
                                'mautic_fimplus_bussiness_invoice_action',
                                [
                                    'objectAction' => 'new',
                                    // 'objectId'     => $bussinessInvoice->getId(),
                                ]
                            ),
                        ]
                    );

                    if ($form->get('buttons')->get('save')->isClicked()) {
                        $returnUrl = $this->generateUrl('mautic_fimplus_bussiness_invoice_index', $viewParameters);
                        $template  = 'FimplusBundle:Bussiness:index';
                    } else {
                        //return edit view so that all the session stuff is loaded
                        return $this->editAction($bussinessInvoice->getId(), true);
                    }
                }
            } else {
                $returnUrl = $this->generateUrl('mautic_fimplus_bussiness_invoice_index', $viewParameters);
                $template  = 'FimplusBundle:Bussiness:index';
            }

            if ($cancelled || ($valid && $form->get('buttons')->get('save')->isClicked())) {
                return $this->postActionRedirect(
                    [
                        'returnUrl'       => $returnUrl,
                        'viewParameters'  => $viewParameters,
                        'contentTemplate' => $template,
                        'passthroughVars' => [
                            'activeLink'    => '#mautic_fimplus_bussiness_invoice_index',
                            'mauticContent' => 'businessInvoice',
                        ],
                    ]
                );
            }
        }

        return $this->delegateView(
            [
                'viewParameters' => [
                    'tmpl'    => $this->request->isXmlHttpRequest() ? $this->request->get('tmpl', 'index') : 'index',
                    'form'   => $form->createView(),
                    'entity'   => $bussinessInvoice,
                ],
                'contentTemplate' => 'FimplusBundle:BussinessInvoice:form.html.php',
                'passthroughVars' => [
                    'activeLink'    => '#mautic_fimplus_bussiness_invoice_index',
                    'mauticContent' => 'businessInvoice',
                    'route'         => $this->generateUrl(
                        'mautic_fimplus_bussiness_invoice_action',
                        [
                            'objectAction' => 'new',
                            'objectId'     => $bussinessInvoice->getId(),
                        ]
                    ),
                ],
            ]
        );
    }
    private function listPackage($key = 1)
    {
        $client   = new Client();
        $url = Param::PACKAGE_URL;
        $token = Param::ACCESS_TOKEN;
        $response = $client->request(
            'GET',
            $url,
            [
                // 'headers' => [
                //     'Access-Token' => $token,
                // ],
                'query' => [
                    'campaignType' => $key
                ]
            ]
        );

        // $statusCode = $response->getStatusCode();
        $data = json_decode($response->getBody()->getContents(), true);
        $listPackage = [];
        if (!empty($data['data'])) {
            foreach ($data['data'] as $package) {
                $listPackage[$package['id']] = $package['description'];
            }
        }
        return $listPackage;
    }
}
