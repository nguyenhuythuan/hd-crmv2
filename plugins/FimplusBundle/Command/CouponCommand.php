<?php

namespace MauticPlugin\FimplusBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputOption;
use Psr\Log\LoggerInterface;
use MauticPlugin\FimplusBundle\Helper\Coupon;
use MauticPlugin\FimplusBundle\Entity\BussinessCode;

class CouponCommand extends ContainerAwareCommand
{
    private $logger;

    protected function configure()
    {
        $this->setName('fimplus:coupon:generate')
            ->addArgument(
                'campaignId',
                InputArgument::REQUIRED,
                'Campaign ID is required'
            )
            ->addArgument(
                'invoiceId',
                InputArgument::REQUIRED,
                'Invoice ID is required'
            )
            ->addArgument(
                'length',
                InputArgument::REQUIRED,
                'Length is required'
            )
            ->addArgument(
                'quantity',
                InputArgument::REQUIRED,
                'Quantity is required'
            )
            ->addArgument(
                'prefix',
                InputArgument::OPTIONAL,
                'Prefix is optional'
            )
            ->addArgument(
                'suffix',
                InputArgument::OPTIONAL,
                'Suffix is optional'
            )
            ->addArgument(
                'hasChecksum',
                InputArgument::OPTIONAL,
                'hasChecksum is optional'
            )
            ->addArgument(
                'nth',
                InputArgument::OPTIONAL,
                'Begin is optional'
            );
        parent::configure();
        // ...
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        
        $quantity = $input->getArgument('quantity');
        $campaignId = $input->getArgument('campaignId');
        $invoiceId = $input->getArgument('invoiceId');
        $nth = $input->getArgument('nth');
        $options = [
            'prefix' => $input->getArgument('prefix'),
            'suffix' => $input->getArgument('suffix'),
            'hasChecksum' => $input->getArgument('hasChecksum'),
            'length' => $input->getArgument('length'),
        ];
        $data = [
            "invoiceId" => $invoiceId,
            "options" => $options,
            "campaignId" => $campaignId,
            "quantity" => (int) $quantity
        ];
        $count = 0;
        $manager =  $this->getContainer()->get('doctrine')->getManager();
        
        $filename =  __DIR__ . "/../Logs/err.log";
        $myfile = fopen($filename, "a+") or die("Unable to open file!aaa");
        file_put_contents($filename, "command {$nth} : start :".date('H:i:s'). PHP_EOL, FILE_APPEND | LOCK_EX);
        
        $this->generateCode($data, $count, $manager);

        file_put_contents($filename, "command {$nth} : end :".date('H:i:s'). PHP_EOL, FILE_APPEND | LOCK_EX);
        fclose($myfile);
        
    }
    
   
    protected function generateCode($data, $count, $manager)
    {
        $invoiceId      = $data["invoiceId"];
        $options        = $data["options"];
        $campaignId     = $data["campaignId"];
        $quantity       = $data["quantity"];
        $batchSize = 20;
        while ($count < $quantity) {
            echo 'Count: ' .$count . "\n";
            $codeEntity = new BussinessCode();
            $code = Coupon::basicGenerate($options);
            echo "code: " . $code . "\n";
            $serial = Coupon::generateSerial();
            echo "serial: " . $serial . "\n";
            echo "suffix: " . $options['suffix'] . "\n";
            echo  "\n";

            $codeEntity->setCampaignId($campaignId);
            $codeEntity->setInvoiceId($invoiceId);
            $codeEntity->setCode($code);
            $codeEntity->setStatus(BussinessCode::STATUS_ACTIVE);
            $codeEntity->setSerial($serial);
            $validator = $this->getContainer()->get('validator');
            $errors = $validator->validate($codeEntity);
            if (count($errors) <= 0) {
                $manager->persist($codeEntity);
                // if (($count % $batchSize) === 0) {
                    $manager->flush();
                    $manager->clear(); // Detaches all objects from Doctrine!
                    usleep(1000);
                //  }
                $count++;
            }
            
        }
        $manager->flush();
        $manager->clear(); // Detaches all objects from Doctrine!
    }
}
