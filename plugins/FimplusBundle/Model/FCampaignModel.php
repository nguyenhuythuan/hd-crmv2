<?php

/*
 * @copyright   2014 Mautic Contributors. All rights reserved
 * @author      Mautic
 *
 * @link        http://mautic.org
 *
 * @license     GNU/GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
 */

namespace MauticPlugin\FimplusBundle\Model;

use Doctrine\DBAL\Query\Expression\ExpressionBuilder;
use Mautic\CoreBundle\Model\FormModel as CommonFormModel;
use Symfony\Component\HttpKernel\Exception\MethodNotAllowedHttpException;
use MauticPlugin\FimplusBundle\Event\PromotionBuilderEvent;
use MauticPlugin\FimplusBundle\FimplusEvents;
use MauticPlugin\FimplusBundle\Entity\FCampaign;

/**
 * Class CampaignModel.
 */
class FCampaignModel extends CommonFormModel
{
    public function getRepository()
    {
        return $this->em->getRepository('FimplusBundle:FCampaign');
    }
    public function createForm($entity, $formFactory, $action = null, $options = [])
    {
        if (!$entity instanceof FCampaign) {
            throw new MethodNotAllowedHttpException(['Campaign Fimplus']);
        }
        if (!empty($action)) {
            $options['action'] = $action;
        }
        // if (empty($options['campaignActions'])) {
        //     $options['campaignActions'] = $this->getCampaignActions();
        // }
        $form = $formFactory->create('fcampaign', $entity, $options);
        return  $form;
    }
    public function getEntity($id = null)
    {
        if ($id === null) {
            return new FCampaign();
        }

        $entity = parent::getEntity($id);

        return $entity;
    }
      
   /*  public function getCampaignActions()
    {
        static $actions;

        if (empty($actions)) {
            //build them
            $actions = [];
            $event   = new PromotionBuilderEvent($this->translator);
            $this->dispatcher->dispatch(FimplusEvents::PROMOTION_ON_BUILD, $event);
            $actions['actions'] = $event->getActions();
            $actions['list']    = $event->getActionList();
            $actions['choices'] = $event->getActionChoices();
        }

        return $actions;
    } */
}
