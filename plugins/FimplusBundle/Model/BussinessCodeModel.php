<?php

/*
 * @copyright   2014 Mautic Contributors. All rights reserved
 * @author      Mautic
 *
 * @link        http://mautic.org
 *
 * @license     GNU/GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
 */

namespace MauticPlugin\FimplusBundle\Model;

use Mautic\CoreBundle\Model\FormModel as CommonFormModel;
use Symfony\Component\HttpKernel\Exception\MethodNotAllowedHttpException;
use MauticPlugin\FimplusBundle\Entity\BussinessCode;

/**
 * Class PromotionModel.
 */
class BussinessCodeModel extends CommonFormModel
{
    public function getRepository()
    {
        return $this->em->getRepository('FimplusBundle:BussinessCode');
    }
    public function getEntity($id = null)
    {
        if ($id === null) {
            return new BussinessCode();
        }

        $entity = parent::getEntity($id);

        return $entity;
    }
}
