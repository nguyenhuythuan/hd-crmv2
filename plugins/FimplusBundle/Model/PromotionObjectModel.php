<?php

/*
 * @copyright   2014 Mautic Contributors. All rights reserved
 * @author      Mautic
 *
 * @link        http://mautic.org
 *
 * @license     GNU/GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
 */

namespace MauticPlugin\FimplusBundle\Model;


use Mautic\CoreBundle\Model\FormModel as CommonFormModel;
use Symfony\Component\HttpKernel\Exception\MethodNotAllowedHttpException;
use MauticPlugin\FimplusBundle\Entity\PromotionObject;

/**
 * Class PromotionObjectModel.
 */
class PromotionObjectModel extends CommonFormModel
{
    public function getRepository()
    {
        return $this->em->getRepository('FimplusBundle:PromotionObject');
    }
    public function createForm($entity, $formFactory, $action = null, $options = [])
    {
        if (!$entity instanceof PromotionObject) {
            throw new MethodNotAllowedHttpException(['Promotion Object']);
        }
        if (!empty($action)) {
            $options['action'] = $action;
        }
       
        $form = $formFactory->create('promotionObject', $entity, $options);
        return  $form;
    }
    public function getEntity($id = null)
    {
        if ($id === null) {
            return new PromotionObject();
        }

        $entity = parent::getEntity($id);

        return $entity;
    }
    
}
