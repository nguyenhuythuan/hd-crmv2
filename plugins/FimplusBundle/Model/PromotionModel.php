<?php

/*
 * @copyright   2014 Mautic Contributors. All rights reserved
 * @author      Mautic
 *
 * @link        http://mautic.org
 *
 * @license     GNU/GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
 */

namespace MauticPlugin\FimplusBundle\Model;

use Doctrine\DBAL\Query\Expression\ExpressionBuilder;
use Mautic\CoreBundle\Form\RequestTrait;
use Mautic\CoreBundle\Helper\DateTimeHelper;
use Mautic\CoreBundle\Helper\InputHelper;
use Mautic\CoreBundle\Model\AjaxLookupModelInterface;
use Mautic\CoreBundle\Model\FormModel as CommonFormModel;
use Mautic\EmailBundle\Helper\EmailValidator;
use Symfony\Component\EventDispatcher\Event;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\HttpKernel\Exception\MethodNotAllowedHttpException;
use MauticPlugin\FimplusBundle\Entity\Promotion;
use MauticPlugin\FimplusBundle\Event\PromotionBuilderEvent;
use MauticPlugin\FimplusBundle\Event\PromotionEvent;
use MauticPlugin\FimplusBundle\FimplusEvents;

/**
 * Class PromotionModel.
 */
class PromotionModel extends CommonFormModel
{
    public function getRepository()
    {
        return $this->em->getRepository('FimplusBundle:Promotion');
    }
    public function createForm($entity, $formFactory, $action = null, $options = [])
    {
        if (!$entity instanceof Promotion) {
            throw new MethodNotAllowedHttpException(['Promotion']);
        }
        if (!empty($action)) {
            $options['action'] = $action;
        }
        if (empty($options['promotionActions'])) {
            $options['promotionActions'] = $this->getPromotionActions();
        }
        $form = $formFactory->create('promotion', $entity, $options);
        return  $form;
    }
    public function getEntity($id = null)
    {
        if ($id === null) {
            return new Promotion();
        }

        $entity = parent::getEntity($id);

        return $entity;
    }
      /**
     * Gets array of custom actions from bundles subscribed PromotionEvents::POINT_ON_BUILD.
     *
     * @return mixed
     */
    public function getPromotionActions()
    {
        static $actions;

        if (empty($actions)) {
            //build them
            $actions = [];
            $event   = new PromotionBuilderEvent($this->translator);
            $this->dispatcher->dispatch(FimplusEvents::PROMOTION_ON_BUILD, $event);
            $actions['actions'] = $event->getActions();
            $actions['list']    = $event->getActionList();
            $actions['choices'] = $event->getActionChoices();
        }

        return $actions;
    }
}
