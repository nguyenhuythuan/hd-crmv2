<?php

/*
 * @copyright   2014 Mautic Contributors. All rights reserved
 * @author      Mautic
 *
 * @link        http://mautic.org
 *
 * @license     GNU/GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
 */

namespace MauticPlugin\FimplusBundle\Model;

use Doctrine\DBAL\Query\Expression\ExpressionBuilder;
use Mautic\CoreBundle\Model\FormModel as CommonFormModel;
use Symfony\Component\HttpKernel\Exception\MethodNotAllowedHttpException;
use MauticPlugin\FimplusBundle\Entity\CampaignActiveCodeDetail;

/**
 * Class CampaignModel.
 */
class CampaignActiveCodeDetailModel extends CommonFormModel
{
    public function getRepository()
    {
        return $this->em->getRepository('FimplusBundle:CampaignActiveCodeDetail');
    }
    public function createForm($entity, $formFactory, $action = null, $options = [])
    {
        if (!$entity instanceof FCampaign) {
            throw new MethodNotAllowedHttpException(['Campaign Active Code']);
        }
        if (!empty($action)) {
            $options['action'] = $action;
        }
       
        $form = $formFactory->create('campaign_active_code_detail', $entity, $options);
        return  $form;
    }
    public function getEntity($id = null)
    {
        if ($id === null) {
            return new CampaignActiveCodeDetail();
        }

        $entity = parent::getEntity($id);

        return $entity;
    }
    public function getExistPrefix(){
        $q = $this->em->createQueryBuilder();
        $q->select('ac.prefix as prefix')
        ->from('FimplusBundle:campaignActiveCodeDetail', 'ac');
        return $q->getQuery()->getResult();
    }
}
