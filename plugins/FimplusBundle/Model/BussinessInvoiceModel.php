<?php

/*
 * @copyright   2014 Mautic Contributors. All rights reserved
 * @author      Mautic
 *
 * @link        http://mautic.org
 *
 * @license     GNU/GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
 */

namespace MauticPlugin\FimplusBundle\Model;

use Mautic\CoreBundle\Model\FormModel as CommonFormModel;
use Symfony\Component\HttpKernel\Exception\MethodNotAllowedHttpException;
use MauticPlugin\FimplusBundle\Entity\BussinessInvoice;

/**
 * Class PromotionModel.
 */
class BussinessInvoiceModel extends CommonFormModel
{
    public function getRepository()
    {
        return $this->em->getRepository('FimplusBundle:BussinessInvoice');
    }
    public function createForm($entity, $formFactory, $action = null, $options = [])
    {
        if (!$entity instanceof BussinessInvoice) {
            throw new MethodNotAllowedHttpException(['Bussiness Invoice']);
        }
        if (!empty($action)) {
            $options['action'] = $action;
        }
        $form = $formFactory->create('bussiness_invoice', $entity, $options);
        return  $form;
    }
    public function getEntity($id = null)
    {
        if ($id === null) {
            return new BussinessInvoice();
        }

        $entity = parent::getEntity($id);

        return $entity;
    }
    public function getActiveCodeCampaign($status = null)
    {
        $q = $this->em->createQueryBuilder();
        $q->select('c')
        ->from('FimplusBundle:FCampaign', 'c')
        ->join('FimplusBundle:CampaignActiveCodeDetail', 'ac')
        ->where('c.id = ac.fcampaignId');
        if($status != null && is_numeric($status)){
            $q->andWhere($q->expr()->eq('ac.status', ':status'))
            ->setParameter('status', $status);
        }
        return $q->getQuery()->getResult();
    }
    public function getTotalCodeCreated($campaignId){
        $q = $this->em->createQueryBuilder();
        $q->select('SUM(bi.quantity) as total')
        ->from('FimplusBundle:BussinessInvoice', 'bi')
        ->where($q->expr()->eq('bi.campaignId', ':campaignId'))
        ->setParameter('campaignId', $campaignId);
        return $q->getQuery()->getResult();
    }
    public function getExistOrderByCampaign($campaignId){
        $q = $this->em->createQueryBuilder();
        $q->select('bi.orders as orders')
        ->from('FimplusBundle:BussinessInvoice', 'bi')
        ->where($q->expr()->eq('bi.campaignId', ':campaignId'))
        ->setParameter('campaignId', $campaignId);
        return $q->getQuery()->getResult();
    }
}
