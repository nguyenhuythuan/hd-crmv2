<?php
use MauticPlugin\FimplusBundle\Entity\Promotion;
/*
 * @copyright   2014 Mautic Contributors. All rights reserved
 * @author      Mautic
 *
 * @link        http://mautic.org
 *
 * @license     GNU/GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
 */
$view->extend('MauticCoreBundle:Default:content.html.php');
$header = ($entity->getId()) ?
    $view['translator']->trans(
        'mautic.promotion.menu.edit',
        ['%name%' => $view['translator']->trans($entity->getName())]
    ) : $view['translator']->trans('mautic.promotion.menu.new');
$view['slots']->set('headerTitle', $header);
echo $view['assets']->includeScript('plugins/FimplusBundle/Assets/js/select2.full.min.js');
echo $view['assets']->includeStylesheet('plugins/FimplusBundle/Assets/css/select2.min.css');
echo $view['assets']->includeScript('plugins/FimplusBundle/Assets/js/promotion.js');
$view['slots']->set('mauticContent', 'promotion');
echo $view['form']->start($form);
?>
<!-- start: box layout -->
<div class="box-layout">
    <!-- container -->
    <div class="col-md-9 bg-auto height-auto bdr-r">
        <div class="row">
            <div class="col-md-6">
                <div class="pa-md">
                    <?php echo $view['form']->row($form['promotion_name']); ?>
                    <?php echo $view['form']->row($form['promotion_type']); ?>
                    <?php echo $view['form']->row($form['promotion_value']); ?>
                    <?php echo $view['form']->row($form['apply_timing']); ?>
                    <?php echo $view['form']->row($form['waiting_apply_duration']); ?>
                    <?php echo $view['form']->row($form['force_expire_date']); ?>
                </div>
            </div>
            <div class="col-md-6">
                <div class="pa-md">
                    <div class='container-svod'>
                        <?php if ($entity->getPromotionType() == Promotion::PROMOTION_TYPE['SVOD_GIFT']) : ?>
                            <div class='content-svod'>
                                <div class='container-package'>
                                    <?php if (isset($form['package'])) : ?>
                                        <div class='content-package'>
                                            <?php echo $view['form']->row($form['package']); ?>
                                        </div>
                                    <?php endif; ?>
                                </div>
                            </div>
                        <?php endif; ?>
                    </div>
                    <div class='container-tvod'>
                        <?php if ($entity->getPromotionType() == Promotion::PROMOTION_TYPE['TVOD_GIFT']) : ?>
                            <div class='content-tvod'>
                                <?php if (isset($form['tvodType'])) : ?>
                                    <?php echo $view['form']->row($form['tvodType']); ?>
                                <?php endif; ?>
                                <div class='container-film'>
                                    <?php if (isset($form['film'])) : ?>
                                        <div class='content-film'>
                                            <?php echo $view['form']->row($form['film']); ?>
                                        </div>
                                    <?php endif; ?>
                                </div>
                                <?php if (isset($form['tvod_apply_condition'])) : ?>
                                    <?php echo $view['form']->row($form['tvod_apply_condition']); ?>
                                <?php endif; ?>
                            </div>
                        <?php endif; ?>
                    </div>

                </div>
            </div>
        </div>
    </div>
    <div class="col-md-3 bg-white height-auto">
        <div class="pr-lg pl-lg pt-md pb-md">
            <?php // echo $view['form']->row($form['weight']); 
            ?>
            <?php
            //  echo $view['form']->row($form['isPublished']);
            // echo $view['form']->row($form['publishUp']);
            // echo $view['form']->row($form['publishDown']);
            ?>
        </div>
    </div>
</div>
<?php echo $view['form']->end($form); ?>