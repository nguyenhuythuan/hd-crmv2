<tr>
    <td>
        <?php echo $item->getUuid(); ?>
    </td>
    <td>
        <?php echo $item->getPromotionName(); ?>
    </td>
    <td>
        <?php echo $item->getTypeLabel($item->getPromotionType()); ?>
    </td>
    <td>
        <?php echo is_numeric($item->getPromotionValue()) ? number_format($item->getPromotionValue()) : $item->getPromotionValue(); ?>
    </td>
    <td>
        <?php echo $item->getPromotionItemList(); ?>
    </td>
    <td>
        <?php echo $item->getApplyTimingLabel($item->getApplyTiming()); ?>
    </td>
    <td>
        <?php echo $item->getWaitingApplyDuration(); ?>
    </td>
    <td>
        <?php echo $view['date']->toText($item->getForceExpireDate()); ?>
    </td>
    <td>
        <?php echo $item->getTVODConditionLabel($item->getTvodApplyCondition()); ?>
    </td>
    <td>
        <?php  echo $view['date']->toText($item->getDateAdded()); ?>
    </td>
    <td>
        <?php  echo $view['date']->toText($item->getDateModified()); ?>
    </td>
    <td>
        <?php  echo $item->getCreatedByUser(); ?>
    </td>
</tr>