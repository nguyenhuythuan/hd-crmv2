<tr>
    <td>
        <?php echo $item->getName(); ?>
    </td>
    <td>
        <?php echo $item->getDescription(); ?>
    </td>
    <td>
        <?php echo $item->getPriority(); ?>
    </td>
    <td>
        <?php echo $item->getTypeLabel($item->getType()); ?>
    </td>
    <td>
        <?php echo $item->getApplyTimes(); ?>
    </td>
    <td>
        <?php echo $item->getPreventApplySameTypeLabel($item->getPreventApplySameType()); ?>
    </td>
    <td>
        <?php echo $item->getEventPage(); ?>
    </td>
    <td>
        <?php echo $item->getDistributorLabel($item->getDistributor()); ?>
    </td>
    <td>
        <?php  echo $view['date']->toText($item->getStartDate()); ?>
    </td>
    <td>
        <?php  echo $view['date']->toText($item->getEndDate()); ?>
    </td>

    <td>
        <?php  echo $view['date']->toText($item->getDateAdded()); ?>
    </td>
    <td>
        <?php  echo $view['date']->toText($item->getDateModified()); ?>
    </td>
    <td>
        <?php  echo $item->getCreatedByUser(); ?>
    </td>
</tr>