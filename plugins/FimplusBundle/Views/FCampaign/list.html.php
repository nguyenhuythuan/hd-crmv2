<?php
    if ($tmpl == 'index') {
        $view->extend('FimplusBundle:FCampaign:index.html.php');
    }
    $sessionVar = 'campaign';
    $target = '#campaign-container';
?>
<?php if (count($items)): ?>

    <div class="table-responsive">
        <table class="table table-hover table-striped table-bordered campaign-list" id="campaignTable">
            <thead>
            <tr>
                
                <?php
                echo $view->render(
                    'MauticCoreBundle:Helper:tableheader.html.php',
                    [
                        'sessionVar' => $sessionVar,
                        'orderBy'    => '',
                        'text'       => 'Name',
                        'class'      => 'visible-md visible-lg col-name',
                    ]
                );

                echo $view->render(
                    'MauticCoreBundle:Helper:tableheader.html.php',
                    [
                        'sessionVar' => $sessionVar,
                        'orderBy'    => '',
                        'text'       => 'Drscription',
                        'class'      => 'visible-md visible-lg col-description',
                    ]
                );

                echo $view->render(
                    'MauticCoreBundle:Helper:tableheader.html.php',
                    [
                        'sessionVar' => $sessionVar,
                        'orderBy'    => '',
                        'text'       => 'Priotity',
                        'class'      => 'visible-md visible-lg col-priority',
                    ]
                );

                echo $view->render(
                    'MauticCoreBundle:Helper:tableheader.html.php',
                    [
                        'sessionVar' => $sessionVar,
                        'orderBy'    => '',
                        'text'       => 'Type',
                        'class'      => 'visible-md visible-lg col-type',
                    ]
                );

                echo $view->render(
                    'MauticCoreBundle:Helper:tableheader.html.php',
                    [
                        'sessionVar' => $sessionVar,
                        'orderBy'    => '',
                        'text'       => 'Apply times',
                        'class'      => 'visible-md visible-lg col-apply-times',
                    ]
                );

                echo $view->render(
                    'MauticCoreBundle:Helper:tableheader.html.php',
                    [
                        'sessionVar' => $sessionVar,
                        'orderBy'    => '',
                        'text'       => 'Prevent apply same type',
                        'class'      => 'visible-md visible-lg col-prevent-apply',
                    ]
                );

                echo $view->render(
                    'MauticCoreBundle:Helper:tableheader.html.php',
                    [
                        'sessionVar' => $sessionVar,
                        'orderBy'    => '',
                        'text'       => 'Event page',
                        'class'      => 'visible-md visible-lg col-event-page',
                    ]
                );

                echo $view->render(
                    'MauticCoreBundle:Helper:tableheader.html.php',
                    [
                        'sessionVar' => $sessionVar,
                        'orderBy'    => '',
                        'text'       => 'Distributor',
                        'class'      => 'visible-md visible-lg col-distributor',
                    ]
                );

                echo $view->render(
                    'MauticCoreBundle:Helper:tableheader.html.php',
                    [
                        'sessionVar' => $sessionVar,
                        'orderBy'    => '',
                        'text'       => 'Start date',
                        'class'      => 'visible-md visible-lg col-start-date',
                    ]
                );

                echo $view->render(
                    'MauticCoreBundle:Helper:tableheader.html.php',
                    [
                        'sessionVar' => $sessionVar,
                        'orderBy'    => '',
                        'text'       => 'End date',
                        'class'      => 'visible-md visible-lg col-end-date',
                    ]
                );

                echo $view->render(
                    'MauticCoreBundle:Helper:tableheader.html.php',
                    [
                        'sessionVar' => $sessionVar,
                        'orderBy'    => '',
                        'text'       => 'Added date',
                        'class'      => 'visible-md visible-lg col-added',
                    ]
                );

                echo $view->render(
                    'MauticCoreBundle:Helper:tableheader.html.php',
                    [
                        'sessionVar' => $sessionVar,
                        'orderBy'    => '',
                        'text'       => 'Modified date',
                        'class'      => 'visible-md visible-lg col-modified',
                    ]
                );

                echo $view->render(
                    'MauticCoreBundle:Helper:tableheader.html.php',
                    [
                        'sessionVar' => $sessionVar,
                        'orderBy'    => '',
                        'text'       => 'Created by user',
                        'class'      => 'visible-md visible-lg col-created-by',
                    ]
                );
                ?>
            </tr>
            </thead>
            <tbody>
            <?php $index = 1; ?>
            <?php 
                foreach ($items as $item): 
                    //Use a separate layout for AJAX generated content
                    echo $view->render('FimplusBundle:FCampaign:campaign.html.php', [
                        'item'        => $item,
                        'index'       => $index
                    ]); 
                    $index++;
                 endforeach; 
            ?>
            </tbody>
        </table>
    </div>
    <div class="panel-footer">
        <?php echo $view->render(
            'MauticCoreBundle:Helper:pagination.html.php',
            [
                'totalItems' => count($items),
                'page'       => $page,
                'limit'      => $limit,
                'menuLinkId' => 'mautic_contacttransaction_index',
                'baseUrl'    => $view['router']->path('mautic_fimplus_campaign_index'),
                'sessionVar' => $sessionVar,
                'target'     => $target,
            ]
        ); ?>
    </div>
<?php else: ?>
    <?php echo $view->render('MauticCoreBundle:Helper:noresults.html.php', ['tip' => 'mautic.campaign.noresults.tip']); ?>
<?php endif; ?>
