<?php

/*
 * @copyright   2014 Mautic Contributors. All rights reserved
 * @author      Mautic
 *
 * @link        http://mautic.org
 *
 * @license     GNU/GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
 */
$view->extend('MauticCoreBundle:Default:content.html.php');
$view['slots']->set('mauticContent', 'campaign');
$view['slots']->set('headerTitle', $view['translator']->trans('mautic.fimplus.campaigns'));
$pageButtons = [];
$pageButtons[] = [
    'attr' => [
        'href' => $view['router']->path('mautic_import_action', ['object' => 'campaign', 'objectAction' => 'new']),
    ],
    'iconClass' => 'fa fa-upload',
    'btnText'   => 'mautic.lead.lead.import',
];
/* if ($permissions['campaign:campaigns:create']) {
     $pageButtons[] = [
        'attr' => [
            'href' => $view['router']->path('mautic_import_action', ['object' => 'companies', 'objectAction' => 'new']),
        ],
        'iconClass' => 'fa fa-upload',
        'btnText'   => 'mautic.lead.lead.import',
    ];

    $pageButtons[] = [
        'attr' => [
            'href' => $view['router']->path('mautic_import_index', ['object' => 'companies']),
        ],
        'iconClass' => 'fa fa-history',
        'btnText'   => 'mautic.lead.lead.import.index',
    ]; 
} */
$extraHtml = <<<button
<div class="btn-group ml-5 sr-only ">
    <span data-toggle="tooltip" title="{$view['translator']->trans(
    'mautic.fimplus.campaign.tooltip.list'
)}" data-placement="left"><a id="table-view" href="{$view['router']->path('mautic_fimplus_campaign_index', ['page' => $page, 'view' => 'list'])}" data-toggle="ajax" class="btn btn-default"><i class="fa fa-fw fa-table"></i></span></a>
    <span data-toggle="tooltip" title="{$view['translator']->trans(
    'mautic.fimplus.campaign.tooltip.grid'
)}" data-placement="left"><a id="card-view" href="{$view['router']->path('mautic_fimplus_campaign_index', ['page' => $page, 'view' => 'grid'])}" data-toggle="ajax" class="btn btn-default"><i class="fa fa-fw fa-th-large"></i></span></a>
</div>
button;
$view['slots']->set(
    'actions',
    $view->render(
        'MauticCoreBundle:Helper:page_actions.html.php',
        [
            'templateButtons' => [
                'new' => $permissions['fimplus:campaign:create'],
            ],
            'routeBase'     => 'fimplus_campaign',
            'langVar'       => 'fimplus.campaigns',
            'customButtons' => $pageButtons,
            'extraHtml'     => $extraHtml,
        ]
    )
);
?>

<div class="panel panel-default bdr-t-wdh-0">
    <?php echo $view->render(
        'MauticCoreBundle:Helper:list_toolbar.html.php',
        [
            'searchValue' => $searchValue,
            'searchHelp'  => 'mautic.core.help.searchcommands',
            'action'      => $currentRoute,
        ]
    ); ?>
    <div class="page-list">
        <?php $view['slots']->output('_content'); ?>
    </div>
</div>

