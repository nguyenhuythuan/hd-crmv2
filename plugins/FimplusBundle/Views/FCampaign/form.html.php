<?php

/*
 * @copyright   2014 Mautic Contributors. All rights reserved
 * @author      Mautic
 *
 * @link        http://mautic.org
 *
 * @license     GNU/GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
 */
$view->extend('MauticCoreBundle:Default:content.html.php');
// echo $view['assets']->includeScript('plugins/FimplusBundle/Assets/js/script.js');
echo $view['assets']->includeScript('plugins/FimplusBundle/Assets/js/select2.full.min.js');
echo $view['assets']->includeStylesheet('plugins/FimplusBundle/Assets/css/select2.min.css');
echo $view['assets']->includeStylesheet('plugins/FimplusBundle/Assets/css/conditiondetail.css');
echo $view['assets']->includeScript('plugins/FimplusBundle/Assets/js/script.js');
$view['slots']->set('mauticContent', 'fcampaign');

$header = ($entity->getId()) ?
    $view['translator']->trans(
        'mautic.fcampaign.menu.edit',
        ['%name%' => $view['translator']->trans($entity->getName())]
    ) : $view['translator']->trans('mautic.campaign.menu.new');
$view['slots']->set('headerTitle', $header);
?>
<div class="container-form">
    <?php echo $view['form']->start($form); ?>
    <?php $view['slots']->set('mauticContent', 'fcampaign'); ?>
    <!-- start: box layout -->
    <div class="box-layout">
        <!-- container -->
        <div class="col-md-9 bg-auto height-auto bdr-r">
            <div class="row">
                <div class="col-md-5">
                    <div class="pa-md">
                        <h4><b>Campaign</b></h4>
                    </div>
                    <div class="pa-md">
                        <?php echo $view['form']->row($form['name']); ?>

                        <?php echo $view['form']->row($form['type']); ?>

                        <?php echo $view['form']->row($form['distributor']); ?>

                        <?php echo $view['form']->row($form['prevent_apply_same_type']); ?>

                        <?php echo $view['form']->row($form['description']); ?>
                    </div>
                    <div class="divider"></div>
                    <div class="promotion-app-container">
                        <div class="promotion-app-content">
                            <?php if (isset($form['promotion'])) : ?>
                                <div class="pa-md">
                                    <h4><b>Add-on</b></h4>
                                </div>
                                <div class="pa-md">
                                    <?php echo $view['form']->widget($form['promotion']); ?>
                                </div>
                            <?php endif; ?>
                        </div>
                    </div>
                </div>
                <div class="col-md-7 condition-app">
                    <div class="pa-md">
                        <h4><b>Detail / Conditions</b></h4>
                    </div>
                    <div class="pa-md app-items" id="campaignDetail">
                        <?php
                        if (isset($form['campaignActiveCodeDetail']))
                            echo $view['form']->row($form['campaignActiveCodeDetail']);
                        if (isset($form['promotionConditionDetail']))
                            echo $view['form']->row($form['promotionConditionDetail']);
                        ?>
                    </div>
                    <!-- <div class="pa-md hidden">
                        <button type="button" class="add_condition_link btn btn-primary">Add condition</button>
                    </div> -->
                </div>
            </div>
        </div>
        <div class="col-md-3 bg-white height-auto">
            <div class="pr-lg pl-lg pt-md pb-md">
                <?php // echo $view['form']->row($form['weight']); 
                ?>
                <?php
                echo $view['form']->row($form['isPublished']);
                //  echo $view['form']->row($form['publishUp']);
                //  echo $view['form']->row($form['publishDown']);
                echo $view['form']->row($form['apply_times']);
                echo $view['form']->row($form['priority']);
                echo $view['form']->row($form['start_date']);
                echo $view['form']->row($form['end_date']);
                ?>
            </div>
        </div>
    </div>
    <?php echo $view['form']->end($form); ?>
</div>
<script>
    /// can not move script to file .js
</script>