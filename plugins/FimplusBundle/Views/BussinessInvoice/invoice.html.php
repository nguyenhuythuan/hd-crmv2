<tr>
    <td>
        <?php echo $item->getCampaignId(); ?>
    </td>
    <td>
        <?php echo $item->getQuantity(); ?>
    </td>
    <td>
        <?php echo number_format($item->getTotalPrice()); ?>
    </td>
    <td>
        <?php 
            if (!empty($item->getStartDate())) {
                echo date_format($item->getStartDate(), 'd-m-Y');
            }
        ?>
    </td>
    <td>
        <?php
            if (!empty($item->getEndDate())) {
                echo date_format($item->getEndDate(), 'd-m-Y');
            }
        ?>
    </td>
    <td>
        <?php
            if (!empty($item->getDateModified())) {
                echo date_format($item->getDateModified(), 'd-m-Y');
            }
        ?>
    </td>
    <td>
        <?php echo $item->getCreatedByUser(); ?>
    </td>
</tr>