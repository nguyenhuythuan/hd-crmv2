<?php

use MauticPlugin\FimplusBundle\Entity\Promotion;
/*
 * @copyright   2014 Mautic Contributors. All rights reserved
 * @author      Mautic
 *
 * @link        http://mautic.org
 *
 * @license     GNU/GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
 */

$view->extend('MauticCoreBundle:Default:content.html.php');
$view['slots']->set('headerTitle', 'New');
echo $view['assets']->includeScript('plugins/FimplusBundle/Assets/js/select2.full.min.js');
echo $view['assets']->includeStylesheet('plugins/FimplusBundle/Assets/css/select2.min.css');
echo $view['assets']->includeScript('plugins/FimplusBundle/Assets/js/promotion.js');
$view['slots']->set('mauticContent', 'promotion');
echo $view['form']->start($form);
?>
<!-- start: box layout -->
<div class="box-layout">
    <!-- container -->
    <div class="col-md-9 bg-auto height-auto bdr-r">
        <div class="row">
            <div class="offst-3 col-md-6">
                <div class="pa-md">
                    <?php echo $view['form']->row($form['campaign_id']); ?>
                    <?php echo $view['form']->row($form['quantity']); ?>
                    <?php echo $view['form']->row($form['start_date']); ?>
                    <?php echo $view['form']->row($form['end_date']); ?>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-3 bg-white height-auto">
        <div class="pr-lg pl-lg pt-md pb-md">
            <?php // echo $view['form']->row($form['weight']); 
            ?>
            <?php
            //  echo $view['form']->row($form['isPublished']);
            // echo $view['form']->row($form['publishUp']);
            // echo $view['form']->row($form['publishDown']);
            ?>
        </div>
    </div>
</div>
<?php echo $view['form']->end($form); ?>