<?php
    if ($tmpl == 'index') {
        $view->extend('FimplusBundle:BussinessInvoice:index.html.php');
    }
    $sessionVar = 'bussinessInvoice';
    $target = '#bussiness-invoice-container';
?>
<?php if (count($items)): ?>
    <div class="table-responsive">
        <table class="table table-hover table-striped table-bordered campaign-list" id="bissinessInvoiceTable">
            <thead>
            <tr>
                
                <?php
                echo $view->render(
                    'MauticCoreBundle:Helper:tableheader.html.php',
                    [
                        'sessionVar' => $sessionVar,
                        'orderBy'    => '',
                        'text'       => 'Campaign',
                        'class'      => 'visible-md visible-lg col-campaign',
                    ]
                );

                echo $view->render(
                    'MauticCoreBundle:Helper:tableheader.html.php',
                    [
                        'sessionVar' => $sessionVar,
                        'orderBy'    => '',
                        'text'       => 'Quantity',
                        'class'      => 'visible-md visible-lg col-quantity',
                    ]
                );

                echo $view->render(
                    'MauticCoreBundle:Helper:tableheader.html.php',
                    [
                        'sessionVar' => $sessionVar,
                        'orderBy'    => '',
                        'text'       => 'Total price',
                        'class'      => 'visible-md visible-lg col-total-price',
                    ]
                );

                echo $view->render(
                    'MauticCoreBundle:Helper:tableheader.html.php',
                    [
                        'sessionVar' => $sessionVar,
                        'orderBy'    => '',
                        'text'       => 'Start date',
                        'class'      => 'visible-md visible-lg col-start-date',
                    ]
                );

                echo $view->render(
                    'MauticCoreBundle:Helper:tableheader.html.php',
                    [
                        'sessionVar' => $sessionVar,
                        'orderBy'    => '',
                        'text'       => 'End date',
                        'class'      => 'visible-md visible-lg col-end-date',
                    ]
                );

                echo $view->render(
                    'MauticCoreBundle:Helper:tableheader.html.php',
                    [
                        'sessionVar' => $sessionVar,
                        'orderBy'    => '',
                        'text'       => 'Modified date',
                        'class'      => 'visible-md visible-lg col-campaign-id',
                    ]
                );

                echo $view->render(
                    'MauticCoreBundle:Helper:tableheader.html.php',
                    [
                        'sessionVar' => $sessionVar,
                        'orderBy'    => '',
                        'text'       => 'Created by user',
                        'class'      => 'visible-md visible-lg col-campaign-id',
                    ]
                );
                ?>
            </tr>
            </thead>
            <tbody>
            <?php $index = 1; ?>
            <?php 
                foreach ($items as $item): 
                    //Use a separate layout for AJAX generated content
                    echo $view->render('FimplusBundle:BussinessInvoice:invoice.html.php', [
                        'item'        => $item,
                        'index'       => $index
                    ]); 
                    $index++;
                 endforeach; 
            ?>
            </tbody>
        </table>
    </div>
    <div class="panel-footer">
        <?php echo $view->render(
            'MauticCoreBundle:Helper:pagination.html.php',
            [
                'totalItems' => count($items),
                'page'       => $page,
                'limit'      => $limit,
                'menuLinkId' => 'mautic_fimplus_bussiness_invoice_index',
                'baseUrl'    => $view['router']->path('mautic_fimplus_bussiness_invoice_index'),
                'sessionVar' => $sessionVar,
                'target'     => $target,
            ]
        ); ?>
    </div>
<?php else: ?>
    <?php echo $view->render('MauticCoreBundle:Helper:noresults.html.php', ['tip' => 'mautic.promotion.noresults.tip']); ?>
<?php endif; ?>
