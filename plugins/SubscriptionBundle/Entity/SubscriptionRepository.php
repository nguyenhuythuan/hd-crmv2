<?php


namespace MauticPlugin\SubscriptionBundle\Entity;


use Mautic\CoreBundle\Entity\CommonRepository;
use Mautic\LeadBundle\Entity\Lead;

class SubscriptionRepository extends CommonRepository
{
    public function getEntities(array $args = [])
    {
        return parent::getEntities($args);
    }

    protected function addCatchAllWhereClause($q, $filter)
    {
        return $this->addStandardCatchAllWhereClause($q, $filter, [
            'e.uuid'
        ]);
    }
    public function countByFilter($filter = [],$ortherFilter = [])
    {
        $result = $this->_em->getConnection()->createQueryBuilder()
            ->select('count(s.id) as num')
            ->from(MAUTIC_TABLE_PREFIX.'subscription', 's');

        if($ortherFilter && $ortherFilter != null){
            foreach($ortherFilter as $k => $v){
                if($k == 0 ){
                    $result->where("{$v}");
                }else{
                    $result->andwhere("{$v}");
                }
            }
        }

        if($filter && $filter != null){
            foreach($filter as $key => $value){
                $result->andwhere("{$key} = :{$key}")
                    ->setParameter($key, $value);
            }
        }
        
        $result = $result->execute()->fetch();
        return (int)$result['num'];
    }

    public function getStatusDefine(){
        return (object)[
            "INACTIVE" => 0,
            "ACTIVE" => 1,
            "USED" => 3,
            "DEACTIVATED" => 4,
        ];
    }
}
