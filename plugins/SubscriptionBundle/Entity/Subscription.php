<?php
namespace MauticPlugin\SubscriptionBundle\Entity;

use Doctrine\DBAL\Types\DecimalType;
use Doctrine\ORM\Mapping as ORM;
use Mautic\CampaignBundle\Executioner\Scheduler\Mode\DateTime;
use Mautic\CoreBundle\Doctrine\Mapping\ClassMetadataBuilder;
use Mautic\CoreBundle\Entity\FormEntity;
use \Mautic\LeadBundle\Entity\Lead;

class Subscription extends FormEntity
{
    const STATUS = [
        "code" => [
            "NEW" => 1, // mới khởi tạo
            "LINKED" => 2, // liên kết với payment thành công
            "PAYING" => 3, // đang tiến hành thanh toán
            "PAID" => 4, // Đã thanh toán thành công
            "ERROR" => -1, // Lỗi
            "EXPIRED" => -2, // Hết hạn
        ],
        "text" => [
            "NEW" => 'NEW', // mới khởi tạo
            "LINKED" => 'LINKED', // liên kết với payment thành công
            "PAYING" => 'PAYING', // đang tiến hành thanh toán
            "PAID" => 'PAID', // Đã thanh toán thành công
            "ERROR" => 'ERROR', // Lỗi
            "EXPIRED" => 'EXPIRED', // Hết hạn
        ]
    ];

    /**
     * @var int
     */
    private $id;
    private $subsId;
    private $userId;
    private $planId;
    private $titleId;
    private $startDate;
    private $expireAt;
    private $nextRenewDate;
    private $methodId;
    private $channelPayment;
    private $createdAt;
    private $updatedAt;
    private $statusAutoRenew;
    private $action;
    private $status;
    private $profileCode;
    private $lastTransactionId;
    private $distributor;
    private $renewTimesRemain;
    private $retryTimesRemain;


    /** 
     * ================== get function ==========
    */
    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }
    /**
     * @param string $action
     */
    public function setAction($value)
    {
        $this->action = $value;
    }
    public function setExpireAt($value)
    {
        $this->expireAt = $value;
    }
    public function setSubsId($value){
        $this->subsId = $value;
    }
    public function setUserId($value){
        $this->userId = $value;
    }
    public function setPlanId($value){
        $this->planId = $value;
    }
    public function setTitleId($value){
        $this->titleId = $value;
    }
    public function setStartDate($value){
        $this->startDate = $value;
    }
    public function setNextRenewDate($value){
        $this->nextRenewDate = $value;
    }
    public function setMethodId($value){
        $this->methodId = $value;
    }
    public function setChannelPayment($value){
        $this->channelPayment = $value;
    }
    public function setCreatedAt($value){
        $this->createdAt = $value;
    }
    public function setUpdatedAt($value){
        $this->updatedAt = $value;
    }
    public function setStatusAutoRenew($value){
        $this->statusAutoRenew = $value;
    }
    public function setStatus($value){
        $this->status = $value;
    }
    
    public function setProfileCode($value){
        $this->profileCode = $value;
    }
    public function setLastTransactionId($value){
        $this->lastTransactionId = $value;
    }
    public function setDistributor($value){
        $this->distributor = $value;
    }
    public function setRenewTimesRemain($value){
        $this->renewTimesRemain = $value;
    }
    public function setRetryTimesRemain($value){
        $this->retryTimesRemain = $value;
    }

    /** 
     * ================== get function ==========
    */
    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }
    /**
     * @param string $action
     */
    public function getAction()
    {
        return $this->action;
    }
    public function getExpireAt()
    {
        return $this->expireAt;
    }

    public function getSubsId(){
        return $this->subsId;
    }
    public function getUserId(){
        return $this->userId;
    }
    public function getPlanId(){
        return $this->planId;
    }
    public function getTitleId(){
        return $this->titleId;
    }
    public function getStartDate(){
        return $this->startDate;
    }
    public function getNextRenewDate(){
        return $this->nextRenewDate;
    }
    public function getMethodId(){
        return $this->methodId;
    }
    public function getChannelPayment(){
        return $this->channelPayment;
    }
    public function getCreatedAt(){
        return $this->createdAt;
    }
    public function getUpdatedAt(){
        return $this->updatedAt;
    }
    public function getStatusAutoRenew(){
        return $this->statusAutoRenew;
    }
    public function getStatus(){
        return $this->status;
    }
    public function getProfileCode(){
        return $this->profileCode;
    }
    public function getLastTransactionId(){
        return $this->lastTransactionId;
    }
    public function getDistributor(){
        return $this->distributor;
    }
    public function getRenewTimesRemain(){
        return $this->renewTimesRemain;
    }
    public function getRetryTimesRemain(){
        return $this->retryTimesRemain;
    }
    /**
     * chưa edit type data 
     */
    public static function loadMetadata(ORM\ClassMetadata $metadata)
    {
        $builder = new ClassMetadataBuilder($metadata);

        $builder->setTable('subscription')
            ->setCustomRepositoryClass('MauticPlugin\SubscriptionBundle\Entity\SubscriptionRepository');

        $builder->createField('id', 'integer')
            ->makePrimaryKey()
            ->generatedValue()
            ->build();
        $builder->createField('subsId','string')
            ->columnName('subsId')
            ->columnDefinition('CHAR(100) NOT NULL')
            ->build();
        $builder->createField('userId','string')
            ->columnName('userId')
            ->columnDefinition('CHAR(100) NOT NULL')
            ->build();
        $builder->createField('planId','string')
            ->columnName('planId')
            ->columnDefinition('CHAR(100) NULL')
            ->build();
        $builder->createField('titleId','string')
            ->columnName('titleId')
            ->columnDefinition('CHAR(100) NULL')
            ->build();
        $builder->createField('startDate','string')
            ->columnName('startDate')
            ->columnDefinition('CHAR(100) NULL')
            ->build();
        $builder->createField('nextRenewDate','string')
            ->columnName('nextRenewDate')
            ->columnDefinition('CHAR(100) NULL')
            ->build();
        $builder->createField('methodId','string')
            ->columnName('methodId')
            ->columnDefinition('CHAR(100) NULL')
            ->build();
        $builder->createField('channelPayment','string')
            ->columnName('channelPayment')
            ->columnDefinition('CHAR(100) NULL')
            ->build();
        $builder->createField('createdAt','string')
            ->columnName('createdAt')
            ->columnDefinition('datetime NULL')
            ->build();
        $builder->createField('expireAt','string')
            ->columnName('expireAt')
            ->columnDefinition('datetime NULL')
            ->build();
        $builder->createField('updatedAt','string')
            ->columnName('updatedAt')
            ->columnDefinition('datetime NULL')
            ->build();
        $builder->createField('statusAutoRenew','integer')
            ->columnName('statusAutoRenew')
            ->columnDefinition('int(2)  NULL')
            ->build();
        $builder->createField('status','integer')
            ->columnName('status')
            ->columnDefinition('int(2) NULL')
            ->build();
        $builder->createField('action','string')
            ->columnName('action')
            ->columnDefinition('CHAR(100) NULL')
            ->build();
    }

}
