<?php


namespace MauticPlugin\SubscriptionBundle\Model;


use Mautic\CoreBundle\Model\FormModel;
use MauticPlugin\SubscriptionBundle\Entity\Subscription;
use MauticPlugin\SubscriptionBundle\Entity\SubscriptionRepository;

/**
 * Class TransactionModel
 * {@inheritdoc}
 */
class SubscriptionModel extends FormModel
{
    /* STATUS: {
        INACTIVE: 0,
        ACTIVE: 1,
        USED: 3,
        DEACTIVATED: 4,
  
        TVOD_WAIT_WATCH: 0,
      }, */
    /**
     * {@inheritdoc}
     *
     * @return \MauticPlugin\SubscriptionBundle\Entity\SubscriptionRepository
     */
    public function getRepository()
    {
        $repo = $this->em->getRepository('SubscriptionBundle:Subscription');
        return $repo;
    }
    public function getEntity($id = null)
    {
        if ($id === null) {
            return new Subscription();
        }

        $entity = parent::getEntity($id);
        return $entity;
    }
}