<?php
namespace MauticPlugin\SubscriptionBundle\Service;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Validator\Constraints\Date;
use Mautic\LeadBundle\Model\Lead;


class SubsService
{
    public $channel;
    public $connection;
    public $container;
    protected $subsModel;
    const CREATE_SUBS = 'CREATE_SUBS';
    const UPDATE_SUBS = 'UPDATE_SUBS';

    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
    }

    protected $keyRequire = [
        'userId',
        'planId',
        'titleId',
        'startDate',
        'expireAt',
        'nextRenewDate',
        'methodId',
        'profileCode',
        'lastTransactionId',
        'distributor',
        'renewTimesRemain',
        'retryTimesRemain',
        'channelPayment',
        'action',
        'createdAt',
        'updatedAt',
        'statusAutoRenew',
        'status',
    ];
    public function getRecord($whereData = []){
        $this->subsModel = null;
        return true;
    }
    public function insertSubs($data){
        if(is_array($data)){
            $data = (object)$data;
        }

        try{
            $em = $this->container->get('doctrine')->getEntityManager();
            $reponstory = $em->getRepository('SubscriptionBundle:Subscription');
            $subsItem = $reponstory->findOneBy([
                'subsId' => isset($data->id)?$data->id:'',
            ]);
            if($subsItem != null){
                return true;
            }

            $model = $this->container->get('mautic.subscription.model.subscription');
            $entity = $model->getEntity();

            // $entity = $em->getEntity('SubscriptionBundle:Subscription');
            $entity->setSubsId(isset($data->id)?$data->id:'');
            $entity->setUserId(isset($data->userId)?$data->userId:'');
            $entity->setPlanId(isset($data->planId)?$data->planId:null);
            $entity->setTitleId(isset($data->titleId)?$data->titleId:null);
            $startDate = null;
            if(isset($data->startDate)){
                $startDate = $this->convertDateToFomartDb($data->startDate);
            }
            $entity->setStartDate($startDate);
            $nextRenewDate = null;
            if(isset($data->nextRenewDate)){
                $nextRenewDate = $this->convertDateToFomartDb($data->nextRenewDate);
            }
            $entity->setNextRenewDate($nextRenewDate);
            $entity->setMethodId(isset($data->methodId)?$data->methodId:null);
            $entity->setChannelPayment(isset($data->channelPayment)?$data->channelPayment:null);
            $createdAt = null;
            if(isset($data->createdAt)){
                $createdAt = $this->convertDateToFomartDb($data->createdAt);
            }
            $entity->setCreatedAt($createdAt);
            
            $updatedAt = null;
            if(isset($data->updatedAt)){
                $updatedAt = $this->convertDateToFomartDb($data->updatedAt);
            }
            $entity->setUpdatedAt($updatedAt);
            $expireAt = null;
            if(isset($data->expireAt)){
                $expireAt = $this->convertDateToFomartDb($data->expireAt);
            }
            $entity->setExpireAt($expireAt);

            $entity->setStatusAutoRenew(isset($data->statusAutoRenew)?$data->statusAutoRenew:null);
            $entity->setAction(self::CREATE_SUBS);
            $entity->setProfileCode(isset($data->profileCode)?$data->profileCode:null);
            $entity->setLastTransactionId(isset($data->lastTransactionId)?$data->lastTransactionId:null);
            $entity->setDistributor(isset($data->distributor)?$data->distributor:null);
            $entity->setRenewTimesRemain(isset($data->renewTimesRemain)?$data->renewTimesRemain:null);
            $entity->setRetryTimesRemain(isset($data->retryTimesRemain)?$data->retryTimesRemain:null);
            $em->persist($entity);
            $em->flush();
            if($entity->getId()){
                return $entity;
            }
            return false;

        }catch(\Exception $e){
            print_r($e->getMessage());
            return false;
        }
    }

    public function updateSubs($data){
        if(is_array($data)){
            $data = (object)$data;
        }
        try{
            if(!$data || !$data->id){
                return false;
            }
            $em = $this->container->get('doctrine')->getEntityManager();
            $reponstory = $em->getRepository('SubscriptionBundle:Subscription');
            $entity = $reponstory->findOneBy([
                'subsId' => isset($data->id)?$data->id:'',
            ]);

            if($entity == null){
                return false;
            }

            // $model = $this->container->get('mautic.subscription.model.subscription');
            // $entity = $model->getEntity();
            $entity->setSubsId(isset($data->id)?$data->id:'');
            $entity->setUserId(isset($data->userId)?$data->userId:'');
            $entity->setPlanId(isset($data->planId)?$data->planId:null);
            $entity->setTitleId(isset($data->titleId)?$data->titleId:null);
            $startDate = null;
            if(isset($data->startDate)){
                $startDate = $this->convertDateToFomartDb($data->startDate);
            }
            $entity->setStartDate($startDate);
            // exit('12212222');
            $nextRenewDate =null;
            if(isset($data->nextRenewDate)){
                $nextRenewDate = $this->convertDateToFomartDb($data->nextRenewDate);
            }
            $entity->setNextRenewDate($nextRenewDate);

            $entity->setMethodId(isset($data->methodId)?$data->methodId:null);
            $entity->setChannelPayment(isset($data->channelPayment)?$data->channelPayment:null);
            $createdAt = null;
            if(isset($data->createdAt)){
                $createdAt = $this->convertDateToFomartDb($data->createdAt);
            }
            $entity->setCreatedAt($createdAt);
            
            $updatedAt = null;
            if(isset($data->updatedAt)){
                $updatedAt = $this->convertDateToFomartDb($data->updatedAt);
            }
            $entity->setUpdatedAt($updatedAt);
            
            $expireAt = null;
            if(isset($data->expireAt)){
                $expireAt = $this->convertDateToFomartDb($data->expireAt);
            }
            $entity->setExpireAt($expireAt);
            $entity->setStatusAutoRenew(isset($data->statusAutoRenew)?$data->statusAutoRenew:null);
            $entity->setAction(self::UPDATE_SUBS);

            $entity->setProfileCode(isset($data->profileCode)?$data->profileCode:null);
            $entity->setLastTransactionId(isset($data->lastTransactionId)?$data->lastTransactionId:null);
            $entity->setDistributor(isset($data->distributor)?$data->distributor:null);
            $entity->setRenewTimesRemain(isset($data->renewTimesRemain)?$data->renewTimesRemain:null);
            $entity->setRetryTimesRemain(isset($data->retryTimesRemain)?$data->retryTimesRemain:null);
            
            // $em->persist($entity);
            echo "run in update subs";
            $em->flush();
            if($entity->getId()){
                return $entity;
            }
            return false;
            // $em->persist($entity);
            $em->flush();
            if($entity->getId()){
                return $entity;
            }
            return false;

        }catch(\Exception $e){
            print_r($e->getMessage());
            return false;
        }
    }

    public function upChangeTrans($data){
        

        // check isset sub
        // detect function  
        
        $action = $data->action;
        $service = $this->container->get('mautic.service.usercontext');
        $userId = $data->data->userId;
        $service->pushMessage($userId,$service::GROUP_SUBSCRIPTION); 
        switch($action){
            case self::CREATE_SUBS : return $this->insertSubs($data->data); break;
            case self::UPDATE_SUBS : return $this->updateSubs($data->data) ; break;
            default: return false ; break;
        }
        return true;
    }
    
    public function convertDateToFomartDb($time = ''){
        // exit;
        if($time == '' || $time == null || $time == false){
            return null;
        }
        if(is_numeric($time)){
            return date('Y-m-d H:i:s',$time);
        }else{
            $date = date_create($time);
            
            if(date_format('Y-m-d H:i:s',$date)){
                return date_format('Y-m-d H:i:s',$date);    
            }
            return null;

            // return date_format('Y-m-d H:i:s',$date);
        }
    }
}