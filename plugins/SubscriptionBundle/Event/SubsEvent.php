<?php
namespace MauticPlugin\SubscriptionBundle\Event;

use Symfony\Component\EventDispatcher\Event;
use Acme\StoreBundle\Order;

class FilterOrderEvent extends Event
{
    protected $subs;

    public function __construct($subs)
    {
        $this->subs = $subs;
    }

    public function getSubs()
    {
        return $this->subs;
    }
}