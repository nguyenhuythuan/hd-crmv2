<?php
namespace MauticPlugin\SubscriptionBundle\EventListener;

use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use MauticPlugin\SubscriptionBundle\SubsEvents;
use MauticPlugin\SubscriptionBundle\Event\SubsEvent;

class SubsSubscriber implements EventSubscriberInterface
{
    protected $container ;
    public function __construct(ContainerInterface $container)
    {
        $this->container             = $container;
        // die("1212");
    }
    public static function getSubscribedEvents()
    {
        return array(
            SubsEvents::SUBS_ADD     => array('onSubsAdd', 0),
            SubsEvents::SUBS_UPDATE     => array('onSubsUpdate', 0),
        );
    }

    public function onSubsAdd(SubsEvent $subs){

    }
    public function onSubsUpdate(SubsEvent $subs){

    }
}