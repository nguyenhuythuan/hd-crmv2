<?php

return [
    'routes' => [
        'main'  => [
            
        ]
    ],
    'services' => [
        'repositories' => [
            'mautic.subscription.repository.subscription' => [
                'class'     => Doctrine\ORM\EntityRepository::class,
                'factory'   => ['@doctrine.orm.entity_manager', 'getRepository'],
                'arguments' => [
                    'MauticPlugin\SubscriptionBundle\Entity\Subscription',
                ],
            ]
        ],

        'models' => [
            'mautic.subscription.model.subscription' => [
                'class'     => 'MauticPlugin\SubscriptionBundle\Model\SubscriptionModel',
                'arguments' => []
            ]
        ],
        'events' => [
            'mautic.subs.eventlistener.subscriber' => [
                'class'     => \MauticPlugin\SubscriptionBundle\EventListener\SubsSubscriber::class,
                'arguments' => [
                    'service_container'
                    // 'mautic.transaction.model.transaction',
                ]
            ],
        ],
        'orther' => [
            'mautic.subscription.service.subsService' => [
                'class'     => 'MauticPlugin\SubscriptionBundle\Service\SubsService',
                'arguments' => [
                    'service_container',
                ],
            ]
        ],

    ],

];
