<?php
namespace MauticPlugin\SubscriptionBundle;

final class SubsEvents
{
    const SUBS_ADD      = 'subs.add';
    const SUBS_UPDATE   = 'subs.update';

}