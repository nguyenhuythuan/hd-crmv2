<?php


namespace MauticPlugin\SubscriptionBundle\Controller;

use Mautic\CoreBundle\Controller\FormController;
use Mautic\LeadBundle\Controller\LeadAccessTrait;

use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;

class TransactionController extends FormController
{
    use LeadAccessTrait;
    /**
     * Display the transaction view
     */
    public function indexAction($leadId = 0, $page = 1)
    {
        if (empty($leadId)) {
            return $this->accessDenied();
        }

        $lead = $this->checkLeadAccess($leadId, 'view');
        if ($lead instanceof Response) {
            return $lead;
        }

        $this->setListFilters();

        /** @var MauticPlugin\SubscriptionBundle\Model\TransactionModel $model */
        $model = $this->getModel('transaction');
        $session = $this->get('session');
        //set limits
        $limit = $session->get('mautic.transaction.limit', $this->get('mautic.helper.core_parameters')->getParameter('default_pagelimit'));
        $start = ($page === 1) ? 0 : (($page - 1) * $limit);
        if ($start < 0) {
            $start = 0;
        }

        $search = $this->request->get('search', $session->get('mautic.transaction.filter', ''));
        $session->set('mautic.transaction.filter', $search);

        //do some default filtering
        $orderBy    = $session->get('mautic.transaction.orderby', 'e.uuid');
        $orderByDir = $session->get('mautic.transaction.orderbydir', 'ASC');

        $filter      = ['string' => $search, 'force' => ''];

        $results = $model->getEntities([
            'start'          => $start,
            'limit'          => $limit,
            'filter'         => $filter,
            'orderBy'        => $orderBy,
            'orderByDir'     => $orderByDir,
            'withTotalCount' => true,
        ]);

        $this->get('session')->set('mautic.transaction.page', $page);
        $tmpl = $this->request->isXmlHttpRequest() ? $this->request->get('tmpl', 'index') : 'index';

        return $this->delegateView([
           'viewParameters' => [
               'searchValue'      => $search,
               'transactions'     => $results,
               'page'             => $page,
               'limit'            => $limit,
               'tmpl'             => $tmpl,
               'lead'             => $lead,
               'leadId'           => $leadId
           ],
            'contentTemplate' => 'MauticTransactionBundle:Transaction:list.html.php',
            'passthroughVars' => [
                'activeLink'    => '#mautic_contacttransaction_index',
                'mauticContent' => 'transaction',
                'route'         => false,
            ]
        ]);
    }
    /**
     * Generate's edit form and processes post data.
     *
     * @param $leadId
     * @param $objectId
     *
     * @return \Symfony\Component\HttpFoundation\JsonResponse|\Symfony\Component\HttpFoundation\RedirectResponse|Response
     */
    public function editAction($leadId, $objectId)
    {
        // $lead = $this->checkLeadAccess($leadId, 'view');
        // if ($lead instanceof Response) {
        //     return $lead;
        // }

        /** @var MauticPlugin\SubscriptionBundle\Model\TransactionModel $model */
       /*  $model = $this->getModel('transaction');
        $transaction       = $model->getEntity($objectId);

        $closeModal = false;
        $valid      = false;

        if ($transaction === null || !$this->get('mautic.security')->hasEntityAccess('lead:leads:editown', 'lead:leads:editother', $lead->getPermissionUser())) {
            return $this->accessDenied();
        }

        $action = $this->generateUrl(
            'mautic_contacttransaction_action',
            [
                'objectAction' => 'edit',
                'objectId'     => $objectId,
                'leadId'       => $leadId,
            ]
        );
        $form = $model->createForm($transaction, $this->get('form.factory'), $action);

        ///Check for a submitted form and process it
        if ($this->request->getMethod() == 'POST') {
            if (!$cancelled = $this->isFormCancelled($form)) {
                if ($valid = $this->isFormValid($form)) {
                    //form is valid so process the data
                    $model->saveEntity($transaction);
                    $closeModal = true;
                }
            } else {
                $closeModal = true;
            }
        }

        $security    = $this->get('mautic.security');
        $permissions = [
            'edit'   => $security->hasEntityAccess('lead:leads:editown', 'lead:leads:editother', $lead->getPermissionUser()),
            'delete' => $security->hasEntityAccess('lead:leads:deleteown', 'lead:leads:deleteother', $lead->getPermissionUser()),
        ];

        if ($closeModal) {
            //just close the modal
            $passthroughVars['closeModal'] = 1;

            if ($valid && !$cancelled) {
                $passthroughVars['transactionHtml'] = $this->renderView(
                    'MauticTransactionBundle:Transaction:list.html.php',
                    [
                        'permissions' => $permissions,
                        'searchValue'      => $search,
                        'transactions'     => $transaction,
                        'page'             => $page,
                        'limit'            => $limit,
                        'tmpl'             => $tmpl
                    ]
                );
                $passthroughVars['transacitionId'] = $transaction->getId();
            }

            $passthroughVars['mauticContent'] = 'leadTransaction';

            $response = new JsonResponse($passthroughVars);

            return $response;
        } else {
            return $this->delegateView(
                [
                    'viewParameters' => [
                        'form'        => $form->createView(),
                        'transaction'        => $transaction,
                        'permissions' => $permissions,
                    ],
                    'contentTemplate' => 'MauticTransactionBundle:Transaction:form.html.php',
                ]
            );
        } */
    }

    public function executeTransactionAction($objectAction, $objectId = 0, $leadId = 0)
    {
        if (method_exists($this, "{$objectAction}Action")) {
            return $this->{"{$objectAction}Action"}($leadId, $objectId);
        } else {
            return $this->accessDenied();
        }
    }
}
