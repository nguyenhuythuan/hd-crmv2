<li class="">
    <a href="#transaction-container" role="tab" data-toggle="tab">
        <span class="label label-primary mr-sm" id="TransactionCount">
            <?php echo $total; ?>
        </span>
        <?php echo $view['translator']->trans('mautic.transaction.transactions'); ?>
    </a>
</li>