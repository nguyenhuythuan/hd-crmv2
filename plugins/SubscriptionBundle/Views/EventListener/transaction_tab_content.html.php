
<!-- #transaction-container -->
 <div class="tab-pane fade bdr-w-0" id="transaction-container">
     <?php echo $view->render(
            'MauticTransactionBundle:Transaction:list.html.php',
            [
                'transactions' => $transactions,
                'lead' => $lead,
                'tmpl' => $tmpl,
                'leadId' => $leadId,
                'page'              => $page,
                'limit'             => $limit,
                'search'            => $search,
            ]
        ); ?>
 </div>
 <!--/ #transaction-container -->