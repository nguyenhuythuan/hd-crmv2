<?php
namespace MauticPlugin\RabbitmqBundle\UsrService;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Validator\Constraints\Date;
use Mautic\LeadBundle\Model\Lead;

class User
{
    public $container;

    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
    }

    protected $keyRequire = [];

    public function getRecord($whereData = [],$fetch = true){
        $result = $this->container->get('doctrine')->getEntityManager()->getConnection()->createQueryBuilder()
            ->select('*')
            ->from(MAUTIC_TABLE_PREFIX.'leads', 'l');
            if($whereData && count($whereData) > 0){
                foreach($whereData as $key => $value){
                    $result->where("{$key} = :{$key}")
                    ->setParameter($key, $value);
                }
            }
            // ->where(['id_fimplus'=>'e58bf659-11eb-4e19-8516-c472676cf161'])
            $result = $result->execute();
            if($fetch){
                return $result->fetch();
            }
        return $result->fetchAll();
    }

    public function insertData($data){
        if(is_array($data)){
            $data = (object)$data;
        }
        try{
            if(!$data || $data->id == '') return false;

            $where = ['id_fimplus' => $data->id];
            $result = $this->getRecord($where,true);
            if($result && count($result) > 0 ) return true ;
            // Currently tracked lead based on cookies
            $leadModel = $this->container->get('mautic.model.factory')->getModel('lead');
            $lead = $leadModel->getCurrentLead();
            $leadFields = array(
                'id_fimplus' => $data->id,
                'phone' => isset($data->phone)?$data->phone:'',
                'created_at' => isset($data->createdAt)?$data->createdAt:'',
                'updated_at' => isset($data->updatedAt)?$data->updatedAt:'',
                'platform' => isset($data->platform)?$data->platform:'',
                'firstname' => isset($data->fullName) ? $data->fullName : '',
                'email' => isset($data->email) ? $data->email : '',
                'dateIdentified' => date('Y-m-d H:m:i',time()),
                'action' => 'CEATE_USER'
            );
            // $leadModel->setDateIdentified($leadFields['date_identified']);
            // Set the lead's data
            $leadModel->setFieldValues($lead, $leadFields,true);
            $lead->setDateIdentified(date('Y-m-d H:m:i',time()));
            // Save the entity
            $leadModel->saveEntity($lead);
            // Set the updated lead
            $leadModel->setCurrentLead($lead);
            
            $service = $this->container->get('mautic.service.usercontext');
            $userId = $data->id;
            $service->pushMessage($userId,$service::GROUP_ACCOUNT); 
            return !empty($lead->getId()) ? true : false;
        }catch(\Exception $e){
            print_r($e->getMessage());
            return false;
        }
    }

    public function updateData($data){
        if(is_array($data)){
            $data = (object)$data;
        }
        try{
            if(!$data || $data->id == '') return false;

            $where = ['id_fimplus' => $data->id];
            $result = $this->getRecord($where,true);
            if(!$result || count($result) == 0 ) return false ;
            $result = is_array($result) ? (object)$result : $result;
            // Currently tracked lead based on cookies
            $leadModel = $this->container->get('mautic.model.factory')->getModel('lead');
            // $leadModel->setCurrentLead($result);
            // $lead = $leadModel->getCurrentLead();
            $lead = $leadModel->getEntity($result->id);
            // var_dump($lead);
            // exit;
            
            
            $leadFields = array(
                'id_fimplus' => $data->id,
                'phone' => isset($data->phone)?$data->phone:'',
                'created_at' => isset($data->createdAt)?$data->createdAt:'',
                'updated_at' => isset($data->updatedAt)?$data->updatedAt:'',
                'platform' => isset($data->platform)?$data->platform:'',
                'firstname' => isset($data->fullName) ? $data->fullName : '',
                'email' => isset($data->email) ? $data->email : '',
                'dateIdentified' => date('Y-m-d H:m:i',time()),
                'action' => 'CEATE_USER'
            );
            // $leadModel->setDateIdentified($leadFields['date_identified']);
            // Set the lead's data
            $leadModel->setFieldValues($lead, $leadFields,true);
            $lead->setDateIdentified(date('Y-m-d H:m:i',time()));
            // Save the entity
            $leadModel->saveEntity($lead);
            // Set the updated lead
            $leadModel->setCurrentLead($lead);
            return !empty($lead->getId()) ? true : false;
        }catch(\Exception $e){
            print_r($e->getMessage());
            return false;
        }
    }
}