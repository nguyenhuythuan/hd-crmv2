<?php

/*
 * @copyright   2014 Mautic Contributors. All rights reserved
 * @author      Mautic
 *
 * @link        http://mautic.org
 *
 * @license     GNU/GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
 */

namespace MauticPlugin\RabbitmqBundle;

/**
 * Class StageEvents.
 *
 * Events available for StageBundle
 */
final class RabbitmqEvents
{
    /**
     * The mautic.promotion_pre_save event is thrown right before a form is persisted.
     *
     * The event listener receives a Mautic\StageBundle\Event\StageEvent instance.
     *
     * @var string
     */
    const PROMOTION_PRE_SAVE = 'mautic.promotion_pre_save';

    /**
     * The mautic.promotion_post_save event is thrown right after a form is persisted.
     *
     * The event listener receives a Mautic\StageBundle\Event\StageEvent instance.
     *
     * @var string
     */
    const PROMOTION_POST_SAVE = 'mautic.promotion_post_save';

    /**
     * The mautic.promotion_pre_delete event is thrown before a form is deleted.
     *
     * The event listener receives a Mautic\StageBundle\Event\StageEvent instance.
     *
     * @var string
     */
    const PROMOTION_PRE_DELETE = 'mautic.promotion_pre_delete';

    /**
     * The mautic.promotion_post_delete event is thrown after a form is deleted.
     *
     * The event listener receives a Mautic\StageBundle\Event\StageEvent instance.
     *
     * @var string
     */
    const PROMOTION_POST_DELETE = 'mautic.promotion_post_delete';

    /**
     * The mautic.promotion_on_build event is thrown before displaying the promotion builder form to allow adding of custom actions.
     *
     * The event listener receives a Mautic\StageBundle\Event\StageBuilderEvent instance.
     *
     * @var string
     */
    const PROMOTION_ON_BUILD = 'mautic.promotion_on_build';

    /**
     * The mautic.promotion_on_action event is thrown to execute a promotion action.
     *
     * The event listener receives a Mautic\StageBundle\Event\StageActionEvent instance.
     *
     * @var string
     */
    const PROMOTION_ON_ACTION = 'mautic.promotion_on_action';
    /**
     * The mautic.promotion.on_campaign_trigger_action event is fired when the campaign action triggers.
     *
     * The event listener receives a
     * Mautic\CampaignBundle\Event\CampaignExecutionEvent
     *
     * @var string
     */
  // const ON_CAMPAIGN_TRIGGER_ACTION = 'mautic.promotion.on_campaign_trigger_action';
}
