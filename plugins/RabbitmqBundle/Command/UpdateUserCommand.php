<?php
namespace MauticPlugin\RabbitmqBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use MauticPlugin\RabbitmqBundle\Services\TransCons;

use Psr\Log\LoggerInterface;
class UpdateUserCommand extends ContainerAwareCommand
{
    private $logger;

    protected function configure()
    {
        $this->setName('fimplus:RabbitmqBundle:updateUsrCons');
        parent::configure();
        // ...
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        // die("222222");
        $service = $this->getContainer()->get('mautic.rabbitmq.service.updateUsrCons');
        $service->getMessage();
        exit;
    }
}