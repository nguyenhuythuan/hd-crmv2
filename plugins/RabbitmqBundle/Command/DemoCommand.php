<?php
namespace MauticPlugin\RabbitmqBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use MauticPlugin\RabbitmqBundle\Services\TransCons;

use Psr\Log\LoggerInterface;
class DemoCommand extends ContainerAwareCommand
{
    private $logger;

    protected function configure()
    {
        $this->setName('fimplus:RabbitmqBundle:demoCons');
        parent::configure();
        // ...
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        
        // $time = time();
        // echo date('Y-m-d H:i:s',$time);
        // exit;
        $service = $this->getContainer()->get('mautic.service.usercontext');
        $userId = 'e3a9socd-6vv6-4f7b-83f3-ec9185cccx53';
        $service->pushMessage($userId,'subscription');
        // var_dump($service->pushMessage());
        exit;
    }
}