<?php
namespace MauticPlugin\RabbitmqBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use MauticPlugin\RabbitmqBundle\Services\TransCons;

use Psr\Log\LoggerInterface;
class SubsCommand extends ContainerAwareCommand
{
    private $logger;

    protected function configure()
    {
        $this->setName('fimplus:RabbitmqBundle:subsConsumer');
        parent::configure();
        // ...
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        // die("222222");
        $service = $this->getContainer()->get('mautic.rabbitmq.service.subsCons');
        $service->getMessage();
        exit;
    }
}