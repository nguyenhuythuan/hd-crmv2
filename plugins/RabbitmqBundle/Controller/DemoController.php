<?php

namespace MauticPlugin\RabbitmqBundle\Controller;

use Mautic\CoreBundle\Controller\CommonController;
use Mautic\PluginBundle\Helper\IntegrationHelper;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Mautic\CoreBundle\Controller\FormController;
use Mautic\ConfigBundle\Event\ConfigBuilderEvent;
use Mautic\ConfigBundle\ConfigEvents;
use MauticPlugin\RabbitmqBundle\Model\Exchange;

class DemoController extends FormController
{
    public function indexAction($page = 1)
    {
        global $kernel;
        $message = (string)json_encode([
            'name' => 'Longnt',
            'action' => 'tried',
            'to' => 'campaign',
        ]);
        $exName = 'mautic_promotion';
        $routingKey = 'mautic_promotion.campaign';
        $data = new Exchange();
        $return = $data->pushMessageToEx($message,$routingKey,$exName);
        var_dump($return);
        die('2222');
        // $event      = new ConfigBuilderEvent($this->get('mautic.helper.paths'), $this->get('mautic.helper.bundle'));
        // $dispatcher = $this->get('event_dispatcher');
        // $dispatcher->dispatch(ConfigEvents::CONFIG_ON_GENERATE, $event);
       
        // $formConfigs = $this->get('mautic.config.mapper')->bindFormConfigsWithRealValues($event->getForms());
        // $event      = new ConfigBuilderEvent($kernel->getContainer()->get('mautic.helper.paths'), $kernel->getContainer()->get('mautic.helper.bundle'));
        // $formConfigs = $this->get('mautic.config.mapper')->bindFormConfigsWithRealValues($event->getForms());


        $event      = new ConfigBuilderEvent($kernel->getContainer()->get('mautic.helper.paths'), $kernel->getContainer()->get('mautic.helper.bundle'));
        $dispatcher = $kernel->getContainer()->get('event_dispatcher');
        $dispatcher->dispatch(ConfigEvents::CONFIG_ON_GENERATE, $event);
       
        $formConfigs = $kernel->getContainer()->get('mautic.config.mapper')->bindFormConfigsWithRealValues($event->getForms());
        var_dump($formConfigs['queueconfig']);
        exit;
    }

}