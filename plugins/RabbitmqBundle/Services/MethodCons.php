<?php
namespace MauticPlugin\RabbitmqBundle\Services;
use MauticPlugin\RabbitmqBundle\Model\RabbitMq;
use PhpAmqpLib\Connection\AMQPStreamConnection;
use PhpAmqpLib\Exchange\AMQPExchangeType;
use MauticPlugin\RabbitmqBundle\Model\Queue;
use Psr\Log\LoggerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

class MethodCons extends RabbitMq
{
    protected $container;
    public function __construct(ContainerInterface $container){
        $this->container = $container;
        parent::__construct();
    }
    public function __destruct(){
        parent::__destruct();
    }
    public $channel;
    public $connection;
    public $logger;


    public function getMessage(){
        $logger = $this->container->get('logger');
        $queueConfig = $this->container->getParameter('mautic.queue.config.info');
        if(!$queueConfig){
            $logger->error(json_encode(
                [
                    'error' => true,
                    'message' => 'error config',
                    'detail' => `error config in command TestCommand - TestCommand : execute`
                ]
            ));
            exit();
        }
        $queueName =  $queueConfig['queueName']['method'];
        

        if($queueName == '' || $queueName == null){
            $logger->error(json_encode(
                [
                    'error' => true,
                    'message' => 'missing Queue name ',
                    'detail' => 'missing Queue name in Transaction Consumner - SubsCons : getMessage'
                ]
            ));
            echo "[x]missing Queue name in Transaction Consumner - SubsCons : getMessage \n \n";
            exit();
        }
        // $queueName  = 'mautic_promotion';
        $callbackAckHandler = function($req){
            // $this->
            var_dump($req);
            exit();
        };
        $callbackNAckHandler = function($req){
            var_dump($req);
            exit();
        };
        $this->channel->set_ack_handler($callbackAckHandler);
        $this->channel->set_nack_handler($callbackNAckHandler);

        $callback = function ($msg) use ($logger,$queueName){
            $logger->info(json_encode(
                [
                    'error' => false,
                    'message' => '[x] Queue name : '.$queueName.' - routing_key : '.$msg->delivery_info['routing_key'].'\n',
                    'data' => $msg->body
                ]
            ));

            try{
                $dataResult = json_decode($msg->body);
                if($dataResult == '' || $dataResult == null){
                    // push message to queue error
                    $que = new Queue();
                    $message = json_encode([
                        'error' => true,
                        'routingKey' => '',
                        'queueName' => $queueName,
                        'message' =>  'cant decode message',
                        'data' => $msg->body
                    ]);
                    $que->pushMessageToQueue($message);
                }
                
                $service  = $this->container->get('mautic.method.service.methodService');
                $resultService = $service->upChangeData($dataResult);
                if($resultService == false || $resultService == null){
                    // push message to queue error
                    $que = new Queue();
                    $message = json_encode([
                        'error' => true,
                        'routingKey' => '',
                        'queueName' => $queueName,
                        'message' =>  'cant insert data',
                        'data' => $msg->body
                    ]);
                    $que->pushMessageToQueue($message);
                    // $msg->delivery_info['channel']->basic_ack($msg->delivery_info['delivery_tag'], false, true);
                    // exit;
                }
                echo "\n done task \n";
                // $msg->delivery_info['channel']->basic_ack($msg->delivery_info['delivery_tag'], false, true);
                
                // exit();
                // confirm done and exit process
                //process done run this function //requeue message
                //---- $msg->delivery_info['channel']->basic_ack($msg->delivery_info['delivery_tag'], false, true);
                
            }catch(\Exception $e){
                $que = new Queue();
                $message = json_encode([
                    'error' => true,
                    'routingKey' => '',
                    'queueName' => $queueName,
                    'message' =>  'error Exception in catch',
                    'data' => $msg->body
                ]);

                $que->pushMessageToQueue($message);
                $logger->error(json_encode(
                    [
                        'error' => true,
                        'message' => 'error in catch callback message',
                        'detail' => $e->getMessage().'in Transaction Consumner - SubsCons : getMessage'
                    ]
                ));
                // exit;
            }
        };
        try{
            $this->channel->basic_qos(null, 1, null);
            $this->channel->basic_consume((string)$queueName, '', false, false, false, false, $callback);
            while ($this->channel->is_consuming()) {
                echo " [*] Waiting for logs. To exit press CTRL+C\n";
                $this->channel->wait();
            }
        }catch(\Exception $e){
            $logger->error(json_encode(
                [
                    'error' => true,
                    'message' => 'error in catch get queue',
                    'detail' => $e->getMessage().'in Transaction Consumner - SubsCons : getMessage'
                ]
            ));
            echo $e->getMessage()."in Transaction Consumner - SubsCons : getMessage \n";
            exit;
        }
    }
}