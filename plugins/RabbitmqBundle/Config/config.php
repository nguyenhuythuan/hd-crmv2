<?php

/*
 * @copyright   2019 Fimplus. All rights reserved
 * @author      longnv
 *
 * @link        http://fimplus.vn
 *
 * @license     no license
 */

return [
    'name'        => 'RabbitmqBundle',
    'description' => 'RabbitmqBundle Producer',
    'version'     => '1.0',
    'author'      => 'datnt',
    'routes' => [
    ],

    'services' => [
        'orther' => [
            'mautic.rabbitmq.service.transCons' => [
                'class'     => MauticPlugin\RabbitmqBundle\Services\TransCons::class,
                'arguments' => [
                    'service_container',
                ],
            ],
            'mautic.rabbitmq.service.subsCons' => [
                'class'     => MauticPlugin\RabbitmqBundle\Services\SubsCons::class,
                'arguments' => [
                    'service_container',
                ],
            ],
            'mautic.rabbitmq.service.regisUsrCons' => [
                'class'     => MauticPlugin\RabbitmqBundle\Services\RegisUsrCons::class,
                'arguments' => [
                    'service_container',
                ],
            ],
            'mautic.rabbitmq.service.updateUsrCons' => [
                'class'     => MauticPlugin\RabbitmqBundle\Services\UpdateUsrCons::class,
                'arguments' => [
                    'service_container',
                ],
            ],
            'mautic.rabbitmq.service.methodCons' => [
                'class'     => MauticPlugin\RabbitmqBundle\Services\MethodCons::class,
                'arguments' => [
                    'service_container',
                ],
            ],

            'mautic.transaction.service.user_model' => [
                'class'     => MauticPlugin\RabbitmqBundle\UsrService\User::class,
                'arguments' => [
                    'service_container',
                ],
            ]
        ]
    ],

    'menu' => [
        'main' => [
        ],
    ],

    'categories' => [
    ],
];
