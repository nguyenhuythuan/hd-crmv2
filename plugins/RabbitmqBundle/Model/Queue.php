<?php

namespace MauticPlugin\RabbitmqBundle\Model;
use MauticPlugin\RabbitmqBundle\Model\RabbitMq;

use Mautic\CoreBundle\Controller\CommonController;
use Mautic\PluginBundle\Helper\IntegrationHelper;

use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Mautic\CoreBundle\Controller\FormController;
use Mautic\ConfigBundle\Event\ConfigBuilderEvent;
use Mautic\ConfigBundle\ConfigEvents;
use PhpAmqpLib\Connection\AMQPStreamConnection;
use PhpAmqpLib\Message\AMQPMessage;
use PhpAmqpLib\Exchange\AMQPExchangeType;
use PhpAmqpLib\Exception\AMQPIOException;

class Queue extends RabbitMq
{
    public $channel;
    public $connection;
    private $exName     = 'mautic_promotion_error';
    private $queueName  = 'mautic_promotion_error';
    private $routingKey = 'mautic.error';

    public function __construct(){
        parent::__construct();
    }
    public function __destruct(){
        parent::__destruct();
    }

    public function createQueue(){
        $this->channel->queue_declare($this->queueName, false, false, false, false);
        $this->channel->exchange_declare($this->exName, AMQPExchangeType::DIRECT, false, true, false);
        $this->channel->queue_bind($this->queueName, $this->exName);
        // return $this;
    }

    public function pushMessageToQueue($message = ''){
        try{
            $this->createQueue();
            $msg = new AMQPMessage($message);
            $this->channel->basic_publish($msg,$this->exName);
            $result = [
                'error' => false,
                'message' => 'success',
                'data' => [
                    'exName' =>  $this->exName,
                    'routingKey' => $this->routingKey,
                    'message' => $msg
                ]
                ];
            return $result;
        }catch(AMQPIOException $e){
            $result = [
                'error' => true,
                'message' => 'error in catch pushMessageToEx',
                'detail' => `detail {$e->getMessage}`
            ];
            return $result;
        }
        

    }



}