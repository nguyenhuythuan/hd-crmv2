<?php

namespace MauticPlugin\RabbitmqBundle\Model;

use Mautic\CoreBundle\Controller\CommonController;
use Mautic\PluginBundle\Helper\IntegrationHelper;

use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Mautic\CoreBundle\Controller\FormController;
use Mautic\ConfigBundle\Event\ConfigBuilderEvent;
use Mautic\ConfigBundle\ConfigEvents;
use PhpAmqpLib\Connection\AMQPStreamConnection;

class RabbitMq
{
    private $host = 'localhost';
    private $port = '5672';
    private $user = 'guest';
    private $pass = 'guest';

    public function __construct(){
        $this->configData();
        $this->connection = new AMQPStreamConnection($this->host, $this->port, $this->user, $this->pass);
        $this->channel = $this->connection->channel();
    }

    public function __destruct(){
        $this->channel->close();
        $this->connection->close();
    }
    public $channel;
    public $connection;
    public function configData(){
        global $kernel;
        if($kernel){
            $event      = new ConfigBuilderEvent($kernel->getContainer()->get('mautic.helper.paths'), $kernel->getContainer()->get('mautic.helper.bundle'));
            $dispatcher = $kernel->getContainer()->get('event_dispatcher');
            $dispatcher->dispatch(ConfigEvents::CONFIG_ON_GENERATE, $event);
            
            $formConfigs = $kernel->getContainer()->get('mautic.config.mapper')->bindFormConfigsWithRealValues($event->getForms());
            $queueConfig = (object)$formConfigs['queueconfig']['parameters'];
            $this->host  = $queueConfig->rabbitmq_host;
            $this->port  = $queueConfig->rabbitmq_port;
            $this->user  = $queueConfig->rabbitmq_user;
            $this->pass  = $queueConfig->rabbitmq_password;   
        }
    }

}