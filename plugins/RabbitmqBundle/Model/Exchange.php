<?php

namespace MauticPlugin\RabbitmqBundle\Model;
use MauticPlugin\RabbitmqBundle\Model\RabbitMq;

use Mautic\CoreBundle\Controller\CommonController;
use Mautic\PluginBundle\Helper\IntegrationHelper;

use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Mautic\CoreBundle\Controller\FormController;
use Mautic\ConfigBundle\Event\ConfigBuilderEvent;
use Mautic\ConfigBundle\ConfigEvents;
use PhpAmqpLib\Connection\AMQPStreamConnection;
use PhpAmqpLib\Message\AMQPMessage;
use PhpAmqpLib\Exchange\AMQPExchangeType;
use PhpAmqpLib\Exception\AMQPIOException;

class Exchange extends RabbitMq
{
    public $channel;
    public $connection;
    private $exName     = 'mautic_promotion';
    private $routingKey = 'mautic_promotion.promotion';

    public function __construct(){
        parent::__construct();
    }
    public function __destruct(){
        parent::__destruct();
    }

    public function createExchange(){
        $this->channel->exchange_declare($this->exName, AMQPExchangeType::TOPIC, false, true, false);
        // return $this;
    }

    public function pushMessageToEx($message = '',$routingKey = '',$exName = ''){
        try{
            $this->exName = $exName && $exName != '' ? $exName: $this->exName;
            $this->routingKey = $routingKey && $routingKey != '' ? $routingKey: $this->routingKey;
            $this->createExchange();
            $msg = new AMQPMessage($message);
            $this->channel->basic_publish($msg, $this->exName, $this->routingKey);
            $result = [
                'error' => false,
                'message' => 'success',
                'data' => [
                    'exName' =>  $this->exName,
                    'routingKey' => $this->routingKey,
                    'message' => $msg
                ]
                ];
            return $result;
        }catch(AMQPIOException $e){
            $result = [
                'error' => true,
                'message' => 'error in catch pushMessageToEx',
                'detail' => `detail {$e->getMessage}`
            ];
            return $result;
        }
        

    }



}