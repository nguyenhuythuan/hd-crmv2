<?php
namespace MauticPlugin\UsercontextBundle\Service;
use MauticPlugin\RabbitmqBundle\Model\RabbitMq;
use MauticPlugin\FimplusBundle\Model\PromotionModel;
use MauticPlugin\RabbitmqBundle\Model\Queue;
use Psr\Log\LoggerInterface;
use MauticPlugin\RabbitmqBundle\Model\Exchange;
use MauticPlugin\UsercontextBundle\Model\UserContextModel;
use Symfony\Component\DependencyInjection\ContainerInterface;
use MauticPlugin\UsercontextBundle\Document\UserContext as UserContextDocument;

class UserContext
{
    protected $continue = True;
    protected $container;
    protected $userId = 0;
    const GROUP_ACCOUNT = 'account';
    const GROUP_SUBSCRIPTION = 'subscription';
    /**
    * ==================== private var for to check property
    ***/
    /**/    private $indie;
    /**/    private $bundling;
    /**/    private $newUser;
    /**/    private $newRegis;
    //==================== end private 

    //@intro return dataContext avinable to check

    public $context =[
        'indie' => false,
        'bundling' => false,
        'newUser' => true,
        // 'newRegis' => true,
    ];

    //@intro return group for call to function set
    public $groupContext =[
        self::GROUP_SUBSCRIPTION => ['indie','bundling'],
        self::GROUP_ACCOUNT => ['newUser']
    ];
    
    public function __construct(ContainerInterface $container){
        $this->container = $container;
        $this->setDefaulContext();
    }
    public function __destruct(){}

    /** function set */
    public function setDefaulContext(){
        $dm = $this->container->get('doctrine_mongodb')->getManager();
        $docContext = $dm->getRepository('UsercontextBundle:UserContext')
                ->findOneBy(["userId" => $this->userId]);
        if($docContext){
            $context = $docContext->getContext();
            if($context){
                foreach($context as $key => $value){
                    $this->context[$key] = $value;
                }
            }
        }
    }
    public function setUserId($userId){
        $this->userId = $userId;
    }  
    // đa từng mua gói bằng tiền  & đang có gói và amout > 0 (k tính tvod).
    public function setIndie(){
        // $this->context['indie'] = true;
        $em = $this->container->get('doctrine')->getEntityManager();
        $reponstory = $em->getRepository('MauticTransactionBundle:Transaction');
        $count = $reponstory->countByFilter(
            [
                "user_id" => $this->userId,
                "status" => $reponstory->getStatusDefine()->PAID
            ],
            [
                "amount > 0",
                "titleId is null",
                "planId is not null"
            ]
        );
        // var_dump($reponstory->getStatusDefine()->PAID);
        if($count <= 1){
            // phải có ít nhất 2 lượt mua = tiền thành công 1 lần là đã từng mua 1 lần "có thể" là gói đang sử dụng
            // khi count = 1 là mới mua có 1 lần thành công = tiền => k đủ điều kiện để set tiếp điều kiện tiếp theo
            $this->context['indie'] = false;
            return;
        }
        $reponstory = $em->getRepository('SubscriptionBundle:Subscription');
        $dateNow = date_create(time());
        $dateNow = date('Y-m-d H:i:s',time()); //date_format('Y-m-d H:i:s',$dateNow);

        $countz = $reponstory->countByFilter(
            [
                "userId" => $this->userId,
                "status" => $reponstory->getStatusDefine()->ACTIVE
            ],
            [
                "expireAt is NOT NULL",
                "unix_timestamp(expireAt) > ".time(),
                "titleId is NULL",
                "planId is NOT NULL",
            ]
        );
        if($count >= 1){
            $this->context['indie'] = true;
            $this->context['newUser'] = false;
        }else{
            $this->context['indie'] = false;
        }
        return;

    }
    public function setNewRegis(){
        $this->context['newRegis'] = false;
    }
    public function setNewUser(){
        $this->context['newUser'] = false;
    }
    public function setBundling(){
        $em = $this->container->get('doctrine')->getEntityManager();
        $reponstory = $em->getRepository('SubscriptionBundle:Subscription');
        $dateNow = date_create(time());
        $dateNow = date('Y-m-d H:i:s',time()); 
        $count = $reponstory->countByFilter(
            [
                "userId" => $this->userId,
                "status" => $reponstory->getStatusDefine()->ACTIVE
            ],
            [
                "expireAt is not null",
                "unix_timestamp(expireAt) > ".time(),
            ]
        );
        if($count >= 1){
            $this->context['bundling'] = true;
            $this->context['newUser'] = false;
        }else{
            $this->context['bundling'] = false;
        }
        return;
    }
    public function setDataContext($groupContext){
        $needContext = $this->groupContext[$groupContext];
        foreach($needContext as $k => $v){
            $property = (string)'set'.ucfirst($v);
            if(property_exists($this,$v)){
                $this->{$property}();
            }
        }
        // $this->pushMessage();
    }
    /** function get */
    public function getDataContext(){
        try{
            $dm = $this->container->get('doctrine_mongodb')->getManager();
            $docContext = $dm->getRepository('UsercontextBundle:UserContext')
                    ->findOneBy(["userId" => $this->userId]);
            if (!$docContext) {
                $docContext = new \MauticPlugin\UsercontextBundle\Document\UserContext();
            }
            $docContext->setUserId($this->userId);
            $docContext->setContext($this->context);
            $docContext->setUpdatedAt(time());
            $docContext->setCreatedAt(time());
            $dm->persist($docContext);
            $dm->flush();
            return $this->context;
        }catch(\Exception $e){
            var_dump($e->getMessage());
            // exit("123");
        }
        

        
    }
    public function getRoutingKey(){
        $queueConfig = $this->container->getParameter('mautic.queue.config.info');
        return $queueConfig['routingKey']['userContext'];
    }

    public function getExchangeName(){
        $queueConfig = $this->container->getParameter('mautic.queue.config.info');
        return $queueConfig['exchangeName']['userContext'];
    }
    public function getTransRecord(){
        $em = $this->container->get('doctrine')->getEntityManager();
        $reponstory = $em->getRepository('MauticTransactionBundle:Transaction');
        $items = $reponstory->findAllByUserId($this->userId);
        if($items != null){
            return $items;
        }
        return false;
    }
    public function getMethodRecord(){
        $em = $this->container->get('mautic.method.model.method');
        //->getEntityManager();
        $reponstory = $em->getRepository();

        $items = $reponstory->findAllByUserId($this->userId);
        if($items != null){
            return $items;
        }
        return false;
    }

    public function pushMessage($userId,$groupContext = ''){
        if(!$userId){
            //log
            return;
        }
        if(!$groupContext){
            //log
            return;
        }
        $this->setUserId($userId);
        $this->setDataContext($groupContext);

        $dataContext = $this->getDataContext();
        $messageArr = [
            'sender' => 'Mautic_UserContext',
            'userId' => $this->userId,
            'data' => [
                'context' => $dataContext,
                'transHistory'   => $this->getTransRecord(),
                'methodHistory'  => $this->getMethodRecord(),
            ],
            'pushMessageTime' => new \DateTime()
        ];
        $message = (string)json_encode($messageArr);

        $exName = $this->getExchangeName();
        $routingKey = $this->getRoutingKey();
        $data = new Exchange();
        $result = $data->pushMessageToEx($message,$routingKey,$exName);
    }
    
}