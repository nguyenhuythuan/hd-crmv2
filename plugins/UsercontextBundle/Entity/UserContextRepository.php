<?php
namespace MauticPlugin\UsercontextBundle\Entity;


use Mautic\CoreBundle\Entity\CommonRepository;
use Mautic\LeadBundle\Entity\Lead;
use Mautic\TransactionBundle\Entity\Transaction;

class UserContextRepository extends CommonRepository
{
    public function getEntities(array $args = [])
    {
        return parent::getEntities($args);
    }

    protected function addCatchAllWhereClause($q, $filter)
    {
        return $this->addStandardCatchAllWhereClause($q, $filter, [
            
        ]);
    }
}
