<?php
namespace MauticPlugin\UsercontextBundle\Entity;

use Doctrine\DBAL\Types\DecimalType;
use Doctrine\ORM\Mapping as ORM;
use Mautic\CampaignBundle\Executioner\Scheduler\Mode\DateTime;
use Mautic\CoreBundle\Doctrine\Mapping\ClassMetadataBuilder;
use Mautic\CoreBundle\Entity\FormEntity;
use \Mautic\LeadBundle\Entity\Lead;
/*
+----+----------+----------+--------------+
| id | key      | value    | userId       |
+----+----------+----------+--------------+
|  1 | newRegis |        1 |            1 |
+----+----------+----------+--------------+
|  2 | newUser  |        1 |            1 |
+----+----------+----------+--------------+

*/
class UserContext extends FormEntity
{
    /**
     * @var int
     */
    private $id;
    const NEWREGIS = '';
    const NEWUSER = '';
    const FIRSTPAYMENTTVODMOMO = '';
    const FIRSTPAYMENTSVODMOMO = '';
    const FIRSTLINKCCSTRIPE = '';
    const FIRSTMETHODAUTORENEW = '';
    const HISTORYTRANSACTION = '';
    const HISTORYLINKMETHOD = '';
    const SVODPAID = '';
    const TVODPAID = '';
    const MOBIFONEPAID = '';
    const VNPTPAID = '';
    const SVODWHOLESALE = '';
    const TVODWHOLESALE = '';
    const MOBIFONEWHOLESALE = '';
    const VNPTWHOLESALE = '';
    const MONTHLYACTIVE = '';
    const MONTHLYSUBSCRIBERS = '';
    const USERRETURN = '';
    const CONVERSIONFROMBUNDLING = '';
    const CONVERSIONFROMREGISTRATION = '';
    const MANUALRENEWUSER = '';
    const AUTORENEWUSER = '';
    const USEREXPIRED = '';
    const USERLEAVE = '';
    const CURRENTUSER = '';
    
    const FIELD_USER_CONETXT = [
        self::NEWREGIS => "newRegis",
        self::NEWUSER => "newUser",
        self::FIRSTPAYMENTTVODMOMO => "firstPaymentTvodMomo",
        self::FIRSTPAYMENTSVODMOMO => "firstPaymentSvodMomo",
        self::FIRSTLINKCCSTRIPE => "firstLinkCcstripe ",
        self::FIRSTMETHODAUTORENEW => "firstMethodAutoRenew",
        self::HISTORYTRANSACTION => "historyTransaction",
        self::HISTORYLINKMETHOD => "historyLinkMethod",
        self::SVODPAID => "svodPaid",
        self::TVODPAID => "tvodPaid",
        self::MOBIFONEPAID => "mobifonePaid",
        self::VNPTPAID => "vnptPaid",
        self::SVODWHOLESALE => "svodWholesale",
        self::TVODWHOLESALE => "tvodWholesale",
        self::MOBIFONEWHOLESALE => "mobifoneWholesale",
        self::VNPTWHOLESALE => "vnptWholesale",
        self::MONTHLYACTIVE => "monthlyActive",
        self::MONTHLYSUBSCRIBERS => "monthlySubscribers",
        self::USERRETURN => "userReturn",
        self::CONVERSIONFROMBUNDLING => "conversionFromBundling",
        self::CONVERSIONFROMREGISTRATION => "conversionFromRegistration",
        self::MANUALRENEWUSER => "manualRenewUser",
        self::AUTORENEWUSER => "autoRenewUser",
        self::USEREXPIRED => "userExpired",
        self::USERLEAVE => "userLeave",
        self::CURRENTUSER => "currentUser",

    ];
    private $key;
    private $value;

    private $createdAt;
    private $updatedAt;


    private $svodPaid;
    private $tvodPaid;
    private $mobifonePaid;
    private $vnptPaid;
    private $svodWholesale;
    private $tvodWholesale;
    private $mobifoneWholesale;
    private $vnptWholesale;
    private $monthlyActive;
    private $monthlySubscribers;
    private $userReturn;
    private $conversionFromBundling;
    private $conversionFromRegistration;
    private $manualRenewUser;
    private $autoRenewUser;
    private $userExpired;
    private $userLeave;
    private $currentUser;
    private $userId;



    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }
    public function setSvodPaid($svodPaid){
        $this->svodPaid = $svodPaid;
    }
    public function setTvodPaid($tvodPaid){
        $this->tvodPaid = $tvodPaid;
    }
    public function setMobifonePaid($mobifonePaid){
        $this->mobifonePaid = $mobifonePaid;
    }
    public function setVnptPaid($vnptPaid){
        $this->vnptPaid = $vnptPaid;
    }
    public function setSvodWholesale($svodWholesale){
        $this->svodWholesale = $svodWholesale;
    }
    public function setTvodWholesale($tvodWholesale){
        $this->tvodWholesale = $tvodWholesale;
    }
    public function setMobifoneWholesale($mobifoneWholesale){
        $this->mobifoneWholesale = $mobifoneWholesale;
    }
    public function setVnptWholesale($vnptWholesale){
        $this->vnptWholesale = $vnptWholesale;
    }
    public function setMonthlyActive($monthlyActive){
        $this->monthlyActive = $monthlyActive;
    }
    public function setMonthlySubscribers($monthlySubscribers){
        $this->monthlySubscribers = $monthlySubscribers;
    }
    public function setUserReturn($userReturn){
        $this->userReturn = $userReturn;
    }
    public function setConversionFromBundling($conversionFromBundling){
        $this->conversionFromBundling = $conversionFromBundling;
    }
    public function setConversionFromRegistration($conversionFromRegistration){
        $this->conversionFromRegistration = $conversionFromRegistration;
    }
    public function setManualRenewUser($manualRenewUser){
        $this->manualRenewUser = $manualRenewUser;
    }
    public function setAutoRenewUser($autoRenewUser){
        $this->autoRenewUser = $autoRenewUser;
    }
    public function setUserExpired($userExpired){
        $this->userExpired = $userExpired;
    }
    public function setUserLeave($userLeave){
        $this->userLeave = $userLeave;
    }
    public function setCurrentUser($currentUser){
        $this->currentUser = $currentUser;
    }
    public function setUserId($userId){
        $this->userId = $userId;
    }

    
    public function getSvodPaid(){
        return $this->svodPaid;
    }
    public function getTvodPaid(){
        return $this->tvodPaid;
    }
    public function getMobifonePaid(){
        return $this->mobifonePaid;
    }
    public function getMnptPaid(){
        return $this->vnptPaid;
    }
    public function getSvodWholesale(){
        return $this->svodWholesale;
    }
    public function getTvodWholesale(){
        return $this->tvodWholesale;
    }
    public function getMobifoneWholesale(){
        return $this->mobifoneWholesale;
    }
    public function getVnptWholesale(){
        return $this->vnptWholesale;
    }
    public function getMonthlyActive(){
        return $this->monthlyActive;
    }
    public function getMonthlySubscribers(){
        return $this->monthlySubscribers;
    }
    public function getUserReturn(){
        return $this->userReturn;
    }
    public function getConversionFromBundling(){
        return $this->conversionFromBundling;
    }
    public function getConversionFromRegistration(){
        return $this->conversionFromRegistration;
    }
    public function getManualRenewUser(){
        return $this->manualRenewUser;
    }
    public function getAutoRenewUser(){
        return $this->autoRenewUser;
    }
    public function getUserExpired(){
        return $this->userExpired;
    }
    public function getUserLeave(){
        return $this->userLeave;
    }
    public function getCurrentUser(){
        return $this->currentUser;
    }
    public function getUserId(){
        return $this->userId;
    }


    public static function loadMetadata(ORM\ClassMetadata $metadata)
    {
        $builder = new ClassMetadataBuilder($metadata);

        $builder->setTable('user_context')
            ->setCustomRepositoryClass('MauticPlugin\UsercontextBundle\Entity\UserContextRepository');

        $builder->createField('id', 'integer')
            ->makePrimaryKey()
            ->generatedValue()
            ->build();
    }

    

}
