<?php

/*
 * @copyright   2019 Fimplus. All rights reserved
 * @author      longnv
 *
 * @link        http://fimplus.vn
 *
 * @license     no license
 */

return [
    'name'        => 'User Context',
    'description' => 'Custom campaign and action for Fimplus',
    'version'     => '1.0',
    'author'      => 'datnt',
    'routes' => [
        
    ],

    'services' => [
        'models' => [
            'mautic.model.usercontext' => [
                'class'     => 'MauticPlugin\UsercontextBundle\Model\UserContextModel',
                'arguments' => []
            ],
        ],
        'repositories' => [
            'mautic.repository.usercontext' => [
                'class'     => Doctrine\ORM\EntityRepository::class,
                'factory'   => ['@doctrine.orm.entity_manager', 'getRepository'],
                'arguments' => [
                    'MauticPlugin\UserContextBundle\Entity\UserContext',
                ],
            ],
        ],
        'events' => [
            'mautic.usercontext.subscriber' => [
                'class'     => Mautic\LeadBundle\EventListener\LeadSubscriber::class,
                'arguments' => [
                    'mautic.helper.ip_lookup',
                    'mautic.core.model.auditlog',
                    'mautic.usercontext.event.dispatcher',
                    'mautic.helper.template.dnc_reason',
                ],
                'methodCalls' => [
                    'setModelFactory' => ['mautic.model.factory'],
                ],
            ],
        ],
        'orther' => [
            'mautic.service.usercontext' => [
                'class'     => 'MauticPlugin\UsercontextBundle\Service\UserContext',
                'arguments' => [
                    'service_container',
                ],
            ],
            'mautic.service.document.usercontext' => [
                'class'     => 'MauticPlugin\UsercontextBundle\Document\UserContext',
                'arguments' => [
                    'service_container',
                ],
            ],

            'mautic.usercontext.event.dispatcher' => [
                'class'     => \Mautic\LeadBundle\Helper\LeadChangeEventDispatcher::class,
                'arguments' => [
                    'event_dispatcher',
                ],
            ],
            'mautic.usercontext.doctrine.subscriber' => [
                'class'     => 'Mautic\LeadBundle\EventListener\DoctrineSubscriber',
                'tag'       => 'doctrine.event_subscriber',
                'arguments' => ['monolog.logger.mautic'],
            ],

        ]
    ],

    'menu' => [
        
    ],

    'categories' => [
        
    ],
    // 'commands' => [
    //     'fimplus.testcommand.debug' => [
    //         'class'     => \MauticPlugin\UsercontextBundle\Command\TestCommand::class,
    //         'arguments' => [
    //             'monolog.logger.mautic',
    //         ],
    //         'tag' => 'fimplus.testcommand.debug',
    //     ],
    // ]
];
