<?php
namespace MauticPlugin\UsercontextBundle\Document;
use Doctrine\ODM\MongoDB\Mapping\Annotations as MongoDB;
use Doctrine\ODM\MongoDB\Types\ClosureToPHP;
use MongoDB\BSON\UTCDateTime;
use MauticPlugin\UsercontextBundle\Document\MyType;


/**
 * @MongoDB\Document
 */
class UserContext
{
    /**
     * @MongoDB\Id
     */
    private $id;

    /**
     * @MongoDB\Field(type="string")
     */
    protected $userId;

    /**
     * @MongoDB\Field(type="raw")
     */
    protected $context;

    protected $createdAt;
    protected $updateAt;

    /* set function */
    public function setId(string $id): void
    {
        $this->id = $id;
    }
    public function setUserId(string $value): void
    {
        $this->userId = $value;
    }
    public function setContext(array $value)
    {
        $this->context = $value;
    }
    public function setCreatedAt(string $value)
    {
        $this->createdAt = $value;
    }
    public function setUpdatedAt($value = false)
    {
        if(!$value){
            $myType = new MyType();
            $this->updatedAt = $myType->convertToDatabaseValue(time());
        }else{
            $this->updatedAt = $value;
        }
    }

    /* get function */
    public function getId()
    {
        return $this->id;
    }
    public function getUserId()
    {
        return $this->userId;
    }
    public function getContext()
    {
        return $this->context;
    }
    public function getCreatedAt()
    {
        return $this->createdAt;
    }
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

}   