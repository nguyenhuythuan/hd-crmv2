<?php 
namespace MauticPlugin\UsercontextBundle\Model;


use MauticPlugin\UsercontextBundle\Model\Model;
use Mautic\CoreBundle\Model\FormModel;
use MauticPlugin\UsercontextBundle\Entity\UserContext;
use Mautic\UsercontextBundle\Entity\UserContextRepository;

/**
 * Class UserContextModel
 * {@inheritdoc}
 */
class UserContextModel extends FormModel
{
    
    /**
     * {@inheritdoc}
     *
     * @return \Mautic\UserContextBundle\Entity\UserContextRepository
     */
    // public function getRepository()
    // {
    //     $repo = $this->em->getRepository('UsercontextBundle:UserContextRepository');
    //     return $repo;
    // }
    public function getEntity($id = null)
    {
        if ($id === null) {
            return new UserContext();
        }
        
        $entity = parent::getEntity($id);

        return $entity;
    }

}