<?php
namespace MauticPlugin\StoreBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use MauticPlugin\RabbitmqBundle\Services\TransCons;
use Psr\Log\LoggerInterface;
use MauticPlugin\StoreBundle\Document\Product;
use Doctrine\ODM\MongoDB\DocumentManager as DocumentManager;

use MauticPlugin\StoreBundle\StoreEvents;
// use MauticPlugin\StoreBundle\Order;
use MauticPlugin\StoreBundle\Event\FilterOrderEvent;
use MauticPlugin\StoreBundle\Event\StoreSubscriber;
use Symfony\Component\EventDispatcher\EventDispatcher;
use Services\Mail\FimMailler\SendMail;
use Services\Sms\FimSms\SendSms;
class DemoCommand extends ContainerAwareCommand
{
    private $logger;

    protected function configure()
    {
        $this->setName('fimplus:Demo:demoCons');
        parent::configure();
        // ...
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        
        $dispatcher = $this->getContainer()->get('event_dispatcher');
        $event = new FilterOrderEvent([]);
        $dispatcher->dispatch(StoreEvents::STORE_ORDER, $event);
        
        exit();


        // $requestz = new RequestMail();
        /* $requestz = SendSms::sendVMGSaveSMS(
            [
                "phone" => "0899969964",
                "content" => "dabc",
            ]
        );
        var_dump($requestz);
        exit(); */
        // the order is somehow created or retrieved
        // $order = new Order();
        // ...
        /* $service = $this->getContainer()->get('mautic.service.usercontext');
        var_dump($service::GROUP_ACCOUNT); */
        // exit;
        $dispatcher = new EventDispatcher();
        $subscriber = $this->getContainer()->get('mautic.storebunle.event.subscriber');
        $subscriber = new StoreSubscriber();
        $dispatcher->addSubscriber($subscriber);
        exit;
        // create the FilterOrderEvent and dispatch it
        // $logger = $this->getContainer()->get('logger');
        // $logger->info(json_encode(
        //     [
        //         'error' => true,
        //         'message' => 'error config',
        //         'detail' => `error config in command TestCommand - TestCommand : execute`
        //     ]
        // ));
        $event = new FilterOrderEvent([]);
        // echo "12312";
        $dispatcher->dispatch(StoreEvents::STORE_ORDER, $event);
        

        //============================
        // $dm = $this->getContainer()->get('doctrine');//->get('documentManager');
        /* $product = new Product();
        // $product->setName('A Foo Bar');
        // $product->setPrice('19.99');

        // $dm->persist($product);
        $dm = $this->getContainer()->get('doctrine_mongodb')->getManager();
        $dm->persist($product);
        $dm->flush(); */
        // return new Response('Created product id '.$product->getId());
        
        // $time = time();
        // echo date('Y-m-d H:i:s',$time);
        // exit;
        // $service = $this->getContainer()->get('mautic.service.usercontext');
        // $userId = '000a8245-f891-4ec9-bb07-e74e0e9cdf69';
        // $service->pushMessage($userId,'subscription');
        // var_dump($service->pushMessage());
        exit;
    }
}