<?php
namespace MauticPlugin\StoreBundle\Event;

use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpKernel\Event\FilterResponseEvent;
use Symfony\Component\DependencyInjection\ContainerInterface;
use MauticPlugin\StoreBundle\Document\Product;
use MauticPlugin\StoreBundle\StoreEvents;
class StoreSubscriber implements EventSubscriberInterface
{
    protected $container ;
    public function __construct(ContainerInterface $container)
    {
        $this->container             = $container;
        // die("1212");
    }
    public static function getSubscribedEvents()
    {
        /* return [
            StoreEvents::STORE_ORDER => ['onStoreOrder',0],
        ]; */
        return array(
            'kernel.response' => array(
                array('onKernelResponsePre', 10),
                array('onKernelResponseMid', 5),
                array('onKernelResponsePost', 0),
            ),
            'store.order'     => array('onStoreOrder', 0),
        );
    }

    public function onKernelResponsePre(FilterResponseEvent $event)
    {
        // ...
    }

    public function onKernelResponseMid(FilterResponseEvent $event)
    {
        // ...
    }

    public function onKernelResponsePost(FilterResponseEvent $event)
    {
        // ...
    }

    public function onStoreOrder(FilterOrderEvent $event)
    {
        // die('kkkk');
        // $dm = $this->getContainer()->get('doctrine');//->get('documentManager');
        $product = new Product();
        $product->setName('A Foo Ba asdfsadf sdaf r');
        // $product->setPrice('19.99');

        // $dm->persist($product);
        $dm = $this->container->get('doctrine_mongodb')->getManager();
        $dm->persist($product);
        $dm->flush();

        $logger = $this->container->get('logger');
        $logger->error(json_encode(
            [
                'error' => true,
                'message' => 'error config',
                'detail' => `error config in command TestCommand - TestCommand : execute`
            ]
        ));
        echo "123123123132";
        // exit;
        // ...
    }
}
