<?php

/*
 * @copyright   2019 Fimplus. All rights reserved
 * @author      longnv
 *
 * @link        http://fimplus.vn
 *
 * @license     no license
 */

return [
    'name'        => 'User dfadsfa',
    'description' => 'Custom campaign and action for Fimplus',
    'version'     => '1.0',
    'author'      => 'datnt',
    'routes' => [
        
    ],

    'services' => [
        'models' => [
            
        ],
        'repositories' => [
            
        ],
        'events' => [
            'mautic.storebunle.event.subscriber' => [
                'class'     => \MauticPlugin\StoreBundle\Event\StoreSubscriber::class,
                'arguments' => [
                    'service_container'
                    // 'mautic.transaction.model.transaction',
                ]
            ],
        ],
        'orther' => [

        ]
    ],

    'menu' => [
        
    ],

    'categories' => [
        
    ],
];
