<?php
require_once __DIR__ . '/vendor/autoload.php';
use PhpAmqpLib\Connection\AMQPStreamConnection;
use PhpAmqpLib\Exchange\AMQPExchangeType;
use MauticPlugin\FimplusBundle\Model\PromotionModel;
use MauticPlugin\RabbitmqBundle\Model\Queue;



$exName     = 'mautic_promotion';
$routingKey = 'mautic_promotion.*';
$queueName  = 'mautic_promotion';
$test = new PromotionModel();
$connection = new AMQPStreamConnection('localhost', 5672, 'guest', 'guest');
$channel = $connection->channel();
$channel->exchange_declare($exName, AMQPExchangeType::TOPIC, false, false, false);

// $channel->queue_declare($queueName, false, true, false, false);


// $binding_keys = array_slice($argv, 1);
// if (empty($binding_keys)) {
//     file_put_contents('php://stderr', "Usage: $argv[0] [binding_key]\n");
//     exit(1);
// }


$channel->queue_bind($queueName, $exName, $routingKey);
function dataCallback ($req){
    var_dump($req);
}
function dataCallbackz ($req){
    var_dump($req);
}
$channel->set_ack_handler('dataCallback');
$channel->set_nack_handler('dataCallbackz');



$callback = function ($msg) {
    echo ' [x] ', $msg->delivery_info['routing_key'], ':', $msg->body, "\n";
    //process done run this function //requeue message
    //---- $msg->delivery_info['channel']->basic_ack($msg->delivery_info['delivery_tag'], false, true);
    
    // if want retry message 
    //---- $msg->delivery_info['channel']->basic_nack($msg->delivery_info['delivery_tag'], false, true);
    
    //if message is error
    // $que = new Queue();
    // $que->pushMessageToQueue('datnt');
    // die();


};
$channel->basic_qos(null, 1, null);
$channel->basic_consume($queueName, '', false, false, false, false, $callback);

while ($channel->is_consuming()) {
    echo " [*] Waiting for logs. To exit press CTRL+C\n";
    $channel->wait();
}

$channel->close();
$connection->close();